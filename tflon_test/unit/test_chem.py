from __future__ import print_function, division
from builtins import range
import os
import pandas as pd
import numpy as np
import tensorflow as tf
import tflon
import unittest
import pkgutil
import networkx as nx
from pkg_resources import resource_filename
from tflon_test.utils import TflonOpTestCase, with_tempfile, hot, ragged, convert_feed
import pybel
import json

class ChemDataTest(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        cls.sdf_fn = resource_filename('tflon_test.data', 'molecules.sdf')
        cls.molecules = tflon.chem.MoleculeTable(tflon.chem.sdf_to_parquet(cls.sdf_fn, coordinates=True)).deserialize().data
        cls.molecules_no_h = tflon.chem.MoleculeTable(tflon.chem.sdf_to_parquet(cls.sdf_fn, addhs=False)).deserialize().data

    def test_sdf_to_parquet_with_coordinates(self):
        pymols = list(pybel.readfile('sdf', self.sdf_fn))

        for m1,m2 in zip(pymols, self.molecules.Structure):
            assert len(m1.atoms) == len(m2.nodes)

            m2coords = m2.nodes.coordinates
            for a1, a2, c2 in zip(m1.atoms, m2.nodes, m2coords):
                c1 = a1.coords
                np.testing.assert_array_almost_equal(c1, c2, err_msg="Atom coordinates not equal for index %d: %s != %s" % (a2, str(c1), str(c2)))

    @with_tempfile
    def test_atom_to_parquet(self, tfilename):
        data = pd.read_csv(resource_filename('tflon_test.data', 'molecules.tsv'), sep='\t', index_col=0)
        data = data[data.columns[16:20]]

        df1 = tflon.chem.atom_to_parquet(self.molecules, data, tfilename)
        np.testing.assert_array_equal(df1.loc['Benzene', 'Ring4'], [0.0, 0.0, 0.0, 0.0, 0.0, np.nan, np.nan, np.nan, np.nan, np.nan, np.nan, np.nan, np.nan, np.nan, np.nan])

        df2 = tflon.data.read_parquet(tfilename)
        np.testing.assert_array_equal(df2.loc['Benzene', 'Ring4'], [0.0, 0.0, 0.0, 0.0, 0.0, np.nan, np.nan, np.nan, np.nan, np.nan, np.nan, np.nan, np.nan, np.nan, np.nan])

    @with_tempfile
    def test_atom_to_parquet_no_h(self, tfilename):
        data = pd.read_csv(resource_filename('tflon_test.data', 'molecules.tsv'), sep='\t', index_col=0)
        data = data[data.columns[16:20]]
        df1 = tflon.chem.atom_to_parquet(self.molecules_no_h, data, tfilename)
        np.testing.assert_array_equal(df1.loc['Benzene', 'Ring4'], [0.0, 0.0, 0.0, 0.0, 0.0])

        df2 = tflon.data.read_parquet(tfilename)
        np.testing.assert_array_equal(df2.loc['Benzene', 'Ring4'], [0.0, 0.0, 0.0, 0.0, 0.0])

    @with_tempfile
    def test_bond_to_parquet(self, tfilename):
        data = pd.read_csv(resource_filename('tflon_test.data', 'bond.tsv'), sep='\t', index_col=0)
        data = data[data.columns[16:20]]
        df1 = tflon.chem.bond_to_parquet(self.molecules, data, tfilename, numeric_ids=True )
        np.testing.assert_array_equal(df1.loc['Benzene', 'Atom2_NS_sp2_0'], [0.0, 0.0, np.nan, np.nan, 0.0, np.nan, np.nan, 0.0, np.nan, np.nan, 0.0, np.nan, np.nan, np.nan, np.nan])

        df2 = tflon.data.read_parquet(tfilename)
        np.testing.assert_array_equal(df2.loc['Benzene', 'Atom2_NS_sp2_0'], [0.0, 0.0, np.nan, np.nan, 0.0, np.nan, np.nan, 0.0, np.nan, np.nan, 0.0, np.nan, np.nan, np.nan, np.nan])

    @with_tempfile
    def test_bond_lonepair_to_parquet(self, tfilename):
        sdf_fn = resource_filename('tflon_test.data', 'cyanobenzodioxolic_acid.sdf')
        molecules = tflon.chem.MoleculeTable(tflon.chem.sdf_to_parquet(sdf_fn, lonepair=True)).deserialize().data

        data = pd.read_csv(resource_filename('tflon_test.data', 'bond_and_lonepair.tsv'), sep='\t', index_col=0)
        data = data[data.columns[16:20]]

        df1 = tflon.chem.bond_to_parquet(molecules, data, tfilename, numeric_ids=False, lonepair=True)
        np.testing.assert_array_equal(df1.loc['cyanobenzodioxolic_acid','Atom1_NO_sp2_1'], [0, 0, 0, 2, 2, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 0, 0, 2, 2, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0])

        df2 = tflon.data.read_parquet(tfilename)
        np.testing.assert_array_equal(df2.loc['cyanobenzodioxolic_acid','Atom1_NO_sp2_1'], [0, 0, 0, 2, 2, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 0, 0, 2, 2, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0])

class MoleculeTests(unittest.TestCase):
    def test_remove_hydrogens(self):
        pymol = pybel.readstring('smi', 'c1cc([H])c([H])cc1C(=O)OC')
        M = tflon.chem.Molecule.from_json( tflon.chem.pymol_to_json(pymol) )
        self.run_H_checks(M)
        exp_heavy = M.heavy_bonds
        M.remove_hydrogens()
        self.assertEqual(M.hydrogens, [])
        self.assertEqual(M.light_bonds, [])
        self.assertEqual(M.heavy_bonds, exp_heavy)

    def test_centroids(self):
        pymol, = pybel.readfile('sdf', resource_filename('tflon_test.data', 'cyanobenzodioxolic_acid.sdf'))
        pymol.removeh()
        mol1 = tflon.chem.Molecule.from_json( tflon.chem.pymol_to_json(pymol, addhs=False) )

        pymol = pybel.readstring('smi', 'CCNCNC[O-].[Na+]')
        pymol.removeh()
        mol2 = tflon.chem.Molecule.from_json( tflon.chem.pymol_to_json(pymol, addhs=False) )

        self.assertEqual(mol1.eccentricity, {1:8, 2:7, 3:8, 4:6, 5:6, 6:6, 7:6, 8:5, 9:6, 10:7, 11:8, 12:7, 13:5, 14:6, 15:7})
        self.assertEqual(mol2.eccentricity, {1:7, 2:6, 3:5, 4:4, 5:5, 6:6, 7:7, 8:1})

    def run_checks(self, M):
        self.assertEqual( [1,2,3,4,5,6,7,8,9,10], M.heavy_atoms )
        self.assertEqual( 18, len(M.nodes) )

        self.assertEqual( {(1,2), (2,3), (3,4), (4,5), (5,6), (1,6), (6,7), (7,8), (7,9), (9,10)}, set(M.heavy_bonds) )
        self.assertEqual( 18, len(M.edges) )

    def run_H_checks(self, M):
        self.assertEqual( [1,2,3,5,7,8,9,10,11,12], M.heavy_atoms )
        self.assertEqual( 18, len(M.nodes) )

        self.assertEqual( {(1,2), (2,3), (3,5), (5,7), (7,8), (1,8), (8,9), (9,10), (9,11), (11,12)}, set(M.heavy_bonds) )
        self.assertEqual( 18, len(M.edges) )

        self.assertEqual( {(1,13), (3,4), (5,6), (2,14), (7,15), (12,16), (12,17), (12,18)}, set(M.light_bonds) )

#    @unittest.skipIf( pkgutil.find_loader('rdkit') is None, "Test requires rdkit" )
#    def test_rdmol_convert_to_json_with_interspersed_hydrogens(self):
#        from rdkit import Chem
#        rdmol = Chem.MolFromSmiles('c1cc([H])c([H])cc1C(=O)OC')
#        M = tflon.chem.Molecule.from_json( tflon.chem.rdmol_to_json(rdmol) )
#        self.run_H_checks(M)

    def test_pymol_convert_to_json_with_interspersed_hydrogens(self):
        pymol = pybel.readstring('smi', 'c1cc([H])c([H])cc1C(=O)OC')
        M = tflon.chem.Molecule.from_json( tflon.chem.pymol_to_json(pymol) )
        self.run_H_checks(M)

    @unittest.skipIf( pkgutil.find_loader('rdkit') is None, "Test requires rdkit" )
    def test_rdmol_convert_to_json(self):
        from rdkit import Chem
        rdmol = Chem.MolFromSmiles('c1ccccc1C(=O)OC')
        M = tflon.chem.Molecule.from_json( tflon.chem.rdmol_to_json(rdmol) )
        self.run_checks(M)

    @unittest.skipIf( pkgutil.find_loader('rdkit') is None, "Test requires rdkit" )
    def test_rdmol_convert_to_json_with_coordinates(self):
        from rdkit import Chem
        from rdkit.Chem import AllChem
        rdmol = Chem.MolFromSmiles('c1ccccc1C(=O)OC')
        AllChem.Compute2DCoords(rdmol)
        M = tflon.chem.Molecule.from_json( tflon.chem.rdmol_to_json(rdmol, coordinates=True) )
        self.run_checks(M)
        conf = rdmol.GetConformer()

        for i, a in enumerate(rdmol.GetAtoms()):
            pos = conf.GetAtomPosition(a.GetIdx())
            np.testing.assert_array_almost_equal(tuple(M.nodes.coordinates[a.GetIdx()]), (pos.x, pos.y, pos.z))

    def test_pymol_convert_to_json_and_back_with_coordinates(self):
        it = pybel.readfile('sdf', resource_filename('tflon_test.data', 'molecules.sdf'))
        pmol = next(it)
        coords = np.zeros((len(pmol.atoms),3))
        for i, a in enumerate(pmol.atoms):
            coords[i,:] = a.coords

        tmol = tflon.chem.Molecule.from_json( tflon.chem.pymol_to_json(pmol, coordinates=True) )

        pmol2 = tmol.pymol
        coords2 = np.zeros((len(pmol2.atoms),3))
        for i, a in enumerate(pmol2.atoms):
            coords2[i,:] = a.coords

        np.testing.assert_array_almost_equal(coords, coords2)

    def test_pymol_convert_to_json(self):
        pymol = pybel.readstring('smi', 'c1ccccc1C(=O)OC')
        M = tflon.chem.Molecule.from_json( tflon.chem.pymol_to_json(pymol) )
        self.run_checks(M)

    @unittest.skipIf( pkgutil.find_loader('rdkit') is None, "Test requires rdkit" )
    def test_molecule_to_rdkit(self):
        import rdkit
        from rdkit import Chem
        examples = [('c1ccccc1C(=O)OC', 18, 18),
                    ('c1ccn(C=CC=Cn2cccc2)c1', 26, 27),
                    ('Cc1ccn(C=CC=Cn2cccc2)c1', 29, 30)]
        for smi, atoms, bonds in examples:
            pymol = pybel.readstring('smi', smi)
            rdmol = tflon.chem.Molecule.from_json( tflon.chem.pymol_to_json(pymol) ).rdmol

            rdsmi = Chem.MolToSmiles(Chem.RemoveHs(rdmol))
            osmi = Chem.MolToSmiles(Chem.MolFromSmiles(smi))
            self.assertEqual( rdsmi, osmi )

            self.assertEqual(atoms, rdmol.GetNumAtoms())
            self.assertEqual(bonds, rdmol.GetNumBonds())

    def test_molecule_to_pymol(self):
        examples = [('c1ccccc1C(=O)OC', 18, 18),
                    ('c1ccn(C=CC=Cn2cccc2)c1', 26, 27),
                    ('Cc1ccn(C=CC=Cn2cccc2)c1', 29, 30)]
        for smi, atoms, bonds in examples:
            pymol = pybel.readstring('smi', smi)
            pymol2 = tflon.chem.Molecule.from_json( tflon.chem.pymol_to_json(pymol) ).pymol
            self.assertEqual( pymol2.write('smi').strip().replace("/","").replace("\\",""), smi )

            self.assertEqual(atoms, len(pymol2.atoms))
            self.assertEqual(bonds, pymol2.OBMol.NumBonds())

    def test_molecule_get_topological_classes(self):
        mol = pybel.readstring('smi', 'c1ccccc1C(=O)OC')
        mol.addh()
        M = tflon.chem.Molecule.from_json( tflon.chem.pymol_to_json(mol, coordinates=False) )
        self.assertEqual(M.topological_groups, [1, 2, 3, 2, 1, 4, 5, 6, 7, 8, 9, 10, 11, 10, 9, 12, 12, 12])

        M = tflon.chem.Molecule.from_json( tflon.chem.pymol_to_json(mol, coordinates=True) )
        self.assertEqual(M.topological_groups, [1, 2, 3, 2, 1, 4, 5, 6, 7, 8, 9, 10, 11, 10, 9, 12, 12, 12])

    def test_molecule_get_topological_classes_symmetric_and_non_symmetric(self):
        sym = pybel.readstring('smi', 'c1ccn(C=CC=Cn2cccc2)c1')
        sym.addh()
        nosym = pybel.readstring('smi', 'Cc1ccn(C=CC=Cn2cccc2)c1')
        nosym.addh()

        Msym = tflon.chem.Molecule.from_json( tflon.chem.pymol_to_json(sym) )
        Mnosym = tflon.chem.Molecule.from_json( tflon.chem.pymol_to_json(nosym) )
        self.assertEqual( Msym.topological_groups[:14], [1,1,2,3,4,5,5,4,3,2,1,1,2,2] )
        self.assertEqual( Mnosym.topological_groups[:15], [1,2,3,4,5,6,7,8,9,10,11,12,12,11,13] )

    def test_molecule_with_duplicate_chains(self):
        mol = pybel.readstring('smi', 'CC1=C(C(CCC1)(C)C)/C=C\C(=C/C=C/C(=C/C=C/C=C(/C)\C=C\C=C(\C)/C=C\C1=C(CCCC1(C)C)C)/C)\C')
        mol.addh()
        M = tflon.chem.Molecule.from_json( tflon.chem.pymol_to_json(mol) )

        np.testing.assert_array_equal( M.topological_groups, [1, 2, 3, 4, 5, 6, 7, 8, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 17, 16, 15, 18, 14,
                                                                13, 12, 11, 19, 10, 9, 3, 2, 7, 6, 5, 4, 8, 8, 1, 18, 19, 20, 20, 20, 21, 21, 22,
                                                                22, 23, 23, 24, 24, 24, 24, 24, 24, 25, 26, 27, 28, 29, 30, 31, 31, 30, 32, 32,
                                                                32, 29, 28, 27, 33, 33, 33, 26, 25, 23, 23, 22, 22, 21, 21, 24, 24, 24, 24, 24,
                                                                24, 20, 20, 20, 32, 32, 32, 33, 33, 33])

    def test_homogenous_molecule(self):
        mol = pybel.readstring('smi', 'c1ccc2c(c1)c1ccccc1c1CCC=Cc21')
        mol.addh()
        M = tflon.chem.Molecule.from_json( tflon.chem.pymol_to_json(mol) )

        np.testing.assert_array_equal( M.topological_groups, [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24,
                                                                25, 26, 27, 27, 28, 28, 29, 30] )
class MetricTests(unittest.TestCase):
    @unittest.skipIf(True, "Skipping test of unused feature")
    def test_aveb(self):
        molecules = tflon.chem.MoleculeTable(tflon.data.read_parquet(resource_filename('tflon_test.data', 'sample_shard/molecules.pq')).deserialize())
        molecules = molecules.data
        targets = tflon.data.read_parquet( resource_filename('tflon_test.data', 'sample_shard/targets.pq') )
        fingerprints = tflon.chem.generate_fingerprints(molecules)

        indices = np.random.choice(molecules.index, size=(100,))
        print(tflon.chem.asymmetric_validation_embedding_bias(fingerprints.Fingerprint, targets.PROMISCUOUS_5, indices))

class ToolkitTests(TflonOpTestCase):
    def setUp(self):
        TflonOpTestCase.setUp(self)

        M1 = tflon.chem.pymol_to_json(pybel.readstring('smi', 'Cc1ccccc1C(=O)OC'), addhs=False)
        M2 = tflon.chem.pymol_to_json(pybel.readstring('smi', 'c1ccccc1C(=O)OC'), addhs=False)
        M3 = tflon.chem.pymol_to_json(pybel.readstring('smi', 'c1ccc2c(c1)c1ccccc1c1CCC=Cc21'), addhs=False)
        self.jmols = [[M1],[M2],[M3]]
        molecules = pd.DataFrame(data=[[M1],[M2],[M3]], columns=['Structures'], index=[0,1,2])

        self._batch = {'molecules': tflon.chem.MoleculeTable(molecules).deserialize()}
        self._molecules = self._batch['molecules'].data.iloc[:, 0].tolist()
        self._graph_table = self._test_tower.add_table('molecules', tflon.chem.MoleculeTable)

    def test_atom_properties_error_on_unknown(self):
        M4 = tflon.chem.pymol_to_json(pybel.readstring('smi', 'C=[N+]=O'), addhs=True)
        molecules = pd.DataFrame(data=self.jmols+[[M4]], columns=['Structures'], index=[0,1,2,3])
        batch = {'molecules': tflon.chem.MoleculeTable(molecules).deserialize()}

        ap = tflon.chem.AtomProperties(self._graph_table, atom_types=['H', 'C', 'N'], allow_unknown=False)
        ap()
        try:
            features = ap.featurizer(batch)
        except AssertionError as e:
            self.assertEqual(str(e), "Unrecognized atom type: 8")
        else:
            raise AssertionError("Expected 'AssertionError: Unrecognized atom type: 8' to be raised")


    def test_atom_properties(self):
        M4 = tflon.chem.pymol_to_json(pybel.readstring('smi', 'C=[N+]=O'), addhs=True)
        molecules = pd.DataFrame(data=self.jmols+[[M4]], columns=['Structures'], index=[0,1,2,3])
        batch = {'molecules': tflon.chem.MoleculeTable(molecules).deserialize()}

        ap = tflon.chem.AtomProperties(self._graph_table, atom_types=['H', 'C', 'N'])
        out = ap()
        features = ap.featurizer(batch)
        vals = features['AtomProperties_0/atom_properties'].as_matrix()
        exp = np.zeros((44,5))
        r = np.arange(44)
        c = np.array([2,2,2,2,2,2,2,2,0,0,2,  2,2,2,2,2,2,2,0,0,2,  2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,   2,3,0,1,1])
        exp[r,c] = 1
        exp[40,4] = 1

        np.testing.assert_array_almost_equal(vals, exp)

        with tf.Session() as S:
            batch = self._test_tower.featurizer(batch)
            feed_dict = self._test_tower.feed({k:tflon.data.convert_to_feedable(v) for k,v in batch.items()})
            vals = S.run(out, feed_dict=feed_dict)
            np.testing.assert_array_almost_equal(vals, exp)

    @unittest.skipIf( pkgutil.find_loader('rdkit') is None, "Test requires rdkit" )
    def test_bond_properties(self):
        M4 = tflon.chem.pymol_to_json(pybel.readstring('smi', 'c1ccccc1C#[N+]O'), addhs=False)
        allmols = self.jmols+[[M4]]
        molecules = pd.DataFrame(data=allmols, columns=['Structures'], index=[0,1,2,3])
        batch = {'molecules': tflon.chem.MoleculeTable(molecules).deserialize()}

        bp = tflon.chem.BondProperties(self._graph_table, include_aromatic=True)
        out = bp()
        features = bp.featurizer(batch)
        vals = features['BondProperties_0/bond_properties'].as_matrix()

        molbonds = [len(tflon.chem.Molecule.from_json(m).edges) for m, in allmols]
        bonds = sum(molbonds)
        np.testing.assert_array_equal(vals.shape, (bonds, 5))

        exp = [[7,4,0,6,9],
               [6,4,0,6,9],
               [13,8,0,16,18],
               [5,3,1,6,9]]
        ix=0
        for v, xv in zip(molbonds, exp):
            type_counts = vals[ix:ix+v].sum(axis=0)
            np.testing.assert_array_almost_equal(type_counts, xv)
            ix+=v

        with tf.Session() as S:
            batch = self._test_tower.featurizer(batch)
            feed_dict = self._test_tower.feed({k:tflon.data.convert_to_feedable(v) for k,v in batch.items()})
            out = S.run(out, feed_dict=feed_dict)
            np.testing.assert_array_equal(out, vals)

    def test_graph_encoder(self):
        pl = tf.placeholder(name='atoms', dtype=tf.float32, shape=[None, 13])
        rnn_cell = tf.nn.rnn_cell.LSTMCell(5)

        weight_network = tflon.toolkit.Dense(20, activation=tf.nn.elu) | tflon.toolkit.Dense(13, activation=tf.nn.elu)

        encoder = tflon.graph.Encoder(self._graph_table, rnn_cell, weight_network, iterations=5)
        embedding = encoder(pl)

        T = np.random.random(size=(sum(len(M.nodes) for M in self._molecules), 13))
        batch = self._test_tower.featurizer(self._batch)
        feed_dict = self._test_tower.feed({k:tflon.data.convert_to_feedable(v) for k,v in batch.items()})
        feed_dict[pl] = T

        with tf.Session() as S:
            S.run( tf.variables_initializer( self._test_tower.trainables ) )
            out = S.run(embedding, feed_dict=feed_dict)
            self.assertEqual(out.shape, (3, 5))

    def test_reduce(self):
        pl = tf.placeholder(name='atoms', dtype=tf.float32, shape=[None, 13])
        R = tflon.graph.Reduce(self._graph_table)
        reduced = R(pl)

        vname,=R.slots.values()
        self.assertEqual(vname, 'Reduce_0/reduce')
        tables = R.featurizer(self._batch)
        mat = tables['Reduce_0/reduce'].as_matrix()

        np.testing.assert_array_equal( mat, sum(([i] * len(M.nodes) for i, M in enumerate(self._molecules)), []) )

        with tf.Session() as S:
            T = np.random.random(size=(sum(len(M.nodes) for M in self._molecules), 13))
            out = S.run(reduced, feed_dict={pl: T, self._test_tower.inputs['Reduce_0/reduce']: mat})
            exp = np.zeros((3, 13))

            j=0
            for i,M in enumerate(self._molecules):
                c = len(self._molecules[i].nodes)
                exp[i,:] = np.sum(T[j:j+c, :], axis=0)
                j += c

            np.testing.assert_array_almost_equal( out, exp, decimal=3 )


    def test_pool(self):
        pl = tf.placeholder(name='atoms', dtype=tf.float32, shape=[None, 13])
        R = tflon.graph.Pool(self._graph_table, 2)
        reduced = R(pl)

        sname = 'Pool_0/pool_depth_2'
        vname,=R.slots.values()
        self.assertEqual(vname, sname)
        tables = R.featurizer(self._batch)
        mat = tables[sname].as_matrix()

        with tf.Session() as S:
            T = np.random.random(size=(sum(len(M.nodes) for M in self._molecules), 13))
            out = S.run(reduced, feed_dict={pl: T, self._test_tower.inputs[sname]: mat})
            self.assertEqual(T.shape, out.shape)

    def test_pool_multiple_molecules_featurization(self):
        pl = tf.placeholder(name='atoms', dtype=tf.float32, shape=[None, 13])
        R = tflon.graph.Pool(self._graph_table, 1)
        reduced = R(pl)

        sname = 'Pool_0/pool_depth_1'
        vname,=R.slots.values()
        self.assertEqual(vname, sname)
        tables = R.featurizer(self._batch)
        mat = tables[sname].as_matrix()

        np.testing.assert_array_equal([1, 0, 2, 6, 1, 3, 2, 4, 3, 5, 4, 6, 1, 5, 7, 6, 8, 9, 7, 7, 10, 9] +
                                      [12, 16, 11, 13, 12, 14, 13, 15, 14, 16, 11, 15, 17, 16, 18, 19, 17, 17, 20, 19], mat[:42,0])
        np.testing.assert_array_equal([0, 1, 1, 1, 2, 2, 3, 3, 4, 4, 5, 5, 6, 6, 6, 7, 7, 7, 8, 9, 9, 10] +
                                      [11, 11, 12, 12, 13, 13, 14, 14, 15, 15, 16, 16, 16, 17, 17, 17, 18, 19, 19, 20], mat[:42,1])

    def test_pool_with_small_fragments(self):
        pymol = pybel.readstring('smi', 'CCNCNC[O-].[Na+]')
        pymol.removeh()
        mol = tflon.chem.pymol_to_json(pymol, addhs=False)
        pl = tf.placeholder(name='atoms', dtype=tf.float32, shape=[None, 13])
        R1 = tflon.graph.Pool(self._graph_table, 2)
        R2 = tflon.graph.Pool(self._graph_table, 4)
        reduced1 = R1(pl)
        reduced2 = R2(pl)

        molecules = pd.DataFrame(data=[[mol]], columns=['Structures'], index=[0])
        batch = {'molecules': tflon.chem.MoleculeTable(molecules).deserialize()}

        sname1, = R1.slots.values()
        tables = R1.featurizer(batch)
        mat1 = tables[sname1].as_matrix()
        atom_gather, atom_reduce = mat1[:,0], mat1[:,1]
        np.testing.assert_array_equal(atom_gather, [2, 3, 0, 4, 1, 5, 2, 6, 3, 4, -1])
        np.testing.assert_array_equal(atom_reduce, [0, 1, 2, 2, 3, 3, 4, 4, 5, 6,  7])

        sname2, = R2.slots.values()
        tables = R2.featurizer(batch)
        mat2 = tables[sname2].as_matrix()
        atom_gather, atom_reduce = mat2[:,0], mat2[:,1]
        np.testing.assert_array_equal(atom_gather, [4, 5, 6, -1, 0, 1, 2, -1])
        np.testing.assert_array_equal(atom_reduce, [0, 1, 2,  3, 4, 5, 6,  7])

        with tf.Session() as S:
            T = np.random.random(size=(sum(len(M.nodes) for ix, M in batch['molecules'].data.iloc[:,0].iteritems()), 13))
            out1, out2 = S.run([reduced1, reduced2], feed_dict={pl: T, R1.names[sname1]: mat1, R2.names[sname2]: mat2})
            self.assertEqual(T.shape, out1.shape)
            self.assertEqual(T.shape, out2.shape)


    def test_pool_depth_3(self):
        pymol, = pybel.readfile('sdf', resource_filename('tflon_test.data', 'cyanobenzodioxolic_acid.sdf'))
        pymol.removeh()
        mol = tflon.chem.pymol_to_json(pymol, addhs=False)

        pl = tf.placeholder(name='atoms', dtype=tf.float32, shape=[None, 13])
        R = tflon.graph.Pool(self._graph_table, 3)
        reduced = R(pl)

        molecules = pd.DataFrame(data=[[mol]], columns=['Structures'], index=[0])
        batch = {'molecules': tflon.chem.MoleculeTable(molecules).deserialize()}

        sname, = R.slots.values()
        tables = R.featurizer(batch)
        mat = tables[sname].as_matrix()

        atom_gather, atom_reduce = mat[:,0], mat[:,1]

        self.assertEqual([4, 12, 5, 7, 13], atom_gather[:5].tolist())
        self.assertEqual([0, 0, 1, 1, 1], atom_reduce[:5].tolist())
        self.assertEqual([1, 4, 6, 8, 3, 7], atom_gather[-6:].tolist())
        self.assertEqual([13, 13, 13, 13, 14, 14], atom_reduce[-6:].tolist())
        self.assertEqual(14, atom_reduce[-1])

    def test_weave_featurization(self):
        pymol, = pybel.readfile('sdf', resource_filename('tflon_test.data', 'cyanobenzodioxolic_acid.sdf'))
        pymol.removeh()
        mol = tflon.chem.pymol_to_json(pymol, addhs=False)

        pl = tf.placeholder(name='atoms', dtype=tf.float32, shape=[None, 13])
        W = tflon.graph.Weave(self._graph_table, node_width=10, pair_width=5)
        nodes, pairs = tflon.toolkit.Chain(*[W]*2)(pl)
        atoms = W.outputs(nodes, pairs)

        molecules = pd.DataFrame(data=[[mol]], columns=['Structures'], index=[0])
        batch = {'molecules': tflon.chem.MoleculeTable(molecules).deserialize()}

        node_slot, pair_slot, pair_features_slot = W.slots.values()
        tables = W.featurizer(batch)

        node_mat = tables[node_slot].as_matrix()
        left_atom = node_mat[:,0].tolist()
        right_atom = node_mat[:,1].tolist()

        np.testing.assert_array_equal([0, 0, 0, 1, 1, 1,  1, 2, 3, 3, 3,  3,  3],  left_atom[:13])
        np.testing.assert_array_equal([1, 2, 3, 2, 3, 4, 12, 3, 4, 5, 7, 12, 13], right_atom[:13])

        pair_mat = tables[pair_slot].as_matrix()
        pair_gather = pair_mat[:,0].tolist()
        pair_reduce = pair_mat[:,1].tolist()

        np.testing.assert_array_equal([0, 1, 2, 0, 3, 4, 5, 6, 1, 3, 7, 2, 4, 7, 8, 9, 10, 11, 12], pair_gather[:19])
        self.assertEqual([0, 0, 0, 1, 1, 1, 1, 1, 2, 2, 2, 3, 3, 3, 3, 3,  3,  3,  3], pair_reduce[:19])

        pf_mat = tables[pair_features_slot].as_matrix()
        pair_features = pf_mat.reshape((-1,)).tolist()
        np.testing.assert_array_equal([0, 1, 1, 0, 0, 1, 1, 1, 0, 1, 1, 0, 1], pair_features[:13])

        with tf.Session() as S:
            S.run( tf.variables_initializer( self._test_tower.variables ) )
            T = np.random.random(size=(sum(len(M.nodes) for ix, M in batch['molecules'].data.iloc[:,0].iteritems()), 13))
            out = S.run(atoms, feed_dict={pl: T, self._test_tower.inputs[node_slot]: node_mat,
                                                  self._test_tower.inputs[pair_slot]: pair_mat,
                                                  self._test_tower.inputs[pair_features_slot]: pf_mat})
            self.assertEqual(out.shape, (T.shape[0], 10))

    def test_weave_featurization_with_edge_properties(self):
        pymol, = pybel.readfile('sdf', resource_filename('tflon_test.data', 'cyanobenzodioxolic_acid.sdf'))
        pymol.removeh()
        mol = tflon.chem.pymol_to_json(pymol, addhs=False)

        pl_atom = tf.placeholder(name='atoms', dtype=tf.float32, shape=[None, 13])
        pl_bond = tf.placeholder(name='bonds', dtype=tf.float32, shape=[None, 7])
        W = tflon.graph.Weave(self._graph_table, node_width=10, pair_width=5, edge_properties=True)
        n, p = tflon.toolkit.Chain(*[W]*2)(pl_atom, W.initial_pair_state(pl_bond))
        atoms, bonds = W.outputs(n, p)

        node_slot, pair_slot, pair_features_slot, edge_gather_slot, edge_pairs_slot = W.slots.values()
        molecules = pd.DataFrame(data=[[mol]], columns=['Structures'], index=[0])
        batch = {'molecules': tflon.chem.MoleculeTable(molecules).deserialize()}

        features = W.featurizer(batch)
        ei_mat = features['Weave_0/edge_indices'].as_matrix().reshape((-1,)).tolist()
        np.testing.assert_array_equal([0, -1, -1, 1, 2, -1, -1, -1, 3, -1, -1, 4, -1], ei_mat[:13])

        epi_mat = features['Weave_0/edge_pair_indices'].as_matrix().reshape((-1,)).tolist()
        np.testing.assert_array_equal([0, 3, 4, 8, 11, 13], epi_mat[:6])

        with tf.Session() as S:
            S.run( tf.variables_initializer( self._test_tower.variables ) )

            fd = self._test_tower.feed({k:tflon.data.convert_to_feedable(v) for k,v in features.items()})
            N = np.random.random(size=(sum(len(M.nodes) for ix, M in batch['molecules'].data.iloc[:,0].iteritems()), 13))
            E = np.random.random(size=(sum(len(M.edges) for ix, M in batch['molecules'].data.iloc[:,0].iteritems()), 7))
            fd[pl_atom] = N
            fd[pl_bond] = E

            out_atoms, out_bonds = S.run([atoms, bonds], feed_dict=fd)

            self.assertEqual(out_atoms.shape, (N.shape[0], 10))
            self.assertEqual(out_bonds.shape, (E.shape[0], 5))

    def test_weave_with_small_fragments(self):
        pymol = pybel.readstring('smi', 'CCNCNC[O-].[Na+]')
        pymol.removeh()
        mol = tflon.chem.pymol_to_json(pymol, addhs=False)
        pl = tf.placeholder(name='atoms', dtype=tf.float32, shape=[None, 13])
        W = tflon.graph.Weave(self._graph_table, node_width=10, pair_width=5, distance=1)

        nodes, pairs = tflon.toolkit.Chain(*[W]*2)(pl)
        atoms = W.outputs(nodes, pairs)

        molecules = pd.DataFrame(data=[[mol]], columns=['Structures'], index=[0])
        batch = {'molecules': tflon.chem.MoleculeTable(molecules).deserialize()}

        node_slot, pair_slot, pair_features_slot = W.slots.values()
        tables = W.featurizer(batch)

        node_mat = tables[node_slot].as_matrix()
        left_atom = node_mat[:,0].tolist()
        right_atom = node_mat[:,1].tolist()

        np.testing.assert_array_equal([0, 1, 2, 3, 4, 5],  left_atom)
        np.testing.assert_array_equal([1, 2, 3, 4, 5, 6], right_atom)

        pair_mat = tables[pair_slot].as_matrix()
        pair_gather = pair_mat[:,0].tolist()
        pair_reduce = pair_mat[:,1].tolist()

        np.testing.assert_array_equal([0, 0, 1, 1, 2, 2, 3, 3, 4, 4, 5, 5, -1], pair_gather)
        np.testing.assert_array_equal([0, 1, 1, 2, 2, 3, 3, 4, 4, 5, 5, 6, 7], pair_reduce)

        pf_mat = tables[pair_features_slot].as_matrix()
        pair_features = pf_mat.reshape((-1,)).tolist()
        np.testing.assert_array_equal([0, 0, 0, 0, 0, 0], pair_features)

        with tf.Session() as S:
            S.run( tf.variables_initializer( self._test_tower.variables ) )
            T = np.random.random(size=(sum(len(M.nodes) for ix, M in batch['molecules'].data.iloc[:,0].iteritems()), 13))
            out = S.run(atoms, feed_dict={pl: T, self._test_tower.inputs[node_slot]: node_mat,
                                                 self._test_tower.inputs[pair_slot]: pair_mat,
                                                 self._test_tower.inputs[pair_features_slot]: pf_mat})
            self.assertEqual(out.shape, (T.shape[0], 10))

    def test_weave_with_multiple_molecules(self):
        pl = tf.placeholder(name='atoms', dtype=tf.float32, shape=[None, 13])
        W = tflon.graph.Weave(self._graph_table, node_width=10, pair_width=5, distance=1)
        nodes, pairs = tflon.toolkit.Chain(*[W]*2)(pl)
        atoms = W.outputs(nodes, pairs)

        node_slot, pair_slot, pair_features_slot = W.slots.values()
        tables = W.featurizer(self._batch)

        node_mat = tables[node_slot].as_matrix()
        left_atom = node_mat[:,0].tolist()
        right_atom = node_mat[:,1].tolist()

        # Cc1ccccc1C(=O)OC
        # c1ccccc1C(=O)OC
        # c1ccc2c(c1)c1ccccc1c1CCC=Cc21
        self.assertEqual([0, 1, 1, 2, 3, 4, 5, 6, 7, 7,  9, 11, 11, 12, 13, 14, 15, 16, 17, 17, 19],  left_atom[:21])
        self.assertEqual([1, 2, 6, 3, 4, 5, 6, 7, 8, 9, 10, 12, 16, 13, 14, 15, 16, 17, 18, 19, 20], right_atom[:21])
        self.assertEqual(max(left_atom), 37)
        self.assertEqual(max(right_atom), 38)

        pair_mat = tables[pair_slot].as_matrix()
        pair_gather = pair_mat[:,0].tolist()
        pair_reduce = pair_mat[:,1].tolist()

        seg1 = [0, 0, 1, 2, 1, 3, 3, 4, 4, 5, 5, 6, 2, 6, 7, 7, 8, 9, 8, 9, 10, 10]
        seg2 = [11, 12, 11, 13, 13, 14, 14, 15, 15, 16, 12, 16, 17, 17, 18, 19, 18, 19, 20, 20]
        self.assertEqual(seg1 + seg2, pair_gather[:42])
        self.assertEqual([0, 1, 1, 1, 2, 2, 3, 3, 4, 4, 5, 5, 6, 6, 6, 7, 7, 7, 8, 9, 9, 10] +
                          [11, 11, 12, 12, 13, 13, 14, 14, 15, 15, 16, 16, 16, 17, 17, 17, 18, 19, 19, 20], pair_reduce[:42])

        self.assertEqual(max(pair_gather), 41)
        self.assertEqual(max(pair_reduce), 38)

        pf_mat = tables[pair_features_slot].as_matrix()
        pair_features = pf_mat.reshape((-1,)).tolist()
        self.assertEqual([0] * 42, pair_features)

        with tf.Session() as S:
            S.run( tf.variables_initializer( self._test_tower.variables ) )
            T = np.random.random(size=(sum(len(M.nodes) for M in self._molecules), 13))
            fd = {self._test_tower.inputs[sname]: tables[sname].as_matrix() for sname in W.slots.values()}
            fd[pl] = T

            out = S.run(atoms, feed_dict=fd)
            self.assertEqual(out.shape, (T.shape[0], 10))

    def test_weave_with_dynamic_depth(self):
        pl = tf.placeholder(name='atoms', dtype=tf.float32, shape=[None, 13])
        dp = tf.placeholder(name='depth', dtype=tf.int32, shape=[])
        W = tflon.graph.Weave(self._graph_table, node_width=10, pair_width=5, distance=1)
        W.initialize()
        net = tflon.toolkit.ForLoop(lambda i, n, p: W(n,p), dp) | tflon.toolkit.Apply(lambda n,p: W.outputs(n,p))

        ns = tflon.toolkit.Dense(10)(pl)
        ps = W.initial_pair_state()
        atoms = net(ns, ps)

        features = W.featurizer(self._batch)
        with tf.Session() as S:
            S.run( tf.variables_initializer( self._test_tower.variables ) )

            fd = self._test_tower.feed({k:tflon.data.convert_to_feedable(v) for k,v in features.items()})
            N = np.random.random(size=(sum(len(M.nodes) for ix, M in self._batch['molecules'].data.iloc[:,0].iteritems()), 13))
            fd[pl] = N
            fd[dp] = 5

            out_atoms = S.run(atoms, feed_dict=fd)
            self.assertEqual(out_atoms.shape, (N.shape[0], 10))

    def test_atom_to_bond(self):
        pymol, = pybel.readfile('sdf', resource_filename('tflon_test.data', 'cyanobenzodioxolic_acid.sdf'))
        pymol.removeh()
        mol = tflon.chem.pymol_to_json(pymol, addhs=False)

        pl = tf.placeholder(name='atoms', dtype=tf.float32, shape=[None, 13])
        AB = tflon.graph.NodeToEdge(self._graph_table, width=15)
        bonds = AB(pl)

        molecules = pd.DataFrame(data=[[mol]], columns=['Structures'], index=[0])
        batch = {'molecules': tflon.chem.MoleculeTable(molecules).deserialize()}

        node_gather_slot, = AB.slots.values()
        tables = AB.featurizer(batch)

        node_mat = tables[node_gather_slot].as_matrix()
        left_atom = node_mat[:,0].tolist()
        right_atom = node_mat[:,1].tolist()

        np.testing.assert_array_equal([0,1,1,3,3,4,5,6,6,7,7,8,9,9,12,13],  left_atom)
        np.testing.assert_array_equal([1,2,3,4,12,5,6,7,11,8,12,9,10,11,13,14], right_atom)

        with tf.Session() as S:
            S.run( tf.variables_initializer( self._test_tower.variables ) )
            T = np.random.random(size=(sum(len(M.nodes) for ix, M in batch['molecules'].data.iloc[:,0].iteritems()), 13))
            num_bonds = sum(len(M.edges) for ix, M in batch['molecules'].data.iloc[:,0].iteritems())
            out = S.run(bonds, feed_dict={pl: T, self._test_tower.inputs[node_gather_slot]: node_mat})
            self.assertEqual(out.shape, (num_bonds, 15))

    def test_atom_to_bond_no_bonds(self):
        pymol = pybel.readstring('smi', '[Na+].[K+]')
        pymol.removeh()
        mol = tflon.chem.pymol_to_json(pymol, addhs=False)

        pl = tf.placeholder(name='atoms', dtype=tf.float32, shape=[None, 13])
        AB = tflon.graph.NodeToEdge(self._graph_table, width=15)
        bonds = AB(pl)

        molecules = pd.DataFrame(data=[[mol]], columns=['Structures'], index=[0])
        batch = {'molecules': tflon.chem.MoleculeTable(molecules).deserialize()}

        node_gather_slot, = AB.slots.values()
        tables = AB.featurizer(batch)

        node_mat = tables[node_gather_slot].as_matrix()
        self.assertEqual((0,2), node_mat.shape)

        with tf.Session() as S:
            S.run( tf.variables_initializer( self._test_tower.variables ) )
            T = np.random.random(size=(2, 13))
            out = S.run(bonds, feed_dict={pl: T, self._test_tower.inputs[node_gather_slot]: node_mat})
            self.assertEqual(out.shape, (0, 15))

class GRNNTests(TflonOpTestCase):
    @classmethod
    def setUpClass(cls):
        pymol, = pybel.readfile('sdf', resource_filename('tflon_test.data', 'cyanobenzodioxolic_acid.sdf'))
        pymol.removeh()
        mol_json = tflon.chem.pymol_to_json(pymol, addhs=False)
        cls.mol = tflon.chem.Molecule.from_json(mol_json)

    def setUp(self):
        TflonOpTestCase.setUp(self)
        self._graph_table = self._test_tower.add_table('molecules', tflon.chem.MoleculeTable)

    def test_bfs_block_partition_and_edge_labels(self):
        blocks, ancestors, types = self.mol.bfs([8])

        self.assertEqual([[8], [7, 9, 13], [4, 6, 10, 12, 14], [2, 5, 11, 15], [1, 3]], blocks)
        self.assertEqual({8:[],
                           7:[8],  9:[8],   13:[8],
                           4:[13], 6:[7],   10:[9, 12], 12:[7, 10],  14:[13],
                           2:[4],  5:[4,6], 11:[10],    15:[14],
                           1:[2],  3:[2]}, ancestors)

        for edge in types:
            if edge in {(10, 12)}:
                self.assertEqual('x', types[edge])
            else:
                self.assertEqual('t', types[edge])

    def test_dfs_block_partition_and_edge_labels(self):
        blocks, ancestors, types = self.mol.dfs([1])

        self.assertEqual([[1], [2], [3, 4], [5], [6], [7], [8], [9, 13], [10, 14], [11, 12, 15]], blocks)
        self.assertEqual({ 1:[],       2:[1],    3:[2],
                            4:[2, 13],  5:[4],    6:[5],
                            7:[6, 12],  8:[7],    9:[8],
                           10:[9],     11:[10],  12:[7, 10],
                           13:[4,8],   14:[13],  15:[14]}, ancestors)

        for edge in types:
            if edge in {(7,12), (4,13)}:
                self.assertEqual('b', types[edge])
            else:
                self.assertEqual('t', types[edge])

    def test_graph_scheduler_with_bfs_scheduling_multiple_molecules(self):
        m2 = tflon.chem.Molecule.from_json(tflon.chem.pymol_to_json(pybel.readstring('smi', 'c1ccccc1C(=O)OC'), addhs=False))
        molecules = pd.DataFrame(data=[[self.mol], [m2]], columns=['Structures'], index=[0, 1])
        batch = {'molecules': tflon.data.Table(molecules)}
        W = tflon.graph.GRNN(self._graph_table, rooter=lambda g: [8] if len(g.nodes)==15 else [6])
        W.initialize()

        features = W.featurizer(batch)

        exp = np.array([[1, 1, 1, 1, 2, 1, 1, 0, 1, 2, 1, 2, 1, 1, 1,
                         1, 1, 2, 1, 1, 0, 1, 1, 1, 1],
                        [0, 2, 0, 2, 0, 1, 2, 3, 1, 2, 0, 1, 2, 1, 0,
                         1, 1, 0, 1, 1, 3, 2, 0, 1, 0]], dtype=np.float32).T
        np.testing.assert_array_almost_equal( features['GRNN_0/degrees'].as_matrix(), exp )

        exp = np.vstack([hot([8, 21], 25),
                         hot([7,9,13,16,20,22], 25),
                         hot([4,6,10,12,14,17,19,23,24], 25),
                         hot([2,5,11,15,18,25], 25),
                         hot([1,3], 25)]).T
        val = features['GRNN_0/blocks'].as_matrix()
        np.testing.assert_array_almost_equal( val, exp )

        # Forward pass

        exp = ragged([ [8,21], [7,9,13,16,20,22], [4,6,10,10,12,12,14,17,19,23,24], [2,5,5,11,15,18,18,25], [1,3] ])
        val = features['GRNN_0/forward_nodes'].as_matrix().todense()
        np.testing.assert_array_almost_equal( val, exp )

        exp = ragged([ [0,0],  [8,8,8,21,21,21],  [13,7,9,12,7,10,13,16,20,22,22], [4,4,6,10,14,17,19,24], [2,2] ])
        val = features['GRNN_0/forward_edges'].as_matrix().todense()
        np.testing.assert_array_almost_equal( val, exp )

        exp = ragged([ [0,0],  [0,0,0,0,0,0],  [0,0,0,1,0,1,0,0,0,0,0],  [0,0,0,0,0,0,0,0], [0,0] ])
        val = features['GRNN_0/forward_ports'].as_matrix().todense()
        np.testing.assert_array_almost_equal( val, exp )

        exp = ragged([ [0,1], [0,1,2,3,4,5], [0,1,2,2,3,3,4,5,6,7,8], [0,1,1,2,3,4,4,5], [0,1] ])
        val = features['GRNN_0/forward_reduce'].as_matrix().todense()
        np.testing.assert_array_almost_equal( val, exp )

        # Backward pass

        exp = ragged([ [8,8,8,21,21,21], [7,7,9,13,13,16,20,22,22], [4,4,6,10,10,12,14,17,19,23,24], [2,2,5,11,15,18,25], [1,3] ])
        val = features['GRNN_0/backward_nodes'].as_matrix().todense()
        np.testing.assert_array_almost_equal( val, exp )

        exp = ragged([ [7,9,13,16,20,22], [6,12,10,4,14,17,19,23,24], [2,5,5,11,12,10,15,18,18,0,25], [1,3,0,0,0,0,0], [0,0] ])
        val = features['GRNN_0/backward_edges'].as_matrix().todense()
        np.testing.assert_array_almost_equal( val, exp )

        exp = ragged([ [0,0,0,0,0,0], [0,0,0,0,0,0,0,0,0], [0,0,0,0,1,1,0,0,0,0,0], [0,0,0,0,0,0,0], [0,0] ])
        val = features['GRNN_0/backward_ports'].as_matrix().todense()
        np.testing.assert_array_almost_equal( val, exp )

        exp = ragged([ [0,0,0,1,1,1], [0,0,1,2,2,3,4,5,5], [0,0,1,2,2,3,4,5,6,7,8], [0,0,1,2,3,4,5], [0,1] ])
        val = features['GRNN_0/backward_reduce'].as_matrix().todense()
        np.testing.assert_array_almost_equal( val, exp )

    def test_graph_scheduler_with_fragments(self):
        pymol = pybel.readstring('smi', 'CCNCNC[O-].[Na+]')
        pymol.removeh()
        mol = tflon.chem.pymol_to_json(pymol, addhs=False)

        molecules = pd.DataFrame(data=[[mol]], columns=['Structures'], index=[0])
        batch = {'molecules': tflon.chem.MoleculeTable(molecules).deserialize()}
        selector = tflon.graph.center.select_min
        scorer = tflon.graph.center.eccentricity_scorer
        rooter = lambda g: g.centroids(scorer=scorer, selector=selector)
        W = tflon.graph.GRNN(self._graph_table, rooter=rooter)
        W.initialize()

        features = W.featurizer(batch)

        exp = np.array([[1,1,1,0,1,1,1,0],
                        [0,1,1,2,1,1,0,0]], dtype=np.float32).T
        val = features['GRNN_0/degrees'].as_matrix()
        np.testing.assert_array_almost_equal( val, exp )


        # CCNCNC[O-].[Na+]
        exp = np.vstack([hot([4,8], 8),
                         hot([3,5], 8),
                         hot([2,6], 8),
                         hot([1,7], 8)]).T
        val = features['GRNN_0/blocks'].as_matrix()
        np.testing.assert_array_almost_equal( val, exp )

        exp = ragged([ [4,8], [3,5], [2,6], [1,7] ])
        val = features['GRNN_0/forward_nodes'].as_matrix().todense()
        np.testing.assert_array_almost_equal( val, exp )

        exp = ragged([ [0,0], [4,4], [3,5], [2,6] ])
        val = features['GRNN_0/forward_edges'].as_matrix().todense()
        np.testing.assert_array_almost_equal( val, exp )

        exp = ragged([ [0,0], [0,0], [0,0], [0,0] ])
        val = features['GRNN_0/forward_ports'].as_matrix().todense()
        np.testing.assert_array_almost_equal( val, exp )

        exp = ragged([ [0,1], [0,1], [0,1], [0,1] ])
        val = features['GRNN_0/forward_reduce'].as_matrix().todense()
        np.testing.assert_array_almost_equal( val, exp )

        exp = ragged([ [4,4,8], [3,5], [2,6], [1,7] ])
        val = features['GRNN_0/backward_nodes'].as_matrix().todense()
        np.testing.assert_array_almost_equal( val, exp )

        exp = ragged([ [3,5,0], [2,6], [1,7], [0,0] ])
        val = features['GRNN_0/backward_edges'].as_matrix().todense()
        np.testing.assert_array_almost_equal( val, exp )

        exp = ragged([ [0,0,0], [0,0], [0,0], [0,0] ])
        val = features['GRNN_0/backward_ports'].as_matrix().todense()
        np.testing.assert_array_almost_equal( val, exp )

        exp = ragged([ [0,0,1], [0,1], [0,1], [0,1] ])
        val = features['GRNN_0/backward_reduce'].as_matrix().todense()
        np.testing.assert_array_almost_equal( val, exp )

    def test_grnn_cell_performs_forward_backward_pass(self):
        m2 = tflon.chem.Molecule.from_json(tflon.chem.pymol_to_json(pybel.readstring('smi', 'c1ccccc1C(=O)OC'), addhs=False))
        molecules = pd.DataFrame(data=[[self.mol], [m2]], columns=['Structures'], index=[0, 1])
        batch = {'molecules': tflon.data.Table(molecules)}

        recursion = tflon.graph.GRNN(self._graph_table, rooter=lambda g: [8] if len(g.nodes)==15 else [6])
        forward_cell = tflon.graph.MiniGRUCell(10)
        backward_cell = tflon.graph.MiniGRUCell(10)
        pl = tf.placeholder(name='atoms', dtype=tf.float32, shape=[None, 10])
        result = recursion(pl, forward_cell, backward_cell)

        with tf.Session() as S:
            S.run(tf.variables_initializer(self._test_tower.variables))
            features = {k:tflon.data.convert_to_feedable(v) for k,v in recursion.featurizer(batch).items()}
            fd = self._test_tower.feed(features)
            node_properties = np.random.random((25,10))
            fd[pl] = node_properties

            output = S.run(result, feed_dict=fd)
            self.assertEqual((25,10), output.shape)

class ToolkitCoordinateTests(TflonOpTestCase):
    def setUp(self):
        TflonOpTestCase.setUp(self)

        sdf_fn = resource_filename('tflon_test.data', 'molecules.sdf')
        pymols = list(pybel.readfile('sdf', sdf_fn))
        molecules = tflon.chem.sdf_to_parquet(sdf_fn, coordinates=True)

        self._batch = {'molecules': tflon.chem.MoleculeTable(molecules).deserialize()}
        self._molecules = self._batch['molecules'].data.iloc[:, 0].tolist()
        self._graph_table = self._test_tower.add_table('molecules', tflon.chem.MoleculeTable)

    def test_bond_length_featurize(self):
        BL = tflon.chem.BondLength(self._graph_table)
        op = BL()

        update = BL.featurizer(self._batch)
        bond_length_table = update['BondLength_0/bond_lengths']

        self.assertEqual(bond_length_table.data.shape, self._batch['molecules'].data.shape)
        self.assertEqual(bond_length_table.shape, (15+7,1))

        arr1 = [1.5143047, 1.09502564, 1.0945684, 1.09503316, 1.7766852, 1.09361, 1.0936126]
        np.testing.assert_array_almost_equal(bond_length_table.data.iloc[1].values[0], arr1, decimal=5)

    def test_bond_length_op(self):
        BL = tflon.chem.BondLength(self._graph_table)
        op = BL()
        with tf.Session() as S:
            batch = self._test_tower.featurizer(self._batch)
            feed_dict = self._test_tower.feed({k:tflon.data.convert_to_feedable(v) for k,v in batch.items()})
            values = S.run(op, feed_dict=feed_dict)

        self.assertEqual(len(values), 22)
        arr1 = [1.5143047, 1.09502564, 1.0945684, 1.09503316, 1.7766852, 1.09361, 1.0936126]
        np.testing.assert_array_almost_equal(np.array([i[0] for i in values]).flatten()[15:], arr1, decimal=5)

    def test_molecule_neighbor(self):
        pm1 = pybel.readstring('smi', 'C.C.C.C')
        for i in range(4):
            pm1.atoms[i].OBAtom.SetVector(0,0,i)
        json1 = tflon.chem.pymol_to_json(pm1, coordinates=True)
        mol1 = tflon.chem.Molecule.from_json(json1)

        pm2 = pybel.readstring('smi', 'C.C.C.C')
        for i in range(4):
            pm2.atoms[i].OBAtom.SetVector(i//2,i%2,0)
        json2 = tflon.chem.pymol_to_json(pm2, coordinates=True)
        mol2 = tflon.chem.Molecule.from_json(json2)

        neighbor_edges_mol1_r1 = [(1,2), (2,3), (3,4)]
        neighbor_edges_mol1_r2 = [(1,2), (1,3), (2,3), (2,4), (3,4)]
        neighbor_edges_mol1_r3 = [(1,2), (1,3), (1,4), (2,3), (2,4), (3,4)]

        neighbor_edges_mol2_r1 = [(1,2), (1,3), (2,4), (3,4)]
        neighbor_edges_mol2_r2 = [(1,2), (1,3), (1,4), (2,3), (2,4), (3,4)]

        np.testing.assert_array_equal(neighbor_edges_mol1_r1, mol1.neighbor_graph(1).edges)
        np.testing.assert_array_equal(neighbor_edges_mol1_r2, mol1.neighbor_graph(2).edges)
        np.testing.assert_array_equal(neighbor_edges_mol1_r3, mol1.neighbor_graph(3).edges)

        np.testing.assert_array_equal(neighbor_edges_mol2_r1, mol2.neighbor_graph(1).edges)
        np.testing.assert_array_equal(neighbor_edges_mol2_r2, mol2.neighbor_graph(2).edges)

    def test_molecule_mst(self):
        pm1 = pybel.readstring('smi', 'C.C.C.C')
        for i in range(4):
            pm1.atoms[i].OBAtom.SetVector(0,0,i)
        json1 = tflon.chem.pymol_to_json(pm1, coordinates=True)
        mol1 = tflon.chem.Molecule.from_json(json1)

        pm2 = pybel.readstring('smi', 'Oc1c(c(CO)cnc1C)CN')
        pm2.make3D()
        pm2.removeh()
        json2 = tflon.chem.pymol_to_json(pm2, coordinates=True)
        mol2 = tflon.chem.Molecule.from_json(json2)

        neighbor_edges_mol1_r1 = [(1,2), (2,3), (3,4)]
        neighbor_edges_mol1_r2 = [(1,2), (1,3), (2,3), (2,4), (3,4)]
        neighbor_edges_mol1_r3 = [(1,2), (1,3), (1,4), (2,3), (2,4), (3,4)]

        mst_edges_mol2 = [(1, 2), (2, 3), (2, 9), (3, 11), (4, 5), (4, 7), (5, 6), (7, 8), (8, 9), (9, 10), (11, 12)]
        neighbor_edges_mol2_r1 = [(1, 2), (2, 3), (2, 9), (3, 11), (4, 5), (4, 7), (5, 6), (7, 8), (8, 9), (9, 10), (11, 12)]
        neighbor_edges_mol2_r2 = [(1, 2), (2, 3), (2, 9), (3, 4), (3, 11), (4, 5), (4, 7), (5, 6), (7, 8), (8, 9), (9, 10), (11, 12)]
        neighbor_edges_mol2_r2_5 = [(1, 2), (1, 3), (1, 9), (2, 3), (2, 4), (2, 8), (2, 9), (3, 4), (3, 7), (3,9), (3,11)] +\
            [(4, 5), (4, 6), (4, 7), (4, 8), (5, 6), (7, 8), (7, 9), (8, 9), (8, 10), (9, 10), (11, 12)]

        np.testing.assert_array_equal(neighbor_edges_mol1_r1, mol1.mst_graph(1).edges)
        np.testing.assert_array_equal(neighbor_edges_mol1_r2, mol1.mst_graph(2).edges)
        np.testing.assert_array_equal(neighbor_edges_mol1_r3, mol1.mst_graph(3).edges)

        np.testing.assert_array_equal(neighbor_edges_mol2_r1, mol2.mst_graph(1).edges)
        np.testing.assert_array_equal(neighbor_edges_mol2_r2, mol2.mst_graph(2).edges)
        np.testing.assert_array_equal(neighbor_edges_mol2_r2_5, mol2.mst_graph(2.5).edges)

        np.testing.assert_array_equal(mst_edges_mol2, mol2.mst_graph(2.5)._reference_graph.edges)

    def test_voronoi_tesselate(self):
        coords1 = [(0,0,0), (0,1.5,0), (1,0,0), (1,1,0)]
        coords2 = [(-1,1,2), (1,1,1), (1,-1,1), (-1,-1,1), (-1,1,-1), (1,1,-1), (1,-1,-2), (-1,-1,-1)]

        delaunay_edges1 = {(1,2), (1,3), (1,4), (2,4), (3,4)}
        delaunay_edges2 = {(1,2),(1,3),(1,4),(1,5),(2,3),(2,4),(2,5)}|\
                          {(2,6),(3,4),(3,6),(3,7),(3,8),(4,5)} |\
                          {(4,8),(5,6),(5,7),(5,8),(6,7),(6,8),(7,8)}

        np.testing.assert_array_equal(tflon.chem.voronoi_tesselate(coords1), delaunay_edges1)
        np.testing.assert_array_equal(tflon.chem.voronoi_tesselate(coords2), delaunay_edges2)

    def test_molecule_delaunay(self):
        mol = pybel.readstring('smi', 'C(C)CC')
        coordinates = [(0, 0, 0), (0, 1.5, 0), (1, 0, 0), (1, 1, 0)]
        for i in range(len(mol.atoms)):
            mol.atoms[i].OBAtom.SetVector(*coordinates[i])
        mol1 = tflon.chem.Molecule.from_json( tflon.chem.pymol_to_json(mol, coordinates=True) )
        mol = pybel.readstring('smi', 'C.C.C.C.C.C.C.C')
        coordinates = [(-1, 1, 2), (1, 1, 1), (1, -1, 1), (-1, -1, 1), (-1, 1, -1), (1, 1, -1), (1, -1, -2), (-1, -1, -1)]
        for i in range(len(mol.atoms)):
            mol.atoms[i].OBAtom.SetVector(*coordinates[i])
        mol2 = tflon.chem.Molecule.from_json( tflon.chem.pymol_to_json(mol, coordinates=True) )

        delaunay_edges1 = [(1,2), (1,3), (1,4), (2,4), (3,4)]
        delaunay_edges2 = [(1,2),(1,3),(1,4),(1,5),(2,3),(2,4),(2,5)]+\
                          [(2,6),(3,4),(3,6),(3,7),(3,8),(4,5)] +\
                          [(4,8),(5,6),(5,7),(5,8),(6,7),(6,8),(7,8)]

        np.testing.assert_array_equal(mol1.delaunay_graph().edges, delaunay_edges1)
        np.testing.assert_array_equal(mol2.delaunay_graph().edges, sorted(delaunay_edges2))

    def test_graph_converter_featurize_neighbor(self):
        mol = pybel.readstring('smi', 'C(CC)(CC)(CC)CC')
        coordinates = [(0, 0, 0), (1, 0, 0), (2, 0, 0), (0, 1, 0), (0, 2, 0), (-1, 0, 0), (-2, 0, 0), (0, -1, 0), (0, -2, 0)]
        for i in range(len(mol.atoms)):
            mol.atoms[i].OBAtom.SetVector(*coordinates[i])
        mol1 = tflon.chem.Molecule.from_json( tflon.chem.pymol_to_json(mol, coordinates=True) )
        neighbor_edges1 = [(1,2), (1,4), (1,6), (1,8), (2,3), (2,4), (2,8), (4,5), (4,6), (6,7), (6,8), (8,9)]

        batch = {'molecules': tflon.data.tables.Table(pd.DataFrame(np.array([mol1])))}
        graph_conversion = tflon.chem.GraphConverter(self._graph_table, gtype='neighbor', radius=1.42)
        graphs, opout = graph_conversion()

        update = graph_conversion.featurizer(batch)
        graph_table = update[graphs]
        bond_gather = update['GraphConverter_0/bond_gather']

        self.assertEqual(graph_table.shape, (1,1))
        self.assertEqual(graph_table.data.shape, (1,1))
        self.assertEqual(len(graph_table.data.iloc[0].values[0].nodes.ids), 9)
        self.assertEqual(len(graph_table.data.iloc[0].values[0].nodes.coordinates), 9)

        np.testing.assert_array_equal(graph_table.data.iloc[0].values[0].nodes.coordinates, mol1.nodes.coordinates)
        np.testing.assert_array_equal(graph_table.data.iloc[0].values[0].nodes.ids, mol1.nodes.ids)
        np.testing.assert_array_equal(graph_table.data.iloc[0].values[0].edges, neighbor_edges1)

        self.assertEqual(bond_gather.shape, (len(neighbor_edges1),1))
        self.assertEqual(bond_gather.data.shape, (1,2))

        bond_gather_targ1 = [0,1,2,3,4,-1,-1,5,-1,6,-1,7]
        np.testing.assert_array_equal(bond_gather.data.iloc[0].values[0], bond_gather_targ1)

    def test_graph_converter_featurize_delaunay(self):
        mol = pybel.readstring('smi', 'C(C)CC')
        coordinates = [(0, 0, 0), (0, 1.5, 0), (1, 0, 0), (1, 1, 0)]
        for i in range(len(mol.atoms)):
            mol.atoms[i].OBAtom.SetVector(*coordinates[i])
        mol1 = tflon.chem.Molecule.from_json( tflon.chem.pymol_to_json(mol, coordinates=True) )

        mol = pybel.readstring('smi', 'CC1CC1.CC.CC')
        coordinates = [(-1, 1, 2), (1, 1, 1), (1, -1, 1), (-1, -1, 1), (-1, 1, -1), (1, 1, -1), (1, -1, -2), (-1, -1, -1)]
        for i in range(len(mol.atoms)):
            mol.atoms[i].OBAtom.SetVector(*coordinates[i])
        mol2 = tflon.chem.Molecule.from_json( tflon.chem.pymol_to_json(mol, coordinates=True) )

        mol = pybel.readstring('smi', 'CCC')
        coordinates = [(-1, 1, 2), (1, 1, 1), (1, -1, 1)]
        for i in range(len(mol.atoms)):
            mol.atoms[i].OBAtom.SetVector(*coordinates[i])
        mol3 = tflon.chem.Molecule.from_json( tflon.chem.pymol_to_json(mol, coordinates=True) )

        mol = pybel.readstring('smi', 'C1CCCCC1')
        coordinates = [(0, 0, 0), (0, -1, 1), (1, -2, 1), (2, -2, 0), (2, -1, -1), (1, 0, -1)]
        for i in range(len(mol.atoms)):
            mol.atoms[i].OBAtom.SetVector(*coordinates[i])
        mol4 = tflon.chem.Molecule.from_json( tflon.chem.pymol_to_json(mol, coordinates=True) )

        mol = pybel.readstring('smi', 'CCC')
        coordinates = [(0, 0, 0), (1, 0, 0), (2, 0, 0)]
        for i in range(len(mol.atoms)):
            mol.atoms[i].OBAtom.SetVector(*coordinates[i])
        mol5 = tflon.chem.Molecule.from_json( tflon.chem.pymol_to_json(mol, coordinates=True) )

        batch = {'molecules': tflon.data.tables.Table(pd.DataFrame(np.array([mol1, mol2, mol3, mol4, mol5])))}

        delaunay_edges1 = [(1,2), (1,3), (1,4), (2,4), (3,4)]
        delaunay_edges2 = [(1,2),(1,3),(1,4),(1,5),(2,3),(2,4),(2,5)]+\
                          [(2,6),(3,4),(3,6),(3,7),(3,8),(4,5)] +\
                          [(4,8),(5,6),(5,7),(5,8),(6,7),(6,8),(7,8)]
        delaunay_edges3 = [(1,2), (1,3), (2,3)]
        delaunay_edges4 = [(1,2),(1,6),(2,3),(3,4),(4,5),(5,6)]
        delaunay_edges5 = [(1,2), (2,3)]

        graph_conversion = tflon.chem.GraphConverter(self._graph_table, gtype='delaunay')
        graphs, opout = graph_conversion()

        update = graph_conversion.featurizer(batch)
        graph_table = update[graphs]
        bond_gather = update['GraphConverter_0/bond_gather']

        self.assertEqual(graph_table.shape, (5,1))
        self.assertEqual(graph_table.data.shape, (5,1))

        self.assertEqual(len(graph_table.data.iloc[0].values[0].nodes.ids), 4)
        self.assertEqual(len(graph_table.data.iloc[0].values[0].nodes.coordinates), 4)
        self.assertEqual(len(graph_table.data.iloc[1].values[0].nodes.ids), 8)
        self.assertEqual(len(graph_table.data.iloc[1].values[0].nodes.coordinates), 8)

        np.testing.assert_array_equal(graph_table.data.iloc[0].values[0].nodes.coordinates, mol1.nodes.coordinates)
        np.testing.assert_array_equal(graph_table.data.iloc[0].values[0].nodes.ids, mol1.nodes.ids)
        np.testing.assert_array_equal(graph_table.data.iloc[1].values[0].nodes.coordinates, mol2.nodes.coordinates)
        np.testing.assert_array_equal(graph_table.data.iloc[1].values[0].nodes.ids, mol2.nodes.ids)

        np.testing.assert_array_equal(graph_table.data.iloc[0].values[0].edges, delaunay_edges1)
        np.testing.assert_array_equal(graph_table.data.iloc[1].values[0].edges, delaunay_edges2)
        np.testing.assert_array_equal(graph_table.data.iloc[2].values[0].edges, delaunay_edges3)
        np.testing.assert_array_equal(graph_table.data.iloc[3].values[0].edges, delaunay_edges4)
        np.testing.assert_array_equal(graph_table.data.iloc[4].values[0].edges, delaunay_edges5)

        self.assertEqual(bond_gather.data.shape, (5,2))
        self.assertEqual(bond_gather.shape, (len(delaunay_edges1) + len(delaunay_edges2) +\
                                              len(delaunay_edges3) + len(delaunay_edges4) +\
                                              len(delaunay_edges5), 1))

        bond_gather_targ1 = [0,1,-1,-1,2]
        bond_gather_targ2 = [0,-1,-1,-1,1,2,-1,-1,3,-1,-1,-1,-1,-1,4,-1,-1,-1,-1,5]
        bond_gather_targ3 = [0,-1,1]
        bond_gather_targ4 = [0,1,2,3,4,5]
        bond_gather_targ5 = [0,1]
        np.testing.assert_array_equal(bond_gather.data.iloc[0].values[0], bond_gather_targ1)
        np.testing.assert_array_equal(bond_gather.data.iloc[1].values[0], bond_gather_targ2)
        np.testing.assert_array_equal(bond_gather.data.iloc[2].values[0], bond_gather_targ3)
        np.testing.assert_array_equal(bond_gather.data.iloc[3].values[0], bond_gather_targ4)
        np.testing.assert_array_equal(bond_gather.data.iloc[4].values[0], bond_gather_targ5)

    def test_graph_converter_featurize_mst(self):
        mol = pybel.readstring('smi', 'C(CC)(CC)(CC)CC')
        coordinates = [(0, 0, 0), (1, 0, 0), (2, 0, 0), (0, 1, 0), (0, 2, 0), (-1, 0, 0), (-2, 0, 0), (0, -1, 0), (0, -2, 0)]
        for i in range(len(mol.atoms)):
            mol.atoms[i].OBAtom.SetVector(*coordinates[i])
        mol1 = tflon.chem.Molecule.from_json( tflon.chem.pymol_to_json(mol, coordinates=True) )
        neighbor_edges1 = [(1,2), (1,4), (1,6), (1,8), (2,3), (2,4), (2,8), (4,5), (4,6), (6,7), (6,8), (8,9)]

        batch = {'molecules': tflon.data.tables.Table(pd.DataFrame(np.array([mol1])))}
        graph_conversion = tflon.chem.GraphConverter(self._graph_table, gtype='mst', radius=1.42)
        graphs, opout = graph_conversion()

        update = graph_conversion.featurizer(batch)
        graph_table = update[graphs]
        bond_gather = update['GraphConverter_0/bond_gather']

        self.assertEqual(graph_table.shape, (1,1))
        self.assertEqual(graph_table.data.shape, (1,1))
        self.assertEqual(len(graph_table.data.iloc[0].values[0].nodes.ids), 9)
        self.assertEqual(len(graph_table.data.iloc[0].values[0].nodes.coordinates), 9)

        np.testing.assert_array_equal(graph_table.data.iloc[0].values[0].nodes.coordinates, mol1.nodes.coordinates)
        np.testing.assert_array_equal(graph_table.data.iloc[0].values[0].nodes.ids, mol1.nodes.ids)
        np.testing.assert_array_equal(graph_table.data.iloc[0].values[0].edges, neighbor_edges1)

        self.assertEqual(bond_gather.shape, (len(neighbor_edges1),1))
        self.assertEqual(bond_gather.data.shape, (1,2))

        bond_gather_targ1 = [0,1,2,3,4,-1,-1,5,-1,6,-1,7]
        np.testing.assert_array_equal(bond_gather.data.iloc[0].values[0], bond_gather_targ1)

    def test_graph_converter_op_neighbor(self):
        neighbor_edges1 = [(1,2), (1,4), (1,6), (1,8), (2,3), (2,4), (2,8), (4,5), (4,6), (6,7), (6,8), (8,9)]
        mol = pybel.readstring('smi', 'C(CC)(CC)(CC)CC')
        coordinates = [(0, 0, 0), (1, 0, 0), (2, 0, 0), (0, 1, 0), (0, 2, 0), (-1, 0, 0), (-2, 0, 0), (0, -1, 0), (0, -2, 0)]
        for i in range(len(mol.atoms)):
            mol.atoms[i].OBAtom.SetVector(*coordinates[i])
        mol1 = tflon.chem.Molecule.from_json( tflon.chem.pymol_to_json(mol, coordinates=True) )

        batch = {'molecules': tflon.data.tables.Table(pd.DataFrame(np.array([mol1])))}

        graph_conversion = tflon.chem.GraphConverter(self._graph_table, gtype='neighbor', radius=1.42)
        graphs, opout = graph_conversion()

        with tf.Session() as S:
            batch = self._test_tower.featurizer(batch)
            feed_dict = self._test_tower.feed({k:tflon.data.convert_to_feedable(v) for k,v in batch.items()})
            values = S.run(opout, feed_dict=feed_dict)

        is_bond_mol1 = np.array([[1,1,1,1,1,0,0,1,0,1,0,1]], dtype=float).T

        self.assertEqual(len(values), 12)
        np.testing.assert_array_equal(values, is_bond_mol1)

    def test_graph_converter_op_delaunay(self):
        mol = pybel.readstring('smi', 'C(C)CC')
        coordinates = [(0, 0, 0), (0, 1.5, 0), (1, 0, 0), (1, 1, 0)]
        for i in range(len(mol.atoms)):
            mol.atoms[i].OBAtom.SetVector(*coordinates[i])
        mol1 = tflon.chem.Molecule.from_json( tflon.chem.pymol_to_json(mol, coordinates=True) )

        mol = pybel.readstring('smi', 'CC1CC1.CC.CC')
        coordinates = [(-1, 1, 2), (1, 1, 1), (1, -1, 1), (-1, -1, 1), (-1, 1, -1), (1, 1, -1), (1, -1, -2), (-1, -1, -1)]
        for i in range(len(mol.atoms)):
            mol.atoms[i].OBAtom.SetVector(*coordinates[i])
        mol2 = tflon.chem.Molecule.from_json( tflon.chem.pymol_to_json(mol, coordinates=True) )

        batch = {'molecules': tflon.data.tables.Table(pd.DataFrame(np.array([mol1, mol2])))}

        graph_conversion = tflon.chem.GraphConverter(self._graph_table, gtype='delaunay')
        graphs, opout = graph_conversion()

        with tf.Session() as S:
            batch = self._test_tower.featurizer(batch)
            feed_dict = self._test_tower.feed({k:tflon.data.convert_to_feedable(v) for k,v in batch.items()})
            values = S.run(opout, feed_dict=feed_dict)

        is_bond_targ1 = np.array([[1,1,0,0,1]]).T
        is_bond_targ2 = np.array([[1,0,0,0,1,1,0,0,1,0,0,0,0,0,1,0,0,0,0,1]]).T
        np.testing.assert_array_equal(values[:5], is_bond_targ1)
        np.testing.assert_array_equal(values[5:], is_bond_targ2)

    def test_graph_converter_op_mst(self):
        neighbor_edges1 = [(1,2), (1,4), (1,6), (1,8), (2,3), (2,4), (2,8), (4,5), (4,6), (6,7), (6,8), (8,9)]
        mol = pybel.readstring('smi', 'C(CC)(CC)(CC)CC')
        coordinates = [(0, 0, 0), (1, 0, 0), (2, 0, 0), (0, 1, 0), (0, 2, 0), (-1, 0, 0), (-2, 0, 0), (0, -1, 0), (0, -2, 0)]
        for i in range(len(mol.atoms)):
            mol.atoms[i].OBAtom.SetVector(*coordinates[i])
        mol1 = tflon.chem.Molecule.from_json( tflon.chem.pymol_to_json(mol, coordinates=True) )

        batch = {'molecules': tflon.data.tables.Table(pd.DataFrame(np.array([mol1])))}

        graph_conversion = tflon.chem.GraphConverter(self._graph_table, gtype='mst', radius=1.42)
        graphs, opout = graph_conversion()

        with tf.Session() as S:
            batch = self._test_tower.featurizer(batch)
            feed_dict = self._test_tower.feed({k:tflon.data.convert_to_feedable(v) for k,v in batch.items()})
            values = S.run(opout, feed_dict=feed_dict)

        is_bond_mol1 = np.array([[1,1,1,1,1,0,0,1,0,1,0,1]], dtype=float).T

        self.assertEqual(len(values), 12)
        np.testing.assert_array_equal(values, is_bond_mol1)

    def test_graph_converter_op_edge_props(self):
        pl_atom = tf.placeholder(name='atoms', dtype=tf.float32, shape=[None, 4])
        pl_bond = tf.placeholder(name='bonds', dtype=tf.float32, shape=[None, 7])

        mol = pybel.readstring('smi', 'C(C)CC')
        coordinates = [(0, 0, 0), (0, 1.5, 0), (1, 0, 0), (1, 1, 0)]
        for i in range(len(mol.atoms)):
            mol.atoms[i].OBAtom.SetVector(*coordinates[i])
        mol1 = tflon.chem.Molecule.from_json( tflon.chem.pymol_to_json(mol, coordinates=True) )

        batch1 = {'molecules': tflon.data.Table(pd.DataFrame(np.array([mol1])))}

        graph_conversion = tflon.chem.GraphConverter(self._graph_table, gtype='delaunay')
        graphs, opout = graph_conversion(pl_bond)

        with tf.Session() as S:
            batch1 = self._test_tower.featurizer(batch1)
            fd1 = self._test_tower.feed({k:tflon.data.convert_to_feedable(v) for k,v in batch1.items()})
            N1 = np.random.random(size=(sum(len(M.nodes) for ix, M in batch1['molecules'].data.iloc[:,0].iteritems()), 4))
            E1 = np.random.random(size=(sum(len(M.edges) for ix, M in batch1['molecules'].data.iloc[:,0].iteritems()), 7))
            fd1[pl_atom] = N1
            fd1[pl_bond] = E1

            values = S.run(opout, feed_dict=fd1)

        bond_indices1 = values[:,:][values[:,0]>0]

        self.assertEqual(len(bond_indices1), len(mol1.edges.ids))
        self.assertEqual(len(values[0,:]), 7+1)
        np.testing.assert_array_equal(values[:,0], np.array([[1,1,0,0,1]]).reshape(-1))
        np.testing.assert_array_almost_equal(bond_indices1[:,1:], E1)

    def test_graph_converter_preserve_bonds(self):
#        D = {
#            'nodes': [(1,(6,0,(0,0))), (2,(6,0,(1,0))), (3,(6,0,(2,0))),
#                      (4,(6,0,(0,1))), (5,(6,0,(0,2))), (6,(6,0,(-1,0))),
#                      (7,(6,0,(-2,0))), (8,(6,0,(0,-1))), (9,(6,0,(0,-2))),
#                      (10,(6,0,(0,5)))],
#            'edges': [(1,2,(1,)), (1,4,(1,)), (1,6,(1,)), (1,8,(1,)), (2,3,(1,)),
#                      (4,5,(1,)), (5,10,(1,)), (6,7,(1,)), (8,9,(1,))],
#            'nprops': ['atomicnum', 'formalcharge', 'coordinates'],
#            'eprops': ['order']
#        }
#        mol1 = tflon.chem.Molecule.from_json(json.dumps(D))
        # 1, 2, 3, 4, 5, 10, 6, 7, 8, 9
        mol = pybel.readstring('smi', 'C(CC)(CCC)(CC)CC')
        coordinates = [(0, 0, 0), (1, 0, 0), (2, 0, 0),
                       (0, 1, 0), (0, 2, 0), (0, 5, 0), (-1, 0, 0),
                       (-2, 0, 0), (0, -1, 0), (0, -2, 0)]
        for i in range(len(mol.atoms)):
            mol.atoms[i].OBAtom.SetVector(*coordinates[i])
        mol1 = tflon.chem.Molecule.from_json( tflon.chem.pymol_to_json(mol, coordinates=True) )

        neighbor_edges1 = [(1,2), (1,4), (1,7), (1,9), (2,3), (2,4), (2,9), (4,5), (4,7), (5,6), (7,8), (7,9), (9,10)]

        batch = {'molecules': tflon.data.Table(pd.DataFrame(np.array([mol1])))}
        graph_conversion = tflon.chem.GraphConverter(self._graph_table, gtype='neighbor', radius=1.42, preserve_bonds=True)
        graphs, opout = graph_conversion()

        update = graph_conversion.featurizer(batch)
        graph_table = update[graphs]
        bond_gather = update['GraphConverter_0/bond_gather']

        self.assertEqual(graph_table.shape, (1,1))
        self.assertEqual(graph_table.data.shape, (1,1))
        self.assertEqual(len(graph_table.data.iloc[0].values[0].nodes.ids), 10)
        self.assertEqual(len(graph_table.data.iloc[0].values[0].nodes.coordinates), 10)

        np.testing.assert_array_equal(graph_table.data.iloc[0].values[0].nodes.coordinates, mol1.nodes.coordinates)
        np.testing.assert_array_equal(graph_table.data.iloc[0].values[0].nodes.ids, mol1.nodes.ids)
        np.testing.assert_array_equal(graph_table.data.iloc[0].values[0].edges, neighbor_edges1)

        self.assertEqual(bond_gather.shape, (len(neighbor_edges1),1))
        self.assertEqual(bond_gather.data.shape, (1,2))

        bond_gather_targ1 = [0,1,2,3,4,-1,-1,5,-1,6,7,-1,8]
        np.testing.assert_array_equal(bond_gather.data.iloc[0].values[0], bond_gather_targ1)
