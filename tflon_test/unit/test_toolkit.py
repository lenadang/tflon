from builtins import range
import numpy as np
import scipy as sp
import pandas as pd
import tensorflow as tf
import tflon
from sklearn.metrics import roc_auc_score
from tflon_test.utils import random_tensor, TflonOpTestCase

@tflon.toolkit.apply_reduction(argnum=1)
@tflon.toolkit.handle_nan(inarg=1, outarg=1)
def loss_w_nans(X, T):
    output = tf.nn.sigmoid(X)
    loss = - T*tf.log(output) - (1-T)*tf.log(1-output)
    return output, loss

class ToolkitTests( TflonOpTestCase ):
    def test_override_priors(self):
        gp = tflon.toolkit.GammaPrior(k=2, theta=1)
        up = tflon.toolkit.GaussianPrior(scale=0.1)
        _pl = tf.placeholder(shape=[None,5], dtype=tf.float32)
        pos_weights = tflon.toolkit.Dense(23, weight_prior=gp)
        gauss_bias = tflon.toolkit.Dense(27, bias_prior=up, bias_initializer=tf.random_normal_initializer(0, 0.1))
        net = pos_weights | gauss_bias
        net(_pl)

        with tf.Session() as S:
            S.run(tf.variables_initializer(self._test_tower.variables))

            w1,b1=S.run(pos_weights._Ws + [pos_weights._b])
            self.assertTrue(np.all(w1>0))
            self.assertTrue(np.all(b1==0.1))

            w2,b2=S.run(gauss_bias._Ws + [gauss_bias._b])
            self.assertTrue(np.any(w2<0))
            self.assertTrue(np.any(b2<0))
            self.assertTrue(np.any(b2>0))

            self.assertEqual(len(self._test_tower.losses['priors']), 2)

            p1loss, p2loss = S.run(self._test_tower.losses['priors'])

            np.testing.assert_almost_equal(p1loss, np.sum(-np.log(w1) + w1), decimal=4)
            np.testing.assert_almost_equal(p2loss, 0.5 * np.sum(np.square(b2))/0.1, decimal=4)

    def test_override_initializers(self):
        pl = tf.placeholder(shape=[None,5], dtype=tf.float32)
        low_weights = tflon.toolkit.Dense(10, weight_initializer=tf.constant_initializer(-400,))
        low_biases = tflon.toolkit.Dense(15, bias_initializer=tf.constant_initializer(1000,))
        low_both = tflon.toolkit.Dense(20, weight_initializer=tf.constant_initializer(-400,), bias_initializer=tf.constant_initializer(1000))
        net = low_weights | low_biases | low_both
        net(pl)

        with tf.Session() as S:
            S.run(tf.variables_initializer(self._test_tower.variables))

            w,b=S.run(low_weights.trainables)
            self.assertTrue(np.all(w==-400))
            self.assertTrue(np.all(b!=1000))
            w,b=S.run(low_biases.trainables)
            self.assertTrue(np.all(w!=-400))
            self.assertTrue(np.all(b==1000))
            w,b=S.run(low_both.trainables)
            self.assertTrue(np.all(w==-400))
            self.assertTrue(np.all(b==1000))

    def test_modules_can_be_chained(self):
        with tf.variable_scope("test_model"):
            net = tflon.toolkit.Dense( 10, activation=tf.nn.relu ) |\
                  tflon.toolkit.Dense( 15, activation=tf.nn.sigmoid )

            x = tf.placeholder(shape=[None, 5], dtype=tf.float32)
            y = net(x)

        with tf.Session() as S:
            init = tf.variables_initializer(tf.get_collection( tf.GraphKeys.LOCAL_VARIABLES, scope="TestModel" ))

            S.run( init )
            data = random_tensor((10, 5))
            y_actual = S.run(y, feed_dict={x: data})

            W1, b1 = S.run( net.modules[0].trainables )
            W2, b2 = S.run( net.modules[1].trainables )

            d1_logit = np.dot(data, W1) + b1
            d1_expected = np.maximum(np.zeros(d1_logit.shape), d1_logit)
            y_expected = 1. / (1. + np.exp(-(np.dot(d1_expected, W2) + b2)))

            np.testing.assert_array_almost_equal(y_expected, y_actual)

    def test_modules_can_be_joined(self):
        with tf.variable_scope("test_model"):
            net = tflon.toolkit.Dense( 10, activation=tf.nn.relu ) &\
                  tflon.toolkit.Dense( 15, activation=tf.nn.sigmoid ) &\
                  tflon.toolkit.Dense( 20, activation=tf.tanh )

            x0 = tf.placeholder(shape=[None, 5], dtype=tf.float32)
            x1 = tf.placeholder(shape=[None, 8], dtype=tf.float32)
            x2 = tf.placeholder(shape=[None, 13], dtype=tf.float32)
            y = net(x0, x1, x2)

        with tf.Session() as S:
            init = tf.variables_initializer(tf.get_collection( tf.GraphKeys.LOCAL_VARIABLES, scope="TestModel" ))
            S.run( init )
            x0_data = random_tensor((10, 5))
            x1_data = random_tensor((10, 8))
            x2_data = random_tensor((10, 13))
            y_actual = S.run(y, feed_dict={x0: x0_data, x1: x1_data, x2: x2_data})

            W0, b0 = S.run( net.modules[0].trainables )
            W1, b1 = S.run( net.modules[1].trainables )
            W2, b2 = S.run( net.modules[2].trainables )

            y0_logit = np.dot(x0_data, W0) + b0
            y0_expected = np.maximum(np.zeros(y0_logit.shape), y0_logit)

            y1_logit = np.dot(x1_data, W1) + b1
            y1_expected = 1. / (1. + np.exp(-y1_logit))

            y2_logit = np.dot(x2_data, W2) + b2
            y2_expected = np.tanh(y2_logit)

            np.testing.assert_array_almost_equal(y0_expected, y_actual[0])
            np.testing.assert_array_almost_equal(y1_expected, y_actual[1])
            np.testing.assert_array_almost_equal(y2_expected, y_actual[2])

    def test_modules_can_be_broadcast(self):
        with tf.variable_scope("test_model"):
            net = tflon.toolkit.Dense( 10, activation=tf.nn.relu ) ^\
                  tflon.toolkit.Dense( 15, activation=tf.nn.sigmoid ) ^\
                  tflon.toolkit.Dense( 20, activation=tf.tanh )

            x = tf.placeholder(shape=[None, 5], dtype=tf.float32)
            z = net(x)

        with tf.Session() as S:
            init = tf.variables_initializer(tf.get_collection( tf.GraphKeys.LOCAL_VARIABLES, scope="TestModel" ))
            S.run( init )
            x_data = random_tensor((10, 5))
            z_actual = S.run(z, feed_dict={x: x_data})

            W0, b0 = S.run( net.modules[0].trainables )
            W1, b1 = S.run( net.modules[1].trainables )
            W2, b2 = S.run( net.modules[2].trainables )

            z0_logit = np.dot(x_data, W0) + b0
            z0_expected = np.maximum(np.zeros(z0_logit.shape), z0_logit)

            z1_logit = np.dot(x_data, W1) + b1
            z1_expected = 1. / (1. + np.exp(-z1_logit))

            z2_logit = np.dot(x_data, W2) + b2
            z2_expected = np.tanh(z2_logit)

            np.testing.assert_array_almost_equal(z0_expected, z_actual[0])
            np.testing.assert_array_almost_equal(z1_expected, z_actual[1])
            np.testing.assert_array_almost_equal(z2_expected, z_actual[2])


    def test_nan_handler_handles_arg_placement(self):
        T = np.random.randint(0, 2, (10,4)).astype(np.float64)
        T[np.random.randint(0, T.shape[0], (5,)), np.random.randint(0, T.shape[1], (5,))] = np.nan
        L = np.random.random((10,4)) * 10 - 5
        sL = 1./(1.+np.exp(-L))

        exp_loss = -np.nansum(T*np.log(sL)+(1-T)*np.log(1-sL))
        with tf.Session() as S:
            out, loss = S.run( loss_w_nans(L, T, nans=True) )
        np.testing.assert_almost_equal(out, sL, decimal=5)
        np.testing.assert_almost_equal(loss, exp_loss, decimal=5)

class OpTests( TflonOpTestCase ):
    def test_layer_norm_normalizes_on_axis_1(self):
        A = np.random.random((13,10))
        normalizer = tflon.toolkit.LayerNorm()
        normed = normalizer(tf.constant(A, dtype=tf.float32))
        gv = np.random.random((1,10))
        ov = np.random.random((1,10))

        with tf.Session() as S:
            normalizer.trainables[0].load(gv)
            normalizer.trainables[1].load(ov)
            out = S.run(normed)

            m, s = np.mean(A, axis=1, keepdims=True), np.std(A, axis=1, keepdims=True)
            np.testing.assert_array_almost_equal(gv*(A-m)/s+ov, out)

    def test_layer_norm_handles_zero_variance(self):
        A = np.array([np.random.random((13,))]*10).T
        normalizer = tflon.toolkit.LayerNorm()
        normed = normalizer(tf.constant(A, dtype=tf.float32))
        gv = np.random.random((1,10))
        ov = np.random.random((1,10))

        with tf.Session() as S:
            normalizer.trainables[0].load(gv)
            normalizer.trainables[1].load(ov)
            out = S.run(normed)

            m = np.mean(A, axis=1, keepdims=True)
            exp = gv*(A-m)+ov
            np.testing.assert_array_almost_equal(exp, out)

    def test_pca_preprocessing_missing_values(self):
        A = np.random.multivariate_normal(mean=[1.2, 5.3, -1.3, -3, -5],
                                          cov=[[1, 1, 1, 0,   0],
                                               [1, 1, 1, 0,   0],
                                               [1, 1, 1, 0,   0],
                                               [0, 0, 0, 0.5,-1],
                                               [0, 0, 0, -1,0.7]],
                                          size=100000).astype(np.float32)

        Anan = A.copy()
        for i, j in enumerate(np.random.randint(0,5,size=(100000,))):
            Anan[i,j] = np.nan
        assert np.sum(np.isnan(Anan))==100000

        pca = tflon.toolkit.PCA('A', num_components=2, nans=True)

        projected = pca.project(tf.convert_to_tensor(A))
        inverse_norm = pca(projected)
        inverse = pca.denormalize(inverse_norm)

        tables = {'A': tflon.data.Table(pd.DataFrame(Anan))}
        with tf.Session() as S:
            pca.show_data(tables)

            p, i, m = S.run([projected, inverse, pca.imputed_data], feed_dict={pca.imputed_data: tables['PCA_0/A_imputed'].as_matrix()})

            self.assertEqual(p.shape, (100000, 2))
            self.assertEqual(i.shape, (100000, 5))
            self.assertEqual(np.sum(np.isnan(m)), 0)
            self.assertEqual(m.shape, (100000, 5))

            Ac = A.copy()
            Ac[np.isnan(Anan)]=0
            m[np.isnan(Anan)]=0
            np.testing.assert_array_almost_equal(Ac, m)

            rvals = [sp.stats.pearsonr(A[:,j], i[:,j])[0] for j in range(5)]
            self.assertTrue(rvals[0] > 0.99, "%.3f <= 0.99" % (rvals[0]))
            self.assertTrue(rvals[1] > 0.99, "%.3f <= 0.99" % (rvals[1]))
            self.assertTrue(rvals[2] > 0.99, "%.3f <= 0.99" % (rvals[2]))
            self.assertTrue(rvals[3] > 0.89 and rvals[3]<1., "%.3f <= 0.89" % (rvals[3]))
            self.assertTrue(rvals[4] > 0.89 and rvals[4]<1., "%.3f <= 0.89" % (rvals[4]))

    def test_pca_preprocessing(self):
        A = np.random.multivariate_normal(mean=[1.2, 5.3, -1.3, -3, -5],
                                          cov=[[1, 1, 1, 0,   0],
                                               [1, 1, 1, 0,   0],
                                               [1, 1, 1, 0,   0],
                                               [0, 0, 0, 0.5,-1],
                                               [0, 0, 0, -1,0.7]],
                                          size=100000).astype(np.float32)
        sig = np.random.random((100000,2)).astype(np.float32)
        pca = tflon.toolkit.PCA('A', num_components=2)
        projected = pca.project(tf.convert_to_tensor(A))
        inverse_norm, icorr = pca(projected, tf.convert_to_tensor(sig))

        inverse, isig = pca.denormalize(inverse_norm, icorr)

        with tf.Session() as S:
            pca.show_data({'A': tflon.data.Table(pd.DataFrame(A))})
            p, i, s = S.run([projected, inverse, isig])
            self.assertEqual(p.shape, (100000, 2))
            self.assertEqual(i.shape, (100000, 5))
            self.assertEqual(s.shape, (100000, 5, 5))

            rvals = [sp.stats.pearsonr(A[:,j], i[:,j])[0] for j in range(5)]
            np.testing.assert_array_almost_equal(rvals[0:3], [1,1,1])
            self.assertTrue(rvals[3] > 0.89, "%.3f <= 0.89" % (rvals[3]))
            self.assertTrue(rvals[4] > 0.89, "%.3f <= 0.89" % (rvals[4]))

    def test_broadcast_matmul(self):
        A = np.random.random((6,4))
        B = np.random.random((10,4,7))
        C = np.random.random((7,9))

        left_op = tflon.toolkit.broadcast_matmul(A,B)
        right_op = tflon.toolkit.broadcast_matmul(B,C)

        with tf.Session() as S:
            l,r = S.run([left_op, right_op])

            exp_l = np.vstack([np.dot(A,B[i]).reshape((1,6,7)) for i in range(10)])
            np.testing.assert_array_almost_equal(l, exp_l)

            exp_r = np.vstack([np.dot(B[i],C).reshape((1,4,9)) for i in range(10)])
            np.testing.assert_array_almost_equal(r, exp_r)

    def test_batch_matmul(self):
        A = np.random.random((10,4))
        B = np.random.random((10,4,7))
        C = np.random.random((10,7))

        left_op = tflon.toolkit.batch_matmul(A,B)
        right_op = tflon.toolkit.batch_matmul(B,C)

        with tf.Session() as S:
            l,r = S.run([left_op, right_op])

            exp_l = np.vstack([np.dot(A[i],B[i]).reshape((1,7)) for i in range(10)])
            np.testing.assert_array_almost_equal(l, exp_l)

            exp_r = np.vstack([np.dot(B[i],C[i]).reshape((1,4)) for i in range(10)])
            np.testing.assert_array_almost_equal(r, exp_r)

    def test_concat_product_sum_ops(self):
        pl_data1 = tf.placeholder(shape=[None, 5], dtype=tf.float32)
        pl_data2 = tf.placeholder(shape=[None, 5], dtype=tf.float32)
        pl_data3 = tf.placeholder(shape=[None, 5], dtype=tf.float32)

        o1 = tflon.toolkit.Concat(axis=1)(pl_data1, pl_data2, pl_data3)
        o2 = tflon.toolkit.Sum()(pl_data1, pl_data2, pl_data3)
        o3 = tflon.toolkit.Product()(pl_data1, pl_data2, pl_data3)

        with tf.Session() as S:
            inp1 = np.random.random((10,5))
            inp2 = np.random.random((10,5))
            inp3 = np.random.random((10,5))

            v1,v2,v3 = S.run([o1,o2,o3], feed_dict={pl_data1:inp1, pl_data2:inp2, pl_data3:inp3})

            np.testing.assert_array_almost_equal(v1, np.hstack([inp1,inp2,inp3]))
            np.testing.assert_array_almost_equal(v2, inp1+inp2+inp3)
            np.testing.assert_array_almost_equal(v3, inp1*inp2*inp3)

    def test_batch_duplicate(self):
        pl_data1 = tf.placeholder(shape=[None, 5], dtype=tf.float32)
        pl_data2 = tf.placeholder(shape=[10, None, 8], dtype=tf.float32)
        num_copies = tf.placeholder(shape=[], dtype=tf.int32)
        op1 = tflon.toolkit.batch_duplicate(pl_data1, num_copies)
        op2 = tflon.toolkit.batch_duplicate(pl_data2, num_copies, axis=1)

        D1 = np.random.random((10,5))
        D2 = np.random.random((10,5,8))
        with tf.Session() as S:
            result = S.run(op1, feed_dict={pl_data1:D1, num_copies:4})
            tensors = sum([[D1[i,:]]*4 for i in range(10)], [])
            exp = np.stack(tensors, axis=0)
            np.testing.assert_array_almost_equal(result, exp)

            result = S.run(op2, feed_dict={pl_data2:D2, num_copies:8})
            tensors = sum([[D2[:,i,:]]*8 for i in range(5)], [])
            exp = np.stack(tensors, axis=1)
            np.testing.assert_array_almost_equal(result, exp)


    def test_for_loop_with_dynamic_size(self):
        pl_data = tf.placeholder(shape=[None, 5], dtype=tf.float32)
        pl_iters = tf.placeholder(shape=[], dtype=tf.int32)

        floop = tflon.toolkit.ForLoop(lambda i, x: 2*x, pl_iters)
        out = floop(pl_data)

        with tf.Session() as S:
            data = np.random.random((10,5))

            for i in range(10):
                val = S.run(out, feed_dict={pl_data:data, pl_iters:i})
                exp = 2**i*data
                np.testing.assert_almost_equal(val, exp, decimal=3)

    def test_for_loop_save_intermediates(self):
        pl_x = tf.placeholder(shape=[None, 5], dtype=tf.float32)
        pl_y = tf.placeholder(shape=[None, 10], dtype=tf.float32)
        pl_iters = tf.placeholder(shape=[], dtype=tf.int32)

        floop = tflon.toolkit.ForLoop(lambda i, x, y: (2*x, 3*y), pl_iters, collect_outputs=True)
        collected = floop(pl_x, pl_y)

        with tf.Session() as S:
            xdata = np.random.random((1,5))
            ydata = np.random.random((1,10))
            xval_collected, yval_collected = S.run([ta.stack() for ta in collected], feed_dict={pl_x:xdata, pl_y:ydata, pl_iters:10})

            exp_xout = xdata * (2 ** 10)
            np.testing.assert_array_almost_equal(xval_collected[-1], exp_xout, decimal=1)
            exp_xc = np.array([(2**(i+1))*xdata for i in range(10)])
            np.testing.assert_array_almost_equal(xval_collected, exp_xc, decimal=1)


            exp_yout = ydata * (3 ** 10)
            np.testing.assert_array_almost_equal(yval_collected[-1], exp_yout, decimal=1)
            exp_yc = np.array([(3**(i+1))*ydata for i in range(10)])
            np.testing.assert_array_almost_equal(yval_collected, exp_yc, decimal=1)

    def test_nan_filter(self):
        batch_size = 100
        width = 10
        groups = [2,5,3]
        data = np.random.random((batch_size,width)).astype(np.float32)
        for i in range(batch_size):
            data[np.random.randint(0,width)] = np.nan

        pl = tf.placeholder(shape=[None, width], dtype=tf.float32)
        filtered = tflon.toolkit.nan_filter(pl)
        grouped = tflon.toolkit.nan_filter(pl, partitions=groups)

        with tf.Session() as S:
            result = S.run(filtered, feed_dict={pl:data})
            expected = np.zeros((batch_size,2*width))
            expected[:,0:width] = data
            expected[np.isnan(expected)]=0
            expected[:,width:2*width] = np.isnan(data)
            np.testing.assert_array_equal(result, expected)

            result = S.run(grouped, feed_dict={pl:data})
            expected = np.zeros((batch_size,width+len(groups)))
            expected[:,0:2] = data[:,0:2]
            expected[np.sum(np.isnan(data[:,0:2]), axis=1)>0,2] = 1
            expected[:,3:8] = data[:,2:7]
            expected[np.sum(np.isnan(data[:,2:7]), axis=1)>0,8] = 1
            expected[:,9:12] = data[:,7:10]
            expected[np.sum(np.isnan(data[:,7:10]), axis=1)>0,12] = 1
            expected[np.isnan(expected)]=0
            np.testing.assert_array_equal(result, expected)

    def test_nan_replace(self):
        batch_size = 100
        width = 10
        data = np.random.random((batch_size, width)).astype(np.float32)
        for i in range(batch_size):
            data[np.random.randint(0, width)] = np.nan

        pl = tf.placeholder(shape=[None, width], dtype=tf.float32)
        filtered = tflon.toolkit.nan_replace(pl)

        with tf.Session() as S:
            result = S.run(filtered, feed_dict={pl:data})
            replace = np.tile(np.nanmean(data, axis=0).reshape((1,-1)), [batch_size,1])
            expected = np.where(np.isnan(data), replace, data)
            np.testing.assert_array_equal(result, expected)

    def test_batch_normalization_train_and_test(self):
        pl = tf.placeholder(shape=[None, 5], dtype=tf.float32)
        bn = tflon.toolkit.batch_normalize(pl, beta_initializer=tf.constant_initializer(0.1), gamma_initializer=tf.random_normal_initializer(0,0.1))

        update_ops = tf.get_collection(tf.GraphKeys.UPDATE_OPS)
        with tf.control_dependencies(update_ops):
            tbn = 1.*bn
        with tf.Session() as S:
            S.run(tf.variables_initializer(self._test_tower.variables))

            self.assertEqual(len([v.name for v in self._test_tower.trainables]), 2)
            self.assertEqual(len([v.name for v in self._test_tower.variables]), 5)
            Ts = [np.random.random((20,5)) for i in range(20)]
            m, s = S.run(self._test_tower.variables[-2:])
            g, b = S.run(self._test_tower.trainables)

            self._test_tower.set_training_phase(True)
            for t in Ts:
                normed = S.run(tbn, feed_dict={pl:t})

                m, s = np.mean(t, axis=0), np.std(t, axis=0)
                exp = (t-m)/s*g+b
                np.testing.assert_array_almost_equal(normed, exp, decimal=1)

            self._test_tower.set_training_phase(False)
            m, s = S.run(self._test_tower.variables[-2:])
            g, b = S.run(self._test_tower.trainables)
            for t in Ts:
                normed = S.run(bn, feed_dict={pl:t})

                exp = (t-m)/s*g+b
                np.testing.assert_array_almost_equal(normed, exp, decimal=1)

    def test_interpolated_sequence_features(self):
        I = tflon.toolkit.InterpolatedSequence(sample_points=[0,0.3,0.4,0.6,0.9,1.0], features=3)
        pl = tf.placeholder(shape=[None], dtype=tf.float32)
        y = I(pl)

        with tf.Session() as S:
            S.run( tf.variables_initializer(self._test_tower.trainables) )
            out = S.run(y, feed_dict={pl: np.arange(0,1,0.001)})
            self.assertEqual(out.shape, (1000,3))

    def test_window_input_calculates_max_min(self):
        with tf.variable_scope("test_mode"):
            data = random_tensor((10, 5))
            desc = tflon.data.Table( pd.DataFrame(data) )
            net = tflon.toolkit.WindowInput()
            x = tf.placeholder(shape=(None, 5), dtype=tf.float32, name='desc')
            y = net(x)

        with tf.Session() as S:
            init = tf.variables_initializer( tf.get_collection( tf.GraphKeys.LOCAL_VARIABLES, scope="test_model" ) )
            S.run( init )

            net.show_data({'desc': desc})

            y_actual = S.run(y, feed_dict={x: data})

            mn = data.min(axis=0)
            rng = data.max(axis=0) - mn
            rng[rng==0]=1
            y_exp = (data - mn) / rng

            np.testing.assert_array_almost_equal(y_actual, y_exp)

    def test_log_normalized_input_calculates_moments(self):
        with tf.variable_scope("test_mode"):
            values = np.exp(random_tensor((10, 5)))
            targets = tflon.data.Table( pd.DataFrame(values) )
            t = tf.placeholder(shape=(None, 5), dtype=tf.float32, name='targets')
            y = tflon.toolkit.LogNormalInput()(t)

        with tf.Session() as S:
            mod = self._test_tower.get_module('LogNormalInput_0')
            mod.show_data({'targets': targets})

            y_actual = S.run(y, feed_dict={t: values})

            mu = np.log(values).mean(axis=0)
            std = np.log(values).std(axis=0)

            mu_val, std_val = S.run([mod._mu, mod._sigma])
            np.testing.assert_array_almost_equal(mu_val, mu)
            np.testing.assert_array_almost_equal(std_val, std)

            y_exp = (np.log(values) - mu) / std
            np.testing.assert_array_almost_equal(y_actual, y_exp)

    def test_normalized_input_calculates_moments(self):
        with tf.variable_scope("test_mode"):
            values = random_tensor((10, 5))
            targets = tflon.data.Table( pd.DataFrame(values) )
            t = tf.placeholder(shape=(None, 5), dtype=tf.float32, name='targets')
            y = tflon.toolkit.NormalizedInput()(t)

        with tf.Session() as S:
            mod = self._test_tower.get_module('NormalizedInput_0')
            mod.show_data({'targets': targets})

            y_actual = S.run(y, feed_dict={t: values})

            mu = values.mean(axis=0)
            std = values.std(axis=0)

            mu_val, std_val = S.run([mod._mu, mod._sigma])
            np.testing.assert_array_almost_equal(mu_val, mu)
            np.testing.assert_array_almost_equal(std_val, std)

            y_exp = (values - mu) / std
            np.testing.assert_array_almost_equal(y_actual, y_exp)

    def test_normalized_input_override_moments(self):
        with tf.variable_scope("test_mode"):
            values = random_tensor((10, 5))
            targets = tflon.data.Table( pd.DataFrame(values) )
            t = tf.placeholder(shape=(None, 5), dtype=tf.float32, name='targets')
            y = tflon.toolkit.NormalizedInput(precomputed_moments=(1.851, 0.360))(t)

        with tf.Session() as S:
            mod = self._test_tower.get_module('NormalizedInput_0')
            mod.show_data({'targets': targets})

            y_actual = S.run(y, feed_dict={t: values})

            mu = 1.851
            std = 0.360

            mu_val, std_val = S.run([mod._mu, mod._sigma])
            np.testing.assert_array_almost_equal(mu_val, mu)
            np.testing.assert_array_almost_equal(std_val, std)

            y_exp = (values - mu) / std
            np.testing.assert_array_almost_equal(y_actual, y_exp)


    def test_empirical_distribution_input_maps_empirical_data(self):
        with tf.variable_scope("test_mode"):
            values = random_tensor((1000, 5))
            targets = tflon.data.Table( pd.DataFrame(values) )
            t = tf.placeholder(shape=(None, 5), dtype=tf.float32, name='targets')
            DI = tflon.toolkit.EmpiricalDistributionInput()
            p = DI(t)
            x = DI.inverse(p)

        with tf.Session() as S:
            DI.show_data({'targets': targets})

            p_val = S.run(p, feed_dict={t: values})
            x_val = S.run(x, feed_dict={t: values})

            self.assertEqual(np.min(p_val), 0)
            self.assertEqual(np.max(p_val), 1)

            np.testing.assert_array_almost_equal(x_val, values)

    def test_segment_reduce_op_correctly_reduces_data(self):
        col_data1 = np.arange(13).tolist()
        col_data2 = (np.arange(13)+13).tolist()
        nest_data1 = [col_data1[:3], col_data1[3:9], col_data1[9:13]]
        nest_data2 = [col_data2[:3], col_data2[3:9], col_data2[9:13]]
        nested_data = pd.DataFrame({'ID':[0,1,2], 'col1': nest_data1, 'col2': nest_data2}).set_index('ID')
        t = tflon.data.DenseNestedTable(nested_data)
        pl = self._test_tower.add_input('inputs', shape=[None,2], dtype=tf.float32)

        seg_reduce = tflon.toolkit.SegmentReduce('inputs', reduction_op=tf.segment_max)
        opout = seg_reduce(pl)

        with tf.Session() as S:
            tables = {'inputs':t}
            tables.update( seg_reduce.featurizer(tables) )
            features = {k:tflon.data.convert_to_feedable(v) for k,v in tables.items()}

            result = S.run(opout, feed_dict=self._test_tower.feed(features))
            self.assertEqual(result.shape, (3, 2))

            exp = np.zeros((3, 2))
            exp[0,:] = np.max(nest_data1[0]), np.max(nest_data2[0])
            exp[1,:] = np.max(nest_data1[1]), np.max(nest_data2[1])
            exp[2,:] = np.max(nest_data1[2]), np.max(nest_data2[2])
            np.testing.assert_array_almost_equal(result, exp)

    def test_segment_topN_returns_correct_elements(self):
        data = np.array( [[ 0.55378792,  0.81284334],     # 0
                          [ 0.45053728,  0.9456117 ],     # 1
                          [ 0.91093151,  0.83546926],     # 0
                          [ 0.20418748,  0.91065169],     # 1
                          [ 0.1321604,   0.93694381],     # 2
                          [ 0.57366073,  0.88713995],     # 1
                          [ 0.97191625,  0.1614452 ],     # 1
                          [ 0.27807395,  0.21342298],     # 3
                          [ 0.51869747,  0.89941429],     # 0
                          [ 0.75234443,  0.87545687]],    # 2
                          dtype=np.float32 )
        segs = np.array([0, 1, 0, 1, 2, 1, 1, 3, 0, 2], dtype=np.int32)

        exp_values = np.array( [[0.91093151, 0.55378792, 0.89941429, 0.83546926 ],
                                [0.97191625, 0.57366073, 0.9456117,  0.91065169 ],
                                [0.75234443, 0.1321604,  0.93694381, 0.87545687 ],
                                [0.27807395, 0,          0.21342298, 0          ]], dtype=np.float32 )
        N = 2

        D = tf.placeholder(shape=[None, 2], dtype=tf.float32)
        S = tf.placeholder(shape=[None], dtype=tf.int32)

        g = tflon.toolkit.segment_topN(D, S, N)

        self.assertEqual([d.value for d in g.get_shape()], [None, 4])

        with tf.Session() as session:
            values = session.run(g, feed_dict={D:data, S:segs})
            np.testing.assert_array_almost_equal( exp_values, values )

    def test_SegmentReduceScatter_return_correct_elements(self):
        data = np.array([[1, 2, 3], #0
                        [8, 3, 9], #0
                        [2, 1, 1], #0
                        [3, 1, 1], #0
                        [2, 3, 1], #0
                        [1, 3, 3], #1
                        [4, 7, 3]], #1
                        dtype=np.float32 )

        segs = np.array([0, 0, 0, 0, 0, 1, 1], dtype=np.int32)

        exp_seg_matrix_sum = np.array([[41.000000],
                                       [41.000000],
                                       [41.000000],
                                       [41.000000],
                                       [41.000000],
                                       [21.000000],
                                       [21.000000]], dtype=np.float32)

        exp_seg_column_sum = np.array([[16.000000, 10.000000, 15.000000],
                                [16.000000, 10.000000, 15.000000],
                                [16.000000, 10.000000, 15.000000],
                                [16.000000, 10.000000, 15.000000],
                                [16.000000, 10.000000, 15.000000],
                                [5.000000, 10.000000, 6.000000],
                                [5.000000, 10.000000, 6.000000]], dtype=np.float32)

        exp_seg_matrix_mean= np.array([
                                        [2.7333333],
                                        [2.7333333],
                                        [2.7333333],
                                        [2.7333333],
                                        [2.7333333],
                                        [3.5000000],
                                        [3.5000000] ], dtype=np.float32)

        exp_seg_column_mean = np.array([[3.2, 2.,  3. ],
                                [3.2, 2.,  3. ],
                                [3.2, 2.,  3. ],
                                [3.2, 2.,  3. ],
                                [3.2, 2.,  3. ],
                                [2.5, 5.,  3. ],
                                [2.5, 5.,  3. ]], dtype=np.float32)

        D = tf.placeholder(shape=[None, 3], dtype=tf.float32)
        S = tf.placeholder(shape=[None], dtype=tf.int32)

        #g = tflon.toolkit.segment_norm_within_columns(D, S)
        seg_matrix_sum =tflon.toolkit.segment_reduce_scatter(D, S)
        seg_column_sum =tflon.toolkit.segment_reduce_scatter(D, S, axis=0)
        seg_matrix_mean =tflon.toolkit.segment_reduce_scatter(D, S, axis=None, reduce_func = 'mean')
        seg_column_mean =tflon.toolkit.segment_reduce_scatter(D, S, axis=0, reduce_func = 'mean')

        self.assertEqual([d.value for d in seg_column_mean.get_shape()], [None, 3])

        with tf.Session() as session:
            seg_matrix_sum_values = session.run(seg_matrix_sum, feed_dict={D:data, S:segs})
            np.testing.assert_array_almost_equal( exp_seg_matrix_sum, seg_matrix_sum_values )
            seg_column_sum_values = session.run(seg_column_sum, feed_dict={D:data, S:segs})
            np.testing.assert_array_almost_equal( exp_seg_column_sum, seg_column_sum_values )

            seg_matrix_mean_values = session.run(seg_matrix_mean, feed_dict={D:data, S:segs})
            np.testing.assert_array_almost_equal( exp_seg_matrix_mean, seg_matrix_mean_values )
            seg_column_mean_values = session.run(seg_column_mean, feed_dict={D:data, S:segs})
            np.testing.assert_array_almost_equal( exp_seg_column_mean, seg_column_mean_values )

    def test_linear_interpolation(self):
        fx = sorted(np.random.random(size=(5,)))
        fx = np.array(fx)
        fy = np.random.random(size=(5,10)) * 10 - 5
        f = sp.interpolate.interp1d( fx, fy, axis=0, bounds_error=False, fill_value=(fy[0], fy[-1]) )

        x = np.random.random(size=(100,))
        y = f(x)

        interp_op = tflon.toolkit.interpolate1d( x, fx, fy )

        with tf.Session() as S:
            out = S.run( interp_op )
            np.testing.assert_array_almost_equal( out, y )

    def test_kernel_interpolation(self):
        fx = np.array([0,0.25,0.5,0.75,1.0])
        fy = np.random.random(size=(5,3)) * 10 - 5

        radius = np.random.random(size=(5,)) * 0.07
        fixed_radius = 0.05

        x = np.array(sorted(np.random.random(size=(25,))))

        pl = tf.placeholder(shape=[None], dtype=tf.float64)
        interp_op_var = tflon.toolkit.interpolate1d( pl, fx, fy, method='rbf', radius=radius, dtype=tf.float64 )
        interp_op_fixed = tflon.toolkit.interpolate1d( pl, fx, fy, method='rbf', radius=fixed_radius, dtype=tf.float64 )

        with tf.Session() as S:
            y1 = S.run( interp_op_var, feed_dict={pl: fx} )
            np.testing.assert_array_almost_equal(fy, y1, decimal=2)

            y2 = S.run( interp_op_fixed, feed_dict={pl: fx} )
            np.testing.assert_array_almost_equal(fy, y2, decimal=2)

            y3 = S.run( interp_op_var, feed_dict={pl: x} )
            y4 = S.run( interp_op_fixed, feed_dict={pl: x} )

            self.assertEqual(y3.shape, (25, 3))
            self.assertEqual(y4.shape, (25, 3))

    def test_dense_matmul_with_mixed_inputs(self):
        dense_pl = tf.placeholder(shape=[None, 10], dtype=tf.float32)
        sparse_ind_pl = tf.placeholder(shape=[None, 2], dtype=tf.int64)
        sparse_val_pl = tf.placeholder(shape=[None], dtype=tf.float32)
        sparse_shape_pl = tf.placeholder(shape=[2], dtype=tf.int64)
        sparse_pl = tflon.data.SparseTensor(sparse_ind_pl, sparse_val_pl, sparse_shape_pl, tf.TensorShape([None, 10000]))

        dense = tflon.toolkit.Dense(5, activation=tf.exp)
        dense_out = dense(sparse_pl, dense_pl)

        dT = np.random.random((100,10)).astype(np.float32)
        spT_ind = np.array(sum([ [list(elem) for elem in zip([i]*10, np.random.randint(0, 10000, size=(10)).tolist())] for i in range(100)], []))
        spT_val = np.array(sum([ np.random.random(size=(10,)).tolist() for i in range(100) ], [])).astype(np.float32)

        with tf.Session() as S:
            S.run(tf.variables_initializer(self._test_tower.trainables))
            out = S.run(dense_out, feed_dict={dense_pl: dT, sparse_ind_pl: spT_ind, sparse_val_pl:spT_val, sparse_shape_pl:np.array([100,10000])})
            self.assertEqual(out.shape, (100,5))

    def test_dense_matmul_handles_image_types(self):
        im_pl = tf.placeholder(shape=[None, None, None, 3], dtype=tf.float32)
        dense = tflon.toolkit.Dense(5, activation=tf.exp)
        dense_out = dense(im_pl)

        iT = np.random.random((100,16,16,3)).astype(np.float32)

        with tf.Session() as S:
            S.run(tf.variables_initializer(self._test_tower.trainables))
            out = S.run(dense_out, feed_dict={im_pl:iT})
            self.assertEqual(out.shape, (100,16,16,5))

class LossTests(TflonOpTestCase):
    def test_xent_class_weighting(self):
        logit = np.random.random((100,5))
        true = np.random.randint(0,2,size=(100,5)).astype(np.float64)
        true[np.random.randint(0, true.shape[0], (5,)), np.random.randint(0, true.shape[1], (5,))] = np.nan

        weights = 1.2
        val = tflon.toolkit.xent_weighted_sum(true, logit, weights, nans=True)

        with tf.Session() as S:
            out = S.run(val)

            sig = 1./(1.+np.exp(-logit))
            losses = - true*np.log(sig) - (1-true)*np.log(1-sig)
            losses[np.isnan(true)] = 0
            losses[true==1] *= weights

            np.testing.assert_array_almost_equal(np.sum(losses), out)

    def test_xent_mean(self):
        logit = np.random.random((100,5))
        true = np.random.randint(0,2,size=(100,5)).astype(np.float64)
        true[np.random.randint(0, true.shape[0], (5,)), np.random.randint(0, true.shape[1], (5,))] = np.nan

        val = tflon.toolkit.xent_uniform_sum(true, logit, nans=True, reduction=tflon.toolkit.nanmean(true))

        with tf.Session() as S:
            out = S.run(val)

            sig = 1./(1.+np.exp(-logit))
            losses = - true*np.log(sig) - (1-true)*np.log(1-sig)
            np.testing.assert_array_almost_equal(np.nanmean(losses), out)

class MetricTests(TflonOpTestCase):
    def test_reuse_accumulator(self):
        pl = tf.placeholder(shape=[None, 4], dtype=tf.float32)
        show, update = tflon.toolkit.accumulate_values(pl, axis=0)

        L1 = [np.random.random((6,4)),
              np.random.random((4,4)),
              np.random.random((8,4))]
        L2 = [np.random.random((2,4)),
              np.random.random((7,4))]

        init_op = tf.variables_initializer([show])
        with tf.Session() as S:
            S.run(init_op)

            for l in L1:
                S.run(update, feed_dict={pl:l})
            val = S.run(show)
            np.testing.assert_array_almost_equal(val, np.vstack(L1))

            S.run(init_op)
            for l in L2:
                S.run(update, feed_dict={pl:l})
            val = S.run(show)
            np.testing.assert_array_almost_equal(val, np.vstack(L2))

    def test_auc_metric(self):
        fx = np.random.random((100,5))
        fy = np.random.randint(0,2,size=(100,5)).astype(np.float64)

        true_axis0 = [roc_auc_score(fy[:,i], fx[:,i]) for i in range(5)]
        true_noaxis = roc_auc_score(np.concatenate([fy[:,i] for i in range(5)]),
                                    np.concatenate([fx[:,i] for i in range(5)]))

        logit = tf.placeholder(shape=[None,5], dtype=tf.float64)
        target = tf.placeholder(shape=[None,5], dtype=tf.float64)
        auc_noaxis, update_noaxis = tflon.toolkit.auc(target, logit)
        auc_axis0, update_axis0 = tflon.toolkit.auc(target, logit, axis=0)

        with tf.Session() as S:
            S.run( tf.variables_initializer(self._test_tower.metric_variables) )

            for i in range(0,100,25):
                S.run([update_noaxis, update_axis0], feed_dict={logit:fx[i:i+25], target:fy[i:i+25]})

            val_noaxis, val_axis0 = S.run([auc_noaxis, auc_axis0])
            np.testing.assert_array_almost_equal(true_axis0, val_axis0)
            np.testing.assert_almost_equal(true_noaxis, val_noaxis)

    def test_auc_metric_with_one_class(self):
        fx = np.random.random((100,5))
        fy = np.random.randint(0,1,size=(100,5)).astype(np.float64)

        true_axis0 = [np.nan for i in range(5)]
        true_noaxis = np.nan

        logit = tf.placeholder(shape=[None,5], dtype=tf.float64)
        target = tf.placeholder(shape=[None,5], dtype=tf.float64)
        auc_noaxis, update_noaxis = tflon.toolkit.auc(target, logit)
        auc_axis0, update_axis0 = tflon.toolkit.auc(target, logit, axis=0)

        with tf.Session() as S:
            S.run( tf.variables_initializer(self._test_tower.metric_variables) )

            for i in range(0,100,25):
                S.run([update_noaxis, update_axis0], feed_dict={logit:fx[i:i+25], target:fy[i:i+25]})

            val_noaxis, val_axis0 = S.run([auc_noaxis, auc_axis0])
            np.testing.assert_array_almost_equal(true_axis0, val_axis0)
            np.testing.assert_almost_equal(true_noaxis, val_noaxis)

    def test_average_auc_metric(self):
        fx = np.random.random((125, 5))
        fy = np.concatenate([np.zeros((25,5)).astype(np.float64), np.random.randint(2, size=(100, 5)).astype(np.float64)], axis=0)
        sg = np.concatenate([np.zeros(25), np.ones(25), 2*np.ones(25), 3*np.ones(25), 4*np.ones(25)], axis=0).astype(np.int32)

        def _roc_auc_score(T, S):
            try:
                return roc_auc_score(T, S)
            except:
                return np.nan

        true_noaxis = np.nanmean([_roc_auc_score(fy[sg==i], fx[sg==i]) for i in range(5)])
        true_axis0 = np.nanmean([[_roc_auc_score(fy[sg==i,j], fx[sg==i,j]) for j in range(5)] for i in range(5)], axis=0)

        logit = tf.placeholder(shape=[None,5], dtype=tf.float64)
        target = tf.placeholder(shape=[None,5], dtype=tf.float64)
        segment = tf.placeholder(shape=[None], dtype=tf.int32)
        auc_noaxis, update_noaxis = tflon.toolkit.segment_average_auc(target, logit, segment)
        auc_axis0, update_axis0 = tflon.toolkit.segment_average_auc(target, logit, segment, axis=0)

        with tf.Session() as S:
            S.run( tf.variables_initializer(self._test_tower.metric_variables) )
            S.run([update_noaxis], feed_dict={logit:fx, target:fy, segment:sg})
            val_noaxis = S.run([auc_noaxis])
            np.testing.assert_almost_equal(true_noaxis, val_noaxis)

            S.run([update_axis0], feed_dict={logit:fx, target:fy, segment:sg})
            val_axis0 = S.run([auc_axis0])
            np.testing.assert_almost_equal(np.expand_dims(true_axis0, 0), val_axis0)

    def test_average_auc_metric(self):
        fx = np.random.random((125, 5))
        fy = np.concatenate([np.zeros((25,5)).astype(np.float64), np.random.randint(2, size=(100, 5)).astype(np.float64)], axis=0)

        def _roc_auc_score(T, S):
            try:
                return roc_auc_score(T, S)
            except:
                return np.nan

        true_axis1 = np.nanmean([_roc_auc_score(fy[i,:], fx[i,:]) for i in range(125)])

        logit = tf.placeholder(shape=[None,5], dtype=tf.float64)
        target = tf.placeholder(shape=[None,5], dtype=tf.float64)

        auc_axis1, update_axis1 = tflon.toolkit.average_auc(target, logit, axis=1)

        with tf.Session() as S:
            S.run( tf.variables_initializer(self._test_tower.metric_variables) )
            S.run([update_axis1], feed_dict={logit:fx, target:fy})
            val_axis1 = S.run([auc_axis1])
            np.testing.assert_almost_equal(true_axis1, val_axis1)

    def test_auc_metric_with_nan(self):
        fx = np.random.random((100,5))
        fy = np.random.randint(0,2,size=(100,5)).astype(np.float64)
        fy[np.random.randint(0, fy.shape[0], (5,)), np.random.randint(0, fy.shape[1], (5,))] = np.nan

        true_axis0 = [tflon.toolkit.metrics._nansafe_auc(fy[:,i], fx[:,i]) for i in range(5)]
        true_noaxis = tflon.toolkit.metrics._nansafe_auc(np.concatenate([fy[:,i] for i in range(5)]),
                                                         np.concatenate([fx[:,i] for i in range(5)]))

        logit = tf.placeholder(shape=[None,5], dtype=tf.float64)
        target = tf.placeholder(shape=[None,5], dtype=tf.float64)
        auc_noaxis, update_noaxis = tflon.toolkit.auc(target, logit, nans=True)
        auc_axis0, update_axis0 = tflon.toolkit.auc(target, logit, axis=0, nans=True)

        with tf.Session() as S:
            S.run( tf.variables_initializer(self._test_tower.metric_variables) )

            for i in range(0,100,25):
                S.run([update_noaxis, update_axis0], feed_dict={logit:fx[i:i+25], target:fy[i:i+25]})


            val_noaxis, val_axis0 = S.run([auc_noaxis, auc_axis0])
            np.testing.assert_array_almost_equal(true_axis0, val_axis0)
            np.testing.assert_almost_equal(true_noaxis, val_noaxis)

    def test_avgz_metric(self):
        fx = np.random.random((100,5)) * 100 - 50
        fy = np.random.random((100,5)) * 9 + 1
        fz = np.random.random((100,5)) * 100 - 50
        fz[np.random.randint(0, fz.shape[0], (5,)), np.random.randint(0, fz.shape[1], (5,))] = np.nan

        true_axis0 = [np.nanmean(np.abs((fz[:,i]-fx[:,i])/fy[:,i]),axis=0) for i in range(5)]
        true_noaxis = np.nanmean(np.abs((fz-fx)/fy))

        mu = tf.placeholder(shape=[None,5], dtype=tf.float64)
        sigma = tf.placeholder(shape=[None,5], dtype=tf.float64)
        target = tf.placeholder(shape=[None,5], dtype=tf.float64)

        avgz_noaxis, update_noaxis = tflon.toolkit.average_z_score(target, mu, sigma, nans=True)
        avgz_axis0, update_axis0 = tflon.toolkit.average_z_score(target, mu, sigma, axis=0, nans=True)

        with tf.Session() as S:
            S.run( tf.variables_initializer(self._test_tower.metric_variables) )

            for i in range(0,100,25):
                S.run([update_noaxis, update_axis0], feed_dict={mu:fx[i:i+25], sigma:fy[i:i+25], target:fz[i:i+25]})

            val_noaxis, val_axis0 = S.run([avgz_noaxis, avgz_axis0])
            np.testing.assert_array_almost_equal(true_axis0, val_axis0)
            np.testing.assert_almost_equal(true_noaxis, val_noaxis)


    def test_pearson_metric(self):
        var = 50
        N = 10000
        B = 10
        M = 250

        fx = np.zeros((N,var))
        fz = np.zeros((N,var))
        for j in range(var):
            xz_mean = np.random.random(size=(2,))
            cov = np.random.random()-0.5
            xz_cov = np.zeros((2,2))
            xz_cov[0,0] = np.random.random()/2
            xz_cov[1,1] = np.random.random()/2
            xz_cov[0,1] = cov
            xz_cov = np.dot(xz_cov, xz_cov.T)

            sample = np.random.multivariate_normal(xz_mean, xz_cov, size=(N))
            fx[:,j] = sample[:,0]
            fz[:,j] = sample[:,1]

        for j in range(var-10,var):
            sel = np.random.randint(0,N,size=(M))
            fz[sel] = np.nan

        true_axis0 = [sp.stats.pearsonr(fx[np.logical_not(np.isnan(fz[:,i])),i],
                                        fz[np.logical_not(np.isnan(fz[:,i])),i])[0] for i in range(var)]
        flatx = fx.reshape((-1,))
        flatz = fz.reshape((-1,))
        sel = np.logical_not(np.isnan(flatz))
        true_noaxis, _ = sp.stats.pearsonr(flatx[sel], flatz[sel])

        prediction = tf.placeholder(shape=[None,var], dtype=tf.float64)
        target = tf.placeholder(shape=[None,var], dtype=tf.float64)

        pearson_noaxis, update_noaxis = tflon.toolkit.pearson(target, prediction, nans=True)
        pearson_axis0, update_axis0 = tflon.toolkit.pearson(target, prediction, axis=0, nans=True)

        with tf.Session() as S:
            S.run( tf.variables_initializer(self._test_tower.metric_variables) )

            for i in range(0,N,B):
                S.run([update_noaxis, update_axis0], feed_dict={prediction:fx[i:i+B], target:fz[i:i+B]})

            val_noaxis, val_axis0 = S.run([pearson_noaxis, pearson_axis0])
            np.testing.assert_almost_equal(true_noaxis, val_noaxis)
            np.testing.assert_array_almost_equal(true_axis0, val_axis0)


    def test_mae_metric(self):
        var = 50
        N = 10000
        B = 10
        M = 250

        fx = np.zeros((N,var))
        fz = np.zeros((N,var))
        for j in range(var):
            xz_mean = np.random.random(size=(2,))
            cov = np.random.random()-0.5
            xz_cov = np.zeros((2,2))
            xz_cov[0,0] = np.random.random()/2
            xz_cov[1,1] = np.random.random()/2
            xz_cov[0,1] = cov
            xz_cov = np.dot(xz_cov, xz_cov.T)

            sample = np.random.multivariate_normal(xz_mean, xz_cov, size=(N))
            fx[:,j] = sample[:,0]
            fz[:,j] = sample[:,1]

        for j in range(var-10,var):
            sel = np.random.randint(0,N,size=(M))
            fz[sel] = np.nan

        true_axis0 = [np.mean(np.abs(fx[np.logical_not(np.isnan(fz[:,i])),i] - fz[np.logical_not(np.isnan(fz[:,i])),i])) for i in range(var)]
        flatx = fx.reshape((-1,))
        flatz = fz.reshape((-1,))
        sel = np.logical_not(np.isnan(flatz))
        true_noaxis = np.mean(np.abs(flatx[sel] - flatz[sel]))

        prediction = tf.placeholder(shape=[None,var], dtype=tf.float64)
        target = tf.placeholder(shape=[None,var], dtype=tf.float64)

        mae_noaxis, update_noaxis = tflon.toolkit.mean_absolute_error(target, prediction, nans=True)
        mae_axis0, update_axis0 = tflon.toolkit.mean_absolute_error(target, prediction, axis=0, nans=True)

        with tf.Session() as S:
            S.run( tf.variables_initializer(self._test_tower.metric_variables) )

            for i in range(0,N,B):
                S.run([update_noaxis, update_axis0], feed_dict={prediction:fx[i:i+B], target:fz[i:i+B]})

            val_noaxis, val_axis0 = S.run([mae_noaxis, mae_axis0])
            np.testing.assert_almost_equal(true_noaxis, val_noaxis)
            np.testing.assert_array_almost_equal(true_axis0, val_axis0)
