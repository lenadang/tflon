from tflon_test.utils import TflonTestCase
import numpy as np
import tflon

class UtilTests(TflonTestCase):
    def test_nansorted(self):
        L = [(np.nan, 'ko'),(6, 'fru'),(2, 'bla'),(4, 'lo'),(np.nan, 'bah'),(1, 'boo')]
        f = [(np.nan, 'ko'),(1, 'boo'),(2, 'bla'),(4, 'lo'),(6, 'fru'),(np.nan, 'bah')]
        e = [(1, 'boo'),(2, 'bla'),(4, 'lo'),(6, 'fru'),(np.nan, 'ko'),(np.nan, 'bah')]

        v = sorted(L, key=lambda x: x[0])
        self.assertEqual(f,v)

        v = tflon.utils.nansorted(L, key=lambda x: x[0])
        self.assertEqual(e,v)

