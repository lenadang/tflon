import pandas as pd
import numpy as np
from tflon_test.utils import TflonTestCase
from tflon.distributed import DistributedTable
from tflon.data.tables import Table
from subprocess import check_output, CalledProcessError

class DistTest(TflonTestCase):
    def test_mpi_dist_table(self):
        try:
            check_output(["mpirun", "-np", "2", "python", "-m", "tflon_test.unit.test_dist"])
        except CalledProcessError as e:
            self.fail(msg=str(e))

if __name__ == "__main__":
    import tflon
    from tflon.distributed import DistributedTable, init_distributed_resources, is_master

    def assertEqual (x,y, message):
        np.testing.assert_array_almost_equal(x, y, err_msg=message, decimal=2)
    def assertIntEquals(x, y, message):
        assert x==y, "%s: %d != %d" % (message, x, y)

    config = tflon.system.configure_resources(distributed=True)

    expected_cpus = tflon.system.num_cpus()/2
    assertIntEquals(config.intra_op_parallelism_threads, expected_cpus, 'intra_op_parallelism_threads')
    assertIntEquals(config.inter_op_parallelism_threads, expected_cpus, 'inter_op_parallelism_threads')
    assertIntEquals(config.device_count['CPU'], expected_cpus, 'cpu_device_count')

    #df = pd.DataFrame(np.arange(16*19).reshape(16,19))
    df = np.asarray([1]*5+[2]*10+[3]*5)
    df = np.random.permutation(df)
    df = np.repeat([df],10, axis=0)
    offset = np.arange(10)
    offset = np.repeat(offset,20,axis=0).reshape(10,20)
    df = df + offset
    if is_master():
        df = df + 50
    df = np.transpose(df).copy()

    if is_master():
        expected_tab = np.concatenate([df-50, df], axis=0)
    else:
        expected_tab = np.concatenate([df, df+50], axis=0)

    df = Table(pd.DataFrame(df))
    t = DistributedTable(df)

    # test basic statistics
    assertEqual(t.min(), expected_tab.min(axis=0),"min")
    assertEqual(t.max(), expected_tab.max(axis=0),"max")
    assertEqual(t.sum(), expected_tab.sum(axis=0), "sum")

    #test for calculating moments: implicitly test sum, count, sqdiff
    mu = expected_tab.mean(axis=0)
    sig = expected_tab.std(axis=0)
    sig2 = expected_tab.std(axis=0, ddof=2)

    _mu,_sig = t.moments()
    _mu2,_sig2 = t.moments(dof=2)
    assertEqual(_mu, mu, "moments - check Mu")
    assertEqual(_sig, sig, "moments - check sig")
    assertEqual( _mu2, mu, "moments 2 dof - check Mu")
    assertEqual(_sig2, sig2, "moments - check sig")

    # test splitting the distributed table
    ntables = t.split([2, 6, -3])
    assertEqual( len(ntables), 4 , "length of split")
    assertEqual( ntables[0].shape, (20, 2) , "split table dim 1")
    assertEqual( ntables[1].shape, (20, 4) , "split table dim 2")
    assertEqual( ntables[2].shape, (20, 1) , "split table dim 3")
    assertEqual( ntables[3].shape, (20, 3) , "split table dim 4")
