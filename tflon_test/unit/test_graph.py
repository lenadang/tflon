from builtins import range
from tflon_test.utils import TflonTestCase, TflonOpTestCase, ragged, hot, convert_feed
import tensorflow as tf
import pandas as pd
import numpy as np
import dill as pickle
import networkx as nx
import tflon
import unittest
import json
import pkgutil
from sklearn.neighbors.ball_tree import BallTree

class DataTests(TflonTestCase):
    def setUp(self):
        TflonTestCase.setUp(self)
        self.graphs = [tflon.graph.Graph(nx.Graph(incoming_graph_data=[(1,2),(2,3),(2,5),(4,5),(5,6),(5,7)])),
                       tflon.graph.Graph(nx.Graph(incoming_graph_data=[(1,2),(1,3),(1,6),(2,3),(2,4),(4,5),(5,6)]))]
        self.batch = {'graphs': tflon.data.Table(pd.DataFrame(np.array(self.graphs).reshape((-1,1))))}

    def test_dense_nodes_to_table_transform(self):
        t = tflon.graph.NodesToNested('graphs', ['A', 'B', 'C', 'D'])
        data = np.random.random((13, 4))
        o = t(self.batch, data)
        np.testing.assert_array_equal(o.index.values, [0, 1])
        self.assertEqual(o.shape, (2, 4))
        for k in ['A', 'B', 'C', 'D']:
            np.testing.assert_array_equal(len(o[k].loc[0]), 7)
            np.testing.assert_array_equal(len(o[k].loc[1]), 6)


    def test_dense_edges_to_table_transform(self):
        t = tflon.graph.EdgesToNested('graphs', ['A', 'B', 'C'])
        data = np.random.random((13, 3))
        o = t(self.batch, data)
        np.testing.assert_array_equal(o.index.values, [0, 1])
        self.assertEqual(o.shape, (2, 3))
        for k in ['A', 'B', 'C']:
            np.testing.assert_array_equal(len(o[k].loc[0]), 6)
            np.testing.assert_array_equal(len(o[k].loc[1]), 7)

class GraphTests(TflonTestCase):
    def test_get_root_by_graph_property(self):
        nxg1 = nx.Graph(incoming_graph_data=[(1,2),(2,3),(2,5),(4,5),(5,6),(5,7)])
        nxg2 = nx.Graph(incoming_graph_data=[(1,2),(1,3),(1,6),(2,3),(2,4),(4,5),(5,6)])

        for n in nxg1.nodes:
            nxg1.nodes[n]['start']=False
        for n in nxg2.nodes:
            nxg2.nodes[n]['start']=False

        nxg1.nodes[4]['start']=True
        nxg2.nodes[3]['start']=True

        self.graphs = [tflon.graph.Graph(nxg1, node_properties=['start']),
                       tflon.graph.Graph(nxg2, node_properties=['start'])]

        self.assertEqual( self.graphs[0].find_nodes('start', True), [4])
        self.assertEqual( self.graphs[1].find_nodes('start', True), [3])

    def test_radius(self):
        g = nx.Graph()
        for i in range(9):
            for j in range(9):
                g.add_node((i,j))

        for i in range(9):
            for j in range(9):
                if i<8: g.add_edge((i,j), (i+1,j))
                if j<8: g.add_edge((i,j), (i,j+1))

        g = tflon.graph.Graph(g)
        self.assertEqual(g.radius, 9)

    def test_serialize_deserialize_graph(self):
        edges = [(1,2),(1,3),(1,6),(2,3),(2,4),(4,5),(5,6)]
        nxg = nx.Graph(incoming_graph_data=edges)
        node_props = np.random.random((6,3)).round(2)
        np_names = ['A','B','C']
        for i, n in enumerate(nxg.nodes):
            for j, nprop in enumerate(np_names):
                nxg.nodes[n][nprop] = node_props[i,j]

        edge_props = np.random.random((7,4)).round(2)
        ep_names = ['i', 'ii', 'iii', 'iv']
        for i, (u,v) in enumerate(nxg.edges):
            for j, eprop in enumerate(ep_names):
                nxg.edges[u,v][eprop] = edge_props[i,j]

        G = tflon.graph.Graph(nxg, np_names, ep_names)

        jdict = json.loads(G.json)
        self.assertEqual([(n, tuple(p)) for n,p in jdict['nodes']], list(zip(nxg.nodes,
                                                   zip(node_props[:,0], node_props[:,1], node_props[:,2]))))
        self.assertEqual([(u,v,tuple(p)) for u,v,p in jdict['edges']], list(zip([u for u,_v in nxg.edges], [v for _u,v in nxg.edges],
                                                   zip(edge_props[:,0], edge_props[:,1], edge_props[:,2], edge_props[:,3]))))
        self.assertEqual(jdict['nprops'], np_names)
        self.assertEqual(jdict['eprops'], ep_names)

        G2 = tflon.graph.Graph.from_json(G.json)

        self.assertEqual(list(G2.nodes), sorted(nxg.nodes))
        self.assertEqual(list(G2.edges), sorted(nxg.edges))

        exp_node_props = np.array([[G2.nodes.get_value(node, prop) for node in nxg.nodes] for prop in np_names]).T
        np.testing.assert_array_almost_equal(exp_node_props, node_props)

        exp_edge_props = np.array([[G2.edges.get_value(edge, prop) for edge in nxg.edges] for prop in ep_names]).T
        np.testing.assert_array_almost_equal(exp_edge_props, edge_props)

    def test_pickle_graph_removes_concurrency_locks(self):
        g = nx.Graph()
        for i in range(9):
            for j in range(9):
                g.add_node((i,j))

        for i in range(9):
            for j in range(9):
                if i<8: g.add_edge((i,j), (i+1,j))
                if j<8: g.add_edge((i,j), (i,j+1))
        g = tflon.graph.Graph(g)
        g.eccentricity

        pstr = pickle.dumps(g)
        g2 = pickle.loads(pstr)
        self.assertEqual(g.nodes, g2.nodes)
        self.assertEqual(g.edges, g2.edges)
        self.assertEqual(g.eccentricity, g2.eccentricity)

    def test_bfs_block_scheduler(self):
        nxg = nx.Graph(incoming_graph_data=[(1,2),(1,3),(1,6),(2,3),(2,4),(4,5),(5,6)])
        g = tflon.graph.Graph(nxg)

        blocks, ancestors, types = g.bfs([2])

        self.assertEqual(types, {(1,2):'t', (1,3):'x', (1,6):'t',
                                  (2,3):'t', (2,4):'t', (4,5):'t',
                                  (5,6):'x' })
        self.assertEqual(ancestors, {1: [2,3], 2: [], 3: [1,2],
                                      4: [2], 5:[4,6], 6:[1,5]})
        self.assertEqual(blocks, [[2], [1,3,4], [5,6]])

    def test_grid_graph_bfs_edge_types(self):
        G = tflon.graph.Graph(nx.grid_graph([3,3]))

        blocks, ancestors, types = G.bfs([(1,1)])

        self.assertEqual(blocks, [[(1,1)], [(0,1),(1,0),(1,2),(2,1)],
                                   [(0,0), (0,2), (2,0), (2,2)]])
        self.assertEqual(ancestors, {(0,0): [(0,1), (1,0)], (0,1): [(1,1)], (0,2): [(0,1),(1,2)],
                                      (1,0): [(1,1)],        (1,1): [],      (1,2): [(1,1)],
                                      (2,0): [(1,0), (2,1)], (2,1): [(1,1)], (2,2): [(1,2),(2,1)]})
        self.assertEqual(list(types.values()), ['t']*12)

class GraphOpTestCase(TflonOpTestCase):
    def setUp(self):
        TflonOpTestCase.setUp(self)
        self.digraphs = [tflon.graph.DiGraph(nx.DiGraph(incoming_graph_data=[(1,2),(2,5),(3,2),(4,5),(5,2),(5,6),(5,7)])),
                         tflon.graph.DiGraph(nx.DiGraph(incoming_graph_data=[(1,2),(1,3),(2,3),(2,4),(3,2),(4,5),(5,6),(6,1),(6,5)]))]
        self.graphs = [tflon.graph.Graph(nx.Graph(incoming_graph_data=[(1,2),(2,3),(2,5),(4,5),(5,6),(5,7)])),
                       tflon.graph.Graph(nx.Graph(incoming_graph_data=[(1,2),(1,3),(1,6),(2,3),(2,4),(4,5),(5,6)]))]

@unittest.skipIf( pkgutil.find_loader('graph_nets') is None, "Test requires graph_nets" )
class GraphNetsTests(GraphOpTestCase):
    def test_translate_graph_nets_creates_placeholder_tuples(self):
        graph_table = self._test_tower.add_table('graphs', ttype=tflon.graph.GraphTable)
        gl = self._test_tower.add_input('globals', shape=[None, 7])
        nodes = self._test_tower.add_input('nodes', shape=[None, 10])
        edges = self._test_tower.add_input('edges', shape=[None, 12])
        GN = tflon.graph.GraphToGraphsTuple(graph_table, globals=gl, nodes=nodes, edges=edges)

        gt = GN()
        assert gt.nodes is nodes
        self.assertEqual(gt.edges.get_shape()[1].value, edges.get_shape()[1].value)
        assert gt.globals is gl
        self.assertEqual([d.value for d in gt.n_node.get_shape()], [None,])
        self.assertEqual([d.value for d in gt.n_edge.get_shape()], [None,])
        self.assertEqual([d.value for d in gt.receivers.get_shape()], [None,])
        self.assertEqual([d.value for d in gt.senders.get_shape()], [None,])

    def test_translate_graph_nets_featurize_graphs(self):
        graph_table = self._test_tower.add_table('graphs', ttype=tflon.graph.GraphTable)
        edges = self._test_tower.add_input('edges', shape=[None, 12])
        GN = tflon.graph.GraphToGraphsTuple(graph_table, edges=edges)
        GN.initialize()
        batch = {graph_table: tflon.data.Table(pd.DataFrame(np.array(self.graphs).reshape((-1,1))))}
        f = GN.featurizer
        f = pickle.dumps(f)
        update = pickle.loads(f)(batch)

        exp = np.array([[0, 0, 1, 2, 1, 3, 2, 3, 4, 5, 4, 5,    6,  7,  8, 6,  9, 10,  7,  9, 10, 11, 11, 12,  8, 12]]).T
        np.testing.assert_array_equal(update['GraphToGraphsTuple_0/edge_properties'].as_matrix(), exp)

        exp = np.array([[1, 2, 2, 2, 3, 4, 5, 5, 5, 5, 6, 7,    8,  8,  8, 9,  9,  9, 10, 10, 11, 11, 12, 12, 13, 13],
                        [2, 1, 3, 5, 2, 5, 2, 4, 6, 7, 5, 5,    9, 10, 13, 8, 10, 11,  8,  9,  9, 12, 11, 13,  8, 12]]).T
        np.testing.assert_array_equal(update['GraphToGraphsTuple_0/edges'].as_matrix()+1, exp)

        exp = np.array([[7, 6],
                        [12, 14]]).T
        np.testing.assert_array_equal(update['GraphToGraphsTuple_0/shapes'].as_matrix(), exp)

    def test_translate_graph_nets_featurize_digraphs(self):
        graph_table = self._test_tower.add_table('digraphs', ttype=tflon.graph.DiGraphTable)
        edges = self._test_tower.add_input('edges', shape=[None, 12])
        GN = tflon.graph.GraphToGraphsTuple(graph_table, edges=edges)
        GN.initialize()
        batch = {graph_table: tflon.data.Table(pd.DataFrame(np.array(self.digraphs).reshape((-1,1))))}
        update = GN.featurizer(batch)

        exp = np.array([[0, 1, 2, 3, 4, 5, 6,   7, 8, 9, 10, 11, 12, 13, 14, 15]]).T
        np.testing.assert_array_equal(update['GraphToGraphsTuple_0/edge_properties'].as_matrix(), exp)

        #[(1,2),(2,5),(3,2),(4,5),(5,2),(5,6),(5,7)]
        #[(1,2),(1,3),(2,3),(2,4),(3,2),(4,5),(5,6),(6,1),(6,5)]
        exp = np.array([[1, 2, 3, 4, 5, 5, 5,   8,  8,  9,  9, 10, 11, 12, 13, 13],
                        [2, 5, 2, 5, 2, 6, 7,   9, 10, 10, 11,  9, 12, 13,  8, 12]]).T
        np.testing.assert_array_equal(update['GraphToGraphsTuple_0/edges'].as_matrix()+1, exp)

        exp = np.array([[7, 6],
                        [7, 9]]).T
        np.testing.assert_array_equal(update['GraphToGraphsTuple_0/shapes'].as_matrix(), exp)

    def test_use_graph_nets_module(self):
        from graph_nets.demos import models

        graph_table = self._test_tower.add_table('graphs', ttype=tflon.graph.GraphTable)
        nodes = self._test_tower.add_input('nodes', shape=[None, 10])
        edges = self._test_tower.add_input('edges', shape=[None, 12])
        gl = self._test_tower.add_input('globals', shape=[None, 3])
        GN = tflon.graph.GraphToGraphsTuple(graph_table, nodes=nodes, edges=edges, globals=gl)
        gtuple = GN()

        model = models.EncodeProcessDecode(edge_output_size=2, node_output_size=3, global_output_size=4)
        gtuple_out = model(gtuple, 5)[-1]
        undirected_edges_out = GN.reduce_to_undirected_edges(gtuple_out, apply_op=tflon.toolkit.Dense(6))

        with tf.Session() as S:
            S.run(tf.variables_initializer(self._test_tower.variables))

            batch = {graph_table: tflon.data.Table(pd.DataFrame(np.array(self.graphs).reshape((-1,1))))}
            fd = self._test_tower.feed(convert_feed(self._test_tower.featurizer(batch)))
            nodes_val = np.random.random((13,10))
            edges_val = np.random.random((13,12))
            gl_val = np.random.random((2,3))
            fd[nodes] = nodes_val
            fd[edges] = edges_val
            fd[gl] = gl_val
            nout, eout, gout, udout = S.run([gtuple_out.nodes, gtuple_out.edges, gtuple_out.globals, undirected_edges_out], feed_dict=fd)

            self.assertEqual(nout.shape, (13,3))
            self.assertEqual(eout.shape, (26,2))
            self.assertEqual(udout.shape, (13,6))
            self.assertEqual(gout.shape, (2,4))

class ToolkitTests(GraphOpTestCase):
    def test_element_property_accessors(self):
        edges = [(1,2),(1,3),(1,6),(2,3),(2,4),(4,5),(5,6)]
        nxg = nx.Graph(incoming_graph_data=edges)
        node_props = np.random.random((6,3)).round(2)
        np_names = ['A','B','C']
        for i, n in enumerate(nxg.nodes):
            for j, nprop in enumerate(np_names):
                nxg.nodes[n][nprop] = node_props[i,j]

        edge_props = np.random.random((7,4)).round(2)
        ep_names = ['i', 'ii', 'iii', 'iv']
        for i, (u,v) in enumerate(nxg.edges):
            for j, eprop in enumerate(ep_names):
                nxg.edges[u,v][eprop] = edge_props[i,j]

        G = tflon.graph.Graph(nxg, np_names, ep_names)
        graph_table = self._test_tower.add_table('graphs', ttype=tflon.graph.GraphTable)
        batch = {graph_table: tflon.graph.GraphTable(pd.DataFrame([[G]]))}

        np_op = tflon.graph.NodeProperties(graph_table, ['A', 'C'])()
        ep_op = tflon.graph.EdgeProperties(graph_table, ['ii', 'iii', 'iv'])()

        with tf.Session() as S:
            fd = self._test_tower.feed(convert_feed(self._test_tower.featurizer(batch)))
            np_val, ep_val = S.run([np_op, ep_op], feed_dict=fd)

            np_exp = node_props[:,(0,2)]
            np_exp = np_exp[(0,1,2,4,5,3),:]
            ep_exp = edge_props[:,(1,2,3)]
            ep_exp = ep_exp[(0,1,2,3,4,6,5),:]

            np.testing.assert_array_almost_equal(np_val, np_exp)
            np.testing.assert_array_almost_equal(ep_val, ep_exp)

    def test_graph_encoder_eval(self):
        batch = {'graphs': tflon.data.Table(pd.DataFrame(np.array(self.graphs).reshape((-1,1))))}
        nodes = tf.placeholder(name='nodes', dtype=tf.float32, shape=[None, 10])
        cell = tf.nn.rnn_cell.LSTMCell(10)
        nnet = tflon.graph.Encoder('graphs', cell, tflon.toolkit.Dense(10, activation=tf.nn.elu))
        op = nnet(nodes)

        with tf.Session() as S:
            S.run(tf.variables_initializer(self._test_tower.variables))
            fd = self._test_tower.feed(convert_feed(self._test_tower.featurizer(batch)))
            nprop = np.random.random((13,10))
            fd[nodes] = nprop
            output = S.run(op, feed_dict=fd)

            self.assertEqual(output.shape, (2,10))


    def test_graph_ops_neighborhood(self):
        batch = {'graphs': tflon.data.Table(pd.DataFrame(np.array(self.graphs).reshape((-1,1))))}
        nodes = tf.placeholder(name='nodes', dtype=tf.float32, shape=[None, 7])
        nnet = tflon.graph.Neighborhood('graphs', depth=2)
        op = nnet(nodes)

        with tf.Session() as S:
            fd = self._test_tower.feed(convert_feed(self._test_tower.featurizer(batch)))
            nprop = np.random.random((13,7))
            fd[nodes] = nprop
            output = S.run(op, feed_dict=fd)

            self.assertEqual(output.shape, (13,7*3))

            np.testing.assert_array_almost_equal(output[:,:7], nprop)

            exp_depth_2 = [ nprop[1],
                            nprop[0]+nprop[2]+nprop[4],
                            nprop[1],
                            nprop[4],
                            nprop[1]+nprop[3]+nprop[5]+nprop[6],
                            nprop[4],
                            nprop[4],

                            nprop[8]+nprop[9]+nprop[12],
                            nprop[7]+nprop[9]+nprop[10],
                            nprop[7]+nprop[8],
                            nprop[8]+nprop[11],
                            nprop[10]+nprop[12],
                            nprop[11]+nprop[7] ]
            np.testing.assert_array_almost_equal(output[:,7:14], exp_depth_2)
#            np.testing.assert_array_almost_equal(output[:,14:], exp_depth_3)

    def test_graph_ops_node_to_edge(self):
        batch = {'graphs': tflon.data.Table(pd.DataFrame(np.array(self.graphs).reshape((-1,1))))}
        nodes = tf.placeholder(name='nodes', dtype=tf.float32, shape=[None, 7])
        nnet = tflon.graph.NodeToEdge('graphs', width=16)
        op = nnet(nodes)

        with tf.Session() as S:
            S.run(tf.variables_initializer(self._test_tower.variables))
            feed = self._test_tower.featurizer(batch)
            fd = self._test_tower.feed(convert_feed(feed))
            nprop = np.random.random((13,7))
            fd[nodes] = nprop
            output = S.run(op, feed_dict=fd)

            self.assertEquals((13, 16), output.shape)

    def test_graph_ops_edge_to_node(self):
        batch = {'graphs': tflon.data.Table(pd.DataFrame(np.array(self.graphs).reshape((-1,1))))}
        edges = tf.placeholder(name='edges', dtype=tf.float32, shape=[None, 7])
        nnet = tflon.graph.EdgeToNode('graphs', width=16)
        op = nnet(edges)

        with tf.Session() as S:
            S.run(tf.variables_initializer(self._test_tower.variables))
            feed = self._test_tower.featurizer(batch)
            fd = self._test_tower.feed(convert_feed(feed))
            eprop = np.random.random((13,7))
            fd[edges] = eprop
            output = S.run(op, feed_dict=fd)

            self.assertEquals((13, 16), output.shape)

#    def test_graph_ops_node_to_edge_digraph(self):
#        batch = {'graphs': tflon.data.Table(pd.DataFrame(np.array(self.digraphs).reshape((-1,1))))}
#        nodes = tf.placeholder(name='nodes', dtype=tf.float32, shape=[None, 7])
#        nnet = tflon.graph.NodeToEdge('graphs', width=16)
#        op = nnet(nodes)
#
#        with tf.Session() as S:
#            S.run(tf.variables_initializer(self._test_tower.variables))
#            feed = self._test_tower.featurizer(batch)
#            fd = self._test_tower.feed(convert_feed(feed))
#            nprop = np.random.random((13,7))
#            fd[nodes] = nprop
#            output = S.run(op, feed_dict=fd)
#
#            self.assertEquals((13, 16), output.shape)
#
#    def test_graph_ops_edge_to_node_digraph(self):
#        batch = {'graphs': tflon.data.Table(pd.DataFrame(np.array(self.digraphs).reshape((-1,1))))}
#        edges = tf.placeholder(name='edges', dtype=tf.float32, shape=[None, 7])
#        nnet = tflon.graph.EdgeToNode('graphs', width=16)
#        op = nnet(edges)
#
#        with tf.Session() as S:
#            S.run(tf.variables_initializer(self._test_tower.variables))
#            fd = self._test_tower.feed(convert_feed(self._test_tower.featurizer(batch)))
#            eprop = np.random.random((16,7))
#            fd[edges] = eprop
#            output = S.run(op, feed_dict=fd)
#
#            self.assertEquals((16, 16), output.shape)



class GRNNTests(GraphOpTestCase):
    def get_grnn_output_with_args(self, cell_args={}, module_args={}):
        batch = {'graphs': tflon.data.Table(pd.DataFrame(np.array(self.graphs).reshape((-1,1))))}
        recursion = tflon.graph.GRNN('graphs', rooter=lambda g: [5] if len(g.nodes)==7 else [2], **module_args)
        update_cell = recursion.wrap_cell(tf.nn.rnn_cell.GRUCell(10), **cell_args)

        nodes = tf.placeholder(name='nodes', shape=[None, 7], dtype=tf.float32)

        resize = tflon.toolkit.Dense(10)(nodes)
        result = recursion(resize, update_cell, update_cell)

        with tf.Session() as S:
            S.run(tf.variables_initializer(self._test_tower.variables))

            fd = self._test_tower.feed(convert_feed(recursion.featurizer(batch)))
            node_properties = np.random.random((13,7))
            fd[nodes] = node_properties

            output = S.run(result, feed_dict=fd)
            self.assertEqual((13,10), output.shape)
            nans = np.sum(np.isnan(output))
            self.assertEqual(nans, 0, "Nans detected in output")
        return recursion, output

    def test_mix_gate_norm_by_layer(self):
        mod, out = self.get_grnn_output_with_args(cell_args={'normalization': 'layer'})

    def test_mix_gate_norm_by_degree(self):
        mod, out = self.get_grnn_output_with_args(cell_args={'normalization': 'degree'})

    def test_norm_grnn_cell_output(self):
        mod, out = self.get_grnn_output_with_args(cell_args={'normalize_output': True})

    def test_graph_schedule_with_edge_properties(self):
        batch = {'graphs': tflon.data.Table(pd.DataFrame(np.array(self.graphs).reshape((-1,1))))}
        W = tflon.graph.GRNN('graphs', rooter=lambda g: [5] if len(g.nodes)==7 else [2], edge_properties=True)
        W.initialize()
        features = W.featurizer(batch)

        forward = features['GRNN_0/forward_edge_props']
        forward_ports = features['GRNN_0/forward_ports']
        backward = features['GRNN_0/backward_edge_props']
        backward_ports = features['GRNN_0/backward_ports']

        blocks = features['GRNN_0/blocks'].as_matrix()
        exp = np.vstack([hot([5,9], 13),
                         hot([2,4,6,7,8,10,11], 13),
                         hot([1,3,12,13], 13)]).T
        np.testing.assert_array_equal(blocks, exp)

        fvals = forward.as_matrix().todense()
        exp = ragged([[0,0], [3,4,5,6,7,8,8,10,11], [1,2,12,13,9,13]])
        np.testing.assert_array_equal(fvals, exp)


        fports = forward_ports.as_matrix().todense()
        exp = ragged([[0,0], [0,0,0,0,0,1,1,0,0], [0,0,0,1,0,1]])
        np.testing.assert_array_equal(fports, exp)

        bvals = backward.as_matrix().todense()
        exp = ragged([[3,4,5,6,7,10,11], [1,2,0,0,0,8,9,8,12], [0,0,13,13]])
        np.testing.assert_array_equal(bvals, exp)


        bports = backward_ports.as_matrix().todense()
        exp = ragged([[0,0,0,0,0,0,0], [0,0,0,0,0,1,0,1,0], [0,0,1,1]])
        np.testing.assert_array_equal(bports, exp)

    def test_graph_eval_with_raw_node_properties(self):
        batch = {'graphs': tflon.data.Table(pd.DataFrame(np.array(self.graphs).reshape((-1,1))))}
        forward_cell = tflon.graph.MiniGRUCell(10)
        backward_cell = tflon.graph.MiniGRUCell(10)
        nodes = tf.placeholder(name='nodes', dtype=tf.float32, shape=[None, 7])
        nprops = tf.placeholder(name='node_properties', dtype=tf.float32, shape=[None, 5])
        G = tflon.graph.GRNN('graphs', rooter=lambda g: [5] if len(g.nodes)==7 else [2])

        resize = tflon.toolkit.Dense(10)(nodes)
        result = G(resize, forward_cell, backward_cell, node_properties=nprops)

        with tf.Session() as S:
            S.run(tf.variables_initializer(self._test_tower.variables))

            fd = self._test_tower.feed(convert_feed(G.featurizer(batch)))
            nodes_val = np.random.random((13,7))
            nprops_val = np.random.random((13,5))
            fd[nodes] = nodes_val
            fd[nprops] = nprops_val

            output = S.run(result, feed_dict=fd)
            self.assertEqual((13,10), output.shape)


    def test_graph_eval_with_edge_properties(self):
        batch = {'graphs': tflon.data.Table(pd.DataFrame(np.array(self.graphs).reshape((-1,1))))}
        recursion = tflon.graph.GRNN('graphs', edge_properties=True)
        forward_cell = tflon.graph.MiniGRUCell(10)
        backward_cell = tflon.graph.MiniGRUCell(10)
        nodes = tf.placeholder(name='nodes', dtype=tf.float32, shape=[None, 7])
        edges = tf.placeholder(name='edges', dtype=tf.float32, shape=[None, 5])

        resize = tflon.toolkit.Dense(10)(nodes)
        result = recursion(resize, forward_cell, backward_cell, edges)

        with tf.Session() as S:
            S.run(tf.variables_initializer(self._test_tower.variables))

            fd = self._test_tower.feed(convert_feed(recursion.featurizer(batch)))
            node_properties = np.random.random((13,7))
            edge_properties = np.random.random((13,5))
            fd[nodes] = node_properties
            fd[edges] = edge_properties

            output = S.run(result, feed_dict=fd)
            self.assertEqual((13,10), output.shape)

    def test_graph_eval_with_edge_properties_dynamic(self):
        batch = {'graphs': tflon.data.Table(pd.DataFrame(np.array(self.graphs).reshape((-1,1))))}
        grnn = tflon.graph.GRNN('graphs', edge_properties=True)
        forward_cell = tflon.graph.MiniGRUCell(10)
        backward_cell = tflon.graph.MiniGRUCell(10)
        nodes = tf.placeholder(name='nodes', dtype=tf.float32, shape=[None, 7])
        edges = tf.placeholder(name='edges', dtype=tf.float32, shape=[None, 5])
        iters = tf.placeholder(name='iters', dtype=tf.int32, shape=[])
        grnn.initialize()
        net = tflon.toolkit.Dense(10) | tflon.toolkit.ForLoop(lambda i, n: grnn(n, forward_cell, backward_cell, edges), iters)
        result = net(nodes)

        with tf.Session() as S:
            S.run(tf.variables_initializer(self._test_tower.variables))
            fd = self._test_tower.feed(convert_feed(grnn.featurizer(batch)))
            node_properties = np.random.random((13,7))
            edge_properties = np.random.random((16,5))
            fd[iters] = 10
            fd[nodes] = node_properties
            fd[edges] = edge_properties

            output = S.run(result, feed_dict=fd)
            self.assertEqual((13,10), output.shape)

    def test_graph_schedule_with_edge_properties_and_digraph(self):
        batch = {'graphs': tflon.data.Table(pd.DataFrame(np.array(self.digraphs).reshape((-1,1))))}
        W = tflon.graph.GRNN('graphs', rooter=lambda g: [5] if len(g.nodes)==7 else [2], edge_properties=True, num_ports=4)
        W.initialize()
        features = W.featurizer(batch)


        forward_nodes = features['GRNN_0/forward_nodes']
        forward_edges = features['GRNN_0/forward_edges']
        forward_edge_props = features['GRNN_0/forward_edge_props']
        forward_reduce = features['GRNN_0/forward_reduce']
        forward_ports = features['GRNN_0/forward_ports']
        backward_nodes = features['GRNN_0/backward_nodes']
        backward_edges = features['GRNN_0/backward_edges']
        backward_edge_props = features['GRNN_0/backward_edge_props']
        backward_reduce = features['GRNN_0/backward_reduce']
        backward_ports = features['GRNN_0/backward_ports']

        blocks = features['GRNN_0/blocks'].as_matrix()
        exp = np.vstack([hot([5,9], 13),
                         hot([2,4,6,7,8,10,11], 13),
                         hot([1,3,12,13], 13)]).T
        np.testing.assert_array_equal(blocks, exp)

        # [(1, 2), (2, 5), (3, 2), (4, 5), (5, 2), (5, 6), (5, 7)]
        # [(8, 9), (8, 10), (9, 10), (9, 11), (10, 9), (11, 12), (12, 13), (13, 8), (13, 12)]
        #
        fn = forward_nodes.as_matrix().todense()
        exp = ragged([[5,9], [2,2,4,6,7,8,8,10,10,10,11], [1,3,12,12,12,13,13,13]])
        np.testing.assert_array_equal(fn, exp)

        fe = forward_edges.as_matrix().todense()
        exp = ragged([[0,0], [5,5,5,5,5,9,10,8,9,9,9], [2,2,11,13,13,8,12,12]])
        np.testing.assert_array_equal(fe, exp)

        fep = forward_edge_props.as_matrix().todense()
        exp = ragged([[0,0], [2,5,4,6,7,8,9,9,12,10,11], [1,3,13,14,16,15,16,14]])
        np.testing.assert_array_equal(fep, exp)

        fports = forward_ports.as_matrix().todense()
        exp = ragged([[0,0], [2,0,2,0,0,2,3,1,2,0,0], [2,2,0,3,1,2,3,1]])
        np.testing.assert_array_equal(fports, exp)

        fr = forward_reduce.as_matrix().todense()
        exp = ragged([[0,1], [0,0,1,2,3,4,4,5,5,5,6], [0,1,2,2,2,3,3,3]])
        np.testing.assert_array_equal(fr, exp)

        bn = backward_nodes.as_matrix().todense()
        exp = ragged([[5,5,5,5,5,9,9,9,9], [2,2,4,6,7,8,8,10,11], [1,3,12,12,13,13]])
        np.testing.assert_array_equal(bn, exp)

        be = backward_edges.as_matrix().todense()
        exp = ragged([[2,2,4,6,7,8,10,10,11], [1,3,0,0,0,10,13,8,12], [0,0,13,13,12,12]])
        np.testing.assert_array_equal(be, exp)

        bep = backward_edge_props.as_matrix().todense()
        exp = ragged([[5,2,4,6,7,8,10,12,11], [1,3,0,0,0,9,15,9,13], [0,0,14,16,16,14]])
        np.testing.assert_array_equal(bep, exp)

        bports = backward_ports.as_matrix().todense()
        exp = ragged([[2,0,0,2,2,0,2,0,2], [0,0,0,0,0,3,0,1,2], [0,0,3,1,3,1]])
        np.testing.assert_array_equal(bports, exp)

        br = backward_reduce.as_matrix().todense()
        exp = ragged([[0,0,0,0,0,1,1,1,1], [0,0,1,2,3,4,4,5,6], [0,1,2,2,3,3]])
        np.testing.assert_array_equal(br, exp)

    def test_graph_schedule_with_edge_properties_and_vigraph(self):
        self.vigraphs = [tflon.graph.ViGraph(nx.Graph(incoming_graph_data=[(1,2),(2,3),(2,5),(4,5),(5,6),(5,7)]), tflon.graph.Graph(nx.Graph(incoming_graph_data=[(1,2),(2,3),(2,5)]))),
                    tflon.graph.ViGraph(nx.Graph(incoming_graph_data=[(1,2),(1,3),(1,6),(2,3),(2,4),(4,5),(5,6)]), tflon.graph.Graph(nx.Graph(incoming_graph_data=[(1,2),(1,3),(1,6)])))]

        batch = {'graphs': tflon.data.Table(pd.DataFrame(np.array(self.vigraphs).reshape((-1,1))))}
        W = tflon.graph.GRNN('graphs', rooter=lambda g: [5] if len(g.nodes)==7 else [2], edge_properties=True, num_ports=4, port_properties=['is_bond'])
        W.initialize()
        features = W.featurizer(batch)

        forward = features['GRNN_0/forward_edge_props']
        forward_ports = features['GRNN_0/forward_ports']
        backward = features['GRNN_0/backward_edge_props']
        backward_ports = features['GRNN_0/backward_ports']

        blocks = features['GRNN_0/blocks'].as_matrix()
        exp = np.vstack([hot([5,9], 13),
                         hot([2,4,6,7,8,10,11], 13),
                         hot([1,3,12,13], 13)]).T
        np.testing.assert_array_equal(blocks, exp)

        fvals = forward.as_matrix().todense()
        exp = ragged([[0,0], [3,4,5,6,7,8,8,10,11], [1,2,12,13,9,13]])
        np.testing.assert_array_equal(fvals, exp)


        is_virtual1 = np.array([0,0,0,1,1,1])
        is_virtual2 = np.array([[0,0,0,1,1,1,1]])

        fports = forward_ports.as_matrix().todense()
        exp = ragged([[0,0], [1,0,0,0,1,3,3,0,0], [1,1,0,2,1,2]])
        np.testing.assert_array_equal(fports, exp)

        bvals = backward.as_matrix().todense()
        exp = ragged([[3,4,5,6,7,10,11], [1,2,0,0,0,8,9,8,12], [0,0,13,13]])
        np.testing.assert_array_equal(bvals, exp)

        bports = backward_ports.as_matrix().todense()
        exp = ragged([[1,0,0,0,1,0,0], [1,1,0,0,0,3,1,3,0], [0,0,2,2]])
        np.testing.assert_array_equal(bports, exp)

    def test_graph_schedule_with_edge_properties_and_vigraph_bond_only_scheduling(self):
        self.vigraphs = [tflon.graph.ViGraph(nx.Graph(incoming_graph_data=[(1,2),(1,3),(1,4),(2,3),(2,7),(3,5),(4,6),(4,8),(5,6),(5,7),(6,8),(7,8)]), tflon.graph.Graph(nx.Graph(incoming_graph_data=[(1,3),(1,4),(2,3),(3,5),(4,6),(5,6),(5,7),(6,8)])))]

        batch = {'graphs': tflon.data.Table(pd.DataFrame(np.array(self.vigraphs).reshape((-1,1))))}
        W = tflon.graph.GRNN('graphs', rooter=lambda g: [1], edge_properties=True, num_ports=4, dagger=tflon.graph.ViGraph.bfs, port_properties=['is_bond'])
        W.initialize()
        features = W.featurizer(batch)

        forward = features['GRNN_0/forward_edge_props']
        forward_ports = features['GRNN_0/forward_ports']
        backward = features['GRNN_0/backward_edge_props']
        backward_ports = features['GRNN_0/backward_ports']

        blocks = features['GRNN_0/blocks'].as_matrix()
        exp = np.vstack([hot([1], 8),
                         hot([3,4], 8),
                         hot([2,5,6], 8),
                         hot([7,8], 8)]).T
        np.testing.assert_array_equal(blocks, exp)

        fvals = forward.as_matrix().todense()
        exp = ragged([[0], [2,3], [1,4,6,9,7,9], [5,11,12,8,10,12]])
        np.testing.assert_array_equal(fvals, exp)

        fports = forward_ports.as_matrix().todense()
        exp = ragged([[0], [1,1], [0,0,1,3,1,3], [0,1,2,0,1,2]])
        np.testing.assert_array_equal(fports, exp)

        bvals = backward.as_matrix().todense()
        exp = ragged([[1,2,3], [4,6,7,8], [5,9,11,9,10], [12,12]])
        np.testing.assert_array_equal(bvals, exp)

        bports = backward_ports.as_matrix().todense()
        exp = ragged([[0,1,1], [0,1,1,0], [0,3,1,3,1], [2,2]])
        np.testing.assert_array_equal(bports, exp)

    def test_graph_eval_with_edge_properties_and_digraph(self):
        batch = {'graphs': tflon.data.Table(pd.DataFrame(np.array(self.digraphs).reshape((-1,1))))}
        recursion = tflon.graph.GRNN('graphs', edge_properties=True, num_ports=4)
        forward_cell = tflon.graph.MiniGRUCell(10)
        backward_cell = tflon.graph.MiniGRUCell(10)
        nodes = tf.placeholder(name='nodes', dtype=tf.float32, shape=[None, 7])
        edges = tf.placeholder(name='edges', dtype=tf.float32, shape=[None, 5])
        resize = tflon.toolkit.Dense(10)(nodes)
        result = recursion(resize, forward_cell, backward_cell, edges)

        with tf.Session() as S:
            S.run(tf.variables_initializer(self._test_tower.variables))

            fd = self._test_tower.feed(convert_feed(recursion.featurizer(batch)))
            node_properties = np.random.random((13,7))
            edge_properties = np.random.random((16,5))
            fd[nodes] = node_properties
            fd[edges] = edge_properties

            output = S.run(result, feed_dict=fd)
            self.assertEqual((13,10), output.shape)

@unittest.skipIf( True, "Tests not implemented" )
class WeaveTests(GraphOpTestCase):
    def test_featurize_with_edge_properties_and_digraph(self):
        batch = {'graphs': tflon.data.Table(pd.DataFrame(np.array([self.digraphs[0]]).reshape((-1,1))))}
        weave = tflon.graph.Weave('graphs', node_width=5, pair_width=5, distance=2, edge_properties=True, digraphs=True)
        weave.initialize()

        features = weave.featurizer(batch)

        # edge lists
        # (1,2) (2,5) (3,2) (4,5) (5,2) (5,6) (5,7)

        # pair lists
        # (1,2) (1,3) (1,5) (2,3) (2,4) (2,5) (2,6) (2,7) (3,5) (4,5) (4,6) (4,7) (5,2) (5,6) (5,7) (6,7)
        # 1     2     3     4     5     6     7     8     9     10    11    12    13    14    15    16

        # all pairs+properties
        #
        # 1,1,1, 2,2,2,2,2,5, 3,3,3, 4,4,4,4, 2,5,5,5,5,5,5, 6,6,6,6, 7,7,7,7
        # 2,3,5, 1,3,4,5,6,2, 1,2,5, 2,5,6,7, 5,1,2,3,4,6,7, 2,4,5,7, 2,4,5,6
        #
        # legend: out edge=0, in edge=1, distant pair=2
        # 0,2,2, 1,1,2,0,1,2, 2,0,2, 2,0,2,2, 2,1,0,2,1,0,0, 2,2,1,2, 2,2,1,2

        node_gather = features['Weave_0/node_to_pair']
        ng = node_gather.as_matrix()
        left_atom = ng[:,0]
        exp = [1,1,1, 2,2,2, 4,4,4, 5,5,5]
        np.testing.assert_array_equal(left_atom+1, exp)

        right_atom = ng[:,1]
        exp = [2,3,5, 3,4,6, 5,6,7, 2,6,7]
        np.testing.assert_array_equal(right_atom+1, exp)

        pair_pool = features['Weave_0/pair_pool']
        pp = pair_pool.as_matrix()
        pair_gather = pp[:,0]
        exp = [1,2,3, 1,4,5,6,7,13, 2,4,9, 5,10,11,12, 6,3,13,9,10,14,15, 7,11,14,16, 8,12,15,16]
        np.testing.assert_array_equal(pair_gather+1, exp)

        pair_reduce = pp[:,1]
        exp = [1,1,1, 2,2,2,2,2,2,  3,3,3, 4,4,4,4,    5,5,5,5,5,5,5,     6,6,6,6,    7,7,7,7]
        np.testing.assert_array_equal(pair_reduce+1, exp)

        edge_indices = features['Weave_0/edge_indices'].as_matrix()[:,0]
        exp = [1, 0,0, 2, 0, 3, 0,0,0, 4, 0,0, 5, 6, 7, 0]
        np.testing.assert_array_equal(edge_indices+1, exp)

        edge_pair_indices = features['Weave_0/edge_pair_indices'].as_matrix()[:,0]
        exp = [1, 4, 6, 10, 13, 14, 15]
        np.testing.assert_array_equal(edge_pair_indices+1, exp)

        atom_pair_ports = features['Weave_0/atom_pair_ports']
        ap_ports = atom_pair_ports.as_matrix()[:,0]
        # legend: connected pair=0, distant pair=1
        exp = [0,1,1, 0,1,1, 0,1,1, 0,0,0]
        np.testing.assert_array_equal(ap_ports, exp)

        pair_atom_ports = features['Weave_0/pair_atom_ports']
        pa_ports = pair_atom_ports.as_matrix()[:,1]
        # legend: in edge=0, out edge=1, distant pair=2
        exp = [0,2,2, 1,1,2,0,1,2, 2,0,2, 2,0,2,2, 2,1,0,2,1,0,0, 2,2,1,2, 2,2,1,2]
        np.testing.assert_array_equal(pp_ports, exp)

    def test_graph_eval_with_edge_properties_and_digraph(self):
        batch = {'graphs': tflon.data.Table(pd.DataFrame(np.array(self.digraphs).reshape((-1,1))))}
        weave = tflon.graph.Weave('graphs', node_width=10, pair_width=8, edge_properties=True, digraphs=True)
        nodes = tf.placeholder(name='nodes', dtype=tf.float32, shape=[None, 7])
        edges = tf.placeholder(name='edges', dtype=tf.float32, shape=[None, 5])
        net = tflon.toolkit.Chain(weave, weave.outputs)
        result = net(nodes, weave.initial_pair_state(edges))

        with tf.Session() as S:
            S.run(tf.variables_initializer(self._test_tower.variables))
            features = weave.featurizer(batch).items()
            fd = self._test_tower.feed({k:tflon.data.convert_to_feedable(v) for k,v in features})
            node_properties = np.random.random((13,7))
            edge_properties = np.random.random((16,5))
            fd[nodes] = node_properties
            fd[edges] = edge_properties

            out_nodes, out_edges = S.run(result, feed_dict=fd)
            self.assertEqual((13,10), out_nodes.shape)
            self.assertEqual((13,8), out_edges.shape)

class MPNNTests(GraphOpTestCase):
    def test_graph_eval(self):
        batch = {'graphs': tflon.data.Table(pd.DataFrame(np.array(self.graphs).reshape((-1,1))))}
        mpnn = tflon.graph.MPNN('graphs', width=10)
        nodes = tf.placeholder(name='nodes', dtype=tf.float32, shape=[None, 7])
        resized = tflon.toolkit.Dense(10)(nodes)
        result = mpnn(resized, steps=3)

        with tf.Session() as S:
            S.run(tf.variables_initializer(self._test_tower.variables))
            features = mpnn.featurizer(batch).items()
            fd = self._test_tower.feed({k:tflon.data.convert_to_feedable(v) for k,v in features})
            node_properties = np.random.random((13,7))
            fd[nodes] = node_properties

            output = S.run(result, feed_dict=fd)
            self.assertEqual((13,10), output.shape)

    def test_graph_eval_with_master(self):
        batch = {'graphs': tflon.data.Table(pd.DataFrame(np.array(self.graphs).reshape((-1,1))))}
        mpnn = tflon.graph.MPNN('graphs', width=10, master=True)
        nodes = tf.placeholder(name='nodes', dtype=tf.float32, shape=[None, 7])
        resized = tflon.toolkit.Dense(10)(nodes)
        result = mpnn(resized, steps=3)

        with tf.Session() as S:
            S.run(tf.variables_initializer(self._test_tower.variables))
            features = mpnn.featurizer(batch).items()
            fd = self._test_tower.feed({k:tflon.data.convert_to_feedable(v) for k,v in features})
            node_properties = np.random.random((13,7))
            fd[nodes] = node_properties

            output = S.run(result, feed_dict=fd)
            self.assertEqual((13,10), output.shape)

    def test_graph_eval_with_gate_before_sum(self):
        batch = {'graphs': tflon.data.Table(pd.DataFrame(np.array(self.graphs).reshape((-1,1))))}
        mpnn = tflon.graph.MPNN('graphs', width=10, master=True, apply_order='gate_sum')
        nodes = tf.placeholder(name='nodes', dtype=tf.float32, shape=[None, 7])
        resized = tflon.toolkit.Dense(10)(nodes)
        result = mpnn(resized, steps=4)

        with tf.Session() as S:
            S.run(tf.variables_initializer(self._test_tower.variables))
            features = mpnn.featurizer(batch).items()
            fd = self._test_tower.feed({k:tflon.data.convert_to_feedable(v) for k,v in features})
            node_properties = np.random.random((13,7))
            fd[nodes] = node_properties

            output = S.run(result, feed_dict=fd)
            self.assertEqual((13,10), output.shape)

    def test_graph_featurize_with_edge_properties_and_lone_nodes(self):
        g = self.graphs[0].graph
        g.add_node(8)
        g = tflon.graph.Graph(g)
        batch = {'graphs': tflon.data.Table(pd.DataFrame(np.array([g]).reshape((-1,1))))}
        mpnn = tflon.graph.MPNN('graphs', width=10, edges=True)
        mpnn.initialize()

        features = mpnn.featurizer(batch)

        pool = features['MPNN_0/node_pool']
        edge_indices = features['MPNN_0/edge_gather']

        ri = pool.as_matrix()[:,1]
        exp = np.array([0, 1,1,1, 2, 3, 4,4,4,4, 5, 6, 7])
        np.testing.assert_array_equal(ri, exp)

        ni = pool.as_matrix()[:,0]+1
        exp = np.array([2, 1,3,5, 2, 5, 2,4,6,7, 5, 5, 0])
        np.testing.assert_array_equal(ni, exp)

        ei = edge_indices.as_matrix()+1
        exp = np.array([[1, 1,2,3, 2, 4, 3,4,5,6, 5, 6, 0]]).T
        np.testing.assert_array_equal(ei, exp)

    def test_graph_featurize_with_edge_properties(self):
        batch = {'graphs': tflon.data.Table(pd.DataFrame(np.array(self.graphs).reshape((-1,1))))}
        mpnn = tflon.graph.MPNN('graphs', width=10, edges=True)
        mpnn.initialize()

        features = mpnn.featurizer(batch)

        pool = features['MPNN_0/node_pool']
        edge_indices = features['MPNN_0/edge_gather']

        ri = pool.as_matrix()[:,1]
        exp = np.array([0, 1,1,1, 2, 3, 4,4,4,4, 5, 6, \
                        7,7,7, 8,8,8, 9,9, 10,10, 11,11, 12,12])
        np.testing.assert_array_equal(ri, exp)

        ni = pool.as_matrix()[:,0]+1
        exp = np.array([2, 1,3,5, 2, 5, 2,4,6,7, 5, 5, \
                        9,10,13, 8,10,11, 8,9, 9,12, 11,13, 8,12])
        np.testing.assert_array_equal(ni, exp)

        ei = edge_indices.as_matrix()+1
        exp = np.array([[1, 1,2,3, 2, 4, 3,4,5,6, 5, 6, \
                         7,8,9, 7,10,11, 8,10, 11,12, 12,13, 9,13]]).T
        np.testing.assert_array_equal(ei, exp)

    def test_graph_eval_with_edge_properties_dynamic(self):
        batch = {'graphs': tflon.data.Table(pd.DataFrame(np.array(self.graphs).reshape((-1,1))))}
        mpnn = tflon.graph.MPNN('graphs', width=10, edges=True, message_func=tflon.graph.EdgeMessage)
        nodes = tf.placeholder(name='nodes', dtype=tf.float32, shape=[None, 7])
        edges = tf.placeholder(name='edges', dtype=tf.float32, shape=[None, 5])
        iters = tf.placeholder(name='iters', dtype=tf.int32, shape=[])
        resized = tflon.toolkit.Dense(10)(nodes)
        result = mpnn(resized, edges, steps=iters)

        with tf.Session() as S:
            S.run(tf.variables_initializer(self._test_tower.variables))
            features = mpnn.featurizer(batch).items()
            fd = self._test_tower.feed({k:tflon.data.convert_to_feedable(v) for k,v in features})
            node_properties = np.random.random((13,7))
            edge_properties = np.random.random((16,5))
            fd[iters] = 10
            fd[nodes] = node_properties
            fd[edges] = edge_properties

            output = S.run(result, feed_dict=fd)
            self.assertEqual((13,10), output.shape)

    def test_graph_featurize_with_edge_properties_and_digraph(self):
        batch = {'graphs': tflon.data.Table(pd.DataFrame(np.array(self.digraphs).reshape((-1,1))))}
        mpnn = tflon.graph.MPNN('graphs', width=10, edges=True, digraph=True)
        mpnn.initialize()

        features = mpnn.featurizer(batch)

        pool = features['MPNN_0/node_pool']
        edge_indices = features['MPNN_0/edge_gather']
        edge_ports = features['MPNN_0/edge_ports']

        ri = pool.as_matrix()[:,1]
        exp = np.array([0, 1,1,1,1, 2, 3, 4,4,4,4,4, 5, 6, \
                        7,7,7, 8,8,8,8, 9,9,9, 10,10, 11,11,11, 12,12,12])
        np.testing.assert_array_equal(ri, exp)

        ni = pool.as_matrix()[:,0]+1
        exp = np.array([2, 1,3,5,5, 2, 5, 2,2,4,6,7, 5, 5, \
                        9,10,13, 8,10,10,11, 8,9,9, 9,12, 11,13,13, 8,12,12])
        np.testing.assert_array_equal(ni, exp)

        ei = edge_indices.as_matrix()+1
        exp = np.array([[1, 1,3,2,5, 3, 4, 2,5,4,6,7, 6, 7,
                         8,9,15, 8,10,12,11, 9,10,12, 11,13, 13,14,16, 15,14,16]]).T
        np.testing.assert_array_equal(ei, exp)

        ep = edge_ports.as_matrix()
        exp = np.array([[1, 0,0,1,0, 1, 1, 0,1,0,1,1, 0, 0,
                         1,1,0, 0,1,0,1, 0,0,1, 0,1, 0,1,0, 1,0,1]]).T
        np.testing.assert_array_equal(ep, exp)

    def test_graph_eval_with_edge_properties_and_digraph(self):
        batch = {'graphs': tflon.data.Table(pd.DataFrame(np.array(self.digraphs).reshape((-1,1))))}
        mpnn = tflon.graph.MPNN('graphs', width=10, edges=True, digraph=True, master=True)
        nodes = tf.placeholder(name='nodes', dtype=tf.float32, shape=[None, 7])
        edges = tf.placeholder(name='edges', dtype=tf.float32, shape=[None, 5])
        resized = tflon.toolkit.Dense(10)(nodes)
        result = mpnn(resized, edges)

        with tf.Session() as S:
            S.run(tf.variables_initializer(self._test_tower.variables))
            features = mpnn.featurizer(batch).items()
            fd = self._test_tower.feed({k:tflon.data.convert_to_feedable(v) for k,v in features})
            node_properties = np.random.random((13,7))
            edge_properties = np.random.random((16,5))
            fd[nodes] = node_properties
            fd[edges] = edge_properties

            output = S.run(result, feed_dict=fd)
            self.assertEqual((13,10), output.shape)

    def test_graph_eval_edge_message_func(self):
        batch = {'graphs': tflon.data.Table(pd.DataFrame(np.array(self.graphs).reshape((-1,1))))}
        mpnn = tflon.graph.MPNN('graphs', width=10, edges=True, master=True, message_func=tflon.graph.EdgeMessage)
        nodes = tf.placeholder(name='nodes', dtype=tf.float32, shape=[None, 7])
        edges = tf.placeholder(name='edges', dtype=tf.float32, shape=[None, 5])
        resized = tflon.toolkit.Dense(10)(nodes)
        result = mpnn(resized, edges)

        with tf.Session() as S:
            S.run(tf.variables_initializer(self._test_tower.variables))
            features = mpnn.featurizer(batch).items()
            fd = self._test_tower.feed({k:tflon.data.convert_to_feedable(v) for k,v in features})
            node_properties = np.random.random((13,7))
            edge_properties = np.random.random((16,5))
            fd[nodes] = node_properties
            fd[edges] = edge_properties

            output = S.run(result, feed_dict=fd)
            self.assertEqual((13,10), output.shape)

class DynamicPassesTests(GraphOpTestCase):
    def test_dynamic_passes_featurize(self):
        batch = {'graphs': tflon.data.Table(pd.DataFrame(np.array(self.graphs).reshape((-1,1))))}

        clip_passes = tflon.graph.DynamicPasses('graphs')
        clip_passes.initialize()
        features = clip_passes.featurizer(batch)

        passes = features['DynamicPasses_0/node_pass'].as_matrix()
        index = features['DynamicPasses_0/node_index'].as_matrix()

        val = np.hstack([passes,index])

        exp = [[2, 0],[2,1],[2, 2],[2, 3],[2, 4],[2, 5], [2, 6], [1, 7], [1,8], [1,9], [1,10], [1,11], [1,12]]
        np.testing.assert_array_equal(val, exp)

    def test_dynamic_passes_call(self):
        batch = {'graphs': tflon.data.Table(pd.DataFrame(np.array(self.graphs).reshape((-1,1))))}
        clip_passes = tflon.graph.DynamicPasses('graphs')
        runtime_states = tf.TensorArray(dtype=tf.float32, size=6)
        states = []

        for i in range(6):
            new_state = np.random.random((13,10)).astype(np.float32)
            states.append(np.array(new_state))
            runtime_states = runtime_states.write(i, new_state)

        states = np.array(states)
        exp_result = np.zeros((13,10))
        exp_result[:7,:] = states[2,:7]
        exp_result[7:,:] = states[1,7:]
        max_passes = clip_passes.batch_max_passes
        result = clip_passes(runtime_states)

        with tf.Session() as S:
            S.run(tf.variables_initializer(self._test_tower.variables))
            fd = self._test_tower.feed(convert_feed(clip_passes.featurizer(batch)))
            mp, output = S.run([max_passes, result], feed_dict=fd)

            self.assertEqual(3, mp)
            self.assertEqual((13,10), output.shape)
            self.assertEqual(exp_result.shape, output.shape)
            np.testing.assert_array_equal(exp_result, output)

    def test_dynamic_passes_with_edges(self):
        batch = {'graphs': tflon.data.Table(pd.DataFrame(np.array(self.graphs).reshape((-1,1))))}
        clip_passes = tflon.graph.DynamicPasses('graphs', get_edges=True)
        node_states = tf.TensorArray(dtype=tf.float32, size=6)
        edge_states = tf.TensorArray(dtype=tf.float32, size=6)
        node_state_list = []
        edge_state_list = []


        for i in range(6):
            new_state = np.random.random((13,10)).astype(np.float32)
            node_state_list.append(np.array(new_state))
            node_states = node_states.write(i, new_state)

            new_state = np.random.random((13,10)).astype(np.float32)
            edge_state_list.append(np.array(new_state))
            edge_states = edge_states.write(i, new_state)

        exp_node_result = np.zeros((13,10))
        exp_node_result[:7,:] = node_state_list[2][:7]
        exp_node_result[7:,:] = node_state_list[1][7:]

        exp_edge_result = np.zeros((13,10))
        exp_edge_result[:6,:] = edge_state_list[2][:6]
        exp_edge_result[6:,:] = edge_state_list[1][6:]

        result = clip_passes(node_states, edge_states)

        with tf.Session() as S:
            S.run(tf.variables_initializer(self._test_tower.variables))
            fd = self._test_tower.feed(convert_feed(clip_passes.featurizer(batch)))
            node_out, edge_out = S.run(result, feed_dict=fd)

            np.testing.assert_array_almost_equal(exp_node_result, node_out)
            np.testing.assert_array_almost_equal(exp_edge_result, edge_out)

    def test_dynamic_passes_quadratic_call(self):
        batch = {'graphs': tflon.data.Table(pd.DataFrame(np.array(self.graphs).reshape((-1,1))))}
        clip_passes = tflon.graph.DynamicPasses('graphs', feature='quadratic')
        runtime_states = tf.TensorArray(dtype=tf.float32, size=6)
        states = []

        for i in range(6):
            new_state = np.random.random((13,10)).astype(np.float32)
            states.append(np.array(new_state))
            runtime_states = runtime_states.write(i, new_state)

        states = np.array(states)
        exp_result = np.zeros((13,10))
        exp_result[:7,:] = states[1,:7]
        exp_result[7:,:] = states[1,7:]
        result = clip_passes(runtime_states)

        with tf.Session() as S:
            S.run(tf.variables_initializer(self._test_tower.variables))
            fd = self._test_tower.feed(convert_feed(clip_passes.featurizer(batch)))
            output = S.run(result, feed_dict=fd)

            self.assertEqual((13,10), output.shape)
            self.assertEqual(exp_result.shape, output.shape)
            np.testing.assert_array_equal(exp_result, output)

    def test_dynamic_passes_logarithmic_call(self):
        batch = {'graphs': tflon.data.Table(pd.DataFrame(np.array(self.graphs).reshape((-1,1))))}
        clip_passes = tflon.graph.DynamicPasses('graphs', feature='logarithmic')
        runtime_states = tf.TensorArray(dtype=tf.float32, size=6)
        states = []

        for i in range(6):
            new_state = np.random.random((13,10)).astype(np.float32)
            states.append(np.array(new_state))
            runtime_states = runtime_states.write(i, new_state)

        states = np.array(states)
        exp_result = np.zeros((13,10))
        exp_result[:7,:] = states[1,:7]
        exp_result[7:,:] = states[1,7:]
        result = clip_passes(runtime_states)

        with tf.Session() as S:
            S.run(tf.variables_initializer(self._test_tower.variables))
            fd = self._test_tower.feed(convert_feed(clip_passes.featurizer(batch)))
            output = S.run(result, feed_dict=fd)

            self.assertEqual((13,10), output.shape)
            self.assertEqual(exp_result.shape, output.shape)
            np.testing.assert_array_equal(exp_result, output)

class TestCenter(TflonTestCase):
    """
        These are all tested with disjoint nx graphs.
        Primary points to test:
            - do scorers return the right scores (and shape)
            - do selectors return the right weights
    """
    def setUp(self):
        g = nx.Graph()
        for i in range(8):
            for j in range(8):
                g.add_node((i,j))

        for i in range(8):
            for j in range(8):
                if i<6: g.add_edge((i,j), (i+1,j))
                if j<6: g.add_edge((i,j), (i,j+1))

        self.graph = tflon.graph.Graph(g)
        self.subc = sorted(nx.connected_components(self.graph._graph), key=lambda c: sorted(c)[0])
        self.subg = g.subgraph(self.subc[2])

    def test_uniform_centroids_random_selector(self):
        scorer = tflon.graph.center.uniform_scorer
        selector = tflon.graph.center.select_random

        centers = self.graph.centroids(scorer=scorer, selector=selector)
        self.assertEqual(len(centers), len(self.subc))
        self.assertEqual(len(centers), 4)

    def test_eccentricity_centroids_top_epsilon_random_selector(self):
        scorer = lambda g: tflon.graph.center.eccentricity_scorer(g, invert=True)
        selector = tflon.graph.center.select_random_top_epsilon

        centers = self.graph.centroids(scorer=scorer, selector=selector)
        cnodes = [self.graph.nodes[c] for c in centers]

        selector_bige = lambda x: tflon.graph.center.select_random_top_epsilon(x, epsilon=.20)
        centers_bige = self.graph.centroids(scorer=scorer, selector=selector_bige)
        cnodes_bige = [self.graph.nodes[c] for c in centers_bige]

        self.assertEqual(set(cnodes), {59,31,27,63})
        self.assertEqual(len(centers), 4)
        self.assertTrue(cnodes_bige[0] in [26,27,28,19,35])
        self.assertEqual(cnodes_bige[1],31)
        self.assertEqual(cnodes_bige[2],59)

    def test_cfcc_centroids_epsilon_random_selector(self):
        scorer = tflon.graph.center.cfc_centrality_scorer
        selector = tflon.graph.center.select_random_top_epsilon

        centers = self.graph.centroids(scorer=scorer, selector=selector)
        cnodes = [self.graph.nodes[c] for c in centers]

        self.assertEqual(cnodes[0], 27)
        self.assertEqual(cnodes[1], 31)
        self.assertEqual(cnodes[2], 59)
        self.assertEqual(cnodes[3], 63)
        self.assertEqual(len(centers), 4)

    def test_eccentricity_scorer(self):
        scores = tflon.graph.center.eccentricity_scorer(self.subg)

        exp = {(7, 0): 7, (7, 1): 6, (7, 2): 5, (7, 3): 4, (7, 4): 5, (7, 5): 6, (7, 6): 7}
        self.assertTrue(isinstance(scores, dict))
        self.assertEqual(set(scores.keys()), set(self.subg.nodes))
        self.assertEqual(set(scores.values()), set(exp.values()))

    def test_uniform_scorer(self):
        scores = tflon.graph.center.uniform_scorer(self.subg)
        self.assertEquals(set(scores.keys()), set(self.subg.nodes))
        self.assertEquals(set(scores.values()), {1.0})

    def test_cfcc_centroids_scorer(self):
        scores = tflon.graph.center.cfc_centrality_scorer(self.subg)

        exp = { (7, 0): 0.04761904761904764,
                (7, 1): 0.06250000000000001,
                (7, 2): 0.07692307692307693,
                (7, 3): 0.08333333333333334,
                (7, 4): 0.07692307692307694,
                (7, 5): 0.06250000000000001,
                (7, 6): 0.04761904761904764 }
        self.assertTrue(isinstance(scores, dict))
        self.assertEqual(set(scores.keys()), set(self.subg.nodes))
        keys = sorted(exp.keys())
        np.testing.assert_array_almost_equal([scores[e] for e in keys], [exp[e] for e in keys])

    def test_softmax_selector(self):
        scorer = tflon.graph.center.eccentricity_scorer
        scores = list(scorer(self.subg).values())

        selector = tflon.graph.center.select_softmax
        weights =  selector(scores)

        exp = [0.01629043, 0.32720195, 0.12037087, 0.32720195] +\
              [0.04428197, 0.12037087, 0.04428197]
        np.testing.assert_array_almost_equal(weights, exp)

    def test_min_selector(self):
        scorer = lambda x: tflon.graph.center.eccentricity_scorer(x)
        selector = tflon.graph.center.select_min

        scores = list(scorer(self.graph.graph).values())
        min_idx = np.argmin(np.array(scores))
        weights = selector(scores)

        self.assertEqual(weights[min_idx], 1)
        self.assertEqual(np.sum(weights), 1)

    def test_max_selector(self):
        scorer = tflon.graph.center.cfc_centrality_scorer
        selector = tflon.graph.center.select_max

        scores = list(scorer(self.subg).values())
        max_idx = np.argmax(np.array(scores))
        weights = selector(scores)

        self.assertEqual(weights[max_idx], 1)
        self.assertEqual(np.sum(weights), 1)

class EMedianBFSTests(TflonTestCase):
    def setUp(self):
        import pybel
        TflonTestCase.setUp(self)
        pm1 = pybel.readstring('smi', 'Oc1c(c(CO)cnc1C)CN')
        pm1.make3D()
        pm1.removeh()
        json1 = tflon.chem.pymol_to_json(pm1, coordinates=True)
        self.mol1 = tflon.chem.Molecule.from_json(json1)

        pm2 = pybel.readstring('smi', 'ON1CC[C@@H](N)C1=O')
        pm2.make3D()
        pm2.removeh()
        json2 = tflon.chem.pymol_to_json(pm2, coordinates=True)
        self.mol2 = tflon.chem.Molecule.from_json(json2)

        self.root1 = [self.mol1.center_of_system()]
        self.root2 = [self.mol2.center_of_system()]
        self.bfs = lambda g,r: g.bfs(r)

    def test_graph_tree_construction(self):
        tree = self.mol1.tree

        self.assertEqual(tree, self.mol1.tree)
        self.assertTrue(isinstance(tree, BallTree))

    def test_single_nearest_node(self):
        exp = (1.3699047978491208, 2, 1)
        np.testing.assert_array_almost_equal(self.mol1.nearest_node([1]), exp)
        np.testing.assert_array_almost_equal(self.mol1.nearest_node([1,10]), exp)

        exp = (1.5013458493382985, 9, 10)
        np.testing.assert_array_almost_equal(self.mol1.nearest_node([10]), exp)

        exp = (1.501345849, 10, 9)
        traversed = {1,2,3,4,7,8,9,11,12}
        np.testing.assert_array_almost_equal(self.mol1.nearest_node(list(traversed), exclude_args=True),exp)

    def test_node_query(self):
        exp = {1: [2], 10: []}
        res_15r = self.mol1.query_nodes([10,1], 1.5)
        self.assertEqual(exp[1], res_15r[1])
        self.assertEqual(exp[10], res_15r[10])

        exp = {1: [2], 10: [9]}
        res_2r = self.mol1.query_nodes([10,1], 2)
        self.assertEqual(exp[1], res_2r[1])
        self.assertEqual(exp[10], res_2r[10])

        exp = [[1.3699047978491208],[]]
        dist, nodes = self.mol1.query_nodes([1,10], 1.37, return_distance=True)
        np.testing.assert_array_almost_equal(exp[0], dist[0])
        np.testing.assert_array_almost_equal(exp[1], dist[1])

    def test_center_of_system(self):
        centroid = self.mol2.center_of_system()
        self.assertEqual(centroid, 7)

    def test_emedian_bfs(self):
        bfsb, bfsa, bfst = tflon.graph.Graph.bfs(self.mol1, self.root1)
        epsb, epsa, epst = tflon.graph.Graph.bfs(self.mol1.emedian_graph(1, epsilon=.05), self.root1)

        for b1, b2 in zip(bfsb, epsb):
            np.testing.assert_array_equal(b1,b2)
        for k in bfsa.keys():
            np.testing.assert_array_equal(bfsa[k], epsa[k])
        for k in bfst.keys():
            np.testing.assert_array_equal(bfst[k], epst[k])

        bfsb2, bfsa2, bfst2 = tflon.graph.Graph.bfs(self.mol2, self.root2)
        epsb2, epsa2, epst2 = tflon.graph.Graph.bfs(self.mol2.emedian_graph(1, epsilon=.15), self.root2)

        for b1, b2 in zip(bfsb2, epsb2):
            np.testing.assert_array_equal(b1,b2)
            self.assertEqual(len(epst2.keys()),7)

    def test_emedian_bfs_cross_edges(self):
        _,_,types = self.bfs(self.mol1.emedian_graph(2.4), self.root1)
        self.assertEqual([k for k in types.keys() if types[k] == 'x'], [(1,9),(7,9)])

    def test_emedian_disconnected_graph(self):
        json3 = json.dumps({"nodes": [[1, [6, 0, [0.753, 0.1563, 0.5646]]], [2, [6, -1, [0.4847, 0.0552, 1.8604]]],\
                   [3, [1, 0, [1.3205, -0.0005, 2.5461]]],\
                   [4, [6, 0, [-0.8985, 0.0226, 2.3392]]], [5, [8, 0, [-1.8923, 0.0791, 1.6493]]],\
                   [6, [7, 0, [-3.4727, -0.2334, -1.4343]]], [7, [7, 0, [-2.3747, -0.2629, -1.4112]]],\
                   [8, [1, 0, [-0.9635, -0.0615, 3.4438]]], [9, [8, 0, [0.9779, 0.2452, -0.5619]]]],\
         "edges": [[1, 2, [1]], [1, 9, [1]], [2, 3, [1]], [2, 4, [1]], [4, 5, [2]], [4, 8, [1]], [6, 7, [2]]], \
         "nprops": ["atomicnum", "formalcharge", "coordinates"], "eprops": ["order"]})
        mol3 = tflon.chem.Molecule.from_json(json3)
        vg = mol3.emedian_graph(2.5)

        root = vg.centroids()
        b,a,t = self.bfs(vg, root)

        crosses = [(1, 3), (2, 5), (2, 8), (5, 8)]
        self.assertEquals(crosses, sorted([k for k in t.keys() if t[k] == 'x']))
        self.assertEquals(b, [[4], [2, 5, 8], [1, 3, 7], [6, 9]])

