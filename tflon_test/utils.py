from __future__ import print_function
import os, sys
import shutil
import numpy as np
import tempfile
import unittest
import tensorflow as tf
from functools import wraps
import tflon

class LogisticRegressor(tflon.model.Model):
    def _model(self):
        num_desc = self.get_parameter('num_desc')
        I = self.add_input('desc', shape=[None, num_desc])
        T = self.add_target('targ', shape=[None, 1])
        net = tflon.toolkit.Dense(1)
        L = net(I)

        self.add_output( "pred", tf.nn.sigmoid(L) )
        self.add_loss( "xent", tflon.toolkit.xent_uniform_sum(T, L) )
        self.add_loss( "l2_weight", self.Lw*tflon.toolkit.l2_penalty( self.weights ) )
        self.add_loss( "l2_bias", self.Lb*tflon.toolkit.l2_penalty( self.biases ) )
        self.add_metric( "auc", tflon.toolkit.auc(T, L) )

    def _parameters(self):
        return {'num_desc':5, 'Lw':1.0, 'Lb':0.0}

class NeuralNet(tflon.model.Model):
    def _model(self):
        I = self.add_input('desc', shape=[None, 210])
        T = self.add_target('targ', shape=[None, 1])
        net = tflon.toolkit.WindowInput() | tflon.toolkit.Dense(10, activation=tf.nn.relu)
        intermediate = net(I)
        net = tflon.toolkit.Dense(1)
        L = net(intermediate)
        loss = tflon.toolkit.xent_uniform_sum(T, L, reduction=None)

        self.add_output( "intermediate", intermediate )
        self.add_output( "pred", tf.nn.sigmoid(L) )
        self.add_output( "raw_losses", loss )

        self.add_loss( "xent", tf.reduce_sum(loss) )
        self.add_loss( "l2", tflon.toolkit.l2_penalty( self.weights ) )
        self.add_metric( "auc", tflon.toolkit.auc(T, L) )

def rm_if(path):
    if os.path.exists(path):
        if os.path.isdir(path):
            shutil.rmtree(path)
        else:
            os.remove(path)

class TestTower(tflon.model.Tower):
    pass

class TflonTestCase(unittest.TestCase):
    def setUp(self):
        tflon.data.TensorQueue.DEFAULT_TIMEOUT=1000
        tflon.logging.clear_test_logs()
        tflon.logging.set_test_mode()
        tflon.system.reset()

class TflonOpTestCase(TflonTestCase):
    def setUp(self):
        TflonTestCase.setUp(self)

        self._scoper = tf.variable_scope('TestModel')
        self._test_tower = TestTower()
        self._test_builder = tflon.toolkit.build_tower( self._test_tower )
        self._scoper.__enter__()
        self._test_builder.__enter__()

    def tearDown(self):
        self._test_builder.__exit__()
        self._scoper.__exit__(None, None, None)

def random_tensor(shape):
    return np.random.random(size=shape)

class RandomGenerator(object):
    """
        An iteratable object which generates named tensors of specified shape
        The iterator also stores a history of generated tensor maps

    """
    def __init__(self, shapes, limit=None):
        """
            Parameters:
                shapes:   A dictionary of (name, shape) pairs
        """
        self.shapes = shapes
        self.limit = limit
        self.history = []

    def __iter__(self):
        """
            Returns:
                iterator: An iterator which returns dictionaries of (name, np.array) pairs forever
        """
        j = 0
        while j!=self.limit:
            d = {name: np.random.random(size=self.shapes[name]) for name in self.shapes}
            self.history.append(d)
            yield d
            j+=1

def expect_exception(exception):
    """Marks test to expect the specified exception. Call assertRaises internally"""
    def test_decorator(fn):
        def test_decorated(self, *args, **kwargs):
            self.assertRaises(exception, fn, self, *args, **kwargs)
        return test_decorated
    return test_decorator

def capture_stderr(fn):
    @wraps(fn)
    def test_decorated(self, *args, **kwargs):
        handle, path = tempfile.mkstemp()
        args = (path,) + args
        try:
            restore = sys.stderr
            sys.stderr = open(path, 'w')
            fn(self, *args, **kwargs)
        except:
            raise
        finally:
            sys.stderr.close()
            sys.stderr = restore
            if os.path.exists(path):
                os.remove(path)
            os.close(handle)
    return test_decorated


def with_tempfile(fn):
    @wraps(fn)
    def test_decorated(self, *args, **kwargs):
        handle, path = tempfile.mkstemp()
        args = (path,) + args
        try:
            fn(self, *args, **kwargs)
        except:
            raise
        finally:
            if os.path.exists(path):
                os.remove(path)
            os.close(handle)
    return test_decorated

def with_tempdir(fn):
    @wraps(fn)
    def test_decorated(self, *args, **kwargs):
        path = tempfile.mkdtemp()
        args = (path,) + args
        try:
            fn(self, *args, **kwargs)
        except:
            raise
        finally:
            if os.path.exists(path):
                shutil.rmtree(path)
    return test_decorated

def ragged(values):
    length = max(len(v) for v in values)
    return np.hstack([np.array(v + [0]*(length-len(v))).reshape(length,1) for v in values])

def hot(indices, length):
    indices = np.array(indices, dtype=np.int32)-1
    Z = np.zeros((1,length), dtype=np.int32)
    Z[0,indices] = 1
    return Z

def convert_feed(D):
    features={}
    for k, v in D.items():
        try:
            features[k] = tflon.data.convert_to_feedable(v)
        except:
            print("Error on converting feed item '%s'" % (k), file=sys.stderr)
            raise
    return features
