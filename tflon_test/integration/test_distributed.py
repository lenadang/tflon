from __future__ import print_function
import os, sys
import scipy as sp
import numpy as np
import pandas as pd
import tensorflow as tf
import tflon
import unittest
from tflon.distributed.distributed import has_distributed_capability
from tflon_test.utils import TflonTestCase, with_tempdir
from pkg_resources import resource_filename
from subprocess import check_output, CalledProcessError

WIMin = np.asarray([ 0.       ,  0.       , -1.0397   , 16.04246  ,  6.921    ,
        0.       ,  0.       ,  0.       ,  0.       ,  0.       ,
        0.       ,  0.       ,  1.       ,  0.       ,  0.       ,
        0.       ,  0.       ,  0.       ,  0.       ,  0.       ,
        0.       ,  0.       ,  0.       ,  0.       ,  0.       ,
        0.       ,  0.       ,  0.       ,  0.       ,  0.       ,
        0.       ,  0.       ,  0.       ,  0.       ,  0.       ,
        0.       ,  0.       ,  0.       ,  0.       ,  0.       ,
        0.       ,  0.       ,  0.       ,  0.       ,  0.       ,
        0.       ,  0.       ,  0.       ,  0.       ,  0.       ,
        0.       ,  0.       ,  0.       ,  0.       ,  0.       ,
        0.       ,  0.       ,  0.       ,  0.       ,  0.       ,
        0.       ,  0.       ,  0.       ,  0.       ,  0.       ,
        0.       ,  0.       ,  0.       ,  0.       ,  0.       ,
        0.       ,  0.       ,  0.       ,  0.       ,  0.       ,
        0.       ,  0.       ,  0.       ,  0.       ,  0.       ,
        0.       ,  0.       ,  0.       ,  0.       ,  0.       ,
        0.       ,  0.       ,  0.       ,  0.       ,  0.       ,
        0.       ,  0.       ,  0.       ,  0.       ,  0.       ,
        0.       ,  0.       ,  0.       ,  0.       ,  0.       ,
        0.       ,  0.       ,  0.       ,  0.       ,  0.       ,
        0.       ,  0.       ,  0.       ,  0.       ,  0.       ,
        0.       ,  0.       ,  0.       ,  0.       ,  0.       ,
        0.       ,  0.       ,  0.       ,  0.       ,  0.       ,
        0.       ,  0.       ,  0.       ,  0.       ,  0.       ,
        0.       ,  0.       ,  0.       ,  0.       ,  0.       ,
        0.       ,  0.       ,  0.       ,  0.       ,  0.       ,
        0.       ,  0.       ,  0.       ,  0.       ,  0.       ,
        0.       ,  0.       ,  0.       ,  0.       ,  0.       ,
        0.       ,  0.       ,  0.       ,  0.       ,  0.       ,
        0.       ,  0.       ,  0.       ,  0.       ,  0.       ,
        0.       ,  0.       ,  0.       ,  0.       ,  0.       ,
        0.       ,  0.       ,  0.       ,  0.       ,  0.       ,
        0.       ,  0.       ,  0.       ,  0.       ,  0.       ,
        0.05     ,  0.       ,  0.       ,  0.       ,  0.       ,
        0.       ,  0.       ,  0.       ,  0.       ,  2.19     ,
       -0.07     ,  1.       , 10.36     , 12.0107   ,  0.57     ,
        1.395    ,  0.       ,  0.       ,  0.       ,  0.       ,
        0.       ,  0.       ,  0.       ,  0.       ,  0.       ,
        0.       ,  0.       ,  0.       ,  0.       ,  0.       ,
        0.       ,  0.       ,  0.       ,  0.       ,  0.       ,
        0.       ,  0.       ,  0.       , -0.5103864,  1.       ])


WIRng = np.asarray([2.0400000e+02, 3.6156000e+02, 1.0740700e+01, 1.2466207e+03,
       3.8040540e+02, 1.2000000e+01, 1.4000000e+02, 2.5000000e+01,
       1.9200000e+02, 1.3000000e+01, 2.0000000e+00, 3.6000000e+01,
       8.8000000e+01, 1.1500000e+02, 1.1000000e+01, 1.0000000e+00,
       1.0000000e+00, 2.0000000e+00, 4.0000000e+00, 2.0000000e+00,
       2.0000000e+00, 3.3333334e-01, 4.0000000e+00, 1.0000000e+00,
       1.0000000e+00, 1.0000000e+00, 1.0000000e+00, 1.0000000e+00,
       1.0000000e+00, 1.0000000e+00, 1.0000000e+00, 1.0000000e+00,
       4.0000000e+00, 1.0000000e+00, 3.0000000e+00, 1.0000000e+00,
       4.0000000e+00, 1.0000000e+00, 1.0000000e+00, 1.0000000e+00,
       2.0000000e+00, 1.0000000e+00, 3.0000000e+00, 7.5000000e-01,
       2.0000000e+00, 6.6666669e-01, 1.0000000e+00, 3.3333334e-01,
       1.0000000e+00, 3.3333334e-01, 8.0000000e+00, 1.0000000e+00,
       4.0000000e+00, 1.0000000e+00, 3.0000000e+00, 1.0000000e+00,
       1.0000000e+00, 1.0000000e+00, 2.0000000e+00, 1.0000000e+00,
       6.0000000e+00, 1.0000000e+00, 3.0000000e+00, 5.0000000e-01,
       2.0000000e+00, 5.0000000e-01, 2.0000000e+00, 4.0000001e-01,
       1.0000000e+01, 1.0000000e+00, 3.0000000e+00, 1.0000000e+00,
       5.0000000e+00, 1.0000000e+00, 1.0000000e+00, 1.0000000e+00,
       2.0000000e+00, 1.0000000e+00, 6.0000000e+00, 1.0000000e+00,
       4.0000000e+00, 6.6666669e-01, 1.0000000e+00, 5.0000000e-01,
       1.0000000e+00, 2.5000000e-01, 1.1000000e+01, 1.0000000e+00,
       4.0000000e+00, 1.0000000e+00, 6.0000000e+00, 1.0000000e+00,
       1.0000000e+00, 1.0000000e+00, 2.0000000e+00, 1.0000000e+00,
       6.0000000e+00, 7.5000000e-01, 4.0000000e+00, 1.0000000e+00,
       2.0000000e+00, 5.0000000e-01, 2.0000000e+00, 3.3333334e-01,
       1.0000000e+00, 1.0000000e+00, 1.0000000e+00, 1.0000000e+00,
       1.0000000e+00, 1.0000000e+00, 1.0000000e+00, 1.0000000e+00,
       1.0000000e+00, 1.0000000e+00, 1.0000000e+00, 1.0000000e+00,
       1.0000000e+00, 1.0000000e+00, 1.0000000e+00, 1.0000000e+00,
       3.0000000e+00, 3.0000000e+00, 2.0000000e+00, 1.0000000e+00,
       4.0000000e+00, 4.0000000e+00, 2.0000000e+00, 3.0000000e+00,
       2.0000000e+00, 4.0000000e+00, 1.0000000e+00, 1.0000000e+00,
       1.0000000e+00, 7.0000000e+00, 4.0000000e+00, 3.0000000e+00,
       1.0000000e+00, 7.0000000e+00, 7.0000000e+00, 2.0000000e+00,
       3.0000000e+00, 2.0000000e+00, 8.0000000e+00, 1.0000000e+00,
       1.0000000e+00, 1.0000000e+00, 7.0000000e+00, 3.0000000e+00,
       5.0000000e+00, 1.0000000e+00, 8.0000000e+00, 9.0000000e+00,
       2.0000000e+00, 4.0000000e+00, 2.0000000e+00, 1.0000000e+01,
       2.0000000e+00, 1.0000000e+00, 2.0000000e+00, 7.0000000e+00,
       4.0000000e+00, 6.0000000e+00, 1.0000000e+00, 9.0000000e+00,
       1.1000000e+01, 2.0000000e+00, 4.0000000e+00, 1.0000000e+00,
       1.2000000e+01, 1.9000000e+01, 9.4999999e-01, 1.0000000e+00,
       4.0000000e+00, 1.0000000e+00, 1.0000000e+00, 1.0000000e+00,
       1.0000000e+00, 1.0000000e+00, 1.0000000e+00, 1.7900000e+00,
       3.6827240e+00, 5.0000000e+00, 7.0627999e+00, 1.1489377e+02,
       8.1999999e-01, 5.8499998e-01, 6.0000000e+00, 2.2000000e+01,
       3.6000000e+01, 3.6000000e+01, 4.0000000e+01, 6.0000000e+00,
       4.0000000e+00, 3.0000000e+00, 2.0000000e+00, 1.0000000e+00,
       8.0000000e+00, 6.0000000e+00, 3.0000000e+00, 1.0000000e+00,
       1.0000000e+00, 1.0000000e+00, 1.0000000e+00, 1.0000000e+00,
       3.0000000e+00, 4.0000000e+00, 1.0000000e+00, 1.0000000e+00,
       1.0856962e+00, 5.0000000e+00])

@unittest.skipIf(not has_distributed_capability(), "skip distributed tests: install horovod")
class DistributedTests(TflonTestCase):
    @with_tempdir
    def test_dist(self, tdir):
        ## check that the parameters of the trained sets are correct
        try:
            check_output(["mpirun", "-np", "2", "python", "-m" + "tflon_test.integration.test_distributed", tdir])

            NN = tflon.model.Model.load(tdir + "/save_disttest_0.p")
            with tf.Session() as S:
                NN.initialize()
                WIMin1, = NN.fetch("minimum").values()
                WIRng1, = NN.fetch("range").values()

            tflon.system.reset()

            NN = tflon.model.Model.load(tdir + "/save_disttest_1.p")
            with tf.Session() as S:
                NN.initialize()
                WIMin2, = NN.fetch("minimum").values()
                WIRng2, = NN.fetch("range").values()

            np.testing.assert_array_almost_equal(WIMin,WIMin1)
            np.testing.assert_array_almost_equal(WIMin,WIMin2)

            np.testing.assert_array_almost_equal(WIRng,WIRng1, decimal=4)
            np.testing.assert_array_almost_equal(WIRng,WIRng2, decimal=4)
            np.testing.assert_array_almost_equal(WIRng1,WIRng2)

        except CalledProcessError:
            self.fail(msg="")


if __name__ == "__main__":
    from .dist_model import NeuralNet

    tsv_reader = lambda fpath: pd.read_csv(fpath, sep='\t', dtype={'ID':str}).set_index('ID')

    if len(sys.argv) != 2:
        print("Usage: python test_distributed.py <tmpdir>", file=sys.stderr)
        sys.exit(1)
    tflon.data.TensorQueue.DEFAULT_TIMEOUT=1000
    config = tflon.system.configure_resources(distributed=True)

    graph = tf.Graph()
    with graph.as_default():
        NN = NeuralNet(use_gpu=True)
        trainer = tflon.distributed.DistributedTrainer( tf.train.AdamOptimizer(1e-3), iterations=1000 )
    feed = tflon.distributed.make_distributed_table_feed( resource_filename('tflon_test.data', 'distributed'),
                                                          NN.schema.map(desc=('descriptors.tsv', tsv_reader), targ=('targets.tsv', tsv_reader)),
                                                          master_table='desc' )

    with tf.Session(graph=graph, config=config):
        NN.fit( feed.shuffle(batch_size=100), trainer, restarts=2, source_tables=feed )
        NN.save(sys.argv[1] + "/save_disttest_" + str(tflon.distributed.get_rank()) + ".p")

        if tflon.distributed.is_master():
            metrics = NN.evaluate( feed )
            print(metrics['auc'])
