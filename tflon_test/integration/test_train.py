from __future__ import division
from builtins import range
import unittest
import os, sys
import scipy as sp
import numpy as np
import pandas as pd
import tensorflow as tf
import tflon
from tflon_test.utils import TflonTestCase, with_tempdir, capture_stderr, with_tempfile, LogisticRegressor, NeuralNet
from sklearn.metrics import roc_auc_score
from pkg_resources import resource_filename

def rmse(x,y):
    return np.sqrt(1./x.size * np.sum(np.square(x-y)))

class NeuralNet(tflon.model.Model):
    def _model(self):
        I = self.add_input('desc', shape=[None, 210])
        T = self.add_target('targ', shape=[None, 1])
        net = tflon.toolkit.WindowInput() | tflon.toolkit.Dense(10, activation=tf.nn.relu) | tflon.toolkit.Dense(1)
        L = net(I)

        self.add_output( "pred", tf.nn.sigmoid(L) )
        self.add_loss( "xent", tflon.toolkit.xent_uniform_sum(T, L) )
        self.add_loss( "l2", tflon.toolkit.l2_penalty( self.weights ) )
        self.add_metric( "auc", tflon.toolkit.auc(T, L) )

class NeuralNetTwoLosses(tflon.model.Model):
    def _model(self):
        I = self.add_input('desc', shape=[None, 210])
        T = self.add_target('targ', shape=[None, 1])
        I = tflon.toolkit.WindowInput()(I)

        net1 = tflon.toolkit.Dense(10, activation=tf.nn.relu) | tflon.toolkit.Dense(1)
        L1 = net1(I)
        weights_1 = self.weights[:]
        net2 = tflon.toolkit.Dense(15, activation=tf.nn.relu) | tflon.toolkit.Dense(1)
        L2 = net2(I)
        weights_2 = filter(lambda w: w not in weights_1, self.weights)

        self.add_output( "pred1", tf.nn.sigmoid(L1) )
        self.add_output( "pred2", tf.nn.sigmoid(L2) )
        self.add_loss( "xent1", tflon.toolkit.xent_uniform_sum(T, L1) )
        self.add_loss( "xent2", tflon.toolkit.xent_uniform_sum(T, L2) )
        self.add_loss( "l2_a", tflon.toolkit.l2_penalty( weights_1 ) )
        self.add_loss( "l2_b", tflon.toolkit.l2_penalty( weights_2 ) )
        self.add_metric( "auc1", tflon.toolkit.auc(T, L1) )
        self.add_metric( "auc2", tflon.toolkit.auc(T, L2) )

class EmbeddingNet(tflon.model.Model):
    def _model(self):
        inp = self.add_input('desc', shape=[None, 480], dtype=tf.int32, sparse=True)
        targ = self.add_target('targ', shape=[None, 1])

        weights = self.get_weight(shape=[480, 1], name='embedding_weights')
        embed = tf.nn.embedding_lookup_sparse(weights, inp, None, combiner='sum')
        embed = tf.concat([embed, tf.zeros((tf.shape(inp)[0]-tf.shape(embed)[0], 1))], axis=0)
        logit = embed + self.get_bias(shape=[1], name='logit_bias')

        self.add_output('out', tf.nn.sigmoid(logit))
        self.add_loss('xent', tflon.toolkit.xent_uniform_sum(targ, logit, nans=True))
        self.add_loss('l2', tflon.toolkit.l2_penalty(self.weights))
        self.add_metric('auc', tflon.toolkit.auc(targ, logit, nans=True))

class GaussianRegressionNet(tflon.model.Model):
    def _model(self):
        inp = self.add_input('desc', shape=[None, 1])
        targ = self.add_target('targ', shape=[None, self.num_targets])

        mu, sigma, loss = tflon.toolkit.Gaussian(targ)(inp)
        self.add_output('mu', mu)
        self.add_output('sigma', sigma)
        self.add_loss('loss', loss)
        self.add_metric('avg_z_score', tflon.toolkit.average_z_score(targ, mu, sigma))

    def _parameters(self):
        return {'num_targets':50}

class MultivariateGaussianRegressionNet(tflon.model.Model):
    def _model(self):
        inp = self.add_input('desc', shape=[None, 1])
        targ = self.add_target('targ', shape=[None, 10])

        mu, sigma, loss = tflon.toolkit.MultivariateGaussian(targ)(inp)
        self.add_output('mu', mu)
        self.add_output('sigma', sigma)
        self.add_loss('loss', loss)
        self.add_loss('l2', tflon.toolkit.l2_penalty(self.weights))

def make_table_feed():
    fn = resource_filename('tflon_test.data', 'cyp.tsv')
    data = pd.read_csv(fn, sep='\t', dtype={'ID':str}).set_index('ID')
    return tflon.data.TableFeed({ 'desc': tflon.data.Table(data.iloc[:,:-1]), 'targ': tflon.data.Table(data.iloc[:,-1:]) })

def generate_regression_data_fixed_variance():
    X = np.arange(0,1,0.0001).reshape((-1,1))
    m = np.random.random(size=(50,)).reshape((1,-1)) * 2000 - 1000
    b = np.random.random(size=(50,)).reshape((1,-1)) * 2000 - 1000
    mu = np.dot(X,m) + b
    sigma = np.random.uniform(10,100,size=(50,))
    noise = np.random.normal(0,sigma,size=mu.shape)
    Y = mu + noise

    return X, Y, mu, sigma

def generate_regression_data_with_fixed_covariance(num_points, num_vars, magnitude=100, noise=1, covariance=1, midcut=0.1):
    x_min=0.
    x_max=1.
    x=np.arange(x_min,x_max,(x_max-x_min)/float(num_points))
    df = np.zeros((len(x), num_vars))
    A = np.random.normal(0,1,(num_vars,num_vars))
    A[np.logical_and(A>=-midcut,A<=0)] = -midcut
    A[np.logical_and(A>0,A<=midcut)] = midcut
    A = np.diag( noise*np.diag(A) ) + covariance*np.tril(A,k=1)
    cov = np.matmul(np.transpose(A),A)

    assert np.all(np.linalg.eig(cov)>=0), "Not positive semi-definite!"

    m = np.random.randint(0, magnitude, size=(num_vars,)) - magnitude//2
    b = np.random.randint(0, magnitude, size=(num_vars,)) - magnitude//2
    func = lambda x:m*x+b
    e = np.random.multivariate_normal([0 for i in range(num_vars)], cov, size=(len(x),))
    for i in range(0, len(x)):
        df[i,:] = func(x[i]) + e[i,:]

    return x, df, m, b, cov

class LayerTests(TflonTestCase):
    def test_restarts_retain_parameters(self):
        trainer = tflon.train.TFTrainer( tf.train.AdamOptimizer(0.01), iterations=25 )
        M = NeuralNet( )
        mn, = [v for v in M.variables if 'minimum' in v.name]
        rng, = [v for v in M.variables if 'range' in v.name]

        data = make_table_feed()
        with tf.Session() as S:
            M.fit( data.shuffle(batch_size=500), trainer, restarts=2, processes=1, source_tables=data )

            mn_val, rng_val = S.run( [mn, rng] )

            mn_exp = data['desc'].min()
            mx = data['desc'].max()
            rng_exp = mx-mn_exp
            rng_exp[rng_exp==0]=1
            np.testing.assert_array_almost_equal(mn_val, mn_exp)
            np.testing.assert_array_almost_equal(rng_val, rng_exp, decimal=4)

    def test_gaussian_regression_layer_with_fixed_variance(self):
        X, Y, mu, sigma = generate_regression_data_fixed_variance()

        trainer = tflon.train.SciPyTrainer(iterations=200)
        model = GaussianRegressionNet(params={'Gaussian_0/fixed_variance': True})
        data = tflon.data.TableFeed({'desc':tflon.data.Table(pd.DataFrame(X)),
                                     'targ':tflon.data.Table(pd.DataFrame(Y))})
        with tf.Session():
            model.fit(data, trainer, restarts=1)

            outputs = model.infer(data)

            rvals = np.array([rmse(outputs['mu'][:,i], mu[:,i]) for i in range(50)])
            self.assertTrue(np.all(rvals<3))
            np.testing.assert_allclose(outputs['sigma'][0,:], sigma, rtol=0.05)

            metrics = model.evaluate(data)
            np.testing.assert_almost_equal(0.79, metrics['avg_z_score'], decimal=2)

    @unittest.skipIf(True, "Skipping multivariate regression test (waiting for implementation)")
    def test_multivariate_gaussian_regression_layer_with_fixed_covariance(self):
        X, Y, m, b, cov = generate_regression_data_with_fixed_covariance(10000, 10)
        mu = np.dot(X.reshape((-1,1)), m.reshape((1,-1))) + b

        trainer = tflon.train.SciPyTrainer(iterations=200)
        model = MultivariateGaussianRegressionNet(params={'MultivariateGaussian_0/fixed_covariance': True})
        data = tflon.data.TableFeed({'desc':tflon.data.Table(pd.DataFrame(X)),
                                     'targ':tflon.data.Table(pd.DataFrame(Y))})
        with tf.Session():
            model.fit(data, trainer, restarts=1)
            outputs = model.infer(data)

            assert np.all(np.linalg.eig(outputs['sigma'])>0), "Output covariance not positive semi-definite"

            rvals = [rmse(outputs['mu'][:,i], mu[:,i]) for i in range(10)]
            np.testing.assert_allclose(rvals, np.full((10,), 0), rtol=0.0001)


class TestSciPyTrainer(TflonTestCase):
    @classmethod
    def setUpClass(cls):
        cls.feed = make_table_feed()

    def setUp(self):
        super(TestSciPyTrainer, self).setUp()
        self.S = tf.Session()

    def test_logistic_regression_standard_dataset(self):
        model = LogisticRegressor(num_desc=2, num_targets=1, Lw=0.5, Lb=0.5)
        trainer = tflon.train.SciPyTrainer(iterations=200, options={'ftol':1e-9, 'gtol':1e-9})

        W,B = np.array([[0.03844482], [0.03101855]]), np.array([-3.89977794])

        df = pd.read_csv(resource_filename('tflon_test.data', 'marks.txt'), names=['X0', 'X1', 'y'])
        batch = tflon.data.TableFeed({
            'desc': tflon.data.Table(df.iloc[:,:2]),
            'targ': tflon.data.Table(df.iloc[:,2:])})

        with self.S.as_default():
            model.fit(batch, trainer, restarts=3)
            pred = model.infer(batch, query='pred')
            auc = model.evaluate(batch, query='auc')

            acc = np.sum((batch['targ'].data.values==1)==(pred>0.5)) / pred.shape[0]
            np.testing.assert_allclose([acc,auc], [0.89, 0.97], rtol=0.03)

            w,b = self.S.run([model.weights[0], model.biases[0]])
            np.testing.assert_allclose(W, w, atol=0.0001)
            np.testing.assert_allclose(B, b, atol=0.01)

    def test_l2_regularized_weights_with_zero_data_should_decay(self):
        batch_size=100000
        trainer = tflon.train.SciPyTrainer(iterations=200, options={'ftol':1e-9, 'gtol':1e-9})
        model = LogisticRegressor(num_desc=8)

        risk = [0.4, -0.9, 1.5, 3.4, -2.1]
        baseline = 0.5
        data = np.random.randint(0,2,size=(batch_size, 5))
        probs = 1./(1.+np.exp(-(np.sum(data*risk, axis=1, keepdims=True) + baseline)))
        samples = np.random.random((batch_size,1))
        targets = np.zeros((batch_size,1))
        targets[samples<probs]=1
        data = np.hstack([data, np.random.randint(0,2,size=(batch_size,3))])

        with self.S.as_default():
            batch = tflon.data.TableFeed({
                'desc': tflon.data.Table(pd.DataFrame(data)),
                'targ': tflon.data.Table(pd.DataFrame(targets))
            })
            model.fit(batch, trainer)

            w,b = self.S.run([model.weights[0], model.biases[0]])
            np.testing.assert_allclose(risk + [0, 0, 0], w.reshape((-1,)), atol=0.1)
            np.testing.assert_allclose(baseline, b, rtol=0.1)

    def test_logistic_regressor_should_recover_synthetic_data_weights(self):
        batch_size=100000
        trainer = tflon.train.SciPyTrainer(iterations=200, options={'ftol':1e-9, 'gtol':1e-9})
        model = LogisticRegressor()

        risk = [0.4, -0.9, 1.5, 3.4, -2.1]
        baseline = 0.5
        data = np.random.randint(0,2,size=(batch_size, 5))
        probs = 1./(1.+np.exp(-(np.sum(data*risk, axis=1, keepdims=True) + baseline)))
        samples = np.random.random((batch_size,1))
        targets = np.zeros((batch_size,1))
        targets[samples<probs]=1


        with self.S.as_default():
            desc = pd.DataFrame(data)
            targ = pd.DataFrame(targets)

            batch = tflon.data.TableFeed({
                'desc': tflon.data.Table(pd.DataFrame(data)),
                'targ': tflon.data.Table(pd.DataFrame(targets))
            })
            model.fit(batch, trainer)

            w,b = self.S.run([model.weights[0], model.biases[0]])
            np.testing.assert_allclose(risk, w.reshape((-1,)), atol=0.1)
            np.testing.assert_allclose(baseline, b, rtol=0.1)

    def test_scipy_lbfgs(self):
        trainer = tflon.train.SciPyTrainer(iterations=100)
        M = NeuralNet()

        with self.S.as_default():
            M.fit(self.feed, trainer, restarts=2)

            auc = M.evaluate(self.feed, query='auc')
            self.assertTrue(auc > 0.75, "AUC was not good: %.2f" % (auc))

    def test_scipy_sparse_gradients(self):
        trainer = tflon.train.SciPyTrainer(iterations=150)
        pains = tflon.data.read_parquet( resource_filename('tflon_test.data', 'sample_shard/pains.pq') )
        codes = pd.DataFrame( data=list(range(480)) )
        targets = tflon.data.read_parquet( resource_filename('tflon_test.data', 'sample_shard/targets.pq') ).iloc[:,-1:]
        M = EmbeddingNet()

        feed = tflon.data.TableFeed({ 'desc': tflon.data.SparseIndexTable( pains, codes ),
                                      'targ': tflon.data.Table( targets ) })

        with self.S.as_default():
            M.fit(feed, trainer, restarts=2)

            metrics = M.evaluate(feed)
            self.assertFalse(np.isnan(metrics['auc']))

    def test_termination_callback(self):
        trainer = tflon.train.SciPyTrainer(iterations=100, hooks=[lambda s, l: l<1.3e3])
        M = NeuralNet()

        with self.S.as_default():
            M.fit(self.feed, trainer, restarts=1)

            step, loss = self.S.run([tf.train.get_or_create_global_step(), M.loss], feed_dict=M.feed(self.feed))
            self.assertTrue(loss < 1.3e3)
            self.assertTrue(step < 75)

class TestTFTrainer(TflonTestCase):
    @classmethod
    def setUpClass(cls):
        cls.feed = make_table_feed()

    def setUp(self):
        super(TestTFTrainer, self).setUp()
        self.S = tf.Session()

    @with_tempfile
    def test_timeline_profiler(self, tfn):
        trainer = tflon.train.TFTrainer( tf.train.AdamOptimizer(1e-3), iterations=100, gpu_profile_file=tfn)
        M = NeuralNet()

        with self.S.as_default():
            M.fit(self.feed, trainer, restarts=1)
            self.assertTrue( os.path.exists(tfn) )

    def test_tfopt_adam_with_partial_losses(self):
        trainer1 = tflon.train.TFTrainer( tf.train.AdamOptimizer(1e-2), iterations=100 )
        trainer2 = tflon.train.TFTrainer( tf.train.AdamOptimizer(1e-2), iterations=100 )
        M = NeuralNetTwoLosses( )

        with self.S.as_default():
            M.set_loss('xent1', 'l2_a')
            M.fit( self.feed.shuffle(batch_size=500), trainer1, restarts=2, processes=1, source_tables=self.feed )
            outp = M.infer( self.feed, query='pred1' )
            auc_true = roc_auc_score( self.feed['targ'].as_matrix(), outp )
            auc = M.evaluate( self.feed, query='auc1' )
            np.testing.assert_almost_equal(auc_true, auc, decimal=4)
            self.assertTrue( auc > 0.75, "AUC was not good: %.2f" % (auc) )

            M.set_loss('xent2', 'l2_b')
            M.fit( self.feed.shuffle(batch_size=500), trainer2, restarts=2, processes=1, source_tables=self.feed )
            outp = M.infer( self.feed, query='pred2' )
            auc_true = roc_auc_score( self.feed['targ'].as_matrix(), outp )
            auc = M.evaluate( self.feed, query='auc2' )
            np.testing.assert_almost_equal(auc_true, auc, decimal=4)
            self.assertTrue( auc > 0.75, "AUC was not good: %.2f" % (auc) )

    def test_tfopt_adam(self):
        trainer = tflon.train.TFTrainer( tf.train.AdamOptimizer(0.01), iterations=400 )
        M = NeuralNet( )

        data = self.feed
        with self.S.as_default():
            M.fit( data, trainer, restarts=2 )

            outp = M.infer( data )
            auc = roc_auc_score( data['targ'].as_matrix(), outp['pred'] )
            self.assertTrue( auc > 0.75, "AUC was not good: %.2f" % (auc) )

    @unittest.skipIf(True, "Skip graph locking test, which breaks session tensor deletion")
    def test_tfopt_adam_lock_run(self):
        trainer = tflon.train.TFTrainer( tf.train.AdamOptimizer(0.01), iterations=400 )
        M = NeuralNet( )

        data = self.feed
        with self.S.as_default():
            M.fit( data.shuffle(batch_size=500), trainer, restarts=2, source_tables=data, lock=True )

            outp = M.infer( data )
            auc = roc_auc_score( data['targ'].as_matrix(), outp['pred'] )
            self.assertTrue( auc > 0.75, "AUC was not good: %.2f" % (auc) )

    def test_tfopt_adam_minibatch(self):
        trainer = tflon.train.TFTrainer( tf.train.AdamOptimizer(0.01), iterations=400 )
        M = NeuralNet( )

        data = self.feed
        with self.S.as_default():
            M.fit( data.shuffle(batch_size=500), trainer, restarts=2, processes=1, source_tables=self.feed )

            outp = M.infer( data )
            auc = roc_auc_score( data['targ'].as_matrix(), outp['pred'] )
            self.assertTrue( auc > 0.75, "AUC was not good: %.2f" % (auc) )

    def test_multithreading(self):
        trainer = tflon.train.TFTrainer( tf.train.AdamOptimizer(0.01), iterations=400 )
        M = NeuralNet( )

        data = self.feed
        with self.S.as_default():
            M.fit( data.shuffle(batch_size=500), trainer, restarts=2, processes=2, source_tables=self.feed )

            outp = M.infer( data )
            auc = roc_auc_score( data['targ'].as_matrix(), outp['pred'] )
            self.assertTrue( auc > 0.75, "AUC was not good: %.2f" % (auc) )

    def test_multiprocessing(self):
        trainer = tflon.train.TFTrainer( tf.train.AdamOptimizer(0.01), iterations=400 )
        M = NeuralNet( )

        data = self.feed
        with self.S.as_default():
            M.fit( data.shuffle(batch_size=500), trainer, restarts=2, processes=2, pipeline='multiprocessing', source_tables=self.feed )

            outp = M.infer( data )
            auc = roc_auc_score( data['targ'].as_matrix(), outp['pred'] )
            self.assertTrue( auc > 0.75, "AUC was not good: %.2f" % (auc) )

    def test_pyro(self):
        trainer = tflon.train.TFTrainer( tf.train.AdamOptimizer(0.01), iterations=400 )
        M = NeuralNet( )

        data = self.feed
        with self.S.as_default():
            M.fit( data.shuffle(batch_size=500), trainer, restarts=1, processes=2, pipeline='pyro', source_tables=self.feed )

            outp = M.infer( data )
            auc = roc_auc_score( data['targ'].as_matrix(), outp['pred'] )
            self.assertTrue( auc > 0.75, "AUC was not good: %.2f" % (auc) )

    def test_termination_callback(self):
        trainer = tflon.train.TFTrainer( tf.train.AdamOptimizer(0.01), iterations=400, hooks=[lambda s, l: l<70] )
        M = NeuralNet()
        it = self.feed.shuffle(batch_size=500)

        with self.S.as_default():
            M.fit( it, trainer, restarts=1, processes=1, source_tables=self.feed )

            step, loss = self.S.run([tf.train.get_or_create_global_step(), M.loss], feed_dict=M.feed(next(it)))
            self.assertTrue(loss < 120)
            self.assertTrue(step < 250)

    @with_tempdir
    def test_summary_hook_activations(self, tdir):
        model = NeuralNet()
        summary_hook = tflon.train.SummaryHook(tdir, frequency=50, summarize_activations=True)
        trainer = tflon.train.TFTrainer( tf.train.AdamOptimizer(0.01), iterations=200, hooks=[summary_hook] )

        with self.S.as_default():
            model.fit( self.feed.shuffle(batch_size=500), trainer, restarts=1, processes=1, source_tables=self.feed )

        logfile = tflon.summary.get_latest_file(tdir)
        steps, values = tflon.summary.search_summary(logfile, "activations/", "losses/" )

        self.assertEqual(steps, [50,100,150,200])
        self.assertEqual(len(values.keys()), 6)
        for k in values:
            self.assertEqual(sorted(values[k].keys()), [50,100,150,200])

    @with_tempdir
    def test_summary_hook(self, tdir):
        model = NeuralNet()
        summary_hook = tflon.train.SummaryHook(tdir, frequency=50, summarize_trainables=True, summarize_gradients=True, summarize_losses=True)
        trainer = tflon.train.TFTrainer( tf.train.AdamOptimizer(0.01), iterations=200, hooks=[summary_hook] )

        with self.S.as_default():
            model.fit( self.feed.shuffle(batch_size=500), trainer, restarts=1, processes=1, source_tables=self.feed )

        logfile = tflon.summary.get_latest_file(tdir)
        steps, values = tflon.summary.search_summary(logfile, 'losses/', 'gradients/', 'losses/', 'trainables/' )

        self.assertEqual(steps, [50,100,150,200])
        self.assertEqual(len(values.keys()), 11)
        for k in values:
            self.assertEqual(sorted(values[k].keys()), [50,100,150,200])

    @with_tempdir
    def test_summary_monitors_resources(self, tdir):
        model = NeuralNet()
        summary_hook = tflon.train.SummaryHook(tdir, frequency=50)
        trainer = tflon.train.TFTrainer( tf.train.AdamOptimizer(0.01), iterations=200, hooks=[summary_hook] )

        with self.S.as_default():
            model.fit( self.feed.shuffle(batch_size=500), trainer, restarts=1, processes=1, source_tables=self.feed )

        logfile = tflon.summary.get_latest_file(tdir)
        args = [logfile, 'resources/memory_used_MB', 'resources/cpu_utilization_pct', 'resources/queue_utilization_pct']
        if tflon.system.num_gpus()>0:
            args += ['resources/gpu_%d_utilization_pct' % (i) for i in range(tflon.system.num_gpus())]
            args += ['resources/gpu_%d_memory_pct' % (i) for i in range(tflon.system.num_gpus())]
        steps, values = tflon.summary.search_summary(*args)

        self.assertEqual(steps, [50,100,150,200])
        self.assertEqual(len(values.keys()), len(args)-1)
        for k in values:
            self.assertEqual(sorted(values[k].keys()), [50,100,150,200])

    def test_learning_rate_decay(self):
        lrd_hook = tflon.train.LearningRateDecayHook(1e-2, 1e-1, 75)

        learning_rates = []
        def record_learning_rate(step, lr):
            learning_rates.append((step, lr))
            return False

        class RecordLearningRateHook(tflon.train.Hook):
            def call(self, step, loss):
                return tf.numpy_function(record_learning_rate, [step, lrd_hook.learning_rate], [tf.bool])

        trainer = tflon.train.TFTrainer( tf.train.AdamOptimizer(0.01), iterations=200, hooks=[lrd_hook, RecordLearningRateHook(frequency=1)] )
        model = NeuralNet()
        with self.S.as_default():
            model.fit( self.feed.shuffle(batch_size=500), trainer, restarts=1, processes=1, source_tables=self.feed )

        assert len(learning_rates) == 200
        exp = [1e-2]*74 + [1e-3]*75 + [1e-4]*51
        actual = [lr for _, lr in learning_rates]
        np.testing.assert_array_almost_equal(exp, actual)

    @with_tempdir
    def test_ckpt(self, tdir):
        model = NeuralNet()
        steps = []
        def record_hook(s, l):
            steps.append(s)
            return False
        trainer = tflon.train.TFTrainer( tf.train.AdamOptimizer(0.01), iterations=100, checkpoint=(tdir, 50), hooks=[record_hook] )
        with self.S.as_default() as S:
            model.fit( self.feed.shuffle(batch_size=500), trainer, restarts=1, processes=1, source_tables=self.feed )
            gs = S.run(tf.train.get_or_create_global_step())

            opt_vars = trainer.variables
            opt_saved_vars = {k.name:v for k,v in zip(opt_vars, S.run(opt_vars))}
            model_vars = model.trainables
            model_saved_vars = {k.name:v for k,v in zip(model_vars, S.run(model_vars))}

        self.assertEqual(gs, 100)
        self.assertEqual(set(fn.rsplit('.',1)[0] for fn in os.listdir(tdir)), {'checkpoint', 'ckpt.50', 'ckpt.100'})
        self.assertEqual(list(range(1,101)), steps)

        tflon.system.reset()
        model = NeuralNet()

        trainer = tflon.train.TFTrainer( tf.train.AdamOptimizer(0.01), iterations=200, checkpoint=(tdir, 50), resume=True )
        with tf.Session() as S:
            model.initialize()
            trainer.setup(model)
            trainer._restore()

            opt_vars = trainer.variables
            opt_restored_vars = {k.name:v for k,v in zip(opt_vars, S.run(opt_vars))}

            model_vars = model.trainables
            model_restored_vars = {k.name:v for k,v in zip(model_vars, S.run(model_vars))}

            self.assertEqual(set(opt_saved_vars.keys()), set(opt_restored_vars.keys()))
            for k in opt_saved_vars:
                np.testing.assert_array_almost_equal(opt_saved_vars[k], opt_restored_vars[k])

            self.assertEqual(set(model_saved_vars.keys()), set(model_restored_vars.keys()))
            for k in model_saved_vars:
                np.testing.assert_array_almost_equal(model_saved_vars[k], model_restored_vars[k])

        tflon.system.reset()
        model = NeuralNet()

        resume_steps = []
        def record_hook(s, l):
            resume_steps.append(s)
            return False

        trainer = tflon.train.TFTrainer( tf.train.AdamOptimizer(0.01), iterations=200, checkpoint=(tdir, 50), resume=True, hooks=[record_hook] )
        with tf.Session() as S:
            model.fit( self.feed.shuffle(batch_size=500), trainer, restarts=1, processes=1, source_tables=self.feed )
            gs = S.run(tf.train.get_or_create_global_step())

        self.assertEqual(gs, 200)
        self.assertEqual(set(fn.rsplit('.',1)[0] for fn in os.listdir(tdir)), {'checkpoint', 'ckpt.50', 'ckpt.100', 'ckpt.150', 'ckpt.200'})
        self.assertEqual(list(range(101,201)), resume_steps)

    @with_tempdir
    def test_gradient_clipping_optimizer(self, tdir):
        model = NeuralNet()
        summary_hook = tflon.train.SummaryHook(tdir, frequency=50, summarize_trainables=False, summarize_gradients=True, summarize_losses=False)
        trainer = tflon.train.TFTrainer( tflon.train.GradientClippingOptimizer( tf.train.AdamOptimizer(0.01), clip_value=5.0 ), iterations=200, hooks=[summary_hook] )

        with self.S.as_default():
            model.fit( self.feed.shuffle(batch_size=500), trainer, restarts=1, processes=1, source_tables=self.feed )
        logfile = tflon.summary.get_latest_file(tdir)
        steps, values = tflon.summary.search_summary(logfile, 'gradients/')

        for k in values:
            for s in steps:
                self.assertTrue(values[k][s]<5.0, "Gradient %s at step %d too large" % (k, s))

    def test_curriculum_training_iterator(self):
        model = NeuralNet()
        batch_sizes = []
        def append_batch_size(shape):
            batch_sizes.append(shape[0])
            return False

        class LogBatchSizeHook(tflon.train.Hook):
            def call(self, step, loss):
                return tf.numpy_function(append_batch_size, [tf.shape(model.inputs['desc'])], [tf.bool])

        curriculum = tflon.train.FixedIntervalCurriculum([self.feed.shuffle(batch_size=100), self.feed.shuffle(batch_size=200)], frequency=100)
        trainer = tflon.train.TFTrainer( tf.train.AdamOptimizer(0.01), iterations=300, hooks=[LogBatchSizeHook(frequency=1), curriculum] )
        with self.S.as_default():
            model.fit( curriculum.iterate(), trainer, restarts=1, processes=4, source_tables=self.feed )

        level1_count = np.sum([bs==100 for bs in batch_sizes])
        level2_count = np.sum([bs==200 for bs in batch_sizes])

        self.assertTrue(level1_count>100)
        self.assertTrue(level2_count>0)
        self.assertTrue(level1_count>level2_count)

    @capture_stderr
    def test_progress_bar_turns_off_when_no_tty(self, stderr_fn):
        trainer = tflon.train.TFTrainer( tf.train.AdamOptimizer(0.01), iterations=400 )
        M = NeuralNet( )

        with self.S.as_default():
            M.fit( self.feed.shuffle(batch_size=500), trainer, restarts=1, processes=1, source_tables=self.feed )

        sys.stderr.flush()
        self.assertEqual(open(stderr_fn, 'r').read().strip(), "")

class TestSubnetworkTFTrainer(TflonTestCase):

    @classmethod
    def setUpClass(cls):
        cls.feed = make_table_feed()

    def setUp(self):
        super(TestTFTrainer, self).setUp()
        self.S = tf.Session()

    def test_model_subnetwork_training(self):
        trainer = tflon.train.TFTrainer( tf.train.AdamOptimizer(1e-3), iterations=100)
        M = NeuralNet()

        with self.S.as_default():
            M.fit(self.feed, trainer)
            s1 = M.snapshot()
        trainer = tflon.train.SubnetworkTFTrainer(optimizer=tf.train.AdamOptimizer(1e-3), iterations=100, freeze_variables=M.weights)
        with self.S.as_default():
            M.fit(self.feed, trainer,reset=False)
            s2 = M.snapshot()
        for key in s1.values:
            if "weight" in key:
                self.assertTrue(s1.values[key]==s2.values[key], "Weights changed while only training biases in 2nd stage: %s" %(key))
            if "bias" in key:
                self.assertTrue(s1.values[key]!=s2.values[key], "Biases unchanged while only training biases in 2nd stage: %s" %(key))

