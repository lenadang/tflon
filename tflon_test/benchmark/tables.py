from builtins import range
from __future__ import print_function
import tflon
import time
import types
from functools import wraps
import numpy as np
import pandas as pd

def times(count):
    def wrapper(fn):
        @wraps(fn)
        def wrapped(*args, **kwargs):
            for i in range(count):
                fn(*args, **kwargs)
        return wrapped
    return wrapper

class Benchmark(object):
    class __metaclass__(type):
        __inheritors__ = list()

        def __new__(meta, name, bases, dct):
            klass = type.__new__(meta, name, bases, dct)
            for base in klass.mro()[1:-1]:
                meta.__inheritors__.append(klass)
            return klass

    @classmethod
    def runAll(cls):
        for klass in cls.__inheritors__:
            klass().run()

    def run(self):
        self.setUpClass()
        for key in dir(self):
            obj = getattr(self, key)
            if isinstance(obj, type(self.run)) and \
               key.startswith('benchmark_'):
                self.setUp()
                start = time.time()
                obj()
                end = time.time()
                print("%-20s %-50s %.4f" % (self.__class__.__name__, key[10:], end-start))
                self.tearDown()
        self.tearDownClass()

    @classmethod
    def setUpClass(self):
        pass

    def setUp(self):
        pass

    def tearDown(self):
        pass

    @classmethod
    def tearDownClass(self):
        pass

class RaggedTableBenchmarks(Benchmark):
    @classmethod
    def create_ragged_table(cls):
        rows = 64
        max_entries = 50
        entry_size = 10
        nested_data = pd.DataFrame(columns=['ID', 'data', 'lengths']).set_index('ID')
        for i in range(20):
            num_entries = np.random.randint(0, max_entries)
            if num_entries==0:
                row_data = None
            else:
                row_data = np.random.random((num_entries,)).tolist()
                partitions = []
                p = 0
                while p<num_entries:
                    v = np.random.randint(2,entry_size)
                    partitions.append(v + (min(num_entries-(p+v), 0)))
                    p += v
                nested_data.loc[i] = [row_data, partitions]
        return tflon.data.RaggedTable(nested_data)

    @classmethod
    def create_ragged_index_table(cls):
        data = cls.ragged_table.data.copy()
        data['data'] = data['data'].apply(lambda v: np.random.randint(0, 10, size=(len(v),)).tolist())
        data['increments'] = data['data'].apply(max)
        return tflon.graph.RaggedIndexTable(data)

    @classmethod
    def create_ragged_reduce_table(cls):
        data = cls.ragged_table.data.copy()
        data['data'] = data['data'].apply(lambda v: np.random.randint(0, 10, size=(len(v),)).tolist())
        return tflon.graph.RaggedReduceTable(data)

    @classmethod
    def setUpClass(cls):
        cls.ragged_table = cls.create_ragged_table()
        cls.ragged_index_table = cls.create_ragged_index_table()
        cls.ragged_reduce_table = cls.create_ragged_reduce_table()

    @times(100)
    def benchmark_ragged_table_matrix_conversions(self):
        self.ragged_table.as_matrix()

    @times(100)
    def benchmark_ragged_index_table_matrix_conversions(self):
        self.ragged_index_table.as_matrix()

    @times(100)
    def benchmark_ragged_reduce_table_matrix_conversions(self):
        self.ragged_reduce_table.as_matrix()

if __name__=='__main__':
    Benchmark.runAll()
