#!/bin/bash
set -e

#pushd tflon_core/cc
#TF_INC=$(python -c 'import tensorflow as tf; print(tf.sysconfig.get_include())')
#TF_CFLAGS=$(python -c 'import tensorflow as tf; print(" ".join(tf.sysconfig.get_compile_flags()))')
#TF_LFLAGS=$(python -c 'import tensorflow as tf; print(" ".join(tf.sysconfig.get_link_flags()))')
#g++ -std=c++11 -shared -shared tflon_ops.cc -o tflon_ops.so -fPIC -I $TF_INC -O2 $TF_CFLAGS $TF_LFLAGS
#popd
#
mkdir -p tflon
#mkdir -p tflon/lib
#touch tflon/lib/__init__.py
#mv tflon_core/cc/*.so tflon/lib

for fn in `ls tflon_core/python`
do
	if [ ! -e tflon/$fn ]
	then
		ln -sf ../tflon_core/python/$fn tflon/
	fi
done
