import os
from setuptools import Extension

class TensorflowExtension(Extension):
    def __init__(self, module, sources):
        import tensorflow as tf
        os.environ["CC"] = "g++"

        Extension.__init__(self, module,
                           sources=sources,
                           include_dirs=[tf.sysconfig.get_include()],
                           extra_compile_args=["-std=c++11", "-shared", '-fPIC']+\
                                              tf.sysconfig.get_compile_flags()+\
                                              tf.sysconfig.get_link_flags())
