Usage Examples
==============

Basic neural network
--------------------
.. literalinclude:: ../tflon_core/python/examples/basic.py

Distributed training
--------------------
.. literalinclude:: ../tflon_core/python/examples/distributed.py

Molecule neighborhood convolution
---------------------------------
.. literalinclude:: ../tflon_core/python/examples/neighborhood.py
