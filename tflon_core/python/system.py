from __future__ import division
from threading import Thread
from tflon import toolkit, logging
from GPUtil import getGPUs
import tensorflow as tf
import psutil
import time

def configure_resources(tensorflow_cpus=None, reserved_cpus=0, allocated_gpus=None, distributed=False):
    distributed_mode = distributed
    from tflon import distributed
    """
        Initialize tflon to reserve appropriate GPUs and CPUs. Automatically handles dividing resources among distributed processes on the same machine.

        Keyword Arguments:
            tensorflow_cpus (int): Limit the number of CPUs used by tensorflow, if None, use all available resources (default=None)
            reserved_cpus (int):   Number of cpus to reserve for featurizer processes, setting this one can help avoid priority deadlocks
                                   in which tensorflow outcompetes the featurizer processes for cpu time (default=0)
            allocated_gpus (str):  List of available GPUs to allocate to tensorflow, ignored in distributed mode (default=None)
            distributed (bool):    Whether to initialize MPI/horovod and distribute cpu resources (default=False)
    """
    cpu_count = num_cpus() if tensorflow_cpus is None else tensorflow_cpus
    if distributed_mode:
        distributed.init_distributed_resources()
        cpu_count = cpu_count // distributed.num_local_processes()
    available_cpus = cpu_count-reserved_cpus
    config = tf.ConfigProto(device_count={'CPU': available_cpus},
                                          intra_op_parallelism_threads=available_cpus,
                                          inter_op_parallelism_threads=available_cpus,
                                          allow_soft_placement=True)
    if num_gpus() > 0:
        config.gpu_options.allow_growth = True
        if allocated_gpus is not None:
            config.gpu_options.visible_device_list = allocated_gpus
        if distributed_mode:
            config.gpu_options.visible_device_list = str(distributed.get_local_rank())
    return config

def get_available_gpus():
    try:
        return [gpu.name for gpu in getGPUs()]
    except OSError:
        return []

def num_gpus():
    return len(get_available_gpus())

def gpu_usage(i):
    if SystemMonitor.instance is None: return 0.
    return float(SystemMonitor.instance.gpu_utilization[i])

def gpu_memory(i):
    if SystemMonitor.instance is None: return 0.
    return float(SystemMonitor.instance.gpu_memory[i])

def reset():
    toolkit.reset_name_scope_IDs()
    tf.reset_default_graph()

def memory_usage():
    if SystemMonitor.instance is None: return 0.
    return float(SystemMonitor.instance.cpu_memory[0])

def num_cpus():
    p = psutil.Process()
    return len(p.cpu_affinity())

def cpu_usage():
    if SystemMonitor.instance is None: return 0.
    return float(SystemMonitor.instance.cpu_utilization[0])

class SystemMonitor(Thread):
    instance=None
    DELAY=5

    def __init__(self):
        super(SystemMonitor, self).__init__()
        self._stopped = False
        self._delay = SystemMonitor.DELAY
        self._num_cpus = num_cpus()
        self._num_gpus = num_gpus()
        self.gpu_utilization = [0.] * self._num_gpus
        self.gpu_memory = [0.] * self._num_gpus
        self.cpu_utilization = [0.]
        self.cpu_memory = [0.]
        p = psutil.Process()
        self.processes = {p.pid: p}
        self.parent = p
        SystemMonitor.instance=self

    def __enter__(self):
        self.start()

    def __exit__(self, *args):
        self.stop()

    def run(self):
        inc = 0
        while not self._stopped:
            if inc % self._delay==0:
                for c in self.parent.children():
                    if c.pid not in self.processes:
                        logging.debug("Monitoring child process %d", c.pid)
                        self.processes[c.pid] = c

                cpu_mem = 0
                cpu_util = 0
                pids = self.processes.keys()
                for pid in pids:
                    p = self.processes[pid]
                    try:
                        with p.oneshot():
                            cpu_mem += p.memory_info()[0] / float(2 ** 20)
                            cpu_util += p.cpu_percent()
                    except Exception as e:
                        logging.debug("Stop monitoring child process %d: %s", pid, str(e))
                        del self.processes[pid]

                self.cpu_memory[0] = cpu_mem
                self.cpu_utilization[0] = cpu_util

                if self._num_gpus>0:
                    for i, G in enumerate(getGPUs()):
                        self.gpu_utilization[i] = G.load
                        self.gpu_memory[i] = G.memoryUtil
                inc = 0
            inc+=1
            time.sleep(1)

    def stop(self):
        self._stopped = True
