from __future__ import print_function
import warnings
with warnings.catch_warnings():
    warnings.filterwarnings("ignore",category=DeprecationWarning)
try:
    from tensorflow.python.util import module_wrapper as deprecation
    deprecation._PER_MODULE_WARNING_LIMIT = 0
except ImportError:
    pass
try:
    from tensorflow.python.util import deprecation_wrapper as deprecation
    deprecation._PER_MODULE_WARNING_LIMIT = 0
except ImportError:
    pass
from tflon import chem, data, model, train, system, image, logging, summary, toolkit, distributed
from tflon.model import Model
