from __future__ import division
import sys
from tflon.graph import Graph, IndexMap

def edge(i, j):
    if i>j: return (j,i)
    return (i,j)

def block_preorder(ancestors):
    '''returns nodes as list of lists in the layer order they are visited in the bfs
    [[n0],[n1,n2],[n3]]
    It does this becuase for a node to go into empty and get appended to blocks, it must
    not have been put into blocks and visited yet, but all of its ancestors must have been put into blocks and visited.
    Thus it can only work outward one layer at a time, and all those unvisited with all ancestors visited will be in the same layer.'''

    blocks = []
    nodes = set(ancestors.keys()) #all unique nodes
    visited = set()
    while visited != nodes: #
        empty = sorted([v for v in ancestors if v not in visited and ancestors[v] <= visited])
        assert len(empty)>0, "Could not block preorder ancestor list: %s" % (str(ancestors))
        blocks.append(empty)
        visited.update(empty)
    return blocks

def flip_ancestors(D):
    """This reverses the order of all the graph connections, putting the descendents as the ancestors in a new dictionary
    It does this by adding the former keys as values for the new keys that are the old values. So it reverses the traversal order
    of every edge.

    Input: ancestors: dict with history one back so now like {n0:{}, n1:{n0}, n2:{n0}, n3:{n1,n2}}
    Output: {n0:{n1,n2},n1:{n3},n2:{n3}}
    """
    nD = {k:set() for k in D}
    for k in D:
        for v in D[k]:
            nD[v].add(k)

    return {k:sorted(nD[k]) for k in nD}

class ImageGraph2D(Graph):
    """Issue: i wrote all of these assuming the actual image itself was going to be passed into Graph, and so I thought I needed to return
    blocks full of actual image pixel values...

    1. Do these things being initialized need to be something different? as it is, it seems we are generating a bunch of data for little reason,
    and there are likely other things to modify to make it faster. Yes. he says make them generators. Perhaps do that later.
    2. Question: how the heck could we do neighbors if we didn't know the number node id to expect in each position?

    Now graph will be a tuple, (hight,width)
    """
    def __init__(self, graph):
        self._graph = graph
        self.height = graph[0]
        self.width = graph[1]
        self._components = [i for i in range(1,self.height*self.width+1)]
        self._nodes = IndexMap([i for i in range(1,self.height*self.width+1)])
        edges = self.get_edges_init()
        edge_index = {(u,v):i for i, (u,v) in enumerate(edges)}
        edge_index.update({(v,u):i for i, (u,v) in enumerate(edges)})
        self._edges = IndexMap(edges, edge_index)

    def get_edges_init(self):
        """Should still be improved"""
        edges = []
        I = [self._components[i:i+self.width] for i in range(0, len(self._components), self.width)]
        for i in range(self.height):
            for j in range(self.width):

                if j<self.width-1:
                    edges.append((I[i][j],I[i][j+1]))
                if i<self.height-1:
                    edges.append((I[i][j],I[i+1][j]))
        return edges


    def neighborhood(self, node, max_depth=sys.maxsize):
        """Just passing in the node number to start on. It's used by pool_featurize.
        Note: I'm assuming the node will be a # in a grid graph, starting at 1 raster scan style [[1,2,3],[4,5,6],[7,8,9]]
        Note: This should potentially be changed to be 0-indexed
        """
        D = {node: 0}
        node_location = ((node-1)//self.width,(node-1)%self.width)
        for i in range(max(node_location[0]-max_depth,0),min(node_location[0]+max_depth+1,self.height)):
            for j in range(max(node_location[1]-max_depth,0),min(node_location[1]+max_depth+1,self.width)):
                new_node = self.get_node_id(i,j)
                manhattan_distance = abs(i - node_location[0])+abs(j - node_location[1])
                if manhattan_distance <= max_depth:
                    D[new_node] = manhattan_distance
        return D

    def get_node_id(self,i,j):
        """Returns the node number, which is rastor scan style"""
        node_id = i*self.width+(j+1)
        return node_id

    def centroids(self):
        '''returns center pixel in image graph.
        Inputs: self._graph should be an image, not an nx.graph object
        outputs: the center pixel's value as a vector (of dimension 512 if using the VGG)
        Note that it will return the center pixel for odd-dimensions, and the left/top of center pair for even dimensions.
        For a line of 4 pixels, that means it picks the second pixel, having python index 1.'''

        c_coords = (self.height//2,self.width//2)
        return c_coords

    def bfs(self, c_coords):
        '''This returns blocks and ancestors given an image.
        Blocks is a list of lists containing the nodes (pixels) in each shell.
        Ancestors is the dicitonary with each pixel as keys, and as values a list (automatically sorted) of the immediate predecessors.

        For the four if statements below:
            This obtains all the ancestors. Those in the same row or column as the starting will have only one ancestor.
            This new ordering will spit out with the ancestor lists sorted by position in the array.
        '''

        longest_man_dist = c_coords[0]+c_coords[1]
        blocks = [[] for i in range(longest_man_dist+1)]
        ancestors = {i:[] for i in range(1,self.height*self.width+1)}
        for i in range(self.height):
            for j in range(self.width):
                d = abs(i-c_coords[0])+abs(j-c_coords[1])
                blocks[d].append(self.get_node_id(i,j))

                if i - c_coords[0] > 0: # in lower half
                    ancestors[self.get_node_id(i,j)].append(self.get_node_id(i-1,j))
                if j - c_coords[1] < 0: # in left half
                    ancestors[self.get_node_id(i,j)].append(self.get_node_id(i,j+1))
                if j-c_coords[1] > 0: # in right half
                    ancestors[self.get_node_id(i,j)].append(self.get_node_id(i,j-1))
                if i - c_coords[0] < 0: #in upper half
                    ancestors[self.get_node_id(i,j)].append(self.get_node_id(i+1,j))

        types = type_overwrite('t')
        return blocks, ancestors, types

    def get_edges(self, u, v):
        yield edge(u,v)

class type_overwrite(dict):
    def __init__(self,edge):
        self.edge = edge
    def __getitem__(self, item):
        return self.edge
