from __future__ import division
from tflon.data import Table
import numpy as np

def adjust_to_size(channel, width, height):
    if channel.shape[0] > width:
        hmargin = channel.shape[0] - width
        channel = channel[hmargin//2:-(hmargin//2+hmargin%2)]
    elif channel.shape[0] < width:
        hmargin = width - channel.shape[0]
        channel = np.pad(channel, ((hmargin//2,hmargin//2+hmargin%2), (0,0)), 'constant')
    if channel.shape[1] > height:
        vmargin = channel.shape[1] - height
        channel = channel[:,vmargin//2:-(vmargin//2+vmargin%2)]
    elif channel.shape[1] < height:
        vmargin = height - channel.shape[1]
        channel = np.pad(channel, ((0,0), (vmargin//2,vmargin//2+vmargin%2)), 'constant')
    return channel

def clip_to_size(channel, width, height):
    hmargin = channel.shape[0] - width
    vmargin = channel.shape[1] - height
    assert hmargin >= 0
    assert vmargin >= 0
    if hmargin > 0:
        channel = channel[hmargin//2:-(hmargin//2+hmargin%2)]
    if vmargin > 0:
        channel = channel[:,vmargin//2:-(vmargin//2+vmargin%2)]
    return channel

def pad_to_size(channel, width, height):
    hmargin = width - channel.shape[0]
    vmargin = height - channel.shape[1]
    assert hmargin >= 0
    assert vmargin >= 0
    return np.pad(channel, ((hmargin//2, hmargin//2+hmargin%2), (vmargin//2, vmargin//2+vmargin%2)), 'constant')

class ImageTable(Table):
    """
        A table for generating image tensors from flattened image data

        Parameters:
            data (DataFrame): nested flattened image data with columns channel_1 (list), channel_2 (list), ..., width, height

        Keyword Arguments:
            size (tuple):     If set, clip or pad images to the specified size (default=None)
            adjustment (str): 'clip', 'pad' or 'none', if images are variable size, 'clip' to smallest image size or 'pad' to largest.
                              If 'none' and images are not constant width/height, this will throw an error (default='none')
    """
    def __init__(self, data, size=None, adjustment='none'):
        self._data = data
        self._size = size
        self._adjustment = adjustment
        self._channels = len(data.columns)-2

    def totuple(self):
        return self.__class__, (self._data,), dict(size=self._size, adjustment=self._adjustment)

    def _copy(self, data):
        return ImageTable(data, self._size, self._adjustment)

    def get_shape(self):
        if self._size is not None:
            w, h = self._size
        elif self._adjustment == 'clip':
            w = self._data[self._data.columns[-2]].min()
            h = self._data[self._data.columns[-1]].min()
        elif self._adjustment == 'pad':
            w = self._data[self._data.columns[-2]].max()
            h = self._data[self._data.columns[-1]].max()
        elif self._adjustment == 'none':
            w, h = self._data.iloc[0,-2:]

        return (self._data.shape[0], w, h, self._channels)

    def as_matrix(self):
        df = self._data
        shape = self.shape
        batch = np.zeros(shape, dtype=np.float32)
        channel_cols = df.columns[:-2]
        size_cols = df.columns[-2:]

        if self._size is not None:
            channel_processor = lambda c: adjust_to_size(c, shape[1], shape[2])
        elif self._adjustment == 'clip':
            channel_processor = lambda c: clip_to_size(c, shape[1], shape[2])
        elif self._adjustment == 'pad':
            channel_processor = lambda c: pad_to_size(c, shape[1], shape[2])
        elif self._adjustment == 'none':
            channel_processor = lambda c: c


        for i, (_, row) in enumerate(df.iterrows()):
            w, h = row[size_cols]
            for j, c in enumerate(channel_cols):
                batch[i,:,:,j] = channel_processor( np.array(row[c]).reshape((w,h)) )

        return batch
