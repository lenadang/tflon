from builtins import range
from tflon.data import Table
from tflon.toolkit.toolkit import Module, Featurizer, op_name, register_callable
from tflon.toolkit.ops import interpolate1d, broadcast_matmul, interpolate_linear
from tflon.toolkit.losses import gaussian_error, multivariate_gaussian_error
from tflon.data import BlockReduceTable
import tensorflow_probability as tfp
import sklearn.decomposition
import numpy as np
import pandas as pd
import tensorflow as tf

def check_args_types(args):
    if len(args)==1 and \
            (isinstance(args[0], list) or \
             isinstance(args[0], tuple)):
        args=args[0]
    return args

class Identity(Module):
    """
        Output the input unchanged
    """

    def build(self):
        pass

    def call(self, *args):
        if len(args)==1: args=args[0]
        return args

class Merge(Module):
    """
        Merge a list of lists of tensors into a single list of tensors
    """
    def build(self):
        pass

    def call(self, *args):
        rval = tuple()
        for arg in args:
            if isinstance(arg, tuple):
                rval = rval + arg
            elif isinstance(arg, list):
                rval = rval + tuple(arg)
            else:
                rval = rval + (arg,)
        return rval

class Concat(Module):
    """
        Concatenate the input tensors

        Parameters:
            axis (int): The concatenation axis
    """

    def build(self, axis):
        self._axis=axis

    def call(self, *args):
        args = check_args_types(args)
        return tf.concat(args, axis=self._axis)

class Sum(Module):
    """
        Perform sum on multiple outputs (usually used with Inject or Join)
    """
    def build(self):
        pass

    def call(self, *args):
        args = check_args_types(args)
        return tf.add_n(args)

class Product(Module):
    """
        Perform product on multiple outputs (usually used with Inject or Join)
    """
    def build(self):
        pass

    def call(self, *args):
        args = check_args_types(args)
        val = args[0]
        for arg in args[1:]:
            val = val * arg
        return val

class Apply(Module):
    """
        Apply the given function or lambda expression.

        This is used to inject functions or lambda expressions into Module pipes. For example:

        >>> net = Apply(lambda x: x+5) | Dense(10)
        >>> outputs = net(inputs)

        Parameters:
            func (callable): A callable, function or lambda-type object
    """
    def build(self, func):
        self._func = func

    def call(self, *args):
        """
            Parameters:
                ``*args``: Arbitrary arguments passed to the func callable

            Returns:
                The result of ``func(*args)``
        """
        return self._func(*args)

class Get(Module):
    """
        Get arguments from an input tuple and return a selected subset of them.

        Use this module to select an output from a multi-output module (e.g an RNN) to pass to the next element of a chain.

        Parameters:
            ``*groups`` (slice): Slices to apply to ``*inputs`` to select the output
    """
    def build(self, *groups):
        if len(groups)==1 and type(groups[0]) is int:
            self._index = groups[0]
            self._groups = None
        else:
            self._groups = groups

    def call(self, *inputs):
        """
            Select slice groups from the tuple ``*inputs``

            Parameters:
                ``*inputs``: arguments from which to select
        """
        if self._groups is None:
            return inputs[self._index]
        else:
            rval = tuple()
            for slc in self._groups:
                slc_v = inputs[slc]
                if isinstance(slc_v, list) or isinstance(slc_v, tuple):
                    rval += tuple(slc_v)
                else:
                    rval += (slc_v,)
            return rval

class BatchFill(Module):
    """
        Tile a tensor (e.g a vector) along a new first dimension. This op is used to
        e.g copy a vector for every input example in a batch.

        Parameters:
            value (Tensor): The value to be tiled
    """
    def build(self, value):
        self._value = value

    def call(self, input):
        """
            Parameters:
                inputs (Tensor): Batch tensor, used to calculate batch size for tiling
        """
        batch_size = tf.shape(input)[0]
        return tf.tile(tf.expand_dims(self._value, 0), [batch_size, 1])

class WindowInput(Module):
    """
        Rescale inputs to range between 0-1 by the following transformation:

            I - min(I, axis=0) / (max(I, axis=0) - min(I, axis=))

        Keyword Arguments:
            distribution_table: The name of the table to collect distribution information, if None
                                infer the table name from the name of the input tensor passed to call (default=None)
    """
    def build(self, distribution_table=None, slicer=slice(None)):
        inputs = self._input_shapes[0][-1]
        self._input_name = distribution_table
        self._slicer = slicer
        self._mn = self.get_variable( "minimum", [inputs] )
        self._rng = self.get_variable( "range", [inputs] )

    def call(self, X):
        """
            Parameters:
                X (tensor): The tensor to be scaled
        """
        if self._input_name is None:
            self._input_name = op_name(X)
        return (X - self._mn) / self._rng

    def inverse(self, Y):
        return Y * self._rng + self._mn

    def show_data(self, table_map):
        table = table_map[ self._input_name ]

        min_val = table.min()
        max_val = table.max()
        min_val = min_val[self._slicer]
        max_val = max_val[self._slicer]

        rng_val = max_val - min_val
        rng_val[ rng_val==0 ] = 1
        self._mn.load( min_val )
        self._rng.load( rng_val )

class NormalizedInput(Module):
    """
        Normalize inputs to Z-distributions

        Keyword Arguments:
            distribution_table: The name of the table to collect distribution information, if None
                                infer the table name from the name of the input tensor passed to call (default=None)

        Other Parameters:
            precomputed_moments (tuple): Either two scalars or two 1-D arrays representing precomputed mean and standard deviations
    """
    def build(self, distribution_table=None, slicer=slice(None)):
        inputs = self._input_shapes[0][-1]
        dtype = self._input_types[0]
        self._input_name = distribution_table
        self._slicer = slicer

        if self.has_parameter('precomputed_moments'):
            self._mu, self._sigma = self._get_precomputed_moments(inputs, dtype)
        else:
            self._mu = self.get_variable( "mu", [inputs] )
            self._sigma = self.get_variable( "sigma", [inputs] )

    def _get_precomputed_moments(self, inputs, dtype):
        mu_val, sigma_val = (np.array(v) for v in self.precomputed_moments)
        if mu_val.shape==tuple():
            mu_val = np.array([mu_val]*inputs)
        if sigma_val.shape==tuple():
            sigma_val = np.array([sigma_val]*inputs)
        return tf.constant(mu_val, dtype=dtype), tf.constant(sigma_val, dtype=dtype)

    @property
    def moments(self):
        return self._mu, self._sigma

    def call(self, X):
        """
            Parameters:
                X (tensor): The tensor to be scaled
        """
        if self._input_name is None:
            self._input_name = op_name(X)
        return (X - self._mu) / self._sigma

    def inverse(self, Y):
        return Y * self._sigma + self._mu

    def show_data(self, table_map):
        if not self.has_parameter('precomputed_moments'):
            table = table_map[ self._input_name ]
            mu_val, sigma_val = table.moments()
            sigma_val[sigma_val==0] = 1
            mu_val = mu_val[self._slicer]
            sigma_val = sigma_val[self._slicer]

            self._mu.load( mu_val )
            self._sigma.load( sigma_val )

    def _parameters(self):
        return {'precomputed_moments': None}

class LogNormalInput(Module):
    def build(self, distribution_table=None, slicer=slice(None)):
        inputs = self._input_shapes[0][-1]
        self._input_name = distribution_table
        self._slicer = slicer
        self._mu = self.get_variable( "mu", [inputs] )
        self._sigma = self.get_variable( "sigma", [inputs] )

    @property
    def moments(self):
        return self._mu, self._sigma

    def call(self, X):
        """
            Parameters:
                X (tensor): The tensor to be scaled
        """
        if self._input_name is None:
            self._input_name = op_name(X)
        return (tf.log(X) - self._mu) / self._sigma

    def inverse(self, Y):
        return tf.exp(Y * self._sigma + self._mu)

    def show_data(self, table_map):
        table = table_map[ self._input_name ]
        logtable = table.applymap(np.log)

        mu_val, sigma_val = logtable.moments()
        sigma_val[sigma_val==0] = 1

        mu_val = mu_val[self._slicer]
        sigma_val = sigma_val[self._slicer]

        self._mu.load( mu_val )
        self._sigma.load( sigma_val )

class EmpiricalDistributionInput(Module):
    """
        Computes an empirical cumulative distribution function and transforms input variable x by:

        >>> p = CDF(x)

        This module is not fully functional in distributed mode.

        Keyword Arguments:
            distribution_table: The name of the table to collect distribution information, if None
                                infer the table name from the name of the input tensor passed to call (default=None)

        Other Parameters:
            quantile_count (int): The number of quantiles to calculate when estimating the empirical CDF.
                             For example, to compute quartiles, use quantile_count=4. (default=100)
    """

    def build(self, distribution_table=None, slicer=slice(None)):
        self._num_inputs = self._input_shapes[0][-1]
        self._input_name = distribution_table
        self._slicer = slicer
        self._quantiles = self.get_variable('quantiles', [self.quantile_count+1, self._num_inputs], dtype=self._input_types[0])
        self._percentiles = np.linspace(0, 1, self.quantile_count+1)
        self._tf_percentiles = tf.constant(self._percentiles.reshape((-1,1)), dtype=self._input_types[0])

    def call(self, x):
        if self._input_name is None:
            self._input_name = op_name(x)
        return tf.concat([interpolate_linear(x[:,i], self._quantiles[:,i], self._tf_percentiles)
                          for i in range(self._num_inputs)], axis=1)


    def inverse(self, p):
        slice1d = self._tf_percentiles[:,0]
        return tf.concat([interpolate_linear(p[:,i], slice1d, self._quantiles[:,i:i+1])
                          for i in range(self._num_inputs)], axis=1)

    def show_data(self, table_map):
        table = table_map[ self._input_name ]
        quantiles_val = np.quantile(table.as_matrix(), self._percentiles, axis=0)
        quantiles_val = quantiles_val[:,self._slicer]
        self._quantiles.load(quantiles_val)

    def _parameters(self):
        return {'quantile_count': 100}

class SegmentReduceFeaturizer(Featurizer):
    def _featurize(self, batch):
        data = batch[self.input_table].data

        indices = np.full((data.shape[0],), None, dtype=np.object_)
        for i in range(data.shape[0]):
            dlen = len(data.iloc[i,0])
            indices[i] = [dlen]
        return {'reduce': pd.DataFrame(data=indices, index=data.index)}

class SegmentReduce(Module):
    """
        Reduce segmented data to a single vector for each input example.

        Parameters:
            input_table (str): The name of the input table to use when constructing reference segment indices (Passed input must be a DenseNestedTable)

        Keyword Arguments:
            reduction_op (func):   A tensorflow segment reduce operation (default=tf.math.segment_sum
            weighting (callable):  A weighting function with signature ``func(tensor, segments)`` (Default: None)
                                    * tensor: N-D tensor for weight calculation
                                    * segments: 1-D tensor of segment indices

    """
    def build(self, input_table, reduction_op=tf.math.segment_sum, weighting=None):
        self._input_table = input_table
        self._reduce = self.add_input( 'reduce', shape=[None], dtype=tf.int32, ttype=BlockReduceTable)
        self._reduction = reduction_op
        self._weighting = weighting

    def call(self, inputs):
        """
            Parameters:
                inputs (tensor): The tensor to be reduced
        """
        weights = 1.0 if self._weighting is None else self._weighting( inputs, self._reduce )
        return self._reduction(inputs * weights, self._reduce)

    def _featurizer(self):
        return SegmentReduceFeaturizer(self, input_table=self._input_table)

class Dense(Module):
    """
        Fully connected layer with optional activation function

        Parameters:
            outputs (int):          The width of the output

        Keyword Arguments:
            activation (callable):  An activation op (default=lambda x:x)
    """
    def build(self, outputs):
        self._Ws = []
        for i, shape in enumerate(self._input_shapes):
            if len(shape)==4:
                W = self.get_weight( "weights_%d" % (i), [1, 1, shape[-1], outputs] )
            elif len(shape)==2:
                W = self.get_weight( "weights_%d" % (i), [shape[-1], outputs] )
            else:
                raise ValueError("Unsupported input tensor shape to Dense: %s" % (str(shape)))
            self._Ws.append(W)

        self._b = self.get_bias( "biases", [outputs] )

    def call(self, *inputs):
        """
            Parameters:
                *inputs (tensor-like):   One or more input tensors
        """
        def matmul_handle_types(X, W):
            if len(X.get_shape())==4:
                return tf.nn.conv2d(X, W, [1,1,1,1], padding='SAME')
            if isinstance(X, tf.SparseTensor):
                return tf.sparse_tensor_dense_matmul(X, W)
            return tf.matmul( X, W )
        return self.activation(tf.add_n([matmul_handle_types(X, W) for X, W in zip(inputs, self._Ws)]) + self._b)

    def _parameters(self):
        return {'activation': lambda x: x}

class Highway(Module):
    """
        Add a highway network bypass gate to a given computational block.

        https://arxiv.org/abs/1505.00387

        Parameters:
            block (callable): A module or function which executes the main computation.
    """

    def build(self, block):
        self._gate = Dense(self.input_shapes[0][-1], activation=tf.nn.sigmoid, bias_initializer=tf.constant_initializer(-1.))
        self._block = block

    def call(self, *inputs):
        """
            Compute:
                block(inputs) * gate(inputs) + inputs * (1 - gate(inputs))

            Parameters:
                inputs (tensor): An input tensor passed through both block and highway
        """
        hidden = self._block(*inputs)
        gate = self._gate(inputs[0])
        return hidden * gate + inputs[0] * (1-gate)

class Gaussian(Module):
    """
        This module outputs parameters of gaussian distributions (mean, stddev) for each input example.
        Mean and standard deviation are calculated via fully connected layers.
        A gaussian error loss function calculated in normalized space is also added to the model.

        Parameters:
            targets: Target values, automatically normalized prior to loss calculations

        Keyword Arguments:
            distribution:   Name of the input table containing the target data distribution.
            fixed_variance: Assume variance is constant w.r.t. the input domain
            nans:           Handle nan values in error calculation (default=False)
            reduction:      Apply reduction to loss values (default=tf.reduce_sum)

        Returns:
            mu, sigma:      The mean and standard deviation, in the same scale as the un-normalized targets
    """
    def build(self, targets, distribution=None, nans=False, reduction=tf.reduce_sum):
        fixed_variance = self.get_parameter('fixed_variance')
        self.normalizer = NormalizedInput(distribution, name="%s_normalizer" % (self.name))
        self._targets = self.normalizer(targets)
        self._nans = nans
        self._reduction = reduction
        I = self._input_shapes[0][-1]
        O = targets.get_shape()[-1].value

        self.W = self.get_weight("mu_weights", shape=(I, O))
        self.b = self.get_bias("mu_bias", shape=(O,))
        if fixed_variance:
            self.sigma = BatchFill( tf.exp(self.get_weight(shape=[O], name="log_sigma")) )
        else:
            self.sigma = Dense( O, name='%s_log_sigma' % (self.name), activation=tf.exp )

    def call(self, inputs, compute_loss=True):
        """
            Parameters:
                inputs (tensor): The input features for computing mu and sigma via linear ops
        """
        mu = tf.matmul(inputs, self.W) + self.b
        sigma = self.sigma(inputs)
        if compute_loss:
            loss = gaussian_error(self._targets, mu, sigma, nans=self._nans, reduction=self._reduction)
            return self.normalizer.inverse(mu), self.normalizer._sigma * sigma, loss
        else:
            return self.normalizer.inverse(mu), self.normalizer._sigma * sigma

    def _parameters(self):
        return {'fixed_variance': False}

class MultivariateGaussian(Module):
    def build(self, targets, distribution=None, nans=False, reduction=tf.reduce_sum):
        self.normalizer = NormalizedInput(distribution, name="%s_normalizer" % (self.name))
        self._targets = self.normalizer(targets)
        self._nans = nans
        self._reduction = reduction
        O = targets.get_shape()[-1].value

        self.mu = Dense( O, name='%s_mu' % (self.name) )
        if self.fixed_covariance:
            self.L = self.get_weight(shape=[O*(O+1)/2], name="sigma_lower_triangular")
        else:
            self.L = Dense( O*(O+1)/2, name='%s_sigma_lower_triangular' % (self.name) )
        self.scale = tf.matmul(tf.reshape(self.normalizer._sigma, (O,1)), tf.reshape(self.normalizer._sigma, (1,O)))

    def call(self, inputs):
        """
            Parameters:
                inputs (tensor): The input features for computing mu and sigma via linear ops
        """
        mu = self.mu(inputs)
        Lvec = self.L if self.fixed_covariance else self.L(inputs)
        L = tfp.distributions.fill_triangular(Lvec)

        loss = multivariate_gaussian_error(self._targets, mu, L, nans=self._nans, reduction=self._reduction, batch_covariance=not self.fixed_covariance)
        cov = tf.matmul(L, tf.matrix_transpose(L)) * self.scale
        mu = self.normalizer.inverse(mu)
        return mu, cov, loss

    def _parameters(self):
        return {'fixed_covariance': False}

class InterpolatedSequence(Module):
    """
        Builds a vector of accumulated sequential feature weights. This can be used with tflon.toolkit.interpolate1d
        to learn samples from the output of a higher dimensional projection function f: R -> R^n

        Keyword Arguments:
            bins (tensor-like): 1-D sequence of fixed, known dimension indicating the sampling points of the function domain
            features (int):           The number of higher dimensional features to sample from the function's range
            interpolator (callable):  An interpolation op (usually a lambda expression calling interpolate1d)
    """
    def build(self):
        self._x = tf.convert_to_tensor(self.sample_points)
        points = self._x.get_shape()[0].value
        features = self.get_parameter('features')
        self._deltas = self.get_weight(name='deltas', shape=(points-1, features), initializer=tf.random_normal_initializer(0, 0.1))
        self._Y = tf.concat([tf.zeros((1, features)), tf.cumsum(self._deltas, axis=0)], axis=0)

    def call(self, x):
        """
            Parameters:
                x (tensor-like): 1-D tensor of points for sampling from the interpolated features
        """
        interpolator = self.get_parameter('interpolator')
        return interpolator(x, self._x, self._Y)

    def _parameters(self):
        return {'sample_points': None, 'features': None, 'interpolator': interpolate1d}

class ForLoop(Module):
    """
        Construct a for loop using ``tf.while_loop`` with a counter

        Parameters:
            loop_body (callable):     A callable function representing the loop body, should be compatible with ``tf.while_loop``.
                                      Must have signature: ``lambda i, *args``, where ``i`` is the current counter value
            iterations (tensor-like): A scalar tensor indicating the number of iterations (can be static or dynamic)
            collect_outputs (bool):   If True, collect all values of each loop variable in a :class:`tf.TensorArray`
    """
    def build(self, loop_body, iterations, collect_outputs=False):
        self._loop = loop_body
        self._iters = iterations
        self._collect = collect_outputs

    def call(self, *args):
        """
            Parameters:
                ``*args``: Initial input loop variables, must match signature of ``loop_body = lambda i, *args``
        """
        N = len(args)
        if isinstance(self._loop, Module):
            self._loop.initialize(*args)

        def stop_criteria(*args):
            return args[0] < self._iters

        def loop_wrapper(*args):
            iter = args[0]
            xargs = tuple()
            if self._collect:
                xargs = args[-N:]
                args = args[:-N]
            rval = self._loop(*args)
            if type(rval) is list or type(rval) is tuple:
                rval = tuple(rval)
            else:
                rval = (rval,)
            if self._collect:
                xargs = tuple(ta.write(iter, rv) for ta, rv in zip(xargs, rval))
            return (iter+1,) + rval + xargs

        xargs = tuple()
        if self._collect:
            xargs = tuple(tf.TensorArray(arg.dtype, size=0, dynamic_size=True) for arg in args)

        iterator = tf.constant(0, dtype=tf.int32)
        rval = tf.while_loop(stop_criteria,
                             loop_wrapper,
                             (iterator,) + args + xargs,
                             back_prop=True,
                             swap_memory=False)

        if self._collect:
            rval = rval[-N:]
        else:
            rval = rval[1:]
        if len(rval)==1:
            rval = rval[0]
        return rval

class PCA(Module):
    """
        Compute a PCA matrix transformation, optionally handles missing values by probabilistic imputation.

        Parameters:
            distribution_table (str): The name of the table from which to compute PCA

        Keyword Arguments:
            nans (bool): If True, use probabilistic PCA to handle missing values

        Other Parameters:
            num_components (int): Number of PCA components to extract (required)
            tolerance (float):    Component fitting tolerance (default=1e-6)
    """
    def build(self, distribution_table, nans=False, imputed_table=None):
        num_features = self._input_shapes[0][-1]
        self._nans = nans
        self._input_name = distribution_table
        if imputed_table is None:
            imputed_table = distribution_table+'_imputed'
        if nans:
            self._imputed_data = self.add_input(imputed_table, shape=(None, num_features), dtype=tf.float32)
        self._pca = self.get_variable(name='principal_components', shape=(self.num_components, num_features), dtype=tf.float32)
        self._mu  = self.get_variable(name='pca_mu', shape=(num_features,), dtype=tf.float32)
        self._sigma = self.get_variable(name='pca_sigma', shape=(num_features,), dtype=tf.float32)

    def show_data(self, table_map):
        table = table_map[self._input_name]

        if self._nans:
            try:
                import ppca
                pca = ppca.PPCA()
                pca.fit(table.data.values, d=self.num_components, tol=self.tolerance, min_obs=2)
                W = pca.C.T
                if W.shape[0]<self.num_components:
                    W = np.pad(W, ((0, self.num_components-W.shape[0]), (0, 0)), mode='constant')
                self._mu.load(pca.means)
                self._sigma.load(pca.stds)
                self._pca.load(W)
                imputed_data = pca.data*pca.stds+pca.means
                table_map[self.slots[self._imputed_data]] = Table(pd.DataFrame(imputed_data, columns=table.columns, index=table.index))
            except ImportError:
                raise ImportError("Please pip install ppca to use the PCA module with nans=True")
        else:
            mu, sigma = table.moments()
            pca = sklearn.decomposition.PCA(self.num_components)
            pca.fit((table.data-mu)/sigma)
            self._mu.load(mu)
            self._sigma.load(sigma)
            self._pca.load(pca.components_)

    @property
    def imputed_data(self):
        return self._imputed_data

    @register_callable
    def project(self, targets):
        """
            Project from a high dimensional space to a low dimensional input space.
        """
        return tf.matmul((targets-self._mu)/self._sigma, tf.transpose(self._pca))

    @register_callable
    def normalize(self, targets):
        """
            Normalize targets in the high dimensional space
        """
        return (targets-self._mu)/self._sigma

    def denormalize(self, inputs, sigma=None):
        rval = inputs*self._sigma+self._mu
        if sigma is not None:
            scale = tf.matmul(tf.reshape(self._sigma, (-1,1)), tf.reshape(self._sigma, (1,-1)))
            cov = sigma * scale
            rval = (rval, cov)
        return rval

    def call(self, inputs, sigma=None):
        """
            Project from a low dimensional input space to a higher dimensional output space using a transformation learned from data

            Parameters:
                inputs: The input values in the low dimensional space

            Keyword Arguments:
                sigma: Optional tensor of sigma values, when provided, inputs and sigma form the parameters of independent normal distributions, which
                       are projected to a multivariate normal distribution in the output space
        """
        rval = tf.matmul(inputs, self._pca)
        if sigma is not None:
            bigsigma = tf.linalg.diag(tf.square(sigma))
            corr = broadcast_matmul(broadcast_matmul(tf.transpose(self._pca), bigsigma), self._pca)
            rval = (rval, corr)
        return rval

    def _parameters(self):
        return {'num_components': None, 'tolerance': 1e-6}

class LayerNorm(Module):
    """ Perform layer normalization

        Other Parameters:
            epsilon (float): A small positive number to prevent divide by zero errors (default=1e-6)
    """
    def build(self):
        width = self._input_shapes[0][-1]
        self._gain = self.get_weight('gain', (1,width), initializer=tf.constant_initializer(1.))
        self._offset = self.get_bias('offset', (1,width), initializer=tf.constant_initializer(0.))

    def call(self, inputs):
        mu, var = tf.nn.moments(inputs, axes=1)
        mu = tf.reshape(mu, (-1,1))
        var = tf.reshape(var, (-1,1))
        var = tf.where(tf.less(var, self.epsilon), tf.ones_like(var), var)
        g = self._gain
        o = self._offset

        return g * (inputs-mu)/tf.sqrt(var) + o

    def _parameters(self):
        return {'epsilon': 1e-6}

class GaussianMonteCarloExpectation(Module):
    """ Calculate the expected value of a (nonlinear) function of independently (not identically) distributed gaussian random variables

        Arguments:
            func (callable): A module or tensorflow op encapsulating the function over

        Other Parameters:
            num_samples (int): The number of samples to generate to calculate the expectation
    """

    def build(self, func):
        self._func = func

    def call(self, mu, sigma):
        N = tf.shape(mu)[0]
        Iw = mu.get_shape()[-1].value
        sv = tf.tile(tf.expand_dims(mu, axis=2), [1,1,self.num_samples]) + \
             tf.expand_dims(sigma, axis=2) * \
             tf.random_normal([N,Iw,self.num_samples], mean=0.0, stddev=1.0, dtype=tf.float32)

        sv2d = tf.reshape(tf.transpose(sv, [0,2,1]), [-1,Iw])
        ov2d = self._func(sv2d)
        Ow = ov2d.get_shape()[-1].value
        ov3d = tf.transpose(tf.reshape(ov2d, [-1,self.num_samples,Ow]), [0,2,1])
        return tf.reduce_mean(ov3d, axis=2, keepdims=False)

    def _parameters(self):
        return {'num_samples': 1000}

