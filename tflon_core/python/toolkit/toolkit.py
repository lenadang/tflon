import types
import tensorflow as tf
import numpy as np
from tflon import logging
from tflon.data import Table
from collections import OrderedDict
from functools import wraps

__name_scopes = {}
__current_tower = None

def is_callable(obj):
    """
        Check whether obj is callable
    """
    return isinstance(obj, Module) or hasattr(obj, '__call__') or \
           isinstance(obj, types.LambdaType) or isinstance(obj, types.FunctionType)

def is_tensor(T):
    """
        Check whether T is a tensor type (tf.Tensor or tf.SparseTensor)
    """
    return isinstance(T, tf.Tensor) or isinstance(T, tf.SparseTensor)

def is_array(A):
    return isinstance(A, np.ndarray)

def is_tensor_shape(T):
    return isinstance(T, tf.TensorShape)

class build_tower(object):
    """
        A context manager used by tflon.model.Tower to set the parent tower of new module objects during tensorflow graph construction
    """
    def __init__(self, tower):
        self._tower=tower

    def __enter__(self):
        reset_name_scope_IDs()
        _set_current_tower( self._tower )
        self._tower._initialize_build()

    def __exit__(self, *args):
        self._tower._finalize_build()
        _set_current_tower( None )

def _set_current_tower(tower):
    global __current_tower
    __current_tower = tower

def current_tower():
    """
        Get the tower currently being constructing, used by Module.__init__
    """
    global __current_tower
    return __current_tower

def reset_name_scope_IDs():
    """
        Reset the unique identifier generators to zero
    """
    global __name_scopes
    __name_scopes = {}

def get_next_name_scope(cls_name):
    """
        Get a unique identifier for a new name scope given a template name

        Parameters:
            cls_name (str): The template name
    """
    global __name_scopes
    count = __name_scopes.get(cls_name, 0)
    __name_scopes[cls_name] = count + 1
    return "%s_%d" % (cls_name, count)

def op_name(op):
    """
        Get the name of a tensorflow op, excluding parent scope and index information
    """
    return op.name.split('/')[-1].split(':')[0]

def decorate_or_return(wrapper_impl, args):
    """
        Helper function for construction decorators with optional callable signatures
    """
    if len(args)==0:
        def decorator(func):
            @wraps(func)
            def wrapper(*args, **kwargs):
                return wrapper_impl(func, *args, **kwargs)
            return wrapper
        return decorator
    else:
        func = args[0]
        @wraps(func)
        def wrapper(*args, **kwargs):
            return wrapper_impl(func, *args, **kwargs)
        return wrapper

def set_name_scope(*args, **kwargs):
    """
        Set the name scope of calls within a function to the function's name, or some other default value

        Keyword Arguments:
            default_name: The default name (default=None, use the function name)
    """
    global __name_scopes
    default_name = kwargs.get('default_name', None)

    def wrapper_impl(func, *args, **kwargs):
        name = kwargs.get('name', default_name)
        if 'name' in kwargs: del kwargs['name']
        if name is None: name = func.__name__
        with tf.name_scope( get_next_name_scope(name) ):
            return func(*args, **kwargs)

    return decorate_or_return(wrapper_impl, args)

def apply_reduction(*args, **kwargs):
    """
        Apply a reduction operation to the output of a tensor operation

        Keyword Arguments:
            default_reduction (callable): A function specifying the default reduction (default=tf.reduce_sum)
            argnum (int):  The index of the output tensor to reduce (default=None, assumes output is a single tensor)
    """
    default_reduction = kwargs.get('default_reduction', tf.reduce_sum)
    argnum = kwargs.get('argnum', None)

    def wrapper_impl(func, *args, **kwargs):
        reducer = kwargs.get('reduction', default_reduction)
        if reducer is None: reducer = lambda x: x
        if 'reduction' in kwargs: del kwargs['reduction']
        result = func(*args, **kwargs)
        if argnum is None:
            result = reducer( result )
        else:
            reduced = reducer( result[argnum] )
            result = result[:argnum] + (reduced,) + result[argnum+1:]
        return result

    return decorate_or_return(wrapper_impl, args)

def handle_nan(*args, **kwargs):
    """
        Decorator to filter nan values from an output tensor, replacing the filtered values with zeros

        Keyword Arguments:
            inarg (int):  the index of the input argument to use for identifying filterable values (default=0, the first argument)
            outarg (int): the index of the output argument to filter (default=None, assumes output is a single tensor)

        Returns:
            Function wrapper to apply nan filtering
    """
    inarg = kwargs.get('inarg', 0)
    outarg = kwargs.get('outarg', None)

    def wrapper_impl(func, *args, **kwargs):
        nans = kwargs.get('nans', False)
        if 'nans' in kwargs: del kwargs['nans']
        if not nans: return func(*args, **kwargs)
        else:
            T = args[inarg]
            z = tf.zeros_like(T)
            nanT = tf.is_nan(T)
            filtered = tf.where( nanT, z, T )
            args = args[:inarg] + (filtered,) + args[inarg+1:]

            parts = func(*args, **kwargs)
            if outarg is None:
                return tf.where( nanT, z, parts )
            else:
                filtered = tf.where( nanT, z, parts[outarg] )
                return parts[:outarg] + (filtered,) + parts[outarg+1:]

    """Decorator to handle nan inputs to a loss function.
       This adds the keyword argument 'nans' to any function,
       and filters the first argument for nan values."""
    return decorate_or_return(wrapper_impl, args)

def register_callable(func):
    @wraps(func)
    def wrapped(self, *args, **kwargs):
        self.initialize(*args, **kwargs)
        return func(self, *args, **kwargs)
    return wrapped


class Module(object):
    """
        This is a base class for creating reusable graph components. Extending classes should implement the following interfaces:

        Required interfaces:

            * ``build(self, *args, **kargs)``
            * ``call(self, *args, **kwargs)``

        Optional interfaces:
            * ``inverse(self, *args, **kwargs)``
            * ``show_data(self, batch)``
            * ``_featurizer(self)``
            * ``_parameters(self)``
    """

    def __init__(self, *args, **kwargs):
        self.name = get_next_name_scope( kwargs.pop('name', self.__class__.__name__) )
        self.calls = 0
        self.parent_scope = tf.get_variable_scope().name
        self._built = False
        self._slots = OrderedDict()
        self._schema = {}
        self.args = args
        self.kwargs = kwargs
        self._callables = {'call'}
        self._tower = current_tower()
        self._tensors = []

        params = {}
        for k in self.parameters:
            params[k] = kwargs.pop(k, self.parameters[k])
        self.inject_parameters()
        self._tower.add_module( self, params )

    @property
    def initialized(self):
        return self._built

    def initialize(self, *args, **kwargs):
        """
            Initialize this module. Performs two steps:

            1. Get the input shapes from ``*args``
            2. Call ``Module.build()``

            Modules need to be initialized before calling methods. If a method is likely called before ``Module.__call__``, then it should be registered
            as a callable method by decorator ``@tflon.toolkit.register_callable``

            Parameters:

                *args: The arguments passed to __call__
                **kwargs: The keyword arguments passed to __call__
        """
        if not self._built:
            with tf.variable_scope( self.name ):
                self._input_shapes, self._input_types = self.get_input_shapes_and_types( *args, **kwargs )
                try:
                    self.build( *self.args, **self.kwargs )
                except:
                    logging.error("Error building module: %s", self.name)
                    raise
                self._built=True

    @property
    def tensors(self):
        """
            Returns a list of tensors produced by the module
        """
        return self._tensors

    def _add_tensors(self, rval):
        if not (type(rval) is list or type(rval) is tuple):
            rval = [rval]
        for t in rval:
            if type(t) in (tf.Tensor, tf.Variable, tf.Operation):
                self._tensors.append(t)

    def __call__(self, *args, **kwargs):
        self.initialize(*args, **kwargs)

        with tf.variable_scope( "%s_%d" % (self.name, self.calls) ):
            try:
                rval = self.call( *args, **kwargs )
            except:
                logging.error("Error in module call: %s", self.name)
                raise
            self.calls += 1
            self._add_tensors(rval)
            return rval

    def __or__(self, other):
        """
            Construct a tflon.toolkit.Chain from two modules.

            Pipes the output of one module's __call__ to the input of the next
        """
        if type(other) is Chain:
            return Chain(*([self] + other.modules))
        return Chain(self, other)

    def __and__(self, other):
        """
            Construct a tflon.toolkit.Join from two modules

            Concatenates the output of all modules into a single tuple
        """
        if type(other) is Join:
            return Join(*([self] + other.modules))
        return Join(self, other)

    def __xor__(self, other):
        """
            Construct a tflon.toolkit.Broadcast from two modules

            Appends the output of all modules into a single tuple
        """
        if type(other) is Broadcast:
            return Broadcast(*([self] + other.modules))
        return Broadcast(self, other)

    def repeat(self, num):
        return Chain(*[self]*num)

    @property
    def input_shapes( self ):
        """
            Get the shapes of the tensors passed to this module's __call__(*args) as a list of tensor shapes, non-tensor types in *args are excluded
        """
        return self._input_shapes[:]

    def add_table( self, name, ttype ):
        return self._tower.add_table(name=name, ttype=ttype, required=False, namespace=self.name)

    def add_input( self, name, shape=[None], dtype=tf.float32, sparse=False, ragged=False, ttype=Table ):
        """
            Add an input to the model, using the scope of this Module

            See tflon.model.Tower.add_input
        """
        inp_name = '%s/%s' % (self.name, name)
        pl = self._tower.add_input(name=name, shape=shape, dtype=dtype, ttype=ttype, sparse=sparse, ragged=ragged, required=False, namespace=self.name)
        self._slots[pl] = inp_name
        return pl

    def add_target( self, name, shape=[None], dtype=tf.float32, sparse=False, ragged=False, ttype=Table ):
        """
            Add a target to the model, using the scope of this Module

            See tflon.model.Tower.add_target
        """
        targ_name = '%s/%s' % (self.name, name)
        pl = self._tower.add_target(name=name, shape=shape, dtype=dtype, ttype=ttype, sparse=sparse, ragged=ragged, required=False, namespace=self.name)
        self._slots[pl] = targ_name
        return pl

    def add_loss(self, name, loss):
        """
            Add a loss to the model, using the scope of this Module

            See tflon.model.Tower.add_loss
        """
        loss_name = '%s/%s' % (self.name, name)
        return loss_name, self._tower.add_loss(name=loss_name, loss=loss)

    def add_prior(self, loss):
        self._tower.add_prior(loss)

    def inject_parameters(self):
        """
            Add the values of Module._parameters as class attributes. For a given (key, value) pair, this will add instance attributes of the form:

                * self.key = value

            This behavior is currently optional, but will eventually become the default.
        """
        def create_property(key):
            return property(lambda self: self.get_parameter(key), lambda self,v: self.set_parameter(key,v))

        for k in self._parameters():
            # assert not hasattr(self, k), "Injected parameter %s overrides an existing property" % (k)
            setattr(self.__class__, k, create_property(k))

    def get_parameter( self, key, default=None, parent=False ):
        """
            Get the value of a parameter if it exists and is not None

            See tflon.model.Tower.get_parameter

            Parameters:
                key (str): The parameter name

            Keyword Arguments:
                default:   The parameter default value, returned if parameter is None or does not exist (default=None)
                parent (bool): Whether to check for the given parameter at the module scope (False), or the parent scope (True) (default=False)
        """
        if parent:
            return self._tower.get_parameter( key, default )
        return self._tower.get_parameter( key, default, namespace=self.name )

    def has_parameter( self, key, default=None, parent=False ):
        """
            Check of this module scope, or the parent scope has the specified parameter setting

            Parameters:
                key (str): The reference name for the query parameter

            Keyword Argument:
                parent (bool): Whether to check for the given parameter at the module scope (False), or the parent scope (True) (default=False)
        """
        if parent:
            return self._tower.has_parameter( key, default )
        return self._tower.has_parameter( key, default, namespace=self.name )

    def set_parameter( self, key, value ):
        """
            Set a parameter value for this module scope

            See tflon.model.Tower.set_parameter
        """
        self._tower.set_parameter( key, value, namespace=self.name )

    def get_variable( self, name, shape, dtype=tf.float32, initializer=None, trainable=False ):
        """
            Get a new or existing variable by name, within the current module scope.

            See tflon.model.Tower.get_variable
        """
        return self._tower.get_variable( name, shape, dtype, initializer, trainable )

    def get_weight( self, name, shape, dtype=tf.float32, initializer=None, prior=None ):
        """
            Get a new or existing weight variable by name, within the current module scope.

            See tflon.model.Tower.get_weight
        """
        return self._tower.get_weight( name, shape, dtype, initializer, prior, scope=self.name )

    def get_bias( self, name, shape, dtype=tf.float32, initializer=None, prior=None ):
        """
            Get a new or existing bias variable by name, within the current module scope.

            See tflon.model.Tower.get_bias
        """
        return self._tower.get_bias( name, shape, dtype, initializer, prior, scope=self.name )

    @property
    def scope(self):
        """
            Get the path of this module scope, including the parent tower scope
        """
        if self.parent_scope != '':
            return '/'.join([self.parent_scope, self.name])
        return self.name

    @property
    def variables( self ):
        """
            Get the collection of all variables (weights + biases + other trainables + non-trainables) at this module scope
        """
        return tf.get_collection(tf.GraphKeys.LOCAL_VARIABLES, scope=self.scope)

    @property
    def trainables( self ):
        """
            Get the collection of trainable variables (weights + biases + other trainables) at this module scope
        """
        return tf.get_collection(tf.GraphKeys.TRAINABLE_VARIABLES, scope=self.scope)

    @property
    def weights( self ):
        """
            Get the collection of weights at this module scope
        """
        return tf.get_collection(tf.GraphKeys.WEIGHTS, scope=self.scope)

    @property
    def biases( self ):
        """
            Get the collection of biases at this module scope
        """
        return tf.get_collection(tf.GraphKeys.BIASES, scope=self.scope)

    def get_input_shapes_and_types(self, *args, **kwargs):
        """
            Set the input shapes using the arguments to the first __call__ of this module

            The default implementation uses the shape of args[0]
        """
        def get_shape(arg):
            if is_tensor(arg):
                return tuple(dim.value for dim in arg.get_shape())
            if is_tensor_shape(arg):
                return tuple(dim.value for dim in arg)
            return None

        def get_dtype(arg):
            if is_tensor(arg) or is_array(arg):
                return arg.dtype
            return None
        return [get_shape(arg) for arg in args], [get_dtype(arg) for arg in args]

    def build(self, *args, **kwargs):
        """
            Instantiate variables required for this module. This is called by ``__init__``, with ``*args`` and ``**kwargs`` passed
            from ``__init__`` arguments list.
        """
        raise NotImplementedError()

    def call(self, *args, **kwargs):
        """
            Instantiate ops executing the module. This is the implementation of ``__call__``, invoked by the base class.

            Returns:
                A tensor or sequence of tensors
        """
        raise NotImplementedError()

    def inverse(self, *args, **kwargs):
        """Instantiate an op which has the reverse structure of ``call``. The input signature should be the same as the output of ``call``,
            and the output signature should match the input of ``call``.

            Where implemented, this may not be an exact inverse, check the documentation for each :class:`Module`

            Returns:
                A tensor or sequence of tensors
        """
        raise NotImplementedError()

    def show_data(self, table_map):
        """
            This method can be overridden to compute parameters from raw data tables for model initialization prior to optimization.

            Parameters:
                table_map (dict): Dictionary of table objects indexed by slot name
        """
        pass

    def _featurizer(self):
        return None

    @property
    def featurizer(self):
        return self._featurizer()

    @property
    def slots(self):
        """
            Return dictionary of placeholders to names of extra feature tables generated by this module.
        """
        return self._slots

    @property
    def schema(self):
        return {k:v for k,v in self._tower._schema.items() if k.startswith(self.name)}

    @property
    def names(self):
        """
            Get the names of all feature input slots as a dictionary of (name, placeholder) pairs.
        """
        return OrderedDict((v,k) for k,v in list(self._slots.items())[::-1])

    @property
    def parameters(self):
        params = self._parameters().copy()
        params.update({'bias_initializer': None, 'bias_prior': None,
                       'weight_initializer': None, 'weight_prior': None})
        return params

    def _parameters(self):
        """
            Set defaults for module-level parameters

            Returns:
                dict: dictionary of str -> value pairs. Keys are string parameter names and values are the default values for those parameters.
                      Names should be python compatible variable names. A value of None indicates a required parameter, however it is not
                      strictly necessary to declare such parameters.
        """
        return {}

class Featurizer(object):
    def __init__(self, module, **xparams):
        schema = module.schema
        self.namespace = module.name
        self.slots = set(schema.keys())
        self.ttypes = {n.split('/',1)[1]: schema[n] for n in self.slots}
        mparams = module._parameters()
        for k in mparams:
            if module.has_parameter(k):
                setattr(self, k, module.get_parameter(k))
        for k in xparams:
            setattr(self, k, xparams[k])

    def _featurize(self, batch):
        """
            Custom override for implementation of Featurizer.__call__().
            Add features to batch to support computations specified by modules.

            Parameters:
                batch (dict): A dictionary of str -> feedable objects (usually tflon.data.Table)

            Returns:
                dict: Additional str -> feedable mappings to add to the batch data
        """
        raise NotImplementedError()

    def __call__(self, batch):
        """
            Add features to batch to support computations specified by modules.

            Parameters:
                batch (dict): A dictionary of str -> feedable objects (usually tflon.data.Table)

            Returns:
                dict: Additional str -> feedable mappings to add to the batch data
        """
        schema = self.ttypes
        update = self._featurize(batch)
        return {'%s/%s' % (self.namespace, name):schema[name].instance(update[name]) for name in update}

class Chain(Module):
    """
        Chain multiple modules together, feeding the output of one into the input of the next

        Parameters:
            *args: Any number of callable objects
    """
    def __init__(self, *args):
        for M in args:
            if not is_callable(M):
                logging.warn("Non-callable object of type %s added to chain" % (str(type(M))))
        self.modules = list(args)

    def __or__(self, other):
        if type(other) is Chain:
            return Chain(*(self.modules + other.modules))
        if not is_callable(other):
            logging.warn("Non-callable object of type %s added to chain" % (str(type(other))))
        return Chain(*(self.modules + [other]))

    def __call__(self, *args):
        for M in self.modules:
            if type(args) is not tuple:
                args = (args,)
            args = M(*args)
        return args

    def __getitem__(self, idx):
        return self.modules[idx]

    @property
    def weights(self):
        return [w for module in self.modules for w in module.weights]

    @property
    def biases(self):
        return [b for module in self.modules for b in module.biases]

class Join(Module):
    """
        Send each argument in a list or tuple of arguments to the corresponding module in a list of modules

        Parameters:
            *args: Any number of callable objects
    """
    def __init__(self, *args):
        for M in args:
            if not is_callable(M):
                logging.warn("Non-callable object of type %s added to join" % (str(type(M))))
        self.modules = list(args)

    def __and__(self, other):
        if type(other) is Join:
            return Join(*(self.modules + other.modules))
        if not is_callable(other):
            logging.warn("Non-callable object of type %s added to join" % (str(type(other))))
        return Join(*(self.modules + [other]))

    def __call__(self, *args):
        if len(args)==1:
            args = args[0]
        assert isinstance(args, list) or isinstance(args, tuple), "Join: Expected list or tuple as input, got %s" % (type(args).__name__)
        assert len(args)==len(self.modules), "Join: Expected %d arguments, but got %d" % (len(self.modules), len(args))
        concat = tuple()
        for M, arg in zip(self.modules, args):
            if not isinstance(arg, tuple):
                arg = (arg,)
            rval = M(*arg)
            if type(rval) is tuple or type(rval) is list:
                concat += tuple(rval)
            else:
                concat += (rval,)
        return tuple(concat)

    def __getitem__(self, idx):
        return self.modules[idx]

class Broadcast(Module):
    """
        Broadcast inputs to multiple modules and append their outputs into a tuple

        Parameters:
            *args: Any number of callable objects
    """
    def __init__(self, *args):
        for M in args:
            if not is_callable(M):
                logging.warn("Non-callable object of type %s added to inject" % (str(type(M))))
        self.modules = list(args)

    def __xor__(self, other):
        if type(other) is Broadcast:
            return Broadcast(*(self.modules + other.modules))
        if not is_callable(other):
            logging.warn("Non-callable object of type %s added to inject" % (str(type(other))))
        return Broadcast(*(self.modules + [other]))

    def __call__(self, *args):
        concat = tuple()
        for M in self.modules:
            rval = M(*args)
            if type(rval) is tuple or type(rval) is list:
                concat += tuple(rval)
            else:
                concat += (rval,)
        return tuple(concat)

    def __getitem__(self, idx):
        return self.modules[idx]

# Define custom py_func which takes also a grad op as argument:
def py_func(func, inp, Tout, name=None, grad=None):
    # Need to generate a unique name to avoid duplicates:
    grd_name = get_next_name_scope('PyFuncGrad')

    tf.RegisterGradient(grd_name)(grad)  # see _MySquareGrad for grad example
    g = tf.get_default_graph()
    with g.gradient_override_map({"PyFunc": grd_name}):
        return tf.numpy_function(func, inp, Tout, name=name)
