import tensorflow as tf

class Prior(object):
    def call(self):
        raise NotImplementedError()

    def __call__(self, W):
        with tf.name_scope('priors'):
            transformed, loss = self.call(W)
        return transformed, loss

class GaussianPrior(Prior):
    def __init__(self, offset=0, scale=1):
        self._offset = offset
        self._scale = scale

    def call(self, W):
        loss = 0.5 * tf.reduce_sum(tf.square(W - self._offset) / self._scale)
        return W, loss

class GammaPrior(Prior):
    def __init__(self, k=1, theta=1):
        self._k = k
        self._theta = theta

    def call(self, W):
        V = tf.exp(W)
        loss = tf.reduce_sum((1 - self._k) * W + V / self._theta)
        return V, loss
