from tflon.toolkit.toolkit import Module, set_name_scope, apply_reduction, handle_nan
from tflon.toolkit.ops import residual_covariance
import tensorflow as tf

class CovarianceLoss(Module):
    """
        Compute the multivariate gaussian distribution loss, assuming covariance is constant over the input domain.

        Parameters:
            targets (tensor-like):  A 2-D tensor of batch_size x num_variables
            mu (tensor-like):       A 2-D tensor of estimated means for each example

        Keyword Arguments:
            sigma (tensor-like):    A 2-D tensor of estimated standard deviations for each example
                                    (default=None, if None, calculate variance from the data, assume constant over the input domain)

        Returns:
            Tensor:                 The multivariate gaussian loss, a 0-D tensor
    """
    def build(self, targets):
        O = targets.get_shape()[-1].value
        self.covariance = self.get_variable(shape=[O, O], name='sigma')
        self.targets = targets

    @apply_reduction
    def _compute_variable_loss(X, sigma, cov):
        N = tf.shape(X)[0]
        var = tf.matrix_diag( tf.square(sigma) )
        cov = tf.tile( tf.expand_dims(cov, axis=0), [N, 1, 1] )
        big_sigma = var + cov

        X = tf.where( tf.is_nan(X), tf.zeros_like(X), X )
        def compute_slice(X, sigma):
            det_sigma = tf.matrix_determinant(sigma)
            sigma_inv = tf.matrix_inverse(sigma)
            X = tf.expand_dims(X, 0)
            Xt = tf.transpose(X)
            return 0.5 * (tf.log(det_sigma) + tf.matmul(tf.matmul(X, sigma_inv), Xt))

        return tf.map_fn(compute_slice, [X, big_sigma], dtype=X.dtype)

    @apply_reduction
    def _compute_fixed_loss(X, cov):
        X = tf.where( tf.is_nan(X), tf.zeros_like(X), X )

        det_sigma = tf.matrix_determinant(cov)
        sigma_inv = tf.matrix_inverse(cov)

        return 0.5 * ( tf.log(det_sigma) + tf.reduce_sum( tf.matmul(X, sigma_inv) * X, axis=1 ) )

    def call(self, mu, sigma=None):
        T = self.targets

        X = T - mu
        cov = residual_covariance(X, include_diag=sigma is None)
        with tf.control_dependencies([self.covariance.assign(cov)]):
            if sigma is not None:
                loss = self._compute_variable_loss(X, sigma, cov)
            else:
                loss = self._compute_fixed_loss(X, cov)
        return loss

@set_name_scope
@apply_reduction
def multivariate_gaussian_error(targets, mu, L, nans=False, batch_covariance=False, lower_triangular=True):
    if lower_triangular:
        invL = tf.linalg.inv(L)
        invS = tf.matmul(tf.matrix_transpose(invL), invL)
        detS = tf.square(tf.matrix_determinant(L))
    else:
        invS = tf.linalg.inv(L)
        detS = tf.matrix_determinant(L)

    R = targets-mu
    if nans:
        R = tf.where(tf.is_nan(targets), tf.zeros_like(targets), R)

    if batch_covariance:
        rhs = tf.map_fn(lambda sig, res: tf.squeeze(tf.matmul(sig, tf.expand_dims(res, axis=1)), axis=1), [invS, R], dtype=tf.float32)
    else:
        rhs = tf.transpose(tf.matmul(invS, tf.transpose(R)))
    return tf.reduce_sum(R * rhs, axis=1) + tf.log(detS)

@set_name_scope
@apply_reduction
@handle_nan
def gaussian_error(targets, mu, sigma):
    """
        Computes the sum of gaussian losses for multiple gaussian targets

        Parameters:
            targets (tensor-like): N-D tensor with target regression values
            mu (tensor-like):      N-D tensor with predicted means
            sigma (tensor-like):   N-D tensor with predicted standard deviations

        Keyword Arguments:
            nans (bool):           Apply nan filter prior to reduction (default=False)
            reduction (callable):  A reduction op to apply to the partial losses (default=tf.reduce_sum)
    """
    return tf.square( mu - targets ) / ( 2*tf.square(sigma) ) + tf.log( sigma )

@set_name_scope
@apply_reduction
@handle_nan
def xent_softmax(T, L):
    """
        Computes an unweighted softmax cross entropy

        Includes optional handling of missing values

        Parameters:
            T (tensor-like): N-D tensor with target class labels
            L (tensor-like): N-D tensor with same shape as T, containing logit network output

        Keyword Arguments:
            nans (bool):     Apply nan filter prior to reduction
    """
    return tf.losses.softmax_cross_entropy(T, L, reduction=tf.losses.Reduction.NONE)

@set_name_scope
@apply_reduction
@handle_nan
def xent_uniform_sum(T, L):
    """
        Computes the unweighted sum of cross entropy losses for multiple binary targets.

        Parameters:
            T (tensor-like): N-D tensor with target class labels
            L (tensor-like): N-D tensor with same shape as T, containing logit network output

        Keyword Arguments:
            nans (bool):     Apply nan filter prior to reduction
    """
    return tf.losses.sigmoid_cross_entropy(T, L, weights=1.0, reduction=tf.losses.Reduction.NONE)

xent = xent_uniform_sum

@set_name_scope
@apply_reduction
@handle_nan
def xent_weighted_sum(T, L, W):
    """
        Computes the example-weighted sum of cross entropy losses for multiple binary targets.

        Parameters:
            T (tensor-like): N-D tensor with target class labels
            L (tensor-like): N-D tensor with same shape as T, containing logit network output
            W (tensor-like): 0-D tensor with the weight for positive examples

        Keyword Arguments:
            nans (bool):     Apply nan filter prior to reduction
    """
    return tf.nn.weighted_cross_entropy_with_logits(T, L, pos_weight=W)

xent_weighted = xent_weighted_sum

@set_name_scope
@apply_reduction
@handle_nan
def square_error(T, L):
    return tf.square(T-L)

@set_name_scope
def l2_penalty(Ws):
    """
        Computes the total L2 penalty of a list of weights

        Parameters:
            Ws (list): tensor-like objects, weights to compute L2 penalty
    """
    return tf.add_n( [tf.reduce_sum( tf.square(W) ) for W in Ws] )

@set_name_scope
def l1_penalty(Ws):
    """
        Computes the total L1 penalty of a list of weights

        Parameters:
            Ws (list): tensor-like objects, weights to compute L1 penalty
    """
    return tf.add_n( [tf.reduce_sum( tf.abs(W) ) for W in Ws] )

@set_name_scope
def elastic_net(Ws, alpha):
    """
        Computes the elastic net penalty of a list of weights:

        >>> (1 - alpha) * sum(W ** 2) + alpha * sum(|W|)

        Parameters:
            Ws (list): tensor-like objects, weights to compute elastic net penalty
            alpha (float): A number between zero and one
    """
    return (1 - alpha) * tf.add_n([tf.reduce_sum(tf.square(W)) for W in Ws]) +\
                alpha  * tf.add_n([tf.reduce_sum(tf.abs(W)) for W in Ws])
