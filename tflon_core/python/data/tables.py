from builtins import range
import itertools
import numpy as np
import pandas as pd
from tflon.data.feeds import RaggedTensorValue
from tflon.utils import nanlen, nanempty
from scipy.sparse import csr_matrix

class Table(object):
    """
        The basic building block of the tflon data model. Tables are essentially a wrapper for pandas DataFrames that specify the transformation
        mapping the dataframe onto input tensors of a model. Examples should have at most a single row per table, with a single index value.
        Multiple extensions to Table are available to handle specific data types. For example, node features of a graph can be handled by
        DenseNestedTable, which represents each graph as a single row with nested lists for node features.
    """
    def __init__(self, data):
        """
            Parameters:
                data (pandas.DataFrame): The input data for this Table
        """
        self._data = data

    def serialize(self):
        return self

    def deserialize(self):
        return self

    def totuple(self):
        return self.__class__, (self._data,), {}

    @property
    def data(self):
        return self._data

    def split(self, partitions):
        cols = len(self._data.columns)
        partitions = [cols+p if p<0 else p for p in partitions]
        partitions = [0] + partitions + [cols]
        new_tables = []
        for start, end in zip(partitions[:-1], partitions[1:]):
            new_tables.append( self.copy(self._data.iloc[:, start:end]) )
        return new_tables

    def copy(self, new_data=None):
        if new_data is None:
            new_data=self._data.copy()
        return self._copy(new_data)

    def _copy(self, data):
        return self.__class__(data)

    def min(self):
        return self._data.min(axis=0).values

    def max(self):
        return self._data.max(axis=0).values

    def count(self):
        return np.array([self._data.shape[0]]*len(self._data.columns))

    def sum(self):
        return self._data.sum(axis=0).values

    def sqdiff(self, diff):
        return np.square(self._data-diff).sum(axis=0).values

    def moments(self, dof=0):
        return self._data.mean(axis=0).values, self._data.std(axis=0, ddof=dof).values

    def applymap(self, fn):
        return self.__class__(self._data.applymap(fn))

    @property
    def index(self):
        return self._data.index.values

    @property
    def columns(self):
        return self._data.columns.values

    def filter_rows(self, index):
        df = self._data
        return self.copy(df.reindex(index))

    def drop(self, index, **kwargs):
        self._data.drop(index, inplace=True, **kwargs)

    def drop_duplicates(self):
        name = self._data.index.name
        if name is None: name = 'index'
        df = self._data.reset_index()
        df = df.drop_duplicates(subset=name)
        self._data = df.set_index(name)

    def align(self, index, axis=0):
        self._data = self._data.reindex(index, axis=axis)

    def as_matrix(self):
        return self._data.values

    def get_shape(self):
        return self._data.shape

    @property
    def shape(self):
        return self.get_shape()

    def concatenate(self, tables):
        return self.copy( pd.concat([self._data] + [t._data for t in tables]) )

class IndexTable(Table):
    def __init__(self, index):
        if isinstance(index, pd.DataFrame):
            self._data = index
        else:
            self._data = pd.DataFrame({'strings': index.astype(np.string_)}, columns=['strings'], index=index)

    def as_matrix(self):
        return self._data.strings.values

class DenseNestedTable(Table):
    """
        Table which converts dataframes containing list type elements into 2-d tensors by flattening lists within a column across rows.

        All lists in a row must have the same length.

        For example, for molecule atom data, the following form is used:

        ID      col1                col2                col3
        mol1    [atom1 atom2 ...]   [atom1 atom2 ...]   [atom1 atom2 ...]
    """
    def min(self):
        return self._data.applymap(np.min).min(axis=0).values

    def max(self):
        return self._data.applymap(np.max).max(axis=0).values

    def count(self):
        return self._data.applymap(len).sum(axis=0).values

    def sum(self):
        return self._data.applymap(np.sum).sum(axis=0).values

    def sqdiff(self, diff):
        return np.array([ self._data[col].apply(lambda v: np.sum(np.square(v - diff[i]))).sum() \
                          for i, col in enumerate(self._data.columns) ])

    def moments(self, dof=0):
        N = self.count()
        mu = self.sum() / N
        var = self.sqdiff(mu)/(N-dof)
        return mu, np.sqrt(var)

    def get_shape(self):
        df = self._data
        N = sum( len(df.iloc[i,0]) for i in range(len(df.index)) )
        C = len( df.columns )
        return (N, C)

    def as_matrix(self):
        df = self._data
        N, C = self.get_shape()

        out = np.zeros((N, C))

        i=0
        for idx, row in df.iterrows():
            size = len( row[df.columns[0]] )

            for j, col in enumerate(df.columns):
                out[i:i+size, j] = row[col]
            i+=size
        return out

class PaddedNestedTable(Table):
    """
        Table which supports storing ragged arrays using a padding strategy. Converts to a sparse output.

        Input dataframes must contain a single column of list-type entries.
    """
    def __init__(self, data, padded_width=None, dtype=np.float32):
        self._data = data
        if padded_width is None:
            padded_width = max( data[data.columns[0]].apply(len) )
        self._padded_width = padded_width
        self._dtype = dtype

    def totuple(self):
        return self.__class__, (self._data,), dict(padded_width=self._padded_width, dtype=self._dtype)

    def _copy(self, data):
        return self.__class__(data, padded_width=self._padded_width, dtype=self._dtype)

    def min(self):
        return self._data.applymap(np.min).min(axis=0).values

    def max(self):
        return self._data.applymap(np.max).max(axis=0).values

    def count(self):
        return self._data.applymap(len).sum(axis=0).values

    def sum(self):
        return self._data.applymap(np.sum).sum(axis=0).values

    def sqdiff(self, diff):
        return np.array([ self._data[col].apply(lambda v: np.sum(np.square(v - diff[i]))).sum() \
                          for i, col in enumerate(self._data.columns) ])

    def moments(self, dof=0):
        N = self.count()
        mu = self.sum() / N
        var = self.sqdiff(mu)/(N-dof)
        return mu, np.sqrt(var)

    def get_shape(self):
        df = self._data
        N = df.shape[0]
        return (N, self._padded_width)

    def as_matrix(self):
        df = self._data
        series = df[df.columns[0]]

        N, C = self.get_shape()
        series.loc[series.isnull()] = series[series.isnull()].apply(lambda d: [])

        row_len = series.apply(len)
        idx_ptr = np.array([0] + list(row_len.cumsum()))
        idx = np.fromiter(itertools.chain.from_iterable(row_len.apply(lambda x: np.arange(x))), dtype=np.int32)
        data = np.fromiter(itertools.chain.from_iterable(series), dtype=self._dtype)

        return csr_matrix((data, idx, idx_ptr), shape=(N, C)).tocoo(copy=False)

    def concatenate(self, tables):
        return self.__class__( pd.concat([self._data] + [t._data for t in tables]), padded_width=max([self._padded_width] + [t._padded_width for t in tables]), dtype=self._dtype )

class RaggedTable(Table):
    """
        Table for managing ragged arrays without using a padding strategy. Values are converted to RaggedTensorValue for input via a RaggedTensor placeholder,which can be sliced by the ragged array index number to extract individual ragged elements.

        Dataframes must contain a single column of list-type elements.
    """
    def __init__(self, data, dtype=np.float32):
        self._data = data
        self._dtype = dtype

    def totuple(self):
        return self.__class__, (self._data,), dict(dtype=self._dtype)

    def split(self):
        raise NotImplementedError()

    def min(self):
        raise NotImplementedError()

    def max(self):
        raise NotImplementedError()

    def count(self):
        raise NotImplementedError()

    def sum(self):
        raise NotImplementedError()

    def sqdiff(self, diff):
        raise NotImplementedError()

    def _copy(self, data):
        return self.__class__(data, dtype=self._dtype)

    def ragged_shapes(self, W):
        df = self._data
        return sum(df[df.columns[1]].apply(lambda v: np.append(nanempty(v), [0]*(W-nanlen(v))))).astype(np.int32)

    def get_shape(self):
        df = self._data
        W = df[df.columns[1]].apply(nanlen).max()
        N = self.ragged_shapes(W).max()
        return N, W

    def as_matrix(self):
        df = self._data

        W = df[df.columns[1]].apply(nanlen, convert_dtype=False).max()
        rshapes = self.ragged_shapes(W)
        N = np.max(rshapes)

        series = df[df.columns[0]]
        lengths = df[df.columns[1]]

        nonzero = np.sum(rshapes)
        values = np.zeros((nonzero,), dtype=self._dtype)

        size = np.zeros((len(lengths), W), dtype=np.int64)
        for i, L in enumerate(lengths):
            L = nanempty(L)
            for j, v in enumerate(L):
                size[i,j] = v

        begin = np.zeros(size.shape, dtype=np.int64)
        total = 0
        for j in range(size.shape[1]):
            for i in range(size.shape[0]):
                begin[i,j] = total
                total += size[i,j]

        for i, (S, L) in enumerate(zip(series, lengths)):
            L = nanempty(L)
            k=0
            for j, _l in enumerate(L):
                b = begin[i,j]
                s = size[i,j]
                values[b:b+s] = S[k:k+s]
                k+=s

        return RaggedTensorValue(values, rshapes, [N, W])

class SparseIndexTable(Table):
    """
        Used for indexing very large tensors, such as a corpus of words, or a list of diagnosis codes.

        This can be used to gather slices from a very large embedding tensor.

        Input dataframes must contain a single column of list-type entries containing integer indexes.

        Converts to a sparse binary matrix.
    """
    def __init__(self, data, code):
        self._data = data
        self._code = code

    def totuple(self):
        return self.__class__, (self._data, self._code), {}

    @property
    def code(self):
        return self._code

    def split(self):
        raise NotImplementedError()

    def _copy(self, data):
        return self.__class__(data, self._code)

    def min(self):
        raise NotImplementedError()

    def max(self):
        raise NotImplementedError()

    def count(self):
        raise NotImplementedError()

    def sum(self):
        raise NotImplementedError()

    def sqdiff(self, diff):
        raise NotImplementedError()

    def moments(self, dof=0):
        N = self.count()
        mu = self.sum() / N
        std = self.sqdiff(mu)/(N-dof)
        return mu, std

    def get_shape(self):
        df, code = self._data, self._code
        N = df.shape[0]
        C = code.shape[0]
        return N, C

    def as_matrix(self):
        df, code = self._data, self._code
        code_index = {v:i for i,v in enumerate(code.index)}

        series = df[df.columns[0]]

        N, C = self.get_shape()

        series.loc[series.isnull()] = series[series.isnull()].apply(lambda d: [])
        series = series.apply(lambda L: [code_index[v] for v in L])

        idx_ptr = np.array([0] + list(series.apply(len).cumsum()))
        idx = np.fromiter(itertools.chain.from_iterable(series), dtype=np.int32)
        data = np.ones(idx.shape)

        return csr_matrix((data, idx, idx_ptr), shape=(N, C)).tocoo(copy=False)

class SparseIndexedValuesTable(SparseIndexTable):
    """
        Used for indexing very large tensors, such as a corpus of words, or a list of diagnosis codes.

        This can be used to gather slices from a very large embedding tensor.

        Input dataframes must contain two columns of list-type entries containing integer indexes (first column) and values at those indexes (second column)

        Converts to a sparse  matrix with the provided values in each index.
    """

    def as_matrix(self):
        df, code = self._data, self._code
        code_index = {v:i for i,v in enumerate(code.index)}
        series = df[df.columns[0]]
        N, C = self.get_shape()

        #Processing data for csr_matrix
        series_data = df[df.columns[1]]
        series_data.loc[series_data.isnull()] = series_data[series_data.isnull()].apply(lambda d: [])
        data = np.concatenate(series_data.to_numpy())

        series.loc[series.isnull()] = series[series.isnull()].apply(lambda d: [])
        series = series.apply(lambda L: [code_index[v] for v in L])
        idx_ptr = np.array([0] + list(series.apply(len).cumsum()))
        idx = np.fromiter(itertools.chain.from_iterable(series), dtype=np.int32)

        assert idx.shape==data.shape, "Shape of index and data doesn't match"
        return csr_matrix((data, idx, idx_ptr), shape=(N, C)).tocoo(copy=False)


class BlockReduceTable(Table):
    """
        Specifies blocks of a tensor for segment operations.

        Input dataframes must contain a single column of list-type elements containing the size of each reduction block.
    """
    def get_shape(self):
        return (self._data.iloc[:,0].apply(sum).sum(),)

    def as_matrix(self):
        shape = self.shape
        mat = np.zeros(shape, dtype=np.int32)

        i = 0
        j = 0
        for L in self._data.iloc[:,0]:
            for k in L:
                mat[i:i+k] = j
                i += k
                j += 1
        return mat
