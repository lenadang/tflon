from __future__ import print_function
import os
import pyarrow.parquet as paq
from tflon import logging
import pyarrow as pa
import pandas as pd
import tarfile
try:
    from StringIO import StringIO
    from backports.tempfile import TemporaryDirectory
except:
    from io import StringIO
    from tempfile import TemporaryDirectory

def info(df):
    s = StringIO()
    df.info(buf=s)
    return s.getvalue()

def _write_index(shard, directory):
    idx_fn = os.path.join(directory, 'index')
    with open(idx_fn, 'w') as ifile:
        for idx in shard.index:
            print(idx, file=ifile)
    return idx_fn

def archive_shard(shard, destination):
    with TemporaryDirectory() as tdir, tarfile.open(destination, 'w:gz') as tfile:
        write_shard(shard, tdir)
        tfile.add(os.path.join(tdir, 'index'), arcname='index')
        for key in shard:
            if key=='_index': continue
            pqfn = os.path.join(tdir, key)+'.pq'
            tfile.add(pqfn, arcname=key+'.pq')

def write_shard(shard, destination):
    if not os.path.exists(destination):
        os.makedirs(destination)
    _write_index(shard, destination)
    for key in shard:
        if key=='_index': continue
        serialized = shard[key].serialize()
        write_parquet(serialized.data, os.path.join(destination, key)+'.pq')

def write_parquet(df, destination):
    """
        Read data from a pandas.DataFrame to parquet

        Parameters:
            df (pandas.DataFrame): A pandas dataframe containing pyarrow parquet-compatible data
            destination (str):     A filepath to write parquet data
    """
    logging.info( "Writing %s" % (destination) )
    paq.write_table( pa.Table.from_pandas(df), destination )
    schema = paq.read_schema( destination ).remove_metadata()
    logging.info( "Finished writing %s" % (destination) )
    logging.debug( "Table schema: \n%s\n" % (schema) )

def read_parquet(source, columns=None, use_pandas_metadata=True):
    """
        Read data from a parquet file

        Parameters:
            source (str):    A filepath to read parquet data

        Keyword Arguments:
            columns (None or list):     A list of columns to read. If None, read all columns (default=None)
            use_pandas_metadata (bool): (default=True)

        Returns:
            pandas.DataFrame
    """
    schema = paq.read_schema( source ).remove_metadata()
    logging.info( "Reading %s" % (source) )
    logging.debug( "Table schema: \n%s\n" % (schema) )
    df = paq.read_table(source, columns=columns,
        use_pandas_metadata=use_pandas_metadata).to_pandas()

    def cast(s):
        if type(s) is bytes: return s.decode()
        return s
    rename = {c:cast(c) for c in df.columns}
    df.rename(rename, axis=1, inplace=True)
    return df

def build_dataframe(columns, dtypes, index_col='ID', index_type=int):
    """
        Build a dataframe with pre-specified column types.

        Parameters:
            columns (list): String names of columns
            dtypes  (?):    If list or tuple, dtypes for columns in same ordering
                            If dict, dtypes for columns with column names as keys
                            Otherwise, dtypes is assumed to be a type to assign all columns:

        Keyword Arguments:
            index_col (str):   The name of the index
            index_type (type): The type of the index
    """
    if isinstance(dtypes, list):
        dtypes=dict(zip(columns, dtypes))
    elif isinstance(dtypes, tuple):
        dtypes=dict(zip(columns, dtypes))
    elif isinstance(dtypes, dict):
        pass
    else:
        dtypes = {c:dtypes for c in columns}
    series = []
    series.append(pd.DataFrame(columns=[index_col], dtype=index_type))
    for c in columns:
        series.append(pd.DataFrame(columns=[c], dtype=dtypes[c]))
    return pd.concat(series, axis=1).set_index(index_col)
