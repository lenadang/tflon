import os
import six
import tarfile
from .feeds import TableFeed
from tflon.data.parquet import read_parquet
try:
    from backports.tempfile import TemporaryDirectory
except:
    from tempfile import TemporaryDirectory

class SchemaEntry(object):
    def __init__(self, name, table_cls, reader=read_parquet, required=True, visible=True):
        self._name = name
        self._filenames = name + '.pq'
        self._cls = table_cls
        self._reader = reader
        self._required = required
        self._visible = visible

    def set_filenames(self, filenames):
        self._filenames = filenames

    def set_reader(self, reader):
        self._reader = reader

    @property
    def required(self):
        return self._required

    @property
    def visible(self):
        return self._visible

    def __call__(self, directory):
        if isinstance(self._filenames, six.string_types):
            self._filenames = [self._filenames]
        filepaths = [os.path.join(directory, fn) for fn in self._filenames]
        exists = [os.path.exists(path) for path in filepaths]
        if all(exists):
            args = [self._reader( path ) for path in filepaths]
            return self._cls(*args)

    def instance(self, data):
        if type(data) is tuple or type(data) is list:
            return self._cls(*data)
        return self._cls(data)

class Schema(object):
    """
        Wrapper for schema specifications returned by tflon.model.Model.schema

        Parameters:
            schema (dict): dictionary of str -> (filenames, type[, reader]). Keys are string table names corresponding
                        to model inputs, filenames is a string or list of strings specifying the names of files to be loaded from the shard
                        directory, type is a class inheriting from Table, reader (optional) is a function with signature
                        reader(filename) -> pandas.DataFrame (default = tflon.data.read_parquet).
    """
    def __init__(self, schema):
        self._schema = schema

    @property
    def required(self):
        return Schema({k:v for k, v in self._schema.items() if v.required})

    @property
    def visible(self):
        return Schema({k:v for k, v in self._schema.items() if v.visible})

    def merge(self, schema):
        nschema = self._schema.copy()
        nschema.update( schema._schema )
        return Schema(nschema)

    def keys(self):
        return self._schema.keys()

    def __getitem__(self, key):
        return self._schema[key]

    def load(self, *shards, **kwargs):
        """
            Load tables from a shard using this schema.

            Parameters:
                *shards (str): paths (directories or tar.gz archives) containing serialized tables

            Keyword Arguments:
                reader (callable): A function used for loading serialized tables. Signature: reader(filename), default: tflon.data.read_parquet

            Returns:
                dict: Map of name -> Table, the loaded table data
        """
        tables = {}
        master_table = kwargs.get('master_table', None)

        def _load_element(directory):
            for name in self._schema:
                table = self._schema[name](directory)
                if table is not None:
                    tables[name] = table

        for directory in shards:
            with TemporaryDirectory() as tdir:
                if directory.endswith('.tar') or \
                   directory.endswith('.tar.gz') or \
                   directory.endswith('.tar.bz2'):
                    tfile = tarfile.open(directory, 'r')
                    tfile.extractall(path=tdir)
                    tfile.close()
                    directory = tdir
                _load_element(directory)

        missing=[]
        for name in self._schema:
            if self._schema[name].required and name not in tables:
                missing.append(name)
        assert len(missing) == 0, "Required tables: [ %s ] could not be loaded from: [ %s ]" % (', '.join(missing), ', '.join(shards))

        return TableFeed(tables, master_table=master_table)

    def map(self, **kwargs):
        for name in kwargs:
            assert name in self._schema, "No table in schema: %s" % (name)
            arg = kwargs[name]
            if type(arg) is tuple:
                filenames, reader = arg
                self._schema[name].set_reader(reader)
            else:
                filenames = arg
            self._schema[name].set_filenames(filenames)
        return self
