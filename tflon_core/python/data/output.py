from builtins import range
import numpy as np
import pandas as pd
from tflon.data.tables import DenseNestedTable

class TensorTransform(object):
    """
        Interface class for output tensor transformations.
    """
    def __call__(self, batch, output):
        raise NotImplementedError()

class DenseToNested(TensorTransform):
    """
        Convert a 2-D tensor to a nested tensor, the number of entries in each nested list is decided by a template table

        Parameters:
            template_table (str): The name of the input table used to set nest sizes and indexes
    """

    def __init__(self, template_table, columns=None):
        self._template_table = template_table
        self._columns = columns

    def __call__(self, batch, output):
        table = batch[self._template_table]
        assert isinstance(table, DenseNestedTable)

        ref_col = table.data[table.data.columns[0]]
        nested = np.full((len(table.index), output.shape[1]), None, np.object_)

        i = 0
        for j, val in enumerate(ref_col):
            rng = len(val)
            for k in range(output.shape[1]):
                nested[j,k] = output[i:i+rng,k].tolist()
            i += rng

        columns = list(range(output.shape[1]))
        if self._columns is True:
            columns = table.columns
        elif self._columns is not None:
            columns = self._columns
        return pd.DataFrame(data=nested, index=table.data.index, columns=columns)

class CopyIndex(TensorTransform):
    """
        Add an index to a 2-D tensor

        Parameters:
            template_table (str): The name of the input table used to set the indexes
    """
    def __init__(self, template_table, columns=None):
        self._template_table = template_table
        self._columns = columns

    def __call__(self, batch, output):
        table = batch[self._template_table]
        columns = list(range(output.shape[1]))
        if self._columns is True:
            columns = table.columns
        elif self._columns is not None:
            columns = self._columns
        return pd.DataFrame(data=output, index=table.index, columns=columns)

IdentityTransform=CopyIndex
