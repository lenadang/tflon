from tflon.chem.molecule import Molecule
from tflon.graph import GraphTable

class MoleculeTable(GraphTable):
    """
        Convert a DataFrame containing serialized json format molecule structures to
        tflon.chem.Molecule objects.

        See documentation of tflon.graph.GraphTable for instantiation
    """
    def __init__(self, data):
        super(MoleculeTable, self).__init__(data, gtype=Molecule)
