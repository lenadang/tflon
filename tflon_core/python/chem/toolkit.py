from builtins import range
from tflon.toolkit import Featurizer, Module
import tensorflow as tf
import numpy as np
from tflon.graph.data import GatherTable, GraphTable
from tflon.data.tables import DenseNestedTable
import pandas as pd

organics = ['H', 'B', 'C', 'N', 'O', 'F', 'P', 'S', 'Cl', 'As', 'Se', 'Br', 'I']

class AtomPropertiesFeaturizer(Featurizer):
    def _featurize(self, batch):
        if self._aromatic:
            import pybel as pb
        gtable = batch[self._molecule_table]
        graphs = gtable.data[gtable.columns[0]]

        C = len(self._columns)
        features = np.full((gtable.shape[0], C), None, dtype=np.object_)
        M = self._mapping

        for i, g in enumerate(graphs):
            N = len(g.nodes)

            Z = [[0]*N for _ in range(C-1)]
            for j, an in enumerate(g.nodes.atomicnum):
                if self._allow_unknown:
                    Z[M.get(an,0)][j] = 1
                else:
                    assert an in M, "Unrecognized atom type: %d" % (an)
                    Z[M[an]][j] = 1
            Z.append(list(g.nodes.formalcharge))

            if self._aromatic:
                pmol = pb.readstring('smi', g.pymol.write('smi'))
                Z.append([1 if a.OBAtom.IsAromatic() else 0 for a in pmol.atoms])

            features[i,:] = Z
        return {'atom_properties': pd.DataFrame(features, index=gtable.index, columns=self._columns)}


class AtomProperties(Module):
    def build(self, molecule_table, atom_types=organics, symbols=True, allow_unknown=True, aromatic=False):
        import openbabel as ob
        et = ob.OBElementTable()
        if symbols:
            atomic_nums = [et.GetAtomicNum(sym) for sym in atom_types]
        else:
            atomic_nums = atom_types
            atom_types = [et.GetSymbol(num) for num in atomic_nums]

        self._atom_types = atom_types
        self._atom_nums = atomic_nums
        self._features = self.add_input('atom_properties', dtype=tf.float32, shape=(None, len(atom_types)+2), ttype=DenseNestedTable)
        self._molecule_table = molecule_table
        self._allow_unknown = allow_unknown
        self._aromatic = aromatic
        if allow_unknown:
            self._columns = ['Unknown'] + atom_types + ['FormalCharge']
            self._mapping = {at:i+1 for i,at in enumerate(atomic_nums)}
        else:
            self._columns = atom_types + ['FormalCharge']
            self._mapping = {at:i for i,at in enumerate(atomic_nums)}

    def _featurizer(self):
        return AtomPropertiesFeaturizer(self, _molecule_table=self._molecule_table, _mapping=self._mapping, _columns=self._columns, _allow_unknown=self._allow_unknown, _aromatic=self._aromatic)

    def call(self):
        return self._features

class BondPropertiesFeaturizer(Featurizer):
    def _featurize(self, batch):
        gtable = batch[self._molecule_table]
        graphs = gtable.data[gtable.columns[0]]

        arom = 'aromatic' in self._columns
        C = len(self._columns)
        features = np.full((gtable.shape[0], C), None, dtype=np.object_)

        for i, g in enumerate(graphs):
            N = len(g.edges)

            Z = [np.zeros((N,)) for _ in range(C)]
            for ix, bo in enumerate(g.edges.order):
                c = bo-1
                Z[c][ix] = 1

            if arom:
                rdmol = g.rdmol
                nindex = g.nodes.index
                for ix,(u,v) in enumerate(g.edges):
                    j,k = nindex[u],nindex[v]
                    b = rdmol.GetBondBetweenAtoms(j,k)
                    Z[3][ix] = 1 if b.GetIsAromatic() else 0
                    Z[4][ix] = 1 if b.GetIsConjugated() else 0

            features[i,:] = Z
        return {'bond_properties': pd.DataFrame(features, index=gtable.index, columns=self._columns)}

class BondProperties(Module):
    def build(self, molecule_table, include_aromatic=False):
        self._molecule_table = molecule_table
        self._columns = ['single', 'double', 'triple']
        if include_aromatic:
            self._columns.append('aromatic')
            self._columns.append('conjugated')
        self._features = self.add_input('bond_properties', dtype=tf.float32, shape=(None, len(self._columns)), ttype=DenseNestedTable)

    def call(self):
        return self._features

    def _featurizer(self):
        return BondPropertiesFeaturizer(self, _molecule_table=self._molecule_table, _columns=self._columns)

class BondLengthFeaturizer(Featurizer):
    def _featurize(self, batch):
        graphs = batch[self._molecule_table].data.iloc[:,0]
        bond_lengths = np.full((graphs.shape[0],1), None, dtype=np.object_)

        for i, (_, g) in enumerate(graphs.iteritems()):
            coords = np.array(g.nodes.coordinates)
            if len(g.edges) > 0:
                node1, node2 = zip(*g.edges)
                node_indices = g.nodes.index
                idx1 = [node_indices[n1] for n1 in node1]
                idx2 = [node_indices[n2] for n2 in node2]

                bond_lengths[i,0] = np.sqrt(np.sum((np.take(coords, idx1, axis=0) - np.take(coords, idx2, axis=0))**2, axis=1))
            else:
                bond_lengths[i,0] = []

        return {'bond_lengths': pd.DataFrame(data=bond_lengths, columns=['BondLength'], index=graphs.index)}

class BondLength(Module):
    def build(self, molecule_table):
        self._bond_lengths = self.add_input('bond_lengths', shape=[None, 1], dtype=tf.float32, ttype=DenseNestedTable)
        self._molecule_table = molecule_table

    def call(self):
        return self._bond_lengths

    def _featurizer(self):
        return BondLengthFeaturizer(self, _molecule_table=self._molecule_table)

class GraphConverterFeaturizer(Featurizer):
    def _featurize(self, batch):
        graphs = batch[self._molecule_table].data.iloc[:,0]
        transformed_graphs = np.full((graphs.shape[0],1), None, dtype=np.object_)
        bond_gather = np.full((graphs.shape[0],2), None, dtype=np.object_)

        for i in range(graphs.shape[0]):
            if self.gtype == 'delaunay':
                new_graph = graphs.iloc[i].delaunay_graph(preserve_bonds=self.preserve_bonds, single_virtual_port=self.single_virtual_port)
            elif self.gtype == 'neighbor':
                new_graph = graphs.iloc[i].neighbor_graph(self.radius, preserve_bonds=self.preserve_bonds, single_virtual_port=self.single_virtual_port)
            elif self.gtype == 'mst':
                new_graph = graphs.iloc[i].mst_graph(self.radius)
            else:
                raise Exception("gtype must be 'delaunay' or 'neighbor'")
            bond_gather[i,:] = new_graph.bond_gather
            transformed_graphs[i,0] = new_graph

        return {'graphs': pd.DataFrame(transformed_graphs, index=graphs.index),
                'bond_gather': pd.DataFrame(bond_gather, index=graphs.index)}

class GraphConverter(Module):
    def build(self, molecule_table):
        self._molecule_table = molecule_table
        self._graph_table = self.add_table('graphs', GraphTable)
        self._bond_gather_input = self.add_input('bond_gather', shape=[None, 1], dtype=tf.int32, ttype=GatherTable)
        self._bond_gather = tf.reshape(self._bond_gather_input, (-1,))

    def call(self, bond_properties=None):
        is_bond = tf.cast( tf.greater(self._bond_gather_input, -1), dtype=tf.float32 )

        if bond_properties is None:
            return self._graph_table, is_bond
        else:
            width = bond_properties.get_shape()[-1].value
            bond_properties = tf.concat([tf.zeros((1, width), dtype=bond_properties.dtype),
                                        bond_properties], axis=0)
            edge_properties = tf.gather(bond_properties, self._bond_gather+1)
            return self._graph_table, tf.concat([is_bond, edge_properties], axis=1)

    def _featurizer(self):
        return GraphConverterFeaturizer(self, _molecule_table=self._molecule_table)

    def _parameters(self):
        return {'gtype': None,
                'radius': None,
                'preserve_bonds': False,
                'single_virtual_port': False}
