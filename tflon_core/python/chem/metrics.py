import pandas as pd
import numpy as np

def generate_fingerprints(molecules):
    df = pd.DataFrame(columns=['ID', 'Fingerprint']).set_index('ID')
    for ID in molecules.index:
        m = molecules.loc[ID,'Structure']
        df.loc[ID] = m.pymol.calcfp('fp2')
    return df

def asymmetric_validation_embedding_bias(fingerprints, labels, indices):
    tr_set = fingerprints.index[~fingerprints.index.isin(indices)]
    ts_set = indices

    tr_fp = fingerprints.loc[tr_set]
    tr_lb = labels.loc[tr_set]
    tr_keep = pd.notna(tr_fp)
    tr_fp = tr_fp.loc[tr_keep]
    tr_lb = tr_lb.loc[tr_keep]

    ts_fp = fingerprints.loc[ts_set]
    ts_lb = labels.loc[ts_set]
    ts_keep = pd.notna(ts_fp)
    ts_fp = ts_fp.loc[ts_keep]
    ts_lb = ts_lb.loc[ts_keep]

    def nearest(V, T):
        return {idx:max(fp | fp2 for _, fp2 in T.iteritems()) for idx, fp in V.iteritems()}

    def S(V, T, d):
        return 1./len(V) * sum(1 if T[idx] < d else 0 for idx in V)

    def H(V, T, step=0.01):
        T = nearest(V, T)
        V = V.index
        return step * sum( S(V, T, d) for d in np.arange(0, 1, step) )

    tr_pos = tr_fp.loc[tr_lb==1]
    tr_neg = tr_fp.loc[tr_lb==0]

    ts_pos = ts_fp.loc[ts_lb==1]
    ts_neg = ts_fp.loc[ts_lb==0]

    pos_bias = ( H(ts_pos, tr_pos) - H(ts_pos, tr_neg) )
    neg_bias = ( H(ts_neg, tr_neg) - H(ts_neg, tr_pos) )
    return pos_bias + neg_bias

