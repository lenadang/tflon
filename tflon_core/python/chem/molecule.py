from builtins import range
import click
from tflon.data.parquet import write_parquet
from tflon.graph import Graph, ViGraph
from tflon.utils import ManagedTTY
import networkx as nx
import pandas as pd
import ujson as json
from tflon import logging
import numpy as np
from sklearn.neighbors import BallTree
from sklearn.metrics.pairwise import euclidean_distances
from scipy.sparse import csr_matrix
from scipy.sparse.csgraph import minimum_spanning_tree

def pymol_topological_groups(mol):
    """A faster implementation of topological indexes than Molecule.topological_groups"""
    import openbabel
    topological_indexes = openbabel.vectorUnsignedInt()
    mol.OBMol.GetGIDVector(topological_indexes)
    nti = {}
    ti = []
    cti = 0

    for i in topological_indexes:
        if i not in nti:
            cti+=1
            nti[i] = cti
        ti.append(nti[i])
    return ti

def edge(i,j):
    if i>j: return j,i
    return i,j

def neighbor_edges(coordinates, radius):
    tree = BallTree(np.array(coordinates), leaf_size=1, metric='euclidean')
    balls = tree.query_radius(coordinates, radius)
    edges = [edge(root+1,node+1) for root, ball in enumerate(balls)
                                 for node in ball
                                 if root != node]
    return set(edges)

def voronoi_tesselate(coords, epsilon=1e-4):
    import tess
    min_limit = np.min(np.array(coords), axis=0)
    max_limit = np.max(np.array(coords), axis=0)
    voronoi_cells = tess.Container(coords-min_limit, limits=max_limit-min_limit+epsilon)

    edges = [edge(i+1,j+1) for i in range(len(coords))
                           for j in voronoi_cells[i].neighbors()
                           if  j >= 0]
    return set(edges)

def mst_trace(coords):
    weights = np.triu(euclidean_distances(coords, coords))
    csr = csr_matrix(weights)
    post_mst = minimum_spanning_tree(csr).toarray()

    edges = [(i+1,j+1) for i,row in enumerate(post_mst) for j,val in enumerate(row) if val>0]
    return set(edges)

class Molecule(Graph):
    """
        Defines a networkx based representation of a molecule for dynamic ML computations
    """

    @property
    def atoms(self):
        return self._nodes

    @property
    def bonds(self):
        return self._edges

    @property
    def heavy_atoms(self):
        """
            Get a list of heavy atom indices
        """
        G = self._graph
        return [idx for idx in self.nodes if G.nodes[idx]['atomicnum']>1]

    @property
    def hydrogens(self):
        G = self._graph
        return [idx for idx in self.nodes if G.nodes[idx]['atomicnum']==1]

    @property
    def light_bonds(self):
        """
            Get a list of all bonds with hydrogen

            Returns:
                A list of tuples of atom indices
        """
        G = self._graph
        return [(a,b) for a,b in self.edges if G.nodes[a]['atomicnum']==1 or G.nodes[b]['atomicnum']==1]

    @property
    def heavy_bonds(self):
        """
            Get a list of heavy bonds

            Returns:
                A list of tuples of atom indices
        """
        G = self._graph
        return [(a,b) for a,b in self.edges if G.nodes[a]['atomicnum']>1 and G.nodes[b]['atomicnum']>1]

    def _all_paths(self, idx):
        G = self._graph
        allpaths = []
        def recursive_paths(i, cpath):
            allpaths.append(cpath)
            for j in G.neighbors(i):
                if j in cpath: continue
                recursive_paths(j, cpath + [j])

        recursive_paths(idx, [idx])
        return allpaths
#        all_paths = [[idx]]
#        for i, j in nx.bfs_edges(self._graph, idx):
#            all_paths += [path[:]+[j] for path in all_paths if path[-1]==i]
#        return all_paths

    @property
    def topological_groups(self):
        """
            Compute topological groups by exhaustive path enumeration (warning: still very slow, use pymol_topological_groups instead)

            Returns:
                atom_topology (dict): a dictionary with keys of atom indices and values topological group (int)
        """
        G = self._graph

        topology = {}
        for i in self.nodes:
            topology[i] = tuple(sorted(tuple((G.nodes[j]['atomicnum'], G.nodes[j]['formalcharge'])
                                                   for j in path) for path in self._all_paths(i)))
        groups = {}
        gnum = 0

        atom_topology = []

        for i in G.nodes:
            if topology[i] not in groups:
                gnum+=1
                groups[topology[i]] = gnum
            atom_topology.append( groups[topology[i]] )
        return atom_topology

    @property
    def pymol(self):
        """
            Convert this Molecule to a pybel.Molecule
        """
        import openbabel as ob
        import pybel as pb
        mol = ob.OBMol()
        nodes = self.nodes
        if 'coordinates' in self._node_properties:
            coords = nodes.coordinates
        else:
            coords = [(0,0,0)] * len(self.nodes)
        for an, fc, vec in zip(nodes.atomicnum, nodes.formalcharge, coords):
            a = mol.NewAtom()
            a.SetAtomicNum( an )
            a.SetFormalCharge( fc )
            a.SetVector( *vec )

        for (i, j), order in zip(self.edges, self.edges.order):
            mol.AddBond(i, j, order)

        return pb.Molecule( mol )

    @property
    def rdmol(self):
        """
            Convert this Molecule to rdkit.RDMol
        """
        from rdkit import Chem
        from rdkit.Geometry.rdGeometry import Point3D
        nodes = self.nodes
        edges = self.edges
        if 'coordinates' in self._node_properties:
            coords = nodes.coordinates
        else:
            coords = [(0,0,0)] * len(self.nodes)

        em = Chem.RWMol()
        conf = Chem.Conformer(len(nodes))
        ixmap = {}
        for n, an, fc, vec in zip(nodes, nodes.atomicnum, nodes.formalcharge, coords):
            a = Chem.Atom(an)
            a.SetFormalCharge(fc)
            ix = em.AddAtom(a)
            c = Point3D(vec[0], vec[1], vec[2])
            conf.SetAtomPosition(ix, c)
            ixmap[n] = ix

        def get_bond_type(order):
            if order == 1:
                return Chem.BondType.SINGLE
            if order == 2:
                return Chem.BondType.DOUBLE
            if order == 3:
                return Chem.BondType.TRIPLE
        for (u,v), order in zip(edges, edges.order):
            em.AddBond(ixmap[u], ixmap[v], get_bond_type(order))

        from rdkit.Chem import rdmolops
        rdmolops.SanitizeMol(em)
        return em


    def _virtual_nxgraph(self, feat, radius=None, preserve_bonds=False, single_virtual_port=False, epsilon=1e-4):
        g = nx.Graph()
        g.add_nodes_from(self._graph.nodes(data=True))

        if feat=='delaunay':
            virtual_edges = voronoi_tesselate(self.nodes.coordinates, epsilon=epsilon)
        elif feat == 'neighbor':
            virtual_edges = neighbor_edges(self.nodes.coordinates, radius)
        elif feat == 'mst':
            virtual_edges = mst_trace(self.nodes.coordinates)
        else:
            raise Exception("did not pass valid virtual graph feature type")

        if preserve_bonds: g.add_edges_from( set(self.edges.ids) | virtual_edges )
        else: g.add_edges_from( virtual_edges )

        for eprop in self._edge_properties:
            order_attrs = {e: {eprop: getattr(self.edges, eprop)[self.edges[e]] if self.edges[e] is not None else None} for e in g.edges}
            nx.set_edge_attributes(g, order_attrs)

        return g

    def neighbor_graph(self, radius, preserve_bonds=False, single_virtual_port=False):
        vg = self._virtual_nxgraph('neighbor', radius=radius, preserve_bonds=preserve_bonds)
        return ViGraph( vg, self, self._node_properties, self._edge_properties, single_virtual_port=single_virtual_port )


    def delaunay_graph(self, preserve_bonds=False, single_virtual_port=False, epsilon=1e-4):
        vg = self._virtual_nxgraph('delaunay', epsilon=epsilon, preserve_bonds=preserve_bonds)
        return ViGraph( vg, self, self._node_properties, self._edge_properties, single_virtual_port=single_virtual_port )

    def mst_graph(self, radius=2.5, vgraph_type='neighbor'):
        assert vgraph_type in ['neighbor', 'delaunay'], "Invalid vgraph type: {0}".format(vgraph_type)

        mstg = self._virtual_nxgraph('mst', radius=radius)
        featg = self._virtual_nxgraph(vgraph_type, radius=radius)

        return ViGraph(nx.compose(mstg,featg),
                       Molecule(mstg, self._node_properties, self._edge_properties),
                       self._node_properties,
                       self._edge_properties)

    def remove_hydrogens(self):
        Hs = set(self.hydrogens)
        remove_nodes = set()
        remove_edges = set()
        for nid,i in self.nodes.index.items():
            if nid in Hs: remove_nodes.add(i)
        for (n1,n2),i in self.edges.index.items():
            if n1 in Hs or n2 in Hs: remove_edges.add(i)

        G = self._graph
        for nid in Hs:
            G.remove_node(nid)

        self._nodes.drop(remove_nodes)
        self._edges.drop(remove_edges)

    def emedian_graph(self, radius=2.5, vgraph_type='neighbor', epsilon=.25):
        assert(vgraph_type in ['neighbor', 'delaunay'], "Invalid vgraph type: {0}".format(vgraph_type))

        root = self.center_of_system()
        emedian_edges = list(self._emedian_edges(root, epsilon=epsilon))

        parentg = nx.Graph()
        parentg.add_nodes_from(self._graph.nodes(data=True))
        parentg.add_edges_from( emedian_edges )

        featg = self._virtual_nxgraph(vgraph_type, radius=radius)

        for eprop in self._edge_properties:
            order_attrs = {e: {eprop: getattr(self.edges, eprop)[self.edges[e]] if self.edges[e] is not None else None} for e in parentg.edges}
            nx.set_edge_attributes(parentg, order_attrs)

        return ViGraph(nx.compose(parentg,featg),
                       Molecule(parentg, self._node_properties, self._edge_properties),
                       self._node_properties,
                       self._edge_properties)

    @classmethod
    def from_json(cls, serialized):
        """
            Generate a networkx representation of a serialized json format molecule

            Parameters:
                serialized (str): The serialized json representation
        """
        gdict = json.loads(serialized)
        nxg, np, ep = Graph.from_dict(gdict, nx.Graph())
        return Molecule(nxg, np, ep)

    @classmethod
    def from_pymol(cls, mol, **kwargs):
        """
            Generate a networkx representation of a pymol format molecule

            Parameters:
                mol (pybel.Molecule): A pybel molecule
                **kwargs: additional keyword arguments (see tflon.chem.pymol_to_json)
        """
        kwargs['dump']=False
        gdict = pymol_to_json(mol, **kwargs)
        nxg, np, ep = Graph.from_dict(gdict, nx.Graph())
        return Molecule(nxg, np, ep)


def _rdmol_to_dict(mol, coordinates=False):
    if coordinates:
        conformer = mol.GetConformer()

    atoms = []
    for a in mol.GetAtoms():
        props = (a.GetAtomicNum(), a.GetFormalCharge())
        if coordinates:
            pos = conformer.GetAtomPosition(a.GetIdx())
            props += ((pos.x, pos.y, pos.z),)
        atoms.append((a.GetIdx()+1, props))

    bonds = []
    for b in mol.GetBonds():
        i = b.GetBeginAtom().GetIdx()+1
        j = b.GetEndAtom().GetIdx()+1
        bo = b.GetBondTypeAsDouble()

        assert bo in {1.0, 2.0, 3.0}, "Bond order %.1f not supported, did you kekulize first?" % (bo)
        if i>j: i,j=j,i
        props = (int(bo),)
        bonds.append((i,j,props))
    bonds = sorted(bonds)

    dict_rep ={'nodes':atoms, 'edges':bonds,
               'nprops': ['atomicnum', 'formalcharge'] + (['coordinates'] if coordinates else []),
               'eprops': ['order']}
    return dict_rep


def rdmol_to_json(mol, addhs=True, kekulize=True, coordinates=False):
    """
        Convert rdmol to json format for storage and import to Molecule class.

        Please note: If hydrogens are interspersed, rdkit breaks any expected ordering constraints. If this is a problem, use pybel with pymol_to_json.

        Molecules must be properly kekulized, as only bond types 1, 2, and 3 are accepted.

        Parameters:
            mol (rdmol): A molecule object obtained from an rdkit reader
        Keyword Arguments:
            addhs (bool):    Add explicit hydrogens (default=True)
            kekulize (bool): Kekulize the molecule (default=True)
    """
    from rdkit import Chem
    from rdkit.Chem import rdmolops
    if addhs or kekulize: mol = Chem.Mol(mol)
    if addhs: mol = rdmolops.AddHs( mol )
    if kekulize: rdmolops.Kekulize( mol, clearAromaticFlags=True )
    return json.dumps( _rdmol_to_dict(mol, coordinates=coordinates) )

def _pymol_to_dict(mol, lonepair=False, coordinates=False):
    obmol = mol.OBMol

    atoms = []
    for a in mol.atoms:
        props = (a.atomicnum, a.formalcharge)
        if coordinates:
            props += (a.coords,)
        atoms.append((a.idx, props))

    bonds = []
    for i in range(obmol.NumBonds()):
        obbond = obmol.GetBond(i)
        b = obbond.GetBeginAtom().GetIdx()
        e = obbond.GetEndAtom().GetIdx()
        bo = obbond.GetBondOrder()

        assert bo in {1, 2, 3}, "Bond order %d not supported, did you kekulize first?" % (bo)
        if b > e: b, e = e, b
        props = (bo,)
        bonds.append((b, e, props))
    bonds = sorted(bonds)

    if lonepair:
        lonepair_id = max([a.idx for a in mol.atoms])
        for a in mol.atoms:
            if a.atomicnum in [7, 8, 15, 16] or (a.atomicnum == 6 and a.hyb !=3):
                lonepair_id += 1
                props = (0, 0)
                if coordinates:
                    props += ((0,0,0),)
                atoms.append((lonepair_id, props))
                bonds.append((a.idx, lonepair_id, (0,)))

    dict_rep = {'nodes':atoms, 'edges':bonds,
                'nprops': ['atomicnum', 'formalcharge'] + (['coordinates'] if coordinates else []),
                'eprops': ['order']}
    return dict_rep

def pymol_to_json(mol, addhs=True, kekulize=True, lonepair=False, coordinates=False, dump=True):
    """
        Convert pybel Molecule to json format for storage and import to Molecule class.

        If hydrogens are interspersed, pybel preserves the correct atom ordering.

        Molecules must be properly kekulized, as only bond types 1, 2, and 3 are accepted.

        Parameters:
            mol (pybel.Molecule): A molecule object obtained from pybel.read*
        Keyword Arguments:
            addhs (bool):       Add explicit hydrogens (default=True)
            kekulize (bool):    Kekulize the molecule (default=True)
            lonepair (bool):    Add dummy atoms and bonds for lone pairs (default=False)
            coordinates (bool): Whether to include coordinates in the json representation, this will override addhs (default=False)
            dump (bool):        If True, return a serialized json string, otherwise return a json compatible dict
    """
    import pybel
    if coordinates:
        addhs=False
    if addhs or kekulize: mol = pybel.Molecule( pybel.ob.OBMol(mol.OBMol) )
    if addhs:    mol.addh()
    if kekulize: mol.OBMol.Kekulize()

    D = _pymol_to_dict(mol, lonepair=lonepair, coordinates=coordinates)
    return json.dumps(D) if dump else D

def sdf_to_parquet( sdf_fn, output_fn=None, numeric_ids=False, id_type=str, indexer=None, removehs=True, addhs=True, lonepair=False, coordinates=False ):
    """
        Generate a parquet file containing tflon.chem.Molecule json blobs from an SDF.

        The resulting file contains a single column 'Structure', indexed by pybel.Molecule.title

        Parameters:
            sdf_fn (str): Path to the sdf file
        Keyword Arguments:
            output_fn (str):    Output parquet file path (default=None)
            numeric_ids (bool): Index molecules by integer order of appearance in the SDF file (default=False)
            id_type (type):     Index molecules by typecasting the molecule title string (default=str)
            indexer (callable): Callable which returns an index for a pybel molecule object (default=None)
            removehs (bool):    Remove hydrogens (default=True)
            addhs (bool):       Add missing hydrogens (default=True)
            lonepair (bool):    Add dummy atoms and bonds for lone pairs (default=False)
            coordinates (bool): Add coordinates to the json molecule representations, overrides removehs (default=False)
    """
    import pybel
    df = pd.DataFrame()

    if coordinates:
        removehs = False

    tty = ManagedTTY()
    pb = click.progressbar(pybel.readfile('sdf', sdf_fn), label="Converting...", show_pos=True, file=tty.stream)
    df = pd.DataFrame(columns=['ID', 'Structure'])
    with pb, tty:
        for i, mol in enumerate(pb):
            if removehs: mol.removeh()
            if numeric_ids:
                name = i
            elif indexer is not None:
                name = indexer(mol)
            else:
                name = id_type(mol.title)
            struct = pymol_to_json(mol, addhs=addhs, lonepair=lonepair, coordinates=coordinates)
            df.loc[i] = [name, struct]
    df = df.set_index('ID')
    if output_fn:
        write_parquet( df, output_fn )
    return df


def atom_to_parquet( molecules, data, output_fn=None, numeric_ids=False, topological_index=False):
    """
        Generate a parquet file containing atom-level data from a pandas.DataFrame.

        The input atom-level data has indexes of the form molecule_id.atom_index, with one atom per row

        Each column of the data is condensed to a list of lists format, with all atoms for
        a given molecule appearing on one row, with the molecule ID as row ID.

        Atoms are sorted by their index.  Values in each list appear in this order. Atoms
        with missing data appear as NaN. This may occur with data files generated without
        hydrogens.

        Parameters:
            molecules (DataFrame): A dataframe containing tflon.chem.Molecule objects
            data (DataFrame):      Pandas dataframe containing atom-level data
            output_fn (str):       Path to an output parquet file

        Keyword Arguments:
            numeric_ids (bool):    Expect molecule_id to be the index of the molecule instead of the title (default = False)
            topological_index (bool):   Expect the atom index of the input data frame contain the topological index (T) in the format
                                        molID.T.atomID (default = False)
    """
    DF = pd.DataFrame(index = molecules.index.tolist(), columns = ['AtomIDs'] + data.columns.tolist())
    DF.index.name = 'ID'

    data['MOLID'] = data.index.map(lambda x: x.split('.')[0])
    data['ATOMID'] = data.index.map(lambda x: int(x.split('.')[-1]))

    if topological_index:
        data['TATOMID'] = data.index.map(lambda x: '.'.join((x.split('.')[-2:])))
    else:
        data['TATOMID'] = data.index.map(lambda x: '.'.join((x.split('.')[-1])))


    for i, title in enumerate(molecules.index.tolist()):
        if numeric_ids:
            dfmol = data[data['MOLID'] == str(i+1)]
        else:
            dfmol = data[data['MOLID'] == title]
        dfmol = dfmol.set_index('ATOMID')
        mol = molecules.loc[title, 'Structure']
        atoms = sorted([idx for idx in mol.graph.nodes])
        dfmol = dfmol.reindex(atoms)
        DF.loc[title] = [dfmol['TATOMID'].tolist() ] + [dfmol[col].tolist() for col in data.columns.tolist()[:-3]]

    if output_fn:
        write_parquet(DF, output_fn)
    return DF


def bond_to_parquet( molecules, data, output_fn, numeric_ids=False, lonepair=False, topological_index=False):
    """Generate a parquet file containing bond-level data from a pandas.DataFrame.

        The input bond-level data has indexes of the form molecule_id.atom1_index.atom2_index, with one bond per row

        Each column of the data is condensed to a list of lists format, with all bonds for
        a given molecule appearing on one row, with the molecule ID as row ID.

        Bonds are sorted by the natural sort on tuples of atom pair indices (i, j), with i < j.
        Values in each list appear in this order. Bonds with missing data appear as NaN. This
        may occur with data files generated without hydrogens.

        Parameters:
            molecules (DataFrame): A dataframe containing tflon.chem.Molecule objects
            data (DataFrame):      Pandas dataframe containing bond-level data
            output_fn (str):       Path to an output parquet file

        Keyword Arguments:
            numeric_ids (bool):    Expect molecule_id to be the index of the molecule instead of the title (default = False)
            topological_index (bool):   Expect the atom index of the input data frame contain the topological index (T) in the format
                                        molID.T.atomID (default = False)
    """

    DF = pd.DataFrame(index = molecules.index.tolist(), columns = ['BondIDs'] + data.columns.tolist())
    DF.index.name = 'ID'

    data['MOLID'] = data.index.map(lambda x: x.split('.')[0])
    data['BONDID'] = data.index.map(lambda x: '.'.join(str(k) for k in sorted([int(x) for x in x.split('.')[-2:]])))
    if topological_index:
        data['TBONDID'] = data.index.map(lambda x: '.'.join(str(k) for k in [x.split('.')[-3]] + sorted([int(x) for x in x.split('.')[-2:]])))
    else:
        data['TBONDID'] = data.index.map(lambda x: '.'.join(str(k) for k in sorted([int(x) for x in x.split('.')[-2:]])))


    for i, title in enumerate(molecules.index.tolist()):
        if numeric_ids:
            dfmol = data[data['MOLID'] == str(i+1)]
        else:
            dfmol = data[data['MOLID'] == title.strip()]
        dfmol = dfmol.set_index('BONDID')
        old_df = len(dfmol)
        mol = molecules.loc[title]['Structure']

        bonds = sorted([(i,j) for i,j in mol.graph.edges])

        if lonepair:
            if len(bonds) != old_df:
                DF = DF.drop(title)
                molecules=molecules.drop(title)
                logging.warn('Not equal len(bonds): %d and len(df): %d' %(len(bonds), old_df))
            else:
                lonepair_ids = [a for a in mol.graph.nodes if mol.graph.nodes[a]['atomicnum']==0]
                bonds = ['.'.join([str(x[0]), str(x[0])]) if x[1] in lonepair_ids else '.'.join([str(x[0]), str(x[1])]) for x in bonds]
                dfmol = dfmol.reindex(bonds)
                DF.loc[title] = [dfmol['TBONDID'].tolist()] + [dfmol[col].tolist() for col in data.columns.tolist()[:-3]]

        else:
            bonds = ['.'.join([str(x[0]), str(x[1])]) for x in bonds]
            dfmol = dfmol.reindex(bonds)
            DF.loc[title] = [dfmol['TBONDID'].tolist()] + [dfmol[col].tolist() for col in data.columns.tolist()[:-3]]

    if output_fn:
        write_parquet(DF, output_fn)
    return DF
