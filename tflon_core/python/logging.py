from __future__ import print_function
import sys
from datetime import datetime

def time():
    return datetime.now().strftime("%Y-%m-%d %H:%M:%S.%f")

__log_streams = {'I': sys.stderr, 'W': sys.stderr, 'E': sys.stderr, 'C': sys.stderr}
__test_mode = False
__test_logs = []

def set_test_mode():
    global __test_mode, __log_streams
    __test_mode=True
    for k in list(__log_streams.keys()):
        del __log_streams[k]

def get_test_logs():
    global __test_logs
    return __test_logs

def clear_test_logs():
    global __test_logs
    __test_logs=[]

def redirect(level, stream=None):
    global __log_streams
    if stream is None:
        if level in __log_streams: del __log_streams[level]
    else:
        __log_streams[level] = stream

def log(level, message, *args, **kwargs):
    global __log_streams, __test_mode, __test_logs
    if __test_mode:
        __test_logs.append((level, message, args, kwargs))
    if level in __log_streams:
        if len(args)>0:
            message = message % args
        print("%s: %s %s" % (time(), level, message), file=__log_streams[level])

def info(message, *args, **kwargs):
    log('I', message, *args, **kwargs)

def warn(message, *args, **kwargs):
    log('W', message, *args, **kwargs)

def error(message, *args, **kwargs):
    log('E', message, *args, **kwargs)

def critical(message, *args, **kwargs):
    log('C', message, *args, **kwargs)

def debug(message, *args, **kwargs):
    log('D', message, *args, **kwargs)
