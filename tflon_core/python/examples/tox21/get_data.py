import os
import gzip
import tflon
import pandas as pd
import pybel as pb

def convert_to_tflon_molecule(smi):
    mol = pb.readstring('smi', smi)
    return tflon.chem.pymol_to_json(mol)

def get_data():
    datadir = os.path.dirname(__file__)
    if not os.path.exists(os.path.join(datadir, 'tox21_molecules.pq')):
        if not os.path.exists(os.path.join(datadir, 'tox21.csv.gz')):
            os.system('wget https://github.com/deepchem/deepchem/blob/master/datasets/tox21.csv.gz?raw=true -O %s' % (os.path.join(datadir, 'tox21.csv.gz')))
        with gzip.open(os.path.join(datadir, 'tox21.csv.gz'), 'r') as toxfile:
            tox21 = pd.read_csv(toxfile, index_col='mol_id')

        assay_targets = tox21.iloc[:,:12]
        molecules = tox21[['smiles']].applymap(convert_to_tflon_molecule)

        tflon.data.write_parquet(assay_targets, os.path.join(datadir, 'tox21_targets.pq'))
        tflon.data.write_parquet(molecules, os.path.join(datadir, 'tox21_molecules.pq'))
    return datadir

if __name__=='__main__':
    get_data()
