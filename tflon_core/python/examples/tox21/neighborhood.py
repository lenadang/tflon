from __future__ import print_function
import tflon
import tensorflow as tf
from .get_data import get_data

class NeighborhoodConvolution(tflon.model.Model):
    def _model(self):
        molecules = self.add_table('molecules', ttype=tflon.chem.MoleculeTable)
        atoms = tflon.chem.AtomProperties(molecules)()
        targets = self.add_target('targets', shape=[None, 12])

        # Collect atom neighborhood features and apply dense layers
        net = tflon.graph.Neighborhood(molecules, depth=self.distance) |\
              tflon.toolkit.Dense(10, activation=tf.nn.tanh)

        # Reduce atom neighborhood convolutions to molecule features and classify
        net = net | tflon.graph.Reduce(molecules) | tflon.toolkit.Dense(12)
        logits = net(atoms)

        self.add_output('outputs', tf.nn.sigmoid(logits))
        self.add_loss('xent', tflon.toolkit.xent(targets, logits, nans=True))
        self.add_loss('l2', tflon.toolkit.l2_penalty(self.weights))
        self.add_metric('auc', tflon.toolkit.auc(targets, logits, axis=0, nans=True))

    def _parameters(self):
        return {'distance': 5}

if __name__=='__main__':
    datadir = get_data()

    # Create a model, filling in required parameters
    model = NeighborhoodConvolution()

    # Load sharded molecule data with the model schema
    schema = model.schema.map(molecules='tox21_molecules.pq', targets='tox21_targets.pq')
    feed = schema.load(datadir)

    # Create an adam optimizer
    trainer = tflon.train.TFTrainer( tf.train.AdamOptimizer(1e-3), iterations=100 )

    with tf.Session():
        model.fit( feed.shuffle(batch_size=100), trainer, restarts=1 )
        auc = model.evaluate( feed.iterate(batch_size=100), query='auc' )
        print("AUC:", auc)
