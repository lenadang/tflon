from __future__ import print_function
import tflon
import tensorflow as tf
from .get_data import get_data


class Weave(tflon.model.Model):
    def _model(self):
        molecules = self.add_table('molecules', tflon.chem.MoleculeTable)
        atoms = tflon.chem.AtomProperties(molecules)()
        targets = self.add_target('targets', shape=[None, 12])

        W = tflon.graph.Weave(molecules, pair_width=10, node_width=15, activation=tf.nn.elu)

        # Apply layers of weave convolution, reduce to molecule features and classify
        net = W.repeat(self.layers-1) | W.outputs | tflon.graph.Reduce(molecules) | tflon.toolkit.Dense(12)
        logits = net(atoms)

        self.add_output('outputs', tf.nn.sigmoid(logits))
        self.add_loss('xent', tflon.toolkit.xent(targets, logits, nans=True))
        self.add_loss('l2', tflon.toolkit.l2_penalty(self.weights))
        self.add_metric('auc', tflon.toolkit.auc(targets, logits, axis=0, nans=True))

    def _parameters(self):
        return {'layers': 10}

if __name__=='__main__':
    datadir = get_data()

    # Create a model, filling in required parameters
    model = Weave(layers=5)

    # Load sharded molecule data with the model schema
    schema = model.schema.map(molecules='tox21_molecules.pq', targets='tox21_targets.pq')
    feed = schema.load(datadir)

    # Create an adam optimizer
    trainer = tflon.train.TFTrainer( tf.train.AdamOptimizer(1e-3), iterations=100 )

    with tf.Session():
        model.fit( feed.shuffle(batch_size=100), trainer, restarts=1, processes=2, pipeline='multiprocessing' )
        metrics = model.evaluate( feed.iterate(batch_size=100) )
        print("AUC:", metrics['auc'])
