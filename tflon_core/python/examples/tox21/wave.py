from __future__ import print_function
import tflon
import tensorflow as tf
from .get_data import get_data

class Wave(tflon.model.Model):
    def _model(self):
        molecules = self.add_table('molecules', ttype=tflon.chem.MoleculeTable)
        atoms = tflon.chem.AtomProperties(molecules)()
        targets = self.add_target('targets', shape=[None, 12])

        resize = tflon.toolkit.Dense(10)(atoms)

        # Apply one pass of wave with state_size of 10

        graph_recursion = tflon.graph.GRNN(molecules)
        forward_cell = graph_recursion.wrap_cell(tf.nn.rnn_cell.GRUCell(10))
        backward_cell = graph_recursion.wrap_cell(tf.nn.rnn_cell.GRUCell(10))
        output = graph_recursion(resize, forward_cell, backward_cell)

        # Reduce wave output to molecule features and classify
        net = tflon.graph.Reduce(molecules) | tflon.toolkit.Dense(12)
        logits = net(output)

        self.add_output('outputs', tf.nn.sigmoid(logits))
        self.add_loss('xent', tflon.toolkit.xent(targets, logits, nans=True))
        self.add_loss('l2', tflon.toolkit.l2_penalty(self.weights))
        self.add_metric('auc', tflon.toolkit.auc(targets, logits, axis=0, nans=True))

if __name__=='__main__':
    config = tflon.system.configure_resources(reserved_cpus=2)
    datadir = get_data()

    # Create a model, filling in required parameters
    model = Wave()

    # Load sharded molecule data with the model schema
    model.schema.map(molecules='tox21_molecules.pq', targets='tox21_targets.pq')
    feed = model.schema.load(datadir)

    # Create an adam optimizer
    trainer = tflon.train.TFTrainer( tf.train.AdamOptimizer(1e-3), iterations=100 )

    with tf.Session(config=config):
        model.fit( feed.shuffle(batch_size=100), trainer, restarts=1, processes=4, pipeline='multiprocessing' )
        metrics = model.evaluate( feed.iterate(batch_size=100) )
        print("AUC:", metrics['auc'])
