from builtins import range
import sys
import numpy as np
import networkx as nx
import pandas as pd
import tflon
import random

class Maze(object):
    def __init__(self, size, start, end):
        self.size = size
        self.start = start
        self.end = end

        self.generate()

    def generate(self):
        self.search = list(self.dfs(self.size, self.start, self.end))

    def goals(self):
        G = np.zeros(np.array(self.size)*2-1, dtype=np.float32)
        G[tuple(np.array(self.start) * 2)] = 1
        G[tuple(np.array(self.end) * 2)] = 1
        return G

    def walls(self):
        M = np.zeros(np.array(self.size)*2-1, dtype=np.float32)
        M[1::2,1::2] = 1
        for t, s, e in self.search:
            if t == 'b': M[self.wall_coord(s,e)] = 1
        return M

    def solution(self):
        M = np.zeros(np.array(self.size)*2-1, dtype=np.float32)
        for p in self.path2goal:
            M[(p[0]*2, p[1]*2)] = 1

        for s,e in zip(self.path2goal[1:], self.path2goal[:-1]):
            M[self.wall_coord(s,e)] = 1

        return M

    def wall_coord(self, c1, c2):
        if c1[0] == c2[0]: return c1[0] * 2, c1[1] + c2[1]
        else: return c1[0] + c2[0], c1[1] * 2

    def neighbors(self, index, size):
        if index[0] - 1 >= 0: yield (index[0] - 1, index[1])
        if index[1] - 1 >= 0: yield (index[0], index[1] - 1)
        if index[0] + 1 < size[0]: yield (index[0] + 1, index[1])
        if index[1] + 1 < size[1]: yield (index[0], index[1] + 1)

    def dfs(self, size, start, goal):
        visited = np.zeros(size, dtype=int)
        here = start

        visited[here] = 1
        neighbors = list(self.neighbors(here, visited.shape))
        random.shuffle(neighbors)
        stack = []

        path = []

        while True:
            if neighbors:
                n = neighbors.pop()
            elif stack:
                here, neighbors = stack.pop()
                path.pop()
                continue
            else:
                break

            if visited[n] == 1:
                yield 'b', here, n
            else:
                yield 't', here, n
                visited[n] = 1
                stack.append((here, neighbors))
                path.append(here)


                if np.all(n == goal):
                    self.path2goal = path + [n]
                    neighbors = []
                else:
                    neighbors = list(e for e in self.neighbors(n, visited.shape) if np.any(e != here))
                    random.shuffle(neighbors)

                here = n

    def display(self, ax=None, color_scheme=['white', 'black', 'blue', 'red']):
        M = self.walls()
        G = self.goals()
        S = self.solution()

        import seaborn as sns
        sns.heatmap(M + G + 2*S, cmap=color_scheme, vmin=0, vmax=3, cbar=False, square=True, ax=ax)

def maze_creator(m,n):
    size = (m,n)
    # Choices define all the possible start statesa and goal states
    choices = [(0,0),(m-1,n-1),(0,n-1),(m-1,0)]
    # Chooses two random points for start and goal
    start, end = random.sample(choices, 2)
    M = Maze(size=size, start=start, end=end)
    return M.walls(), M.goals(), M.solution()


def maze_generator(m,n,batchsize):
    mazes = np.zeros((batchsize, m*2-1, n*2-1, 3))
    i = 0
    while True:
        w,g,s = maze_creator(m,n)
        mazes[i,:,:,0] = w
        mazes[i,:,:,1] = g
        mazes[i,:,:,2] = s
        i += 1
        if i == batchsize:
            yield mazes
            mazes = np.zeros((batchsize, m*2-1, n*2-1, 3))
            i=0

def mazes_to_tables(mat, graph, N):
    index = list(range(N))

    L = len(graph.nodes)
    nodes = np.full((N*L, 2), None, dtype=np.float32)
    solution = np.full((N*L, 1), None, dtype=np.float32)
    graphs = np.full((N, 1), None, dtype=np.object_)
    for i in range(N):
        W = mat[i,:,:,0]
        G = mat[i,:,:,1]
        S = mat[i,:,:,2]

        nodes[i*L:(i+1)*L,0] = W.reshape((-1))
        nodes[i*L:(i+1)*L,1] = G.reshape((-1))
        solution[i*L:(i+1)*L,0] = S.reshape((-1))
        graphs[i,0] = graph

    return {'nodes':nodes,
            'solution':solution,
            'size':int(np.sqrt(L)),
            'graphs':tflon.data.Table(pd.DataFrame(data=graphs, index=index))}

def get_graph(s):
    G = nx.Graph()
    i=2*s-1
    [G.add_node(j*i+k+1) for j in range(i) for k in range(i)]
    for j in range(i):
        for k in range(i):
            if j < i-1:
                G.add_edge(j*i+k+1,(j+1)*i+k+1)
            if k < i-1:
                G.add_edge(j*i+k+1,j*i+(k+1)+1)
    return tflon.graph.Graph(G)

def graph_maze_iterator(size, N):
    iterator = maze_generator(size, size, N)
    graph = get_graph(size)
    while 1:
        mat = next(iterator)
        yield mazes_to_tables(mat, graph, N)
