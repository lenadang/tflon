from builtins import range
from .maze import graph_maze_iterator
from .model import Wave
import tflon
import tensorflow as tf
import click

@click.command()
@click.option('-i', '--iterations', default=60000)
@click.option('-f', '--frequency', default=3000)
@click.option('-b', '--batch-size', default=50)
@click.option('-r', '--resume', default=False, is_flag=True)
@click.argument('modelfile')
def main(iterations, batch_size, resume, frequency, modelfile):
    model = Wave()
    summary_hook = tflon.train.SummaryHook('scratch/maze/logs', frequency=20)
    opt = tflon.train.GradientClippingOptimizer(tf.train.AdamOptimizer(1e-3), 10.0)

    maze_generators = [graph_maze_iterator(size, batch_size) for size in range(3,11)]
    curriculum = tflon.train.FixedIntervalCurriculum(maze_generators, frequency=frequency)
    trainer = tflon.train.TFTrainer(opt, iterations=iterations, checkpoint=('scratch/maze/ckpt', 1000), resume=resume, hooks=[summary_hook, curriculum])

    with tf.Session() as S:
        model.fit(curriculum.iterate(), trainer, processes=2)
        model.save(modelfile)

if __name__=='__main__':
    main()
