import tensorflow as tf
from tflon.toolkit.toolkit import get_next_name_scope

class WrappedOptimizer(tf.train.Optimizer):
    """
        An optimizer that wraps another tf.Optimizer, used to modify gradient computations.
    """

    def __init__(self, optimizer, name=None, use_locking=False):
        super(WrappedOptimizer, self).__init__(name=name, use_locking=use_locking)
        self._optimizer = optimizer

    def variables(self):
        return self._optimizer.variables()

    def get_slot_names(self):
        return self._optimizer.get_slot_names()

    def get_slot(self, var, sn):
        return self._optimizer.get_slot(var, sn)

    def _apply_dense(self, *args, **kwargs):
        """Calls this same method on the underlying optimizer."""
        return self._optimizer._apply_dense(*args, **kwargs)

    def _resource_apply_dense(self, *args, **kwargs):
        """Calls this same method on the underlying optimizer."""
        return self._optimizer._resource_apply_dense(*args, **kwargs)

    def _resource_apply_sparse_duplicate_indices(self, *args, **kwargs):
        """Calls this same method on the underlying optimizer."""
        return self._optimizer._resource_apply_sparse_duplicate_indices(*args, **kwargs)

    def _resource_apply_sparse(self, *args, **kwargs):
        """Calls this same method on the underlying optimizer."""
        return self._optimizer._resource_apply_sparse(*args, **kwargs)

    def _apply_sparse_duplicate_indices(self, *args, **kwargs):
        """Calls this same method on the underlying optimizer."""
        return self._optimizer._apply_sparse_duplicate_indices(*args, **kwargs)

    def _apply_sparse(self, *args, **kwargs):
        """Calls this same method on the underlying optimizer."""
        return self._optimizer._apply_sparse(*args, **kwargs)

    def _prepare(self, *args, **kwargs):
        """Calls this same method on the underlying optimizer."""
        return self._optimizer._prepare(*args, **kwargs)

    def _create_slots(self, *args, **kwargs):
        """Calls this same method on the underlying optimizer."""
        return self._optimizer._create_slots(*args, **kwargs)

    def _valid_dtypes(self, *args, **kwargs):
        """Calls this same method on the underlying optimizer."""
        return self._optimizer._valid_dtypes(*args, **kwargs)

    def _finish(self, *args, **kwargs):
        """Calls this same method on the underlying optimizer."""
        return self._optimizer._finish(*args, **kwargs)

class GradientClippingOptimizer(WrappedOptimizer):
    """
        Wrapper for tf.train.Optimizer instances which applys tf.clip_by_global_norm to the optimizer gradients

        Parameters:
            optimizer (tf.train.Optimizer): An optimizer instance to wrap and apply clipping
            clip_value (float):             The global norm threshold for clipping
    """
    def __init__(self, optimizer, clip_value, **kwargs):
        super(GradientClippingOptimizer, self).__init__(optimizer, name='GradientClippingOptimizer', **kwargs)
        self._clip_value = clip_value

    def compute_gradients(self, *args, **kwargs):
        with tf.name_scope(get_next_name_scope(self.__class__.__name__)):
            grad_ops = self._optimizer.compute_gradients(*args, **kwargs)
            grads, vars = zip(*grad_ops)
            clipped, _norm = tf.clip_by_global_norm(grads, self._clip_value)
        return list(zip(clipped, vars))

class GradientAccumulatorOptimizer(WrappedOptimizer):
    """
        Wrapper for tf.train.Optimizer instances which accumulates gradients over several minibatches before applying the gradients

        Parameters:
            optimizer (tf.train.Optimizer): An optimizer instance to wrap and accumulate gradients
            gradient_steps (int):           The number of steps over which to accumulate gradients
    """
    def __init__(self, optimizer, gradient_steps, **kwargs):
        super(GradientAccumulatorOptimizer, self).__init__(optimizer, name='GradientAccumulatorOptimizer', **kwargs)
        self._grad_steps = gradient_steps
        self._global_iter = tf.train.get_or_create_global_step()

    def compute_gradients(self, *args, **kwargs):
        with tf.name_scope(get_next_name_scope(self.__class__.__name__)):
            grad_ops = self._optimizer.compute_gradients(*args, **kwargs)
            self._grad_acc = [ tf.Variable(tf.zeros(var.get_shape()), name=var.op.name.rsplit('/')[-1]+"_Gradient_Accumulator") for _grad, var in grad_ops ]
            acc_ops = []
            grad_inject = []
            for accumulator, (grad, var) in zip(self._grad_acc, grad_ops):
                acc_ops.append( tf.assign_add( accumulator, grad ) )
                grad_inject.append( (accumulator, var) )
                assert tvar.name == var.name, "Variable order returned was not consistent with trainables..."

            self._update_op = tf.group( *acc_ops )
        return grad_inject

    def apply_gradients(self, gradients):
        with tf.name_scope(get_next_name_scope(self.__class__.__name__)):
            _apply_op = self._optimizer.apply_gradients( gradients )

            def apply_and_reset():
                with tf.control_dependencies([_apply_op]):
                    return tf.variables_initializer( self._grad_acc )

            with tf.control_dependencies([self._update_op]):
                return tf.cond( tf.equal( tf.mod(self._global_iter, self._grad_steps), 0 ),
                                apply_and_reset, tf.no_op )


