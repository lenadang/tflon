from __future__ import division
from builtins import range
import numpy as np
import tensorflow as tf
from tflon.train.trainer import Hook
from tflon.utils import synchronized

class BoostGradient(object):
    INITIAL_VALUE=1000000.
    """
        Parameters:
            feed (tflon.data.TableFeed):   The dataset to sample
            view (tflon.train.View):       A view to the non-reduced loss tensor
    """
    def __init__(self, feed, view):
        self._feed = feed
        view.listen(self.notify)
        self._index = feed.index
        self._index_map = {k:i for i,k in enumerate(feed.index.astype(np.string_))}
        self._weights = np.full(self._index.shape, BoostGradient.INITIAL_VALUE)
        self._probability = self._weights / np.sum(self._weights)

    @synchronized('weights')
    def notify(self, index, values):
        indexer = np.fromiter((self._index_map[ix] for ix in index), np.int32, index.shape[0])
        self._weights[indexer] = values.reshape((-1,))
        self._probability = self._weights - np.min(self._weights)
        self._probability = (self._probability) / np.sum(self._probability)

    @synchronized('weights')
    def sample(self, batch_size):
        return np.random.choice(self._index, size=(batch_size,), replace=False, p=self._probability)

    def shuffle(self, batch_size):
        while 1:
            ix = self.sample(batch_size)
            yield self._feed.batch(ix)

class ClassResampling(object):
    def __init__(self, feed, targets, max_per_epoch=10):
        self._feed = feed
        self._index = feed.index

        targ = feed[targets].as_matrix()
        counts = np.maximum(np.nansum(targ, axis=0), np.ones((targ.shape[1],)))
        relfreq = np.minimum(np.round(max(counts) / counts).astype(np.int32), max_per_epoch)
        self._counts = np.maximum(np.nanmax(relfreq * targ, axis=1), 1)
        self._counts[np.isnan(self._counts)] = 0
        self._used = np.zeros(self._counts.shape)
        self._epoch=0

    @property
    def epoch(self):
        return self._epoch

    @synchronized('weights')
    def sample(self, batch_size):
        remaining = self._counts-self._used
        total = float(remaining.sum())
        valid = remaining>0
        num = valid.sum()
        if num == 0:
            self._epoch+=1
            self._used[:] = 0
            remaining = self._counts-self._used
            total = float(remaining.sum())
            valid = remaining>0
        elif num < batch_size:
            self._epoch+=1
            rest = self._index[valid]
            self._used[:] = 0

            remaining = self._counts-self._used
            total = float(remaining.sum())
            valid = remaining>0
            extra = np.random.choice(self._index[valid], size=(batch_size-rest.shape[0],), replace=False, p=remaining[valid]/total)
            self._used[np.isin(self._index, extra)] += 1
            return np.concatenate([rest, extra], axis=0)

        chosen = np.random.choice(self._index[valid], size=(batch_size,), replace=False, p=remaining[valid]/total)
        self._used[np.isin(self._index, chosen)] += 1
        return chosen

    def shuffle(self, batch_size):
        while 1:
            ix = self.sample(batch_size)
            yield self._feed.batch(ix)

def curriculum_step_distribute(dist, level, decay=0.25):
    level = min(len(dist)-1, level)
    dist = dist.copy()
    residual = np.sum(dist)*decay
    dist = dist*decay
    dist[:level+1] += (1-residual) / (level+1)
    return dist

class Curriculum(Hook):
    """
        Interface for curriculum orchestration, curriculum strategies must implement:
            def evaluate(self, model, step, loss):
                return False

        Usage Example:

            level1 = ... # Some function returning generators of batches
            level2 = ... # ditto
            trainer = tflon.train.TFTrainer(tf.train.AdamOptimizer(1e-3))
            curr = tflon.train.FixedIntervalCurriculum([level1, level2])
            model.fit(curr.iterate(), trainer)

        Parameters:
            levels (list):                  List of functions returning generators corresponding to sorted order of occurence for curriculum examples

        Keyword Arguments:
            frequency (int, required):      Number of steps between curriculum evaluations
            step_function (function):       The type of curriculum update function to use (default=curriculum_step_distribute)
        """

    def __init__(self, levels, step_function=curriculum_step_distribute, **kwargs):
        super(Curriculum, self).__init__(**kwargs)
        self._levels = levels[:]
        self._level_rates = self.get_variable('curriculum_level_sampling_rates', [len(levels)], dtype=tf.float32, initializer=tf.constant_initializer(np.array([1] + [0]*(len(levels)-1), dtype=np.float32)))
        self._current_level = self.get_variable('curriculum_level', [], dtype=tf.int32, initializer=tf.constant_initializer(0))
        self._step_func = step_function

    def evaluate(self, step, loss):
        """
            Curriculum evaluation

            Parameters:
                step (numpy.int64):        The current global optimization step
                loss (numpy.float32):      The current optimizer loss

            Returns:
                boolean:   Indicator for early termination of training (see tflon.train.Hook)
        """
        raise NotImplementedError()

    def iterate(self, samples_per_batch=1):
        while 1:
            current_rates = self._level_rates.eval()
            pdist = current_rates/np.sum(current_rates)
            indices = range(len(self._levels))
            samples = []
            for j in range(samples_per_batch):
                level = np.random.choice(indices, p=pdist)
                samples.append( next(self._levels[level]) )

            if samples_per_batch > 1:
                yield samples[0].merge(*samples[1:])
            else:
                yield samples[0]

    def call(self, step, loss):
        increment_level = self._current_level.assign_add(1)
        update_rates = self._level_rates.assign( tf.numpy_function(self._step_func, [self._level_rates, self._current_level+1], tf.float32) )
        with tf.control_dependencies([increment_level, update_rates]):
            update_op = tf.numpy_function(lambda step, loss: False, [step, loss], [tf.bool])
        return tf.cond( tf.numpy_function(lambda step, loss: self.evaluate(step, loss), [step, loss], tf.bool), lambda: update_op, lambda: False)


class FixedIntervalCurriculum(Curriculum):
    """
        Parameters:
            levels (list):                  List of functions returning generators corresponding to sorted order of occurence for curriculum examples

        Keyword Arguments:
            frequency (int, required): The number of steps between curriculum changes
    """
    def __init__(self, *args, **kwargs):
        Curriculum.__init__(self, *args, **kwargs)

    def evaluate(self, step, loss):
        return True

class MetricGatedCurriculum(Curriculum):
    """
        Parameters:
            criterion (lambda):             Lambda expression taking result of model.evaluate and returning whether the current level is passed
            levels (list):                  List of functions returning generators corresponding to sorted order of occurence for curriculum examples

        Keyword Arguments:
            frequency (int, required):      The number of steps between test evaluations
    """
    def __init__(self, criterion, *args, **kwargs):
        self._criterion = criterion
        Curriculum.__init__(self, *args, **kwargs)

    def setup(self, model, trainer):
        self._eval = self._model.evaluate

    def evaluate(self, step, loss):
        clevel = min(self._current_level.eval(), len(self._levels)-1)
        batch = next(self._levels[clevel])
        metrics = self._eval(batch)
        passed = self._criterion(metrics)
        return passed

def merge_feeds(*iterators):
    """
        Merge multiple feed iterators into a single iterator
    """
    while 1:
        batches = [next(it) for it in iterators]
        yield batches[0].merge(*batches[1:])
