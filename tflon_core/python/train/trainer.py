from builtins import range
import tensorflow as tf
from tflon import logging
from tflon.toolkit.toolkit import get_next_name_scope
from tflon.model.tower import merge
from tflon.system import cpu_usage, num_gpus, gpu_usage, gpu_memory, memory_usage
from tflon.utils import ManagedTTY
import scipy.optimize
import click, sys, os
import numpy as np
import types
from tensorflow.python.client import timeline
import ujson as json

class Hook(object):
    """
        Base class for training hooks. New hooks should inherit this class and override Hook.call (optionally, override Hook.finish)

        Hook.call should return a tensorflow op, usually constructed using tf.numpy_function
        The op returned by Hook.call will be called at fixed intervals

        Hook.finish is called after a Trainer completes Trainer.train

        Parameters:
            frequency (int): Number of iterations between invocations of this hook's call method
    """
    def __init__(self, frequency):
        self._frequency = frequency
        self._name_scope = get_next_name_scope(self.__class__.__name__)

    def __call__(self, step, loss):
        with tf.name_scope(self._name_scope):
            return tf.cond(tf.equal(tf.mod(step, self._frequency), 0), lambda: self.call(step, loss), lambda: False)

    @property
    def variables(self):
        return tf.get_collection( tf.GraphKeys.GLOBAL_VARIABLES, scope=self._name_scope )

    def get_variable(self, name, shape, dtype=tf.float32, initializer=tf.zeros_initializer()):
        with tf.variable_scope(self._name_scope):
            return tf.get_variable(name, shape=shape, dtype=dtype, initializer=initializer, collections=[tf.GraphKeys.GLOBAL_VARIABLES])

    def setup(self, model, trainer, loss):
        with tf.name_scope(self._name_scope):
            self._setup(model, trainer, loss)

    def _setup(self, model, trainer, loss):
        pass

    def start(self):
        pass

    def call(self, step, loss):
        raise NotImplementedError()

    def finish(self):
        pass

class LearningRateDecayHook(Hook):
    def __init__(self, initial_rate, decay, frequency):
        super(LearningRateDecayHook, self).__init__(frequency)
        self._initial_rate = initial_rate
        self._decay = decay
        self._learning_rate = self.get_variable("learning_rate", shape=[], initializer=tf.constant_initializer(initial_rate), dtype=tf.float32)

    @property
    def learning_rate(self):
        return self._learning_rate

    def call(self, step, loss):
        with tf.control_dependencies([ self._learning_rate.assign(self._learning_rate*self._decay) ]):
            return tf.constant(False)

class LambdaHook(Hook):
    """
        A hook used to wrap lambda functions passed as hooks to Trainer. This is automatically applied by Trainer
    """
    def __init__(self, func):
        super(LambdaHook, self).__init__(1)
        self._func = func

    def call(self, step, loss):
        return tf.numpy_function(self._func, [step, loss], [tf.bool])

class LogProgressHook(Hook):
    """
        Hook to print progress to screen at specified intervals, automatically created by TFTrainer and SciPyTrainer.

        Parameters:
            frequency (int): The interval between logging output
            maxiter (int):   The number of training iterations
    """
    def __init__(self, frequency, maxiter):
        super(LogProgressHook, self).__init__(frequency)
        self._maxiter = maxiter

    def _setup(self, model, trainer, loss):
        self._qtil = trainer.queue_utilization

    def call(self, step, loss):
        swidth = str(len(str(self._maxiter)))
        def log_message(step, loss, qpercent):
            logging.info(("Step % "+swidth+"d / % "+swidth+"d, Q: % 3d, Loss: %.3e") % (step, self._maxiter, int(qpercent), loss))
            return False
        return tf.numpy_function(log_message, [step, loss, self._qtil], [tf.bool])

class SummaryHook(Hook):
    """
        Hook to write tensorboard logs. Only logs loss values by default

        Parameters:
            directory (str): The directory to create tensorboard event files

        Keyword Arguments:
            frequency (int): The number of iterations between log entries (default=10)
            summarize_trainables (bool): Include histograms of all weight and bias variables (default=False)
            summarize_gradients (bool):  Include plots of all gradient magnitudes (default=False)
            summarize_losses (bool):  Include plots of all loss magnitudes (default=True)
    """
    def __init__(self, directory, frequency=10, summarize_trainables=False, summarize_gradients=False, summarize_activations=False, summarize_losses=True, summarize_resources=True, learning_rate=None):
        super(SummaryHook, self).__init__(frequency)
        self._directory = directory
        self._writer = None
        self._learning_rate = learning_rate
        self._global_step = tf.train.get_or_create_global_step()
        self._flags = {'trainables': summarize_trainables,
                       'gradients': summarize_gradients,
                       'activations': summarize_activations,
                       'losses': summarize_losses,
                       'resources': summarize_resources}

    def _setup(self, model, trainer, loss):
        if not os.path.exists(self._directory):
            os.makedirs(self._directory)
        self._writer = tf.summary.FileWriter(self._directory, tf.get_default_graph())
        summaries = []
        if self._flags['trainables']:
            for var in model.trainables:
                summaries.append(tf.summary.histogram("trainables/%s" % (var.name), var))
        if self._flags['gradients']:
            for grad, var in model.gradients:
                summaries.append(tf.summary.scalar("gradients/%s" % (var.name), tf.sqrt(tf.reduce_sum(tf.square(grad)))))
        if self._flags['activations']:
            for name, module in model.modules.items():
                for t in module.tensors:
                    summaries.append(tf.summary.histogram("activations/%s" % (t.name), t))
        if self._flags['losses']:
            losses = model.losses
            for key in losses:
                summaries.append(tf.summary.scalar("losses/%s" % (key), losses[key]))
            summaries.append(tf.summary.scalar("losses/total_sum", loss))
        if self._flags['resources']:
            summaries.append( tf.summary.scalar("resources/memory_used_MB", tf.numpy_function(memory_usage, [], tf.float64)) )
            summaries.append( tf.summary.scalar("resources/cpu_utilization_pct", tf.numpy_function(cpu_usage, [], tf.float64)) )
            summaries.append( tf.summary.scalar("resources/queue_utilization_pct", trainer.queue_utilization ) )
            for i in range(num_gpus()):
                summaries.append( tf.summary.scalar("resources/gpu_%d_utilization_pct" % (i), tf.numpy_function(gpu_usage, [i], tf.float64)) )
                summaries.append( tf.summary.scalar("resources/gpu_%d_memory_pct" % (i), tf.numpy_function(gpu_memory, [i], tf.float64)) )
        if self._learning_rate is not None:
            summaries.append( tf.summary.scalar("learning_rate", self._learning_rate) )

        self._summary_op = tf.summary.merge(summaries)

    def call(self, step, loss):
        def write_summaries(summ, step):
            self._writer.add_summary(summ, step)
            return False
        return tf.numpy_function(write_summaries, [self._summary_op, self._global_step], [tf.bool])

    def finish(self):
        if self._writer is not None:
            self._writer.flush()

def convert_to_hook(h):
    if isinstance(h, Hook): return h
    elif type(h) is types.LambdaType: return LambdaHook(h)
    elif type(h) is types.FunctionType: return LambdaHook(h)
    else:
        raise Exception("Expected instance of Hook or callable with arguments (step, loss)")

class TimelineProfiler(object):
    def __init__(self, outfile):
        self._timeline = None
        self._run_metadata = None
        self._timeline_dict = None
        self._step = 0
        self._profiler = tf.profiler.Profiler(tf.get_default_graph())
        self._run_options = tf.RunOptions(trace_level=tf.RunOptions.FULL_TRACE)
        self._writefile = os.path.realpath(outfile)

        if not os.path.exists(os.path.dirname(self._writefile)):
            os.makedirs(os.path.dirname(self._writefile))

    def record_trace(self):
        tl = timeline.Timeline(self._run_metadata.step_stats)
        ctf = tl.generate_chrome_trace_format()
        chrome_trace_dict = json.loads(ctf)
        if self._timeline_dict is None:
            self._timeline_dict = chrome_trace_dict
        else:
            for event in chrome_trace_dict['traceEvents']:
                if 'ts' in event:
                    self._timeline_dict['traceEvents'].append(event)

    @property
    def run_metadata(self):
        return self._run_metadata

    @property
    def run_options(self):
        return self._run_options

    def new_meta(self):
        self._step += 1
        self._run_metadata = tf.RunMetadata()

    def finish(self):
        with open(self._writefile, 'w') as f:
            json.dump(self._timeline_dict, f)

class View(object):
    """
        Provides a view to a tensor value, updated during each training step
    """

    def __init__(self, output):
        self._output = output
        self._listeners = []

    def setup(self, model):
        self._op = model.outputs[self._output]
        return self._op

    def listen(self, listener):
        self._listeners.append(listener)

    def update(self, index, value):
        for l in self._listeners:
            l(index, value)

class Trainer(object):
    """
        Trainer base class for use with tflon Model.fit

        Keyword Arguments:
            hooks (list): Zero or more Hook objects, to be called during training (default=[])
    """
    def __init__(self, hooks=[]):
        self._hooks = []
        self._views = []
        for h in hooks: self.add_hook(h)
        self._global_iter = tf.train.get_or_create_global_step()
        self._name_scope = get_next_name_scope(self.__class__.__name__)
        self._local_iter = self.get_variable("optimization_step", shape=[], initializer=tf.constant_initializer(0), dtype=tf.int32)
        self._step_op = tf.group(self._global_iter.assign_add(1), self._local_iter.assign_add(1))

    def get_variable(self, name, shape, dtype=tf.float32, initializer=tf.zeros_initializer()):
        with tf.variable_scope(self._name_scope):
            return tf.get_variable(name, shape=shape, dtype=dtype, initializer=initializer, collections=[tf.GraphKeys.GLOBAL_VARIABLES])

    def _get_scope_vars(self):
        return tf.get_collection( tf.GraphKeys.GLOBAL_VARIABLES, scope=self._name_scope )

    def _get_hook_vars(self):
        varlist=[]
        for hook in self._hooks:
            varlist += hook.variables
        return varlist

    @property
    def views(self):
        return self._views

    def add_view(self, output):
        v = View(output)
        self._views.append(v)
        return v

    @property
    def hooks(self):
        return self._hooks

    def add_hook(self, hook):
        self._hooks.append(convert_to_hook(hook))

    def process_views(self, model):
        return [v.setup(model) for v in self._views]

    def notify_views(self, index, values):
        assert len(values)==len(self._views)
        for view, value in zip(self._views, values):
            view.update(index, value)

    def process_hooks(self, model, step, loss):
        for h in self.hooks: h.setup(model, self, loss)
        hook_ops = [tf.reshape(tf.cast(hook(step, loss), tf.int32), (-1,)) for hook in self._hooks]
        if len(hook_ops)==0:
            return tf.constant(False)
        return tf.greater( tf.add_n(hook_ops), 0 )

    def finish_hooks(self):
        [hook.finish() for hook in self._hooks]

    def _get_trainables(self,model):
        raise NotImplementedError()

    def setup(self, model):
        raise NotImplementedError()

    def train(self, fetchable):
        raise NotImplementedError()

    @property
    def queue_utilization(self):
        return tf.constant(100., dtype=tf.float32)

class TFTrainer(Trainer):
    """
        Wrapper that applies an optimizer inheriting from tf.train.Optimizer to train a tflon model.

        Parameters:
            optimizer (tf.train.Optimizer): An instance of tensorflow Optimizer
            iterations (int): The number of training iterations

        Keyword Arguments:
            checkpoint (tuple):  None or pair of directory (str) and frequency (int).
                                 If not None, then write checkpoint files to the specified directory at specified intervals (default=None)
            resume (bool):       Resume from checkpoint file, if available (default=False)
            log_frequency (int): Frequency to print progress to screen or log file, if None, don't log progress (default=None)
    """
    def __init__(self, optimizer, iterations, checkpoint=None, resume=False, log_frequency=None, gpu_profile_file=None, fetch_retries=5, **kwargs):
        super(TFTrainer, self).__init__(**kwargs)
        self._iterations = iterations
        self._optimizer = optimizer
        self._checkpoint = checkpoint
        self._resume = resume
        self._tprofiler = None
        self._retries = fetch_retries
        if not sys.stderr.isatty() and log_frequency is not None:
            self.add_hook( LogProgressHook(log_frequency, iterations) )
        if gpu_profile_file is not None:
            self._tprofiler = TimelineProfiler(gpu_profile_file)
        self._queue_utilization = self.get_variable("queue_utilization", shape=[], dtype=tf.float32, initializer=tf.constant_initializer(0.))

    def _get_optimizer_vars(self):
        opt = self._optimizer
        vlist = [opt.get_slot(var, sn) for var in self._trainables for sn in opt.get_slot_names()]
        vlist = filter(lambda v: v is not None, vlist)
        vlist = merge(vlist, opt.variables())
        return vlist

    @property
    def variables(self):
        return self._variables

    def _get_trainables(self,model):
        return model.trainables

    def setup(self, model):
        self._trainables = self._get_trainables(model)
        self._index_op = model.index
        self._loss_op = model.loss
        self._grad_op = self._optimizer.compute_gradients( self._loss_op, var_list=self._trainables )
        model.set_gradients( self._grad_op )
        update_ops = tf.get_collection(tf.GraphKeys.UPDATE_OPS)
        with tf.control_dependencies(update_ops):
            self._min_op = self._optimizer.apply_gradients( self._grad_op )
        self._variables = self._get_optimizer_vars() + self._get_scope_vars() + self._get_hook_vars()
        self._init_op = tf.variables_initializer( self._variables + self._trainables )
        if self._checkpoint is not None:
            ckpt_dir, _ = self._checkpoint
            self._saver = tf.train.Saver(self._variables + self._trainables)
            if not os.path.exists(ckpt_dir):
                os.makedirs(ckpt_dir)
        with tf.control_dependencies([self._min_op]):
            self._hook_op = self.process_hooks(model, self._local_iter, self._loss_op)
        self._view_ops = self.process_views(model)

    @property
    def queue_utilization(self):
        return self._queue_utilization

    def refresh(self):
        S = tf.get_default_session()
        S.run(self._init_op)
        for hook in self._hooks: hook.start()

        self.start_iter=0
        if self._resume and self._checkpoint:
            self.start_iter = self._restore()

    def _restore(self):
        S = tf.get_default_session()
        start_iter = 0

        ckpt_dir, _ = self._checkpoint
        ckpts = []
        for fn in os.listdir(ckpt_dir):
            sp = fn.split('.')
            if sp[0] == 'ckpt' and sp[2]=='index':
                iter_num = int(sp[1])
                ckpts.append((iter_num, 'ckpt.%d' % (iter_num)))

        if len(ckpts)>0:
            start_iter, latest_ckpt = sorted(ckpts)[-1]
            self._saver.restore(S, os.path.join(ckpt_dir, latest_ckpt))
            self._global_iter.load(start_iter)
        return start_iter

    def _fetch_and_retry(self, fetchable):
        t = 0
        feed = None
        while feed is None:
            try:
                t+=1
                feed = fetchable.fetch()
            except Exception as e:
                logging.warn("Fetch attempt failed: %s", str(e))
                if t == self._retries: raise
        return feed

    def train(self, fetchable):
        S = tf.get_default_session()

        start_iter=self.start_iter
        loss = np.nan
        tty = ManagedTTY()
        pb = click.progressbar(length=self._iterations, label="Optimizing...", show_eta=True, show_pos=True, file=tty.stream)
        with pb, tty:
            pb.update(start_iter)
            for i in range(start_iter, self._iterations):
                util = fetchable.utilization
                feed = self._fetch_and_retry(fetchable)

                self._queue_utilization.load(util)
                if self._tprofiler is not None:
                    self._tprofiler.new_meta()
                    result = S.run([ self._index_op, self._loss_op, self._hook_op, self._min_op, self._step_op ] + self._view_ops, feed_dict=feed, run_metadata=self._tprofiler.run_metadata, options=self._tprofiler.run_options)
                    self._tprofiler.record_trace()
                else:
                    result = S.run([ self._index_op, self._loss_op, self._hook_op, self._min_op, self._step_op ] + self._view_ops, feed_dict=feed)
                index, loss, terminate = result[:3]
                view_values = result[5:]
                self.notify_views( index, view_values )

                pb.label = "Loss: %.2e | Q % 3d %%" % (loss, util)
                if self._checkpoint is not None:
                    ckpt_dir, ckpt_freq = self._checkpoint
                    if (i+1) % ckpt_freq==0:
                        self._saver.save(S, os.path.join(ckpt_dir, 'ckpt.%d' % (i+1)))
                if terminate: break
                pb.update(1)
            self.finish_hooks()
            if self._tprofiler is not None: self._tprofiler.finish()
            return loss

class SubnetworkTFTrainer(TFTrainer):
    """
        Trainer that selectively optmizes parts of the network, extending TFTrainer to train a tflon.model.Model.

        Keyword Arguments:
            freeze_variables (list):  list of network variables to freeze.
                                    If no variables are provided, then train all variables for the network (default=[])
    """
    def __init__(self, freeze_variables=None, **kwargs):
        self._freeze_variables = freeze_variables
        super(SubnetworkTFTrainer, self).__init__(**kwargs)

    def _get_trainables(self,model):
        if self._freeze_variables is None:logging.info("No subnetwork specified to freeze weights and biases. Entire network will be trained.")
        return [var for var in model.trainables if var not in self._freeze_variables]

class SciPyEarlyTerminationConditionMet(Exception):
    def __init__(self, xf, message="Terminated early due to trainer hook signal"):
        super(SciPyEarlyTerminationConditionMet, self).__init__(message)
        self.xf = xf

class SciPyTrainer(Trainer):
    """
        Wrapper that applies an optimizer from SciPy (wrapper for scipy.minimize) to train a tflon model.

        Keyword Arguments:
            iterations (int): The number of training iterations (default=150)
            solver (str):     The minimizer to use, valid values include any algorithm implemented by scipy.minimize (default=scipy_lbfgsb)
            options (dict):   Options to pass to the scipy.optimize.minimize module (default={})
            log_frequency (int): Frequency to print progress to screen or log file, if None, don't log progress (default=None)
    """
    def __init__(self, iterations=150, solver="L-BFGS-B", options={}, log_frequency=None, **kwargs):
        super(OpenOptTrainer, self).__init__(**kwargs)
        self._max_iterations = iterations
        self._solver = solver
        self._options = options.copy()
        self._options['maxiter'] = self._max_iterations
        if not sys.stderr.isatty() and log_frequency is not None:
            self.add_hook( LogProgressHook(log_frequency, iterations) )

    def _expand(self, flatW):
        W_list = []
        cur_W = 0
        for W in self._trainables:
            W_shape = W.get_shape()
            limit = W_shape.num_elements()
            W_slice = flatW[cur_W:cur_W+limit]
            W_slice = W_slice.reshape( tuple(W_shape.as_list()) )
            W_list.append( W_slice )
            cur_W += limit
        return W_list

    def _flatten(self, Ws):
        def _convert_to_dense(W):
            if W.__class__.__name__ == 'IndexedSlicesValue':
                nW = np.zeros(W.dense_shape)
                nW[W.indices] = W.values
                W = nW
            return W

        flat_Ws = []
        for W in Ws:
            W = _convert_to_dense(W)
            W_len = np.prod( W.shape )
            flat_Ws.append( W.reshape( W_len ) )
        return np.concatenate( flat_Ws )

    def _gradient(self, Ws):
        S = tf.get_default_session()

        self._assign_values(S, self._trainables, self._expand(Ws))
        gradients = S.run( self._gradient_ops, feed_dict=self._feed )
        flat_G = self._flatten( gradients )

        return flat_G.astype(np.float64)

    def _loss(self, Ws):
        S = tf.get_default_session()
        self._assign_values(S, self._trainables, self._expand(Ws))

        loss = S.run( self._loss_op, feed_dict=self._feed )
        return loss

    def setup(self, model):
        self._gradient_ops = [G for G,V in model.gradients]
        self._index_op = model.index
        self._loss_op = model.loss
        self._trainables = model.trainables
        self._assign_values = model.assign
        self._hook_op = self.process_hooks(model, self._local_iter, self._loss_op)
        self._view_ops = self.process_views(model)
        self._variables = self._get_scope_vars() + self._get_hook_vars()
        self._init_op = tf.variables_initializer(self._variables + self._trainables)

    def refresh(self):
        S = tf.get_default_session()
        S.run(self._init_op)
        for hook in self._hooks: hook.start()

    def train(self, fetchable):
        self._feed = fetchable.fetch()
        tty = ManagedTTY()
        self._pb = click.progressbar(length=self._max_iterations, label="Optimizing...", show_eta=True, show_pos=True, file=tty.stream)
        S = tf.get_default_session()

        def cb_lambda(xk):
            loss = self._loss(xk)
            self._feed = fetchable.fetch()
            result = S.run([ self._index_op, self._hook_op, self._step_op ] + self._view_ops, feed_dict=self._feed)
            index, terminate = result[:2]
            view_values = result[3:]
            self.notify_views( index, view_values )
            self._pb.label = "Loss: %.2e" % (loss)
            self._pb.update(1)
            if terminate:
                raise SciPyEarlyTerminationConditionMet(xk)

        with self._pb, tty:
            initW = self._flatten( S.run(self._trainables) )
            try:
                r = scipy.optimize.minimize(self._loss, initW, method=self._solver, jac=self._gradient, options=self._options, callback=cb_lambda)
                lf, xf = r.fun, r.x
            except SciPyEarlyTerminationConditionMet as e:
                logging.info("Optimizer terminated early due to hook signal")
                lf, xf = self._loss(e.xf), e.xf
            self._assign_values(S, self._trainables, self._expand(xf))
            self.finish_hooks()
            return lf

OpenOptTrainer=SciPyTrainer
