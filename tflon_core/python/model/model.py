from past.builtins import reduce
from builtins import range
import numpy as np
import types
import dill as pickle
import tensorflow as tf
from tflon import logging
from tflon.data import PersistentTensorManager, ThreadCoordinator, QueueCoordinator, PyroCoordinator, convert_to_feedable, Schema, TableFeed, Table
from tflon.system import num_gpus, SystemMonitor
from tflon.utils import nansorted
from .tower import Tower

class Snapshot(object):
    """
        Instantiated by the model.snapshot() method. Returns a tuple containing the classref, params and values for model.variables
        at the time the method is called
    """

    def __init__(self, clsref, params, values):
        self._tuple = (clsref, params, values)

    def restore(self, **xargs):
        class_, params, values = self._tuple
        if 'params' in xargs:
            params.update(xargs['params'])
        xargs['params'] = params
        xargs['_values'] = values
        return class_(**xargs)

    @property
    def params(self):
        return self._tuple[1]

    @property
    def values(self):
        return self._tuple[2]


class Model(object):
    """
        Model base class. User models are implemented by extending this class and overriding the _model method:

        class MyModel(Model):
            def _model(self, *args, **kwargs):
                pass
    """
    def __init__(self, **kwargs):
        """
            Constructs a model object.

            Keyword Arguments:
                use_gpu (bool): Whether to use GPU for model ops (default: autodetect GPU)
                params (dict):  Model parameters passed along to modules, module parameters
                                can be used to generate highly customizable model code for
                                experiments. Parameters are references by keys of the form
                                module_name/arg_name, where valid arg_names are defined by
                                the module default_arguments property.
                **kwargs:       Other keyword arguments are stored as model parameters and
                                accessible by Model.get_parameter('name'). Such parameters
                                should be defined in Model._parameters()
        """
        self._values = kwargs.get('_values', None)
        if '_values' in kwargs: del kwargs['_values']
        self._saver = None

        use_gpu = kwargs.get('use_gpu', None)
        self._use_gpu = num_gpus()>0 if use_gpu is None else use_gpu
        if 'use_gpu' in kwargs: del kwargs['use_gpu']

        self._initialized=False

        params = self._parameters()
        params.update(kwargs.get('params', {}))
        if 'params' in kwargs: del kwargs['params']
        for k in kwargs:
            params[k] = kwargs[k]
        self._build( params )
        self._locked = False

    def _parameters(self):
        """
            This method can be overridden to define model parameter defaults, thus simplifying calls to Tower.get_parameter

            Returns:
                dict: dictionary of str -> value pairs. Keys are string parameter names and values are the default values for those parameters.
                      Names should be python compatible variable names. A value of None indicates a required parameter, however it is not
                      strictly necessary to declare such parameters.
        """
        return {}

    def _data(self, batch):
        """
            DEPRECATED: Use subclasses of tflon.model.Preprocessor with Tower.add_preprocessor instead

            Preprocess a data batch for input to this model. Data returned must conform to one of the feedable types (see
            tflon.data.convert_to_feedable)

            Additional preprocessing is carried out by individual tflon.toolkit.Module objects using the modified batch.

            CRITICAL: This call must be thread safe.

            Parameters:
                batch (dict): a dictionary of str -> feedable containing the raw batch data (typically of type tflon.data.Table)

            Returns:
                dict: A batch dictionary of str -> feedable objects containing preprocessed batch data
        """
        return batch

    def _show_data(self, tables):
        """
            Override to set parameter values based on data, prior to model training.

            Parameters:
                tables (tflon.data.TableFeed): Input tables used for this model training run
        """
        pass

    def _model(self):
        """
            Construct the tensorflow graph for this model.
        """
        raise NotImplementedError()

    def _build(self, params):
        self._scope = self.__class__.__name__

        with tf.variable_scope( self._scope):
            self._tower = Tower( params=params, use_gpu=self._use_gpu )
            self._tower.populate( lambda: self._model() )
            self._init_op = tf.variables_initializer( self.variables + [tf.train.get_or_create_global_step()] )
            self._reset_op = tf.variables_initializer( self.trainables )
            self._metrics_initializer = tf.variables_initializer( self.metric_variables )

    @property
    def schema(self):
        return self._tower.schema

    @property
    def tower(self):
        return self._tower

    def __dir__(self):
        return list(self.__dict__.keys()) + dir(self._tower)

    def __getattr__(self, name):
        if hasattr(self._tower, name):
            return getattr(self._tower, name)
        raise AttributeError("'Model' and 'Tower' have no attribute '%s'" % (name))

    def __len__(self):
        return sum(reduce(int.__mul__, (dim.value for dim in W.get_shape()), 1) for W in self.trainables)

    def initialize(self, restore=True):
        """Reset the model variables to their initialized states.

            This operation will restore variables to saved states loaded from disk if available.

            Parameters:
                restore (boolean): If False, do not use saved variable states even when available (default=True)
        """
        self._initialized=True
        S = tf.get_default_session()

        S.run( self._init_op )
        if self._values is not None and restore:
            values = [self._values[V.name] for V in self.variables]
            self._tower.assign(S, self.variables, values)

    def reset(self):
        """
            Reinitialize model trainable variables.
        """
        S = tf.get_default_session()
        S.run( self._reset_op )

    def infer(self, source, query=None):
        """Perform inference from a data batch or from a data generator.

            Parameters:
                source:    Either (1) a batch dictionary of name, tensor pairs or
                                  (2) a generator, which emits batches (dictionaries of name, tensor pairs)

            Keyword Arguments:
                query (list or str): A key or multiple keys specifying which outputs to gather, if None, gets all outputs (default=None)

            Returns:
                dict or iterator: If source is a batch dictionary, return a dictionary of name, tensor pairs for model output. Otherwise, if source is
                                  a batch generator, return a generator which emits dictionaries of name, tensor pairs, one output for each input batch
        """
        bare_value=False
        if isinstance(query, str):
            bare_value=True
            query = [query]
        if query is None:
            query = self.outputs.keys()
        query = set(query)

        self._tower.set_training_phase(False)
        if type(source) is dict or isinstance(source, TableFeed):
            return self._get_output( source, query, bare_value )
        else:
            return self._iter_infer( source, query, bare_value )

    def fetch(self, patterns):
        """
            Get variable values by regex patterns.

            Parameters:
                patterns (list): list of string queries for variable names

            Returns:
                dict: Variable values as name:value pairs
        """
        if isinstance(patterns, str):
            patterns = [patterns]
        S = tf.get_default_session()
        variables = sum([self.find_variables(p) for p in patterns], [])
        values = S.run(variables)
        return {v.name:val for v,val in zip(variables, values)}

    def _iter_infer( self, source, query, bare_value ):
        for batch in source:
            yield self._get_output( batch, query, bare_value )

    def _get_output( self, batch, query, bare_value ):
        S = tf.get_default_session()
        names, ops = zip(*[(k,v) for k,v in self.outputs.items() if k in query])
        transforms = [self.transforms.get(k, None) for k in names]

        batch = self._preprocess_batch( batch )
        feed = self._tower.feed( self._convert_to_feedable( batch ) )
        values = S.run( ops, feed_dict=feed )

        if bare_value:
            v, t = values[0], transforms[0]
            return v if t is None else t(batch, v)
        else:
            return dict(zip( names, (v if t is None else t(batch, v) for v, t in zip(values, transforms)) ))

    def evaluate( self, source, query=None ):
        """Evaluate model metrics on data feed or batch

            Parameters:
                source:    Either (1) a batch dictionary of name, tensor pairs or
                                  (2) a generator, which emits batches (dictionaries of name, tensor pairs)

            Keyword Arguments:
                query (list or str): A key or multiple keys specifying which metrics to gather, if None, gets all metrics (default=None)

            Returns:
                dict:     A dictionary of metric names and values
        """
        bare_value=False
        if isinstance(query, str):
            bare_value = True
            query = [query]
        if query is None:
            query = self.metrics.keys()
        query = set(query)

        if type(source) is dict or isinstance(source, TableFeed):
            source = [source]


        self._tower.set_training_phase(False)
        S = tf.get_default_session()
        metric_items = [(k,self.metrics[k]) for k in query]
        names, value_ops, update_ops = zip(*[(k,val,up) for k, (val, up) in metric_items])

        S.run(self._metrics_initializer)
        for batch in source:
            S.run(update_ops, feed_dict=self.feed(batch))

        values = S.run( value_ops )
        if bare_value:
            return values[0]
        return dict(zip( names, values ))

    def _get_variable_values( self ):
        S = tf.get_default_session()
        return S.run( self.variables )

    def _set_variable_values( self, values ):
        self._tower.assign(tf.get_default_session(), self.variables, values)

    def fit(self, source, trainer, restarts=1, reset=True, restore=False, source_tables=None, lock=False, **kwargs):
        """Fit a model to data using a chosen trainer.

            This function manages random restarts and delegates training to either _fit_global or _fit_stochastic,
            which are selected based the type of source.

            Parameters:
                source:           Either a dictionary-like of name:feedable pairs, or an iterator which returns such dictionaries
                trainer:          A tflon.train.Trainer

            Keyword Arguments:
                restarts (int):   The number of times to train from a random initialization (default = 1)
                reset (bool):     Whether to reset trainable variable initializations before each optimization (default = True)
                restore (boolean): If True, use saved variable states even when available, for re-fitting a model (default=False)
                lock (bool):      Whether to finalize the tensorflow graph prior to the first training run
                source_tables:    An optional TableFeed object or dictionary of name:Table pairs which is used to initialize model parameters (default = None)
                **kwargs:         Additional arguments passed to either _fit_global or _fit_stochastic
        """
        if not self._initialized:
            self.initialize(restore)

        if isinstance(source, TableFeed):
            source.align()
            if source_tables is None:
                source_tables = source
            fit_func = self._fit_global
        elif type(source) is dict:
            fit_func = self._fit_global
        else:
            fit_func = self._fit_stochastic

        self._tower.set_training_phase(True)
        trainer.setup(self)

        if source_tables is not None:
            self.show_data( source_tables )

        results = []
        for r in range(restarts):
            logging.info("Training model (%d/%d)..." % (r+1, restarts))
            if reset or restarts>1:
                self.reset()
            loss = fit_func(source, trainer, lock, **kwargs)
            results.append( (loss, self._get_variable_values()) )

        best_loss, best_vars = nansorted(results, key=lambda v: v[0])[0]
        self._set_variable_values( best_vars )
        logging.info("Found a solution with loss %.2e", best_loss)

    def lock(self):
        if not self._locked:
            S = tf.get_default_session()
            S.graph.finalize()
            self._locked=True

    def show_data(self, tables):
        """
            Sets model and module parameters from data prior to fit

            Parameters:
                tables (tflon.data.TableFeed): Input tables used for this model training run
        """
        logging.info("Compute global dataset parameters...")
        self._show_data(tables)
        self._tower.show_data(tables)

    def feed(self, batch):
        """
            Generate a feed_dict from a batch dictionary for evaluating session.run calls
        """
        batch = self._preprocess_batch( batch )
        return self._tower.feed( self._convert_to_feedable( batch ) )

    def _preprocess_batch(self, batch):
        batch = batch.copy()
        if isinstance(batch, TableFeed):
            batch.align()
        for k in batch.keys():
            v = batch[k]
            if isinstance(v, Table):
                batch[k] = v.deserialize()
        batch = self._data( batch )
        return self._tower.featurizer( batch )

    def _convert_to_feedable(self, batch):
        return {k:convert_to_feedable(batch[k]) for k in batch if k in self._tower.feedables}

    def _fit_global(self, source, trainer, lock):
        """
            Parameters:
                source:    A dictionary of name:feedable pairs
                trainer:   A tflon.train.Trainer
        """

        queue = self._tower.get_queue(capacity=1)
        queue.enqueue(self.feed(source))

        trainer.refresh()
        with PersistentTensorManager(queue.fetch()) as data, SystemMonitor():
            if lock: self.lock()
            return trainer.train( data )

    def _fit_stochastic(self, source, trainer, lock, processes=1, pipeline=None, coordinator_args={}, tf_queue_args={}):
        """
            Parameters:
                source (iterator):               An iterator returning dictionaries of (str, Table) pairs
                trainer (tflon.train.TFTrainer): Tflon trainer object
                processes (int):                 Number of featurizer processes spawned, if 0 use a threaded batching (default=0)
                pipeline (str):                  The data preprocessor pipeline implementation to use: 'multithreading', 'multiprocessing', or 'pyro'
                                                    (default='multithreading' if processes==1 else 'multiprocessing')
                coordinator_args (dict):         Additional arguments for the Coordinator
                tf_queue_args (dict):            Additional arguments for the TensorQueue
        """
        queue = self._tower.get_queue(**tf_queue_args)
        if pipeline is None:
            pipeline = 'multithreading' if processes==1 else 'multiprocessing'
        if pipeline=='multithreading':
            coordinator = ThreadCoordinator(self, source, queue, processes, **coordinator_args)
        elif pipeline=='multiprocessing':
            coordinator = QueueCoordinator(self, source, queue, processes, **coordinator_args)
        elif pipeline=='pyro':
            coordinator = PyroCoordinator(self, source, queue, processes, **coordinator_args)
        else:
            raise ValueError("Invalid valid '%s' for argument 'pipeline'" % (str(pipeline)))

        trainer.refresh()
        with coordinator, SystemMonitor():
            if lock: self.lock()
            return trainer.train(queue)

    def featurize(self, source, batch_size=None):
        """
            Given a table feed, generate the feature tables required by each module and add them to the table feed

            Parameters:
                source (TableFeed): The input feed

            Keyword Arguments:
                batch_size (int):   Featurize the data in batches of a given size, if None, featurize all the data at once
        """

        if batch_size is not None:
            num_batches = int(np.ceil(len(source.index) / float(batch_size)))
            feature_tables = {}
            for i, batch in enumerate(source.iterate(batch_size)):
                logging.info("Featurizing batch %d/%d with %d elements..." % (i+1, num_batches, len(batch.index)))
                self._tower.featurizer(batch)
                tabkeys = [k for k in batch if k not in source and isinstance(batch[k], Table)]
                for k in tabkeys:
                    feature_tables.setdefault(k, list()).append(batch[k])

            for k in feature_tables:
                source[k] = feature_tables[k][0].concatenate(feature_tables[k][1:])
        else:
            source.align()
            logging.info("Featurizing %d elements", len(source.index))
            self._tower.featurizer(source)

    def checkpoint(self, filename):
        """Generate a tensorflow checkpoint file.

            Parameters:
                filename (str): Path to write the checkpoint file
        """
        S = tf.get_default_session()
        if self._saver is None:
            self._saver = tf.train.Saver(self.variables)
        self._saver.save(S, filename)

    def restore(self, filename):
        """Restore model weights from a tensorflow checkpoint file.

            Parameters:
                filename (str): A path to a checkpoint file
        """
        S = tf.get_default_session()
        if self._saver is None:
            self._saver = tf.train.Saver(self.variables)
        self._saver.restore(S, filename)

    def pickle(self):
        return pickle.dumps((self.__class__, self._tower._params), pickle.HIGHEST_PROTOCOL)

    @classmethod
    def unpickle(cls, data):
        class_, params = pickle.loads( data )
        return class_(params=params)

    def snapshot(self):
        S = tf.get_default_session()
        values = { V.name: S.run(V) for V in self.variables }
        return Snapshot(self.__class__, self._tower._params, values)

    def save(self, filename):
        """
            Save a tflon model pickle file. The resulting file contains a tuple data structure with the following three variables:
                - (class) Model class
                - (dict) Parameters
                - (dict) Values of all model variables

            Parameters:
                filename (str): A path to save the pickled model
        """

        S = tf.get_default_session()

        values = { V.name: S.run(V) for V in self.variables }
        with open(filename, 'wb') as ffile:
            pickle.dump((self.__class__, self._tower._params, values), ffile, pickle.HIGHEST_PROTOCOL)

    @classmethod
    def load(cls, filename, **xargs):
        """
            Load a tflon model pickle file previously saved with Model.save from disk

            Parameters:
                filename: The pickle file containing the saved tflon model
                xargs:    Extra keyword arguments passed to Model.__init__ this argument can be used to override parameter settings
        """
        with open(filename, 'rb') as ffile:
            class_, params, values = pickle.load( ffile )
            if 'params' in xargs:
                params.update(xargs['params'])
            xargs['params'] = params
            xargs['_values'] = values
            return class_(**xargs)
