from builtins import range
import numpy as np
import pandas as pd
import tensorflow as tf
from tflon.data import BlockReduceTable, DenseNestedTable
from tflon.graph.data import PoolTable, GatherTable
from tflon.toolkit import Concat, Broadcast, Featurizer, Module, segment_softmax, Dense, register_callable

class ElementPropertiesFeaturizer(Featurizer):
    def _featurize(self, batch):
        graphs = batch[self._graph_table].data.iloc[:,0]
        prop_values = np.full((graphs.shape[0], len(self._propnames)), None, dtype=np.object_)

        for idx, (_, g) in enumerate(graphs.iteritems()):
            if self._etype == 'nodes':
                for j, pname in enumerate(self._propnames):
                    prop_values[idx, j] = np.array(getattr(g.nodes, pname), dtype=self._dtype.as_numpy_dtype)

            elif self._etype == 'edges':
                for j, pname in enumerate(self._propnames):
                    prop_values[idx, j] = np.array(getattr(g.edges, pname), dtype=self._dtype.as_numpy_dtype)

        return {'properties': pd.DataFrame(prop_values, columns=self._propnames, index=graphs.index)}

class ElementProperties(Module):
    """
        Generate a tensor from graph element property annotations (accessible from graph.nodes.<property_name> or graph.edges.<property_name>

        Parameters:
            graph_table: The table containing objects inheriting from tflon.graph.Graph
            properties:  A list of string names corresponding to annotated properties
            etype:       The element type, either 'nodes' or 'edges'

        Keyword Arguments:
            dtype:       The data type of the output tensor (default=tf.float32)
    """
    def build(self, graph_table, properties, etype, dtype=tf.float32):
        self._properties = self.add_input('properties', shape=[None, len(properties)], dtype=dtype, ttype=DenseNestedTable)
        self._etype = etype
        self._propnames = properties
        self._graph_table = graph_table
        self._dtype = dtype

    def call(self):
        return self._properties

    def _featurizer(self):
        return ElementPropertiesFeaturizer(self, _graph_table=self._graph_table, _etype=self._etype, _dtype=self._dtype, _propnames=self._propnames)

class NodeProperties(ElementProperties):
    """Shorthand class for ElementProperties with etype='nodes'"""
    def build(self, *args, **kwargs):
        super(NodeProperties, self).build(*args, etype='nodes', **kwargs)

class EdgeProperties(ElementProperties):
    """Shorthand class for ElementProperties with etype='edges'"""
    def build(self, *args, **kwargs):
        super(EdgeProperties, self).build(*args, etype='edges', **kwargs)

class Encoder(Module):
    """
        Implementation of the set2set recurrent encoder network: https://arxiv.org/abs/1511.06391

        This encoder is used to produce a graph-level encoding for predicting graph-level properties.

        Parameters:
            graph_table: The table containing objects inheriting from tflon.graph.Graph
            lstm_cell (tf.nn.rnn_cell.RNNCell): An instance of a recursive unit used for encoding
            weight_network (callable):          A function used to calculate attention values from node states

        Other Parameters:
            iterations (int):     The number of encoder steps to apply (default=10)
    """
    def build(self, graph_table, lstm_cell, weight_network):
        def weighting_func(nodes, segs):
            inputs = tf.concat([nodes, self._broadcast], axis=1)
            return segment_softmax(weight_network(inputs), segs)

        self._lstm_cell = lstm_cell
        self._graph_reduce = Reduce(graph_table, reduction=tf.math.segment_sum, weighting=weighting_func)
        self._graph_reduce.initialize()

    def call(self, nodes):
        """
            Perform graph encoding

            Parameters:
                nodes (tensor-like): Node properties

            Returns:
                Tensor: Graph-level encodings
        """
        iterations = self.get_parameter('iterations')

        batch_size = self._graph_reduce.batch_size
        c, q = self._lstm_cell.zero_state( batch_size, dtype=nodes.dtype )

        for i in range(iterations):
            self._broadcast = self._graph_reduce.inverse(q)
            r = self._graph_reduce(nodes)
            _, (c, q) = self._lstm_cell(r, (c, q))

        return q

    def _parameters(self):
        return {'iterations': 10}

class NodeToEdgeFeaturizer(Featurizer):
    def _featurize(self, batch):
        graphs = batch[self._graph_table].data
        node_gather = np.full((graphs.shape[0],3), None, dtype=np.object_)
        for i in range(graphs.shape[0]):
            graph = graphs.iloc[i,0]
            node_index = graph.nodes.index

            if len(graph.edges)==0:
                node1_gather, node2_gather = tuple(), tuple()
            else:
                node1_gather, node2_gather = zip(*[(node_index[u],node_index[v]) for u,v in graph.edges])
            node_gather[i,0] = node1_gather
            node_gather[i,1] = node2_gather
            node_gather[i,2] = len(graph.nodes)
        return {'node_gather': pd.DataFrame(node_gather, index=graphs.index)}


class NodeToEdge(Module):
    """
        Calculate edge representations from pairs of node states using an order invariant weave op.

        Parameters:
            graph_table: The table containing objects inheriting from tflon.graph.Graph

        Other Parameters:
            activation (callable): An activation function (default=tf.nn.relu)
            width (int):           The number of output features, if -1, then output size = input size (default=-1)
    """

    def build(self, graph_table):
        self._graph_table = graph_table
        self._node_gather = self.add_input('node_gather', shape=[None, 2], dtype=tf.int32, ttype=GatherTable)
        self._left_node, self._right_node = [tf.reshape(T, (-1,)) for T in tf.split(self._node_gather, 2, axis=1)]

    def call(self, nodes):
        """
            Parameters:
                nodes (tensor-like): Node properties

            Returns:
                Tensor: edge properties
        """
        activation = self.get_parameter('activation')
        width = self.get_parameter('width')
        size = self.input_shapes[0][-1]
        if width == -1:
            width = size

        transform = Dense(width, activation=activation)

        LG = tf.gather(nodes, self._left_node)
        RG = tf.gather(nodes, self._right_node)
        c1 = tf.concat([LG, RG], axis=1)
        c2 = tf.concat([RG, LG], axis=1)
        return transform(c1) + transform(c2)

    def _featurizer(self):
        return NodeToEdgeFeaturizer(self, _graph_table=self._graph_table)

    def _parameters(self):
        return {'activation': tf.nn.relu, 'width': -1}

class EdgeToNodeFeaturizer(Featurizer):
    def _featurize(self, batch):
        graphs = batch[self._graph_table].data
        pool = np.full((graphs.shape[0],3), None, dtype=np.object_)

        for i in range(graphs.shape[0]):
            graph = graphs.iloc[i,0]
            edge_indices = graph.edges.index
            gather = []
            reduce = []
            for n in graph.nodes:
                nbrs = graph.strict_neighbors(n)
                for n2 in nbrs:
                    eidx = edge_indices[(n,n2)]
                    gather.append(eidx)
                if len(nbrs)==0:
                    gather.append(-1)
                reduce.append(max(len(nbrs),1))
            pool[i,:] = [gather, reduce, len(graph.edges)]
        return {'edge_pool': pd.DataFrame(pool, index=graphs.index)}

class EdgeToNode(Module):
    """
        Calculate node representations from incident edge states using an order invariant pooling op.

        Parameters:
            graph_table: The table containing objects inheriting from tflon.graph.Graph

        Keyword Arguments:
            reduction (callable):  A reduction function with signature ``func(tensor, segments)`` (Default: tf.math.segment_sum)
                                        * tensor: N-D tensor for reduction
                                        * segments: 1-D tensor of segment indices

        Other Parameters:
            activation (callable): An activation function (default=tf.nn.relu)
            width (int):           The number of output features, if -1, then output size = input size (default=-1)
    """

    def build(self, graph_table, reduction=tf.math.segment_sum):
        self._graph_table = graph_table
        self._pool = self.add_input('edge_pool', shape=[None, 2], dtype=tf.int32, ttype=PoolTable)
        self._gather, self._reduce = [tf.reshape(T, (-1,)) for T in tf.split( self._pool, 2, axis=1 )]
        self._reduction = reduction

    def call(self, edges):
        """
            Parameters:
                nodes (tensor-like): Node properties

            Returns:
                Tensor: edge properties
        """
        activation = self.get_parameter('activation')
        width = self.get_parameter('width')
        size = self.input_shapes[0][-1]
        if width == -1:
            width = size

        edges = tf.concat([tf.zeros(shape=(1, tf.shape(edges)[1])), edges], axis=0)
        transform = Dense(width, activation=activation)

        G = tf.gather(transform(edges), self._gather+1)
        return self._reduction(G, self._reduce)

    def _featurizer(self):
        return EdgeToNodeFeaturizer(self, _graph_table=self._graph_table)

    def _parameters(self):
        return {'activation': tf.nn.relu, 'width': -1}


class ReduceFeaturizer(Featurizer):
    def _featurize(self, batch):
        graphs = batch[self._graph_table].data

        indices = np.full((graphs.shape[0],), None, dtype=np.object_)
        for i in range(graphs.shape[0]):
            graph = graphs.iloc[i,0]
            if self._element_type == 'node':
                indices[i] = [len(graph.nodes)]
            else:
                indices[i] = [len(graph.edges)]
        return {'reduce': pd.DataFrame(data=indices, index=graphs.index)}


class Reduce(Module):
    """
        Graph reduce. Pools all nodes or edges within a given graph into a single output.

        Parameters:
            graph_table: The table containing objects inheriting from tflon.graph.Graph

        Keyword Arguments:
            reduction (callable):  A reduction function with signature ``func(tensor, segments)`` (Default: tf.math.segment_sum)
                                        * tensor: N-D tensor for reduction
                                        * segments: 1-D tensor of segment indices
            weighting (callable):  A weighting function with signature: ``func(tensor, segments)`` (Default: None)
                                        * tensor: N-D tensor for weight calculation
                                        * segments: 1-D tensor of segment indices
            element_type: reduce over nodes or edges, defaults = node
    """
    def build(self, graph_table, reduction=tf.math.segment_sum, weighting=None, element_type = 'node'):
        self._graph_table = graph_table
        self._reduction = reduction
        self._reduce = self.add_input( 'reduce', shape=[None], dtype=tf.int32, ttype=BlockReduceTable )
        self._weighting = weighting
        self._element_type = element_type

    def call(self, nodes):
        """
            Parameters:
                nodes (Tensor): node properties of dimension N x ...

            Returns:
                Tensor: of shape G x ... where G is the number of distinct graphs in the batch
        """
        weights = 1.0 if self._weighting is None else self._weighting( nodes, self._reduce )
        return self._reduction(nodes * weights, self._reduce)

    @property
    def batch_size(self):
        """
            Get the expected output tensor size
        """
        return tf.reduce_max(self._reduce) + 1

    @register_callable
    def inverse(self, reduced):
        """
            Broadcast graph-level output of this op back to the node level

            Parameters:
                reduced (Tensor): The output of the reduction op

            Returns:
                Tensor: of shape N x ... where N is the number of nodes in the original batch
        """
        return tf.gather(reduced, self._reduce)

    def _featurizer(self):
        return ReduceFeaturizer(self, _graph_table=self._graph_table, _element_type=self._element_type)

class PoolFeaturizer(Featurizer):
    def _featurize(self, batch):
        graphs = batch[self._graph_table].data

        pool = np.full((graphs.shape[0],3), None, dtype=np.object_)
        for i in range(graphs.shape[0]):
            graph = graphs.iloc[i,0]
            node_indices = graph.nodes.index
            gather = []
            reduce = []
            for nidx in graph.nodes:
                depth = graph.neighborhood(nidx, self._depth)
                neighbors = sorted([node_indices[nidx] for nidx in depth if depth[nidx]==self._depth])
                if len(neighbors)==0:
                    neighbors = [-1]
                gather += neighbors
                reduce += [len(neighbors)]

            pool[i,0] = gather
            pool[i,1] = reduce
            pool[i,2] = len(graph.nodes)

        return { 'pool_depth_%d' % (self._depth): pd.DataFrame( data=pool, index=graphs.index ) }


class Pool(Module):
    """
        Graph pooling. Pools all nodes with a given depth (in number of edges traversed). Only nodes at the specified depth are pooled.

        Parameters:
            graph_table: The table containing objects inheriting from tflon.graph.Graph
            depth (int): The depth to find nodes for pooling.

        Keyword Arguments:
            reduce (callable):     A reduction function with signature ``func(tensor, segments)`` (Default: tf.math.segment_sum)
                                        * tensor: N-D tensor for reduction
                                        * segments: 1-D tensor of segment indices
            weighting (callable):  A weighting function with signature: ``func(tensor, segments)`` (Default: None)
                                        * tensor: N-D tensor for weight calculation
                                        * segments: 1-D tensor of segment indices
    """
    def build(self, graph_table, depth, reduction=tf.math.segment_sum, weighting=None):
        self._graph_table = graph_table
        self._depth = depth
        self._reduction = reduction
        self._weighting = weighting

        self._pool = self.add_input( 'pool_depth_%d' % (depth), shape=[None,2], dtype=tf.int32, ttype=PoolTable )
        self._gather, self._reduce = [tf.reshape(T, (-1,)) for T in tf.split( self._pool, 2, axis=1 )]

    def call(self, nodes):
        """
            Parameters:
                graph_table: The table containing objects inheriting from tflon.graph.Graph
                nodes (Tensor): node properties of dimension N x ...

            Returns:
                Tensor: same shape as nodes
        """
        width = nodes.get_shape()[-1].value
        nodes = tf.concat([tf.zeros((1, width)), nodes], axis=0)

        collected = tf.gather(nodes, self._gather+1)
        weights = 1.0 if self._weighting is None else self._weighting(collected, self._reduce)
        return self._reduction(collected * weights, self._reduce)

    def _featurizer(self):
        return PoolFeaturizer(self, _graph_table=self._graph_table, _depth=self._depth)

class Neighborhood(Module):
    """Pool nodes of a graph by summing over features in progressively larger neighborhoods of a node.
        For an input of shape (nodes, features) this will output a tensor of shape (nodes, (depth+1)*features)

        Parameters:
            graph_table: The table containing objects inheriting from tflon.graph.Graph

        Keyword Arguments:
            depth (int): The maximum depth to find nodes for pooling (default=5)
    """
    def build(self, graph_table):
        self._net = Broadcast(*([lambda x: x]+[Pool(graph_table, i) for i in range(1, self.depth+1)])) | Concat(axis=1)

    def call(self, nodes):
        return self._net(nodes)

    def _parameters(self):
        return {'depth': 5}
