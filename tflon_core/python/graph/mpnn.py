from builtins import range
import numpy as np
import pandas as pd
import tensorflow as tf
from tflon.data import DenseNestedTable, BlockReduceTable
from tflon.graph.data import PoolTable, GatherTable
from tflon.toolkit import register_callable, Dense, Chain, Featurizer, Module, batch_matmul, ForLoop

def logit_net(width, layers, activation):
    """
        Construct a neural network of given layers with given hidden-layer activation function, with linear outputs

        Parameters:
            width (int): Number of hidden nodes and output nodes
            layers (int): Number of layers (hidden layers = layers-1)
            activation (callable): An activation function for hidden layers
    """
    return Chain(*[Dense(width, activation=activation if i<layers-1 else lambda x:x, name="Gate") for i in range(layers)])

class Master(Module):
    """
        Module to execute master node updates

        Parameters:
            width (int): The width of the master node state
            layers (int): The number of layers in the update unit
            activation (callable): An activation function for the update network
    """
    def build(self, width, layers, activation):
        self._master_update = logit_net(width, layers, activation)
        self._node_update = logit_net(width, layers, activation)
        self._width = width

    def call(self, nodes, masters, reduce, update_master):
        """
            Perform an update of all master nodes and compute a master->node message component

            Parameters:
                nodes (tensor-like):   Graph node properties (non-masters)
                masters (tensor-like): Master node properties
                reduce (tensor-like):  Reduction indices (across a graph)
                update_master (bool):  Whether to compute a state update message for the master node. If false, just compute the state message for the graph nodes

            Returns:
                if update_master then returns:
                    tuple (Tensor, Tensor): State update messages for all nodes and master nodes
                otherwise returns:
                    Tensor: State update messages for all graph nodes
        """
        inputs = tf.concat([nodes, tf.gather(masters, reduce)], axis=1)
        Uv = self._node_update(inputs)
        if update_master:
            Um = self._master_update(inputs)
            return Uv, Um
        else:
            return Uv

class EdgeMessage(Module):
    """
        Improved message function for message passing neural network.

        Computes f(Euv) dot Nv, where f(Euv) is a neural network which outputs a d x d matrix and Nv is the neighbor node state

        Parameters:
            width (int):  The width of the node state
            layers (int): The number of layers for the state update network
            activation (callable): An activation function for the state update network
            ports (int):  The number of update ports
    """
    def build(self, width, layers, activation, ports):
        self._width = width
        self._num_ports = ports
        self._port_funcs = [logit_net(width*width, layers, activation) for p in range(ports)]

    def call(self, nodes, edges, neighbors, ports):
        """
            Compute messages for all edges

            Parameters:
                inputs (tensor-like): Current states passed to message function (concatentation of node states and edge states)
                ports (tensor-like): Port numbers for all edges in the state update

            Returns:
                Tensor: Messages for all edges
        """
        inputs = tf.concat([v for v in [nodes,edges] if v is not None], axis=1)
        if self._num_ports==1:
            edge_nets = self._port_funcs[0](inputs)
        else:
            parts = tf.dynamic_partition( inputs, ports, self._num_ports )
            restitch = tf.dynamic_partition( tf.range(0, tf.shape(inputs)[0]), ports, self._num_ports )
            edge_nets = [ func(parts[i]) for i, func in enumerate(self._port_funcs) ]
            edge_nets = tf.dynamic_stitch( restitch, edge_nets )

        edge_nets = tf.reshape(edge_nets, (-1, self._width, self._width))
        result = batch_matmul(edge_nets, neighbors)
        result.set_shape([None, self._width])
        return result

class PairMessage(Module):
    """
        Default message function for message passing neural network

        Computes f(Nu, Euv, Nv), where f is a neural network.

        Parameters:
            width (int):  The width of the node state
            layers (int): The number of layers for the state update network
            activation (callable): An activation function for the state update network
            ports (int):  The number of update ports
    """
    def build(self, width, layers, activation, ports):
        self._num_ports = ports
        self._port_funcs = [logit_net(width, layers, activation) for p in range(ports)]

    def call(self, nodes, edges, neighbors, ports):
        """
            Compute messages for all edges

            Parameters:
                inputs (tensor-like): Current states passed to message function (concatentation of node states and edge states)
                ports (tensor-like): Port numbers for all edges in the state update

            Returns:
                Tensor: Messages for all edges
        """
        inputs = tf.concat([v for v in [nodes,edges,neighbors] if v is not None], axis=1)

        if self._num_ports==1:
            return self._port_funcs[0](inputs)
        else:
            parts = tf.dynamic_partition( inputs, ports, self._num_ports )
            restitch = tf.dynamic_partition( tf.range(0, tf.shape(inputs)[0]), ports, self._num_ports )
            logits = [ func(parts[i]) for i, func in enumerate(self._port_funcs) ]
            return tf.dynamic_stitch( restitch, logits )

class MPNNFeaturizer(Featurizer):
    def _featurize(self, batch):
        graphs = batch[self._graph_table].data

        pool = np.full((graphs.shape[0],3), None, dtype=np.object_)
        if self.edges:
            edge_gather = np.full((graphs.shape[0],2), None, dtype=np.object_)
        if self.digraph:
            edge_ports = np.full((graphs.shape[0],1), None, dtype=np.object_)
        if self.master:
            master_nodes = np.full((graphs.shape[0],), None, dtype=np.object_)

        for i in range(graphs.shape[0]):
            g = graphs.iloc[i,0]
            node_indices = g.nodes.index
            edge_indices = g.edges.index
            gather = []
            reduce = []
            indices = []
            ports = []
            for u in g.nodes:
                nlist = g.neighbors(u)
                elist = g.node_edges(u)
                nidx = [node_indices[v] for v in nlist]
                eidx = [edge_indices[v,w] for v,w in elist]
                pnums = [1 if v==u else 0 for v,w in elist]
                if len(nlist)==0:
                    nidx = [-1]
                    eidx = [-1]
                    pnums = [0]
                gather += nidx
                reduce += [len(nidx)]
                indices += eidx
                ports += pnums

            pool[i,0] = gather
            pool[i,1] = reduce
            pool[i,2] = len(g.nodes)

            if self.edges:
                edge_gather[i,0] = indices
                edge_gather[i,1] = len(g.edges)
            if self.digraph:
                edge_ports[i,0] = ports
            if self.master:
                master_nodes[i] = [len(g.nodes)]

        update = {}
        update['node_pool'] = pd.DataFrame(data=pool, index=graphs.index)
        if self.edges:
            update['edge_gather'] = pd.DataFrame(data=edge_gather, index=graphs.index)
        if self.digraph:
            update['edge_ports'] = pd.DataFrame(data=edge_ports, index=graphs.index)
        if self.master:
            update['master_nodes'] = pd.DataFrame(data=master_nodes, index=graphs.index)
        return update


class MPNN(Module):
    """
        Message passing neural networks (MPNN; https://arxiv.org/pdf/1704.01212), a framework graph convolutional neural nets

        Other Parameters:
            width (int):           The size of the output node state (default=None)
            digraph (bool):        Use digraphs (default=False)
            edges (bool):          Use edge properties (default=False)
            master (bool):         Add master nodes (default=False)
            layers (int):          The number of layers, including output layer, for the message function network (default=2)
            activation (callable): An activation function (default=tf.nn.elu)
            apply_order (str):     The message reduction strategy, either 'sum_gate' or 'gate_sum'.
                                   If 'sum_gate', sum messages and then apply the recurrence unit.
                                   If 'gate_sum', apply reccurent unit to each edge message, then sum all outputs (default='sum_gate').
            message_func:          A class reference to a module implementing the Message interface
            update_cell:           A class reference to a module which implements tf.nn.rnn_cell.RNNCell interface
    """
    def build(self, graph_table):
        self._graph_table = graph_table

        self._pool = self.add_input( 'node_pool', shape=[None,2], dtype=tf.int32, ttype=PoolTable )
        self._node_gather, self._reduce = [tf.reshape(T, (-1,)) for T in tf.split( self._pool, 2, axis=1 )]
        if self.edges:
            self._edge_gather = self.add_input('edge_gather', shape=[None,1], dtype=tf.int32, ttype=GatherTable)
            self._edge_gather_flat = tf.reshape(self._edge_gather, (-1,))
        if self.digraph:
            self._edge_ports = self.add_input('edge_ports', shape=[None,1], dtype=tf.int32, ttype=DenseNestedTable)
            self._edge_ports_flat = tf.reshape(self._edge_ports, (-1,))
        if self.master:
            self._master_nodes = self.add_input('master_nodes', shape=[None], dtype=tf.int32, ttype=BlockReduceTable)

    def pass_messages(self, nodes, edges, func):
        with tf.name_scope('gather_messages'):
            nodes = tf.concat([tf.zeros((1,nodes.get_shape()[-1].value), dtype=nodes.dtype), nodes], axis=0)
            nodes = tf.gather(nodes, self._reduce+1)
            neighbors = tf.gather(nodes, self._node_gather+1)
            if edges is not None:
                edges = tf.concat([tf.zeros((1,edges.get_shape()[-1].value), dtype=edges.dtype), edges], axis=0)
                edges = tf.gather(edges, self._edge_gather_flat+1)
        parts = func(nodes, edges, neighbors, self.__dict__.get('_edge_ports_flat', None))
        return parts

    @register_callable
    def init_master_state(self, nodes):
        """
            Create the initial state for master nodes

            Parameters:
                nodes (Tensor): The initial graph node states, used only for setting dtype of the master node state tensor

            Returns:
                Tensor: The initial zero states for master nodes
        """
        return tf.zeros([tf.reduce_max(self._master_nodes)+1, self.width], dtype=nodes.dtype)

    def aggregate_messages(self, state, messages, reduction, update_cell, extra_messages=None):
        if self.apply_order == 'sum_gate':
            reduced = tf.math.segment_sum(messages, reduction)
            if extra_messages is not None:
                reduced += extra_messages
            updated_state, _ = update_cell(reduced, state)
            return updated_state
        elif self.apply_order == 'gate_sum':
            updated, _ = update_cell(messages, tf.gather(state, reduction))
            reduced = tf.math.segment_sum(updated, reduction)
            if extra_messages is not None:
                extra_updated, _ = update_cell(extra_messages, state)
                reduced += extra_updated
            return reduced
        else:
            raise Exception("Invalid value for parameter 'apply_order': '%s'" % (self.apply_order))

    def call(self, node_state, edge_state=None, master_state=None, steps=2):
        """
            Perform a message passing and node state update phase

            Parameters:
                node_state (tensor-like):   Current node states

            Keyword Arguments:
                edge_state (tensor-like):   Current edge states
                master_state (tensor-like): Current master node states


            Returns:
                tuple:
                    updated node states (tensor-like)
                    edge states (tensor-like), passed through if edge states provided
                    master states (tensor-like), if provided, may be updated if update_master is True
        """
        assertion = tf.Assert(tf.greater(steps,1), ["Number of message passing steps must be greater than 1", steps])
        with tf.control_dependencies([assertion]):
            node_state = tf.identity(node_state)
        node_update = self.update_cell(self.width)
        node_func = self.message_func(self.width, self.layers, self.activation, 2 if self.digraph else 1)
        if self.master:
            master_func = Master(self.width, self.layers, self.activation)
            master_update = self.update_cell(self.width)
            if master_state is None:
                master_state = self.init_master_state(node_state)

        def run_timestep_master(i, node_state, master_state):
            master_state.set_shape([None, self.width])
            node_state.set_shape([None, self.width])
            with tf.name_scope('pass_messages'):
                Mu = self.pass_messages(node_state, edge_state, node_func)
                Mv, Mm = master_func(node_state, master_state, self._master_nodes, True)
            with tf.name_scope('aggregate_messages'):
                master_state = self.aggregate_messages(master_state, Mm, self._master_nodes, master_update)
                node_state = self.aggregate_messages(node_state, Mu, self._reduce, node_update, Mv)
            return node_state, master_state

        def run_timestep(i, node_state):
            node_state.set_shape([None, self.width])
            with tf.name_scope('pass_messages'):
                Mu = self.pass_messages(node_state, edge_state, node_func)
            with tf.name_scope('aggregate_messages'):
                node_state = self.aggregate_messages(node_state, Mu, self._reduce, node_update)
            return node_state

        def final_step(node_state, master_state):
            with tf.name_scope('pass_messages_1'):
                Mu = self.pass_messages(node_state, edge_state, node_func)
                Mv = None
                if self.master:
                    Mv = master_func(node_state, master_state, self._master_nodes, False)
            with tf.name_scope('aggregate_messages_1'):
                node_state = self.aggregate_messages(node_state, Mu, self._reduce, node_update, Mv)
            return node_state

        if self.master:
            node_state, master_state = ForLoop(run_timestep_master, iterations=steps-1)(node_state, master_state)
        else:
            node_state = ForLoop(run_timestep, iterations=steps-1)(node_state)

        node_state = final_step(node_state, master_state)
        node_state.set_shape([None, self.width])
        return node_state

    def _featurizer(self):
        return MPNNFeaturizer(self, _graph_table=self._graph_table)

    def _parameters(self):
        return {'width': None,
                'digraph': False,
                'edges': False,
                'master': False,
                'layers': 1,
                'activation': tf.nn.elu,
                'message_func': PairMessage,
                'apply_order': 'sum_gate',
                'update_cell': lambda ss: tf.nn.rnn_cell.GRUCell(ss, bias_initializer=tf.constant_initializer(1.))}
