from tflon.toolkit import Featurizer, Module
from tflon.data import Table
from tflon.graph.data import GatherTable
import tensorflow as tf
import numpy as np
import pandas as pd

class GraphToGraphsTuple(Module):
    """Generate a graph_nets.GraphsTuple from tflon.graph.Graph objects to enable interoperation between tflon and graph_nets ops

        For undirected graphs, this generates a directed GraphsTuple with two edges of opposite orientation for each edge in the undirected graph.

        This module also provides tools for generating default zero states and for translating directed edge representations back to undirected edges by applying an operation to each directed edge and summing the output

        Parameters:
            graphs: A table containing tflon graph objects

        Keyword Arguments:
            globals (tensor): A tensor containing global properties of the graphs
            nodes (tensor): A tensor containing node properties of the graphs
            edges (tensor): A tensor containing edge properties of the graphs
    """
    def build(self, graphs, globals=None, nodes=None, edges=None):
        self._graph_table = graphs
        self._globals = globals
        self._nprops = nodes
        self._eprops = None

        if edges is not None:
            self._edge_gather_inp = self.add_input('edge_properties', shape=(None, 1), dtype=tf.int32, ttype=GatherTable)
            self._edge_gather = tf.reshape(self._edge_gather_inp, (-1,))
            self._eprops = tf.gather(edges, self._edge_gather)

        self._edges = self.add_input('edges', shape=(None, 2), dtype=tf.int32, ttype=GatherTable)
        self._senders = self._edges[:,0]
        self._receivers = self._edges[:,1]
        self._shapes = self.add_input('shapes', shape=(None, 2), dtype=tf.int32, ttype=Table)
        self._num_nodes = self._shapes[:,0]
        self._num_edges = self._shapes[:,1]

    def call(self):
        """Return a GraphsTuple containing translated graph data"""
        from graph_nets.graphs import GraphsTuple

        return GraphsTuple(nodes=self._nprops, edges=self._eprops, globals=self._globals,
                           senders=self._senders, receivers=self._receivers,
                           n_node=self._num_nodes, n_edge=self._num_edges)

    def _featurizer(self):
        return GraphToGraphsTupleFeaturizer(self, _graph_table=self._graph_table, get_edges=self._eprops is not None)

    def reduce_to_undirected_edges(self, gtuple, apply_op=None, segment_op=tf.math.unsorted_segment_sum):
        """Reduce a GraphsTuple with assymetric, directed edges to an undirected graph

            Parameters:
                gtuple: The input GraphsTuple

            Keyword Arguments:
                apply_op (callable): A tensor operation to apply to each directed edge before summing
                segment_op (callable): A segment reduction op to apply to combine directed edge properties to undirected edges (default=tf.math.unsorted_segment_sum)

            Returns:
                tensor: Reduced edge tensor
            """
        edges = gtuple.edges
        if apply_op is not None:
            edges = apply_op(edges)
        return segment_op(edges, self._edge_gather, tf.reduce_max(self._edge_gather)+1)

    def zero_edge_state(self, state_size):
        """Generate a default edge zero state for each edge"""
        return tf.zeros((tf.reduce_sum(self._num_edges), state_size))

    def zero_global_state(self, state_size):
        """Generate a default global zero state for each graph"""
        return tf.zeros((tf.shape(self._shapes)[0], state_size))

class GraphToGraphsTupleFeaturizer(Featurizer):
    def _featurize(self, batch):
        graphs = batch[self._graph_table].data.iloc[:,0]
        num_examples = graphs.shape[0]

        build_edge_gather = self.get_edges
        if build_edge_gather:
            edge_gather = np.full((num_examples, 2), None, dtype=np.object_)

        edge_nodes = np.full((num_examples, 3), None, dtype=np.object_)
        graph_elements = np.zeros((num_examples, 2))

        for i, (_, g) in enumerate(graphs.iteritems()):
            node_index = g.nodes.index
            bi_edges = g.bidirectional_edges
            n_nodes = len(g.nodes)
            n_edges = len(g.edges)
            n_bi_edges = len(bi_edges)

            if build_edge_gather:
                edge_index = g.edges.index
                edge_gather[i,0] = [edge_index[(u,v)] for u,v in bi_edges]
                edge_gather[i,1] = n_edges

            edge_nodes[i,0] = [node_index[u] for u,v in bi_edges]
            edge_nodes[i,1] = [node_index[v] for u,v in bi_edges]
            edge_nodes[i,2] = n_nodes

            graph_elements[i,0] = n_nodes
            graph_elements[i,1] = n_bi_edges


        update = {'edges': pd.DataFrame(edge_nodes, index=graphs.index),
                'shapes': pd.DataFrame(graph_elements, index=graphs.index)}
        if build_edge_gather:
            update['edge_properties'] = pd.DataFrame(edge_gather, index=graphs.index)
        return update


