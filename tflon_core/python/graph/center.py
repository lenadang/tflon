import numpy as np
import networkx as nx
from functools import wraps

def invertable(scorer):
    @wraps(scorer)
    def invertable_scorer(*args, **kwargs):
        invert = False
        if 'invert' in kwargs:
            invert = kwargs['invert']
            del kwargs['invert']

        scores = scorer(*args, **kwargs)
        if invert:
            scores = {u: -s for u,s in scores.items()}
        return scores
    return invertable_scorer

def normalized(selector):
    @wraps(selector)
    def normalized_selector(*args, **kwargs):
        weights = selector(*args, **kwargs)
        return weights / np.sum(weights)
    return normalized_selector

def uniform_scorer(graph):
    return {u:1. for u in graph.nodes}

@invertable
def cfc_centrality_scorer(graph):
    if len(graph.nodes)==1:
        n, = graph.nodes
        return {n:1}
    return nx.current_flow_closeness_centrality(graph)

@invertable
def eccentricity_scorer(graph):
    return {u: max(len(paths[v]) for v in paths) for u,paths in nx.all_pairs_shortest_path(graph)}

@normalized
def select_max(scores):
    scores = np.array(scores)
    weights = np.zeros(scores.shape)
    amax = np.amax(scores)
    weights[scores == amax] = 1
    return weights

@normalized
def select_min(scores):
    scores = np.array(scores)
    weights = np.zeros(scores.shape)
    amin = np.amin(scores)
    weights[scores == amin] = 1
    return weights

@normalized
def select_random(scores):
    return np.array(scores)

@normalized
def select_random_top_epsilon(scores, epsilon=.01):
    scores = np.array(scores)
    weights = np.zeros(scores.shape)
    max_val = np.amax(scores)
    weights[scores >= (max_val - np.abs(max_val)*epsilon)] = 1
    return weights

@normalized
def select_softmax(scores, base=np.e):
    scores = np.array(scores)
    max_val = np.amax(scores)
    return np.power(base, scores-max_val)

def select_softmin(scores, base=np.e):
    return select_softmax(-np.array(scores))

