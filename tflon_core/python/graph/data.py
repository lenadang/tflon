from builtins import range
import numpy as np
import scipy as sp
import pandas as pd
from .graph import Graph, DiGraph
from tflon.data import Table, RaggedTable, RaggedTensorValue, TensorTransform
from tflon.utils import nanlen, nanempty

class GraphElementsToNested(TensorTransform):
    """
        Convert a 2-D tensor to a nested tensor, the number of entries in each nested list is decided by the corresponding number of graph elements

        Parameters:
            graph_table (str): The name of the table containing tflon.graph.Graph objects.
    """

    def __init__(self, graph_table, etype, columns=None):
        self._graph_table = graph_table
        self._etype = etype
        self._columns = columns

    def __call__(self, batch, output):
        table = batch[self._graph_table]
        nested = np.full((len(table.index), output.shape[1]), None, np.object_)

        i = 0
        for j, g in enumerate(table.data.iloc[:,0]):
            rng = len(g.nodes) if self._etype=='nodes' else len(g.edges)
            for k in range(output.shape[1]):
                nested[j,k] = output[i:i+rng,k].tolist()
            i += rng

        columns = None
        if self._columns is not None:
            columns = self._columns
        return pd.DataFrame(data=nested, index=table.data.index, columns=columns)

class NodesToNested(GraphElementsToNested):
    def __init__(self, graph_table, columns=None):
        super(NodesToNested, self).__init__(graph_table, 'nodes', columns=columns)
DenseNodesToTableTransform = NodesToNested

class EdgesToNested(GraphElementsToNested):
    def __init__(self, graph_table, columns=None):
        super(EdgesToNested, self).__init__(graph_table, 'edges', columns=columns)
DenseEdgesToTableTransform = EdgesToNested

class GraphTable(Table):
    """
        Convert a DataFrame containing serialized json format graph structures to
        tflon.graph.Graph objects.

        Parameters:
            data (pandas.DataFrame): The input dataframe, with one column containing serialized json molecules

        Keyword Arguments:
            convert (bool): Whether to convert from json representation, if False expects dataframe of tflon.graph.Graph objects (default=True)
    """
    def __init__(self, data, gtype=Graph):
        self._gtype = gtype
        self._data = data

    def serialize(self):
        if isinstance(self._data.iloc[0,0], Graph):
            return self.copy(self._data.applymap(lambda g: g.json))
        return self

    def deserialize(self):
        if not isinstance(self._data.iloc[0,0], Graph):
            return self.copy(self._data.applymap(lambda j: self._gtype.from_json(j)))
        return self

class DiGraphTable(GraphTable):
    """
        Convert a DataFrame containing serialized json format digraph structures to
        tflon.graph.DiGraph objects.

        See documentation of tflon.graph.GraphTable for instantiation
    """
    def __init__(self, data):
        super(DiGraphTable, self).__init__(data, gtype=DiGraph)

class PoolTable(Table):
    """
        Table type which manages pooling indices for graph convolution.
        A pool is a collection of indices which are used to gather from a tensor and then reduce to a tensor of the same size as the original.
    """
    def get_shape(self):
        return (self._data.iloc[:,1].apply(sum).sum(), 2)

    def as_matrix(self):
        shape = self.shape
        mat = np.zeros(shape, dtype=np.int32)

        i = 0
        k = 0
        inc = 0
        for _idx, row in self._data.iterrows():
            G, R, L = row
            j = 0
            for r in R:
                view = mat[i:i+r,0]
                view[:] = G[j:j+r]
                view[view>=0] += inc
                mat[i:i+r,1] = [k] * r

                i += r
                j += r
                k += 1
            inc += L
        return mat

class GatherTable(Table):
    """
        Table type which manages indices for gather operations
    """
    def get_shape(self):
        return (self._data.iloc[:,0].apply(len).sum(), self._data.shape[1]-1)

    def as_matrix(self):
        data = self._data
        shape = self.shape
        mat = np.zeros(shape, dtype=np.int32)

        i = 0
        inc = 0
        for _idx, row in data.iterrows():
            total = len(row.iloc[0])
            for j in range(shape[1]):
                view = mat[i:i+total,j]
                view[:] = row.iloc[j]
                view[view>=0] += inc
            i += total
            inc += row.iloc[-1]
        return mat

class SparseRowSelector(Table):
    """
        Table to convert row indices to a sparse binary matrix for dynamic partition operations.
    """
    def get_shape(self):
        return (self._data.iloc[:,2].sum(), self._data.iloc[:,1].apply(len).max())

    def as_matrix(self):
        data = self._data
        shape = self.shape
        num_vals = (self._data.iloc[:,0].apply(len).sum())

        values = np.ones((num_vals,), dtype=np.int32)
        indices = np.zeros((num_vals,), dtype=np.int32)
        ind_ptr = np.zeros((shape[1],), dtype=np.int32)

        k=0
        for i in range(shape[1]):
            inc = 0
            for j in range(data.shape[0]):
                blocks = data.iloc[j,0]
                partitions = data.iloc[j,1]
                num_elements = data.iloc[j,2]

                if i < len(partitions):
                    cum = sum(partitions[:i], 0)
                    indices[k:k+partitions[i]] = blocks[cum:cum+partitions[i]]
                    indices[k:k+partitions[i]] += inc-1
                    ind_ptr[i] += partitions[i]
                    k+=partitions[i]
                inc+=num_elements

        ind_ptr = [0] + np.cumsum(ind_ptr).astype(np.int32).tolist()
        return sp.sparse.csc_matrix((values, indices, ind_ptr), shape=shape)

class SparseToDenseRowSelector(SparseRowSelector):
    """
        Extends SparseRowSelector to convert a sparse row selection to a dense matrix prior to feed
    """
    def as_matrix(self):
        csc = super(SparseToDenseRowSelector, self).as_matrix()
        return np.asarray(csc.todense())

class RaggedIndexTable(RaggedTable):
    """
        Similar to GatherTable, but stores ragged arrays of indices, used for pulling tensor elements for updates on dynamic data structures
    """
    def as_matrix(self):
        df = self._data

        W = df[df.columns[1]].apply(nanlen, convert_dtype=False).max()
        rshapes = self.ragged_shapes(W)
        N = np.max(rshapes)

        series = df[df.columns[0]]
        lengths = df[df.columns[1]]
        increments = df[df.columns[2]]

        nonzero = np.sum(rshapes)
        values = np.zeros((nonzero,), dtype=self._dtype)

        size = np.zeros((len(lengths), W), dtype=np.int64)
        for i, L in enumerate(lengths):
            L = nanempty(L)
            for j, v in enumerate(L):
                size[i,j] = v

        begin = np.zeros(size.shape, dtype=np.int64)
        total = 0
        for j in range(size.shape[1]):
            for i in range(size.shape[0]):
                begin[i,j] = total
                total += size[i,j]

        inc=0
        for i, (S, L, elems) in enumerate(zip(series, lengths, increments)):
            L = nanempty(L)
            k=0
            for j, _l in enumerate(L):
                b = begin[i,j]
                s = size[i,j]
                values[b:b+s] = S[k:k+s]
                view = values[b:b+s]
                view[view>0] += inc
                k+=s
            inc+=elems

        return RaggedTensorValue(values, rshapes, [N, W])

class RaggedReduceTable(RaggedTable):
    """
        Similar to BlockReduceTable, but stores ragged arrays of indices, used for reducing tensor elements for updates on dynamic data structures
    """
    def as_matrix(self):
        df = self._data

        W = df[df.columns[1]].apply(nanlen).max()
        rshapes = self.ragged_shapes(W)
        N = np.max(rshapes)

        counts = df[df.columns[0]]
        lengths = df[df.columns[1]]

        nonzero = np.sum(rshapes)
        values = np.zeros((nonzero,), dtype=self._dtype)

        size = np.zeros((len(lengths), W), dtype=np.int64)
        for i, L in enumerate(lengths):
            L = nanempty(L)
            for j, v in enumerate(L):
                size[i,j] = v

        begin = np.zeros(size.shape, dtype=np.int64)
        total = 0
        for j in range(size.shape[1]):
            for i in range(size.shape[0]):
                begin[i,j] = total
                total += size[i,j]

        inc = np.zeros(rshapes.shape)
        for i, (C, L) in enumerate(zip(counts, lengths)):
            L = nanempty(L)
            cidx = 0
            for j, l in enumerate(L):
                b = begin[i,j]
#                s = size[i,j]
                n = 0
                dq = 0
                while n < l:
                    c = C[cidx+dq]
                    values[b+n:b+n+c] = dq + inc[j]
                    n+=c
                    dq+=1
                inc[j]+=dq
                cidx+=dq

        return RaggedTensorValue(values, rshapes, [N, W])
